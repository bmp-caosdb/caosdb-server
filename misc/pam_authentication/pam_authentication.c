/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
// Pam Authentication
// A. Schlemmer, 07/2018

#include <security/pam_appl.h>
#include <security/pam_misc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char * password; // needs to be global

int supply_password(int num_msg, const struct pam_message **msgm,
                    struct pam_response **responsep, void *appdata_ptr) {
  struct pam_response* response = (struct pam_response*)calloc(sizeof(struct pam_response), num_msg);
  int i;
  for (i=0; i<num_msg; i++) {
    if (msgm[i]->msg_style == PAM_PROMPT_ECHO_OFF) {
      response[i].resp = strdup(password);
      response[i].resp_retcode = 0;
    }
  }
  *responsep = response;
  return PAM_SUCCESS;
}

static struct pam_conv conv =
  {
   supply_password,
   NULL
  };

int main(int args, char** argv) {
  if (args != 3) {
    fprintf(stderr, "Usage: pam_authentication username password\n");
    return 2;
  }

  pam_handle_t *pamh;
  char * username = argv[1];
  password = argv[2];
  int res = pam_start("login", username, &conv, &pamh);

  if (!res == PAM_SUCCESS) {
    fprintf(stderr, "Error in starting pam authentication.\n");
    return 2;
  }

  res = pam_authenticate(pamh, 0);
  // printf("Return code %i: %s\n", res, pam_strerror(pamh, res));
  
  return res;
}
