# Requirements

## Software
* >=Java 8
* >=Apache Maven 3.0.4
* >=Python 3.4
* >=pip 9.0.1
* >=git 1.9.1
* >=Make 3.81
* >=Screen 4.01
* >=MySQL 5.5 (better >=5.6) or >=MariaDB 10.1
* libpam (if PAM authentication is required)
* unzip

### Install the requirements on Debian
On Debian, the required packages can be installed with:

    apt-get install git make mariadb-server maven openjdk-8-jdk-headless \
      python3-pip screen libpam0g-dev unzip

Note that installing MariaDB will uninstall existing MySQL packages and vice
versa.

## System

* >=Linux 4.0.0, x86\_64, e.g. Ubuntu 14.04.1
* Mounted filesytem(s) with enough space
* Working internet connection (for up-to-date python and java libraries)

## Extensions ##

### Web UI ###
- If the WebUI shall run, check out the respective submodule: `git submodule
  update --init caosdb-webui`
- Then configure and compile it according to its
  [documentation](caosdb-webui/README_SETUP.md).

### PAM ###
Authentication via PAM is possible, for this the PAM development library must be
installed and the pam user tool must be compiled:

- `cd misc/pam_authentication/`
- `make`
- If you want, you can run a test now: `./pam_authentication.sh asdf ghjk`
  should print `[FAILED]` and return with a non-zero exit code.  Unless there is
  a user `asdf` with password `ghjk` on your system, of course.

#### Troubleshooting ####
If `make` fails with `pam_authentication.c:4:31: fatal error:
security/pam_appl.h: No such file or directory` the header files are probably
not installed. You can do so under Debian and Ubuntu with `apt-get install
libpam0g-dev`. Then try again.


# First Setup

After a fresh clone of the repository, this is what you need to setup the
server:

1. Compile the server with `make compile`
   1. It is recommended to run the unit tests with `make test`.
2. Create an SSL certificate somewhere with a `Java Key Store` file.  For
   self-signed certificates (not recommended for production use) you can do:
   - `mkdir certificates; cd certificates`
   - `keytool -genkey -keyalg RSA -alias selfsigned -keystore keystore.jks -validity 375 -keysize 2048 -ext san=dns:localhost`
     Replace `localhost` by your host name, if you want.
   - `keytool -importkeystore -srckeystore keystore.jks -destkeystore keystore.p12 -deststoretype PKCS12 -srcalias selfsigned`
   - `openssl pkcs12 -in keystore.p12 -nokeys -out cert.pem`
   - You can check the content of the certificate with `openssl x509 -in cert.pem -text`
3. Copy `conf/core/server.conf` to `conf/ext/server.conf` and change it
   appropriately:
    * Setup for MySQL back-end: Assuming that the mysql back-end is installed
      (see the `README_SETUP.md` of the `caosdb-mysqlbackend` repository),
      specify the fields `MYSQL_USER_NAME`, `MYSQL_USER_PASSWORD`,
      `MYSQL_DATABASE_NAME`, and `MYSQL_HOST`.
    * Choose the ports under which CaosDB will be accessible.
    * Setup the SSL certificate: Assuming that there is an appropriate `Java Key
      Store` file (see above), change the fields `CERTIFICATES_KEY_PASSWORD`,
      `CERTIFICATES_KEY_STORE_PATH`, and `CERTIFICATES_KEY_STORE_PASSWORD`.
      Make sure that the conf file is not readable by other users because the
      certificate passwords are stored in plaintext.
    * Set the file system paths:
   - `FILE_SYSTEM_ROOT`: The root for all the files managed by CaosDB.
   - `DROP_OFF_BOX`: Where to put files for insertion into CaosDB
   - `TMP_FILES`: <To do, what's this good for?>
    * Maybe set another `SESSION_TIMEOUT_MS`.
    * See also [README_CONFIGURATION.md](README_CONFIGURATION.md)
4. Copy `conf/core/usersources.ini.template` to `conf/ext/usersources.ini`.
    * Define the users/groups who you want to include/exclude.
    * Assign at least one user the `administration` role.
      * For example, if the admin user is called `caosdb`, there should be the
        following lines:
        ```
        include.user = caosdb
        user.caosdb.roles = administration
        ```
    * It is important that the file complies with the ini file specification.
      Especially that there are no `properties` (aka `keys`) without a
      `value`. An emtpy value can be represented by `""`. Comments are
      everything from `#` or `;` to the end of the line.
5. Install the pam caller in `misc/pam_authentication/`. See
   [the pam authentication README](misc/pam_authentication/README.md)
Done!

# Start Server

`$ make run`

# Run Unit Tests

`$ make test`


# Setup Eclipse

1. Open Eclipse (recommended version: Oxygen.1a Release (4.7.1a))
2. `File > New > Java Project`: Choose a project name and specify the location
   of this repo. The JRE and Project layout should be configured automatically.
   Now, the project should initially have two source-folders: `./src/main/java`
   and `./src/test/java`. After a build, another one,
   `./target/generated-sources/antlr4` should be generated. If there are more
   than these three source-folders, reconfigure the projects source folders
   appropriately with `Project > Properties > Java Build Path > Source`.
3. In the `Package Explorer` view, right-click on the project and `Configure >
   Convert to Maven Project`.
4. In the `Package Explorer` view, right-click on the project and `Maven >
   Update Project`.
5. Usually a build of the project is started automatically. Otherwise `Project >
   Build Project`.

Done!
