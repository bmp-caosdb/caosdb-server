#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

SHELL:=/bin/bash

compile: easy-units
	mvn compile

runserver:
	mvn exec:java@run

run: compile
	mvn exec:java@run

run-debug:
	java -Dcaosdb.debug=true -jar target/caosdb-server-0.1-SNAPSHOT-jar-with-dependencies.jar

run-single:
	java -jar target/caosdb-server-0.1-SNAPSHOT-jar-with-dependencies.jar

# Compile into a standalone jar file
jar: compile
	mvn clean compile assembly:single
#	mvn assembly:single

antlr:
	mvn antlr4:antlr4

test: easy-units
	mvn test

clean: clean-antlr
	mvn clean
	rm -rf .m2-local

clean-antlr:
	rm -rf target/generated-sources/antlr4/

.PHONY: run-server-screen
run-server-screen:
	@if test "$$(screen -ls | grep -c 'caosdb-screen')" -eq "1"; then \
		echo "server is probably running. try 'screen -ls'"; \
		exit 1 ; \
	fi
	# start and wait for server
	@screen -L -S caosdb-screen -t server -d -m -A make run
	@sleep 2
	@while [ 1 -eq 1 ] ; do \
		screen -S caosdb-screen -X hardcopy .screen.log || break ; \
		[ $$(grep -c "org.restlet.ext.jetty.JettyServerHelper start" .screen.log) -eq 0 ] || break ; \
	done; \


.PHONY: stop-server-screen
stop-server-screen:
	# stop screen session with server
	@screen -S caosdb-screen -X hardcopy screen.log || true
	@screen -S caosdb-screen -p server -X stuff "^C"

.PHONY: run-debug-screen
run-debug-screen:
	@if test "$$(screen -ls | grep -c 'caosdb-debug-screen')" -eq "1"; then \
		echo "server is probably running. try 'screen -ls'"; \
		exit 1 ; \
	fi
	# start and wait for server
	@screen -L -S caosdb-debug-screen -t server -d -m -A make run-debug
	@sleep 2
	@while [ 1 -eq 1 ] ; do \
		screen -S caosdb-debug-screen -X hardcopy .screen.log || break ; \
		[ $$(grep -c "org.restlet.ext.jetty.JettyServerHelper start" .screen.log) -eq 0 ] || break ; \
	done; \


.PHONY: stop-debug-screen
stop-debug-screen:
	# stop screen session with debug server
	@screen -S caosdb-debug-screen -X hardcopy screen.log || true
	@screen -S caosdb-debug-screen -p server -X stuff "^C"

.m2-local:
	mkdir .m2-local

easy-units: .m2-local
	mvn deploy:deploy-file -DgroupId=de.timmfitschen -DartifactId=easy-units -Dversion=0.0.1-SNAPSHOT -Durl=file:./.m2-local/ -DrepositoryId=local-maven-repo -DupdateReleaseInfo=true -Dfile=./lib/easy-units-0.0.1-SNAPSHOT-jar-with-dependencies.jar
