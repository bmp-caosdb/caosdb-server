---
title: Development
author: Daniel Hornung
...


# Developing the CaosDB server #
This file contains information about server development, it is aimed at those
who want to debug, understand or enhance the CaosDB server.

## Testing ##
Whether developing new features, refacturing code or fixing bugs, the server
code should be thoroughly tested for correct and incorrect behvaiour, on correct
and incorrect input.

### Writing tests ###
Tests go into `src/test/java/caosdb/`, the files there can serve as examples for
writing tests.

### Running tests with Maven ###
- Automatic testing can be done with `make test` or, after compilation, `mvn
  test`.
- Tests of single modules can be started with `mvn test -Dtest=TestClass`
- Test of a single method `footest`: `mvn test -Dtest=TestClass#footest`
