/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.datetime;

import java.util.Date;
import java.util.TimeZone;

public class UTCTimeZoneShift extends TimeZone {
  private static final long serialVersionUID = 1L;
  public static final int POSITIVE = 1;
  public static final int NEGATIVE = -1;

  private final int rawOffset;
  private final String str;

  public UTCTimeZoneShift(final int sign, final int h, final int m) {
    if (59 < m || m < 0) {
      throw new IllegalArgumentException("59<m<0 is not allowed.");
    }
    if (12 < h || h < 0) {
      throw new IllegalArgumentException("12<h<0 is not allowed.");
    }

    rawOffset = sign * (3600 * h + 60 * m) * 1000;
    str = generateString(sign, h, m);
  }

  private static String generateString(final int sign, final int h, final int m) {
    final StringBuilder sb = new StringBuilder();
    sb.append(sign == NEGATIVE ? '-' : '+');
    if (h < 10) {
      sb.append('0');
    }
    sb.append(h);
    if (m < 10) {
      sb.append('0');
    }
    sb.append(m);
    return sb.toString();
  }

  /** ISO 8601 conform +/-hhmm. */
  @Override
  public String toString() {
    return str;
  }

  @Override
  public int getOffset(
      final int era,
      final int year,
      final int month,
      final int day,
      final int dayOfWeek,
      final int milliseconds) {
    return getRawOffset();
  }

  @Override
  public int getRawOffset() {
    return rawOffset;
  }

  @Override
  public boolean inDaylightTime(final Date date) {
    return false;
  }

  @Override
  public void setRawOffset(final int offsetMillis) {
    throw new UnsupportedOperationException("RawOffset is final.");
  }

  @Override
  public boolean useDaylightTime() {
    return false;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof TimeZone) {
      final TimeZone tz = (TimeZone) obj;
      if (hasSameRules(tz)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public String getID() {
    return str;
  }
}
