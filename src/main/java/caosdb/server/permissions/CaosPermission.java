/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.permissions;

import java.util.HashSet;
import java.util.Map;
import org.apache.shiro.authz.Permission;
import org.eclipse.jetty.util.ajax.JSON;

public class CaosPermission extends HashSet<PermissionRule> implements Permission {

  public CaosPermission(final HashSet<PermissionRule> rules) {
    super(rules);
  }

  public CaosPermission() {}

  public static CaosPermission parseJSON(final String json) {
    final CaosPermission ret = new CaosPermission();
    @SuppressWarnings("unchecked")
    final Map<String, String>[] rules = (Map<String, String>[]) JSON.parse(json);
    for (final Map<String, String> rule : rules) {
      ret.add(PermissionRule.parse(rule));
    }
    return ret;
  }

  private static final long serialVersionUID = -2730907147406500598L;

  @Override
  public boolean implies(final Permission p) {
    boolean grant = false;
    boolean deny = false;
    boolean grant_priority = false;

    for (final PermissionRule r : this) {
      if (r.getPermission().implies(p)) {
        if (r.isGrant()) {
          if (r.isPriority()) {
            grant_priority = true;
          } else {
            grant = true;
          }
        } else {
          if (r.isPriority()) {
            return false;
          } else {
            deny = true;
          }
        }
      }
    }
    return (grant && !deny) || grant_priority;
  }
}
