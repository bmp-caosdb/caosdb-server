/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.permissions;

import java.util.HashMap;

public final class EntityACI {

  private final ResponsibleAgent agent;
  private final long bitSet;

  public EntityACI(final ResponsibleAgent role, final long bitSet) {
    this.agent = role;
    this.bitSet = bitSet;
  }

  public long getBitSet() {
    return this.bitSet;
  }

  public ResponsibleAgent getResponsibleAgent() {
    return this.agent;
  }

  @Override
  public String toString() {
    return getResponsibleAgent().toString()
        + ":"
        + (EntityACL.isAllowance(getBitSet()) ? "+" : "-")
        + EntityACL.getPermissionsFromBitSet(getBitSet()).toString()
        + (EntityACL.isPriorityBitSet(getBitSet()) ? "P" : "")
        + ";";
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof EntityACI) {
      final EntityACI that = (EntityACI) obj;
      return that.agent.equals(this.agent) && that.bitSet == this.bitSet;
    }
    return false;
  }

  public HashMap<String, Object> toMap() {
    final HashMap<String, Object> map = new HashMap<String, Object>();
    getResponsibleAgent().addToMap(map);
    map.put("bitSet", getBitSet());
    return map;
  }
}
