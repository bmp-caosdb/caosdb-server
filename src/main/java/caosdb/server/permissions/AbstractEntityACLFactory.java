/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.permissions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public abstract class AbstractEntityACLFactory<T extends EntityACL> {

  private final Map<ResponsibleAgent, Long> normalGrants = new HashMap<>();
  private final Map<ResponsibleAgent, Long> priorityGrants = new HashMap<>();
  private final Map<ResponsibleAgent, Long> normalDenials = new HashMap<>();
  private final Map<ResponsibleAgent, Long> priorityDenials = new HashMap<>();

  public void grant(final ResponsibleAgent role, final int... permissionBitNumber) {
    grant(role, false, permissionBitNumber);
  }

  public void deny(final ResponsibleAgent role, final int... permissionBitNumber) {
    deny(role, false, permissionBitNumber);
  }

  public void grant(
      final ResponsibleAgent role, final boolean priority, final int... permissionBitNumber) {
    for (final int i : permissionBitNumber) {
      grant(role, priority, EntityPermission.getEntityPermission(i));
    }
  }

  public void deny(
      final ResponsibleAgent role, final boolean priority, final int... permissionBitNumber) {
    for (final int i : permissionBitNumber) {
      deny(role, priority, EntityPermission.getEntityPermission(i));
    }
  }

  public void grant(final ResponsibleAgent role, final String... permission) {
    grant(role, false, permission);
  }

  public void deny(final ResponsibleAgent role, final String... permission) {
    deny(role, false, permission);
  }

  public void grant(
      final ResponsibleAgent role, final boolean priority, final String... permission) {
    for (final String s : permission) {
      if (s.contains("*")) {
        grant(role, priority, EntityPermission.getPermissionsPerWildCard(s));
      } else {
        grant(role, priority, EntityPermission.getEntityPermission(s));
      }
    }
  }

  public void deny(
      final ResponsibleAgent role, final boolean priority, final String... permission) {
    for (final String s : permission) {
      if (s.contains("*")) {
        deny(role, priority, EntityPermission.getPermissionsPerWildCard(s));
      } else {
        deny(role, priority, EntityPermission.getEntityPermission(s));
      }
    }
  }

  public void deny(
      final ResponsibleAgent role,
      final boolean priority,
      final Collection<EntityPermission> permission) {
    for (final EntityPermission p : permission) {
      deny(role, priority, p);
    }
  }

  public void grant(
      final ResponsibleAgent role,
      final boolean priority,
      final Collection<EntityPermission> permission) {
    for (final EntityPermission p : permission) {
      grant(role, priority, p);
    }
  }

  public void grant(
      final ResponsibleAgent role, final boolean priority, final EntityPermission... permission) {
    if (priority) {
      addACI(this.priorityGrants, role, permission);
    } else {
      addACI(this.normalGrants, role, permission);
    }
  }

  public void deny(
      final ResponsibleAgent role, final boolean priority, final EntityPermission... permission) {
    if (priority) {
      addACI(this.priorityDenials, role, permission);
    } else {
      addACI(this.normalDenials, role, permission);
    }
  }

  private static void addACI(
      final Map<ResponsibleAgent, Long> map,
      final ResponsibleAgent role,
      final EntityPermission permission) {
    long bitSet = permission.getBitSet();
    try {
      bitSet |= map.get(role);
    } catch (final NullPointerException e) {
      // do nothing
    }
    map.put(role, bitSet);
  }

  private static void addACI(
      final Map<ResponsibleAgent, Long> map,
      final ResponsibleAgent role,
      final EntityPermission[] permission) {
    for (final EntityPermission p : permission) {
      addACI(map, role, p);
    }
  }

  private EntityACI[] toEntityACIArray(
      final Map<ResponsibleAgent, Long> map, final long modBitSet) {
    final EntityACI[] ret = new EntityACI[map.size()];
    int i = 0;
    for (final Entry<ResponsibleAgent, Long> e : map.entrySet()) {
      ret[i++] = new EntityACI(e.getKey(), e.getValue() | modBitSet);
    }
    return ret;
  }

  public T create() {
    normalize();
    final ArrayList<EntityACI> acis = new ArrayList<>();
    Collections.addAll(acis, toEntityACIArray(this.normalGrants, 0));
    Collections.addAll(acis, toEntityACIArray(this.normalDenials, Long.MIN_VALUE));
    Collections.addAll(acis, toEntityACIArray(this.priorityGrants, EntityACL.MIN_PRIORITY_BITSET));
    Collections.addAll(
        acis,
        toEntityACIArray(this.priorityDenials, Long.MIN_VALUE | EntityACL.MIN_PRIORITY_BITSET));
    return create(acis);
  }

  private void normalize() {
    for (final Entry<ResponsibleAgent, Long> set : this.priorityDenials.entrySet()) {
      if (this.priorityGrants.containsKey(set.getKey())) {
        this.priorityGrants.put(
            set.getKey(), this.priorityGrants.get(set.getKey()) & ~set.getValue());
      }
      if (this.normalDenials.containsKey(set.getKey())) {
        this.normalDenials.put(
            set.getKey(), this.normalDenials.get(set.getKey()) & ~set.getValue());
      }
      if (this.normalGrants.containsKey(set.getKey())) {
        this.normalGrants.put(set.getKey(), this.normalGrants.get(set.getKey()) & ~set.getValue());
      }
    }
    for (final Entry<ResponsibleAgent, Long> set : this.priorityGrants.entrySet()) {
      if (this.normalDenials.containsKey(set.getKey())) {
        this.normalDenials.put(
            set.getKey(), this.normalDenials.get(set.getKey()) & ~set.getValue());
      }
      if (this.normalGrants.containsKey(set.getKey())) {
        this.normalGrants.put(set.getKey(), this.normalGrants.get(set.getKey()) & ~set.getValue());
      }
    }
    for (final Entry<ResponsibleAgent, Long> set : this.normalDenials.entrySet()) {
      if (this.normalGrants.containsKey(set.getKey())) {
        this.normalGrants.put(set.getKey(), this.normalGrants.get(set.getKey()) & ~set.getValue());
      }
    }
  }

  public void clear() {
    this.normalGrants.clear();
    this.normalDenials.clear();
    this.priorityGrants.clear();
    this.priorityDenials.clear();
  }

  protected abstract T create(Collection<EntityACI> acis);
}
