/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.permissions;

import java.util.HashMap;
import java.util.Map;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.permission.WildcardPermission;
import org.jdom2.Element;

public class PermissionRule {

  private final WildcardPermission permission;
  private final boolean priority;
  private final boolean grant;

  public PermissionRule(final String grant, final String priority, final String permission) {
    this(
        Boolean.parseBoolean(grant),
        Boolean.parseBoolean(priority),
        new WildcardPermission(permission));
  }

  public PermissionRule(
      final boolean grant, final boolean priority, final WildcardPermission permission) {
    this.grant = grant;
    this.priority = priority;
    this.permission = permission;
  }

  public boolean isGrant() {
    return this.grant;
  }

  public boolean isPriority() {
    return this.priority;
  }

  public Permission getPermission() {
    return this.permission;
  }

  public static PermissionRule parse(final Map<String, String> rule) {
    return new PermissionRule(rule.get("grant"), rule.get("priority"), rule.get("permission"));
  }

  public Element toElement() {
    final Element ret = new Element((isGrant() ? "Grant" : "Deny"));
    if (isPriority()) {
      ret.setAttribute("priority", Boolean.toString(true));
    }
    ret.setAttribute("permission", getPermission().toString());
    return ret;
  }

  public static PermissionRule parse(final Element e) {
    return new PermissionRule(
        e.getName().equalsIgnoreCase("Grant"),
        e.getAttribute("priority") != null && Boolean.parseBoolean(e.getAttributeValue("priority")),
        new WildcardPermission(e.getAttributeValue("permission")));
  }

  public Map<String, String> getMap() {
    final HashMap<String, String> ret = new HashMap<String, String>();
    ret.put("priority", Boolean.toString(isPriority()));
    ret.put("grant", Boolean.toString(isGrant()));
    ret.put("permission", getPermission().toString());
    return ret;
  }
}
