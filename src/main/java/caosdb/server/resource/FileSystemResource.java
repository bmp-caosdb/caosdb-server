/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.resource;

import static caosdb.server.FileSystem.getFromFileSystem;
import static java.net.URLDecoder.decode;

import caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import caosdb.server.database.misc.TransactionBenchmark;
import caosdb.server.entity.Entity;
import caosdb.server.entity.FileProperties;
import caosdb.server.entity.RetrieveEntity;
import caosdb.server.entity.container.TransactionContainer;
import caosdb.server.permissions.EntityPermission;
import caosdb.server.transaction.RetrieveSparseEntityByPath;
import caosdb.server.transaction.Transaction;
import caosdb.server.utils.FileUtils;
import caosdb.server.utils.ServerMessages;
import java.io.File;
import java.io.IOException;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.restlet.data.Disposition;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;

/**
 * Download files via GET method from the file system directly without making the detour through the
 * database. Other methods are not supported
 *
 * @author Timm Fitschen
 */
public class FileSystemResource extends AbstractCaosDBServerResource {

  /**
   * Download a File from the CaosDBFileSystem. Only one File per Request.
   *
   * @author Timm Fitschen
   * @return InputRepresentation
   * @throws IOException
   */
  @Override
  protected final Representation httpGetInChildClass() throws Exception {
    final long t1 = System.currentTimeMillis();
    final String specifier =
        decode(
            (getRequest().getAttributes().containsKey("path")
                ? (String) getRequest().getAttributes().get("path")
                : ""),
            "UTF-8");

    final Element rootElem = generateRootElement();

    final File file = getFile(specifier);

    if (file == null) {
      return error(ServerMessages.ENTITY_DOES_NOT_EXIST, Status.CLIENT_ERROR_NOT_FOUND);
    }

    if (file.isDirectory()) {
      String referenceString = getReference().toString();
      if (!referenceString.endsWith("/")) {
        referenceString = referenceString + "/";
      }
      final Element folder = new Element("dir");
      folder.setAttribute("path", (specifier.endsWith("/") ? specifier : specifier + "/"));
      folder.setAttribute("name", file.getName());
      folder.setAttribute("url", referenceString);

      final boolean thumbnailsExist =
          new File(file.getAbsolutePath() + File.separator + ".thumbnails").exists();
      for (final File child : file.listFiles()) {
        Element celem = null;
        if (child.isDirectory()) {
          if (child.getName().equals(".thumbnails")) {
            continue;
          }
          celem = new Element("dir");
          celem.setAttribute("name", child.getName() + "/");
        } else {
          celem = getFileElement(specifier, child);
        }

        if (thumbnailsExist) {
          final Attribute thumbnailAttribute = getThumbnailAttribute(file, child, referenceString);
          if (thumbnailAttribute != null) {
            celem.setAttribute(thumbnailAttribute);
          }
        }

        folder.addContent(celem);
      }
      rootElem.addContent(folder);
      final long t2 = System.currentTimeMillis();

      getBenchmark().addBenchmark(this.getClass().getSimpleName(), t2 - t1);
      rootElem.addContent(getBenchmark().toElememt());
      final Document doc = new Document(rootElem);
      return ok(doc);

    } else {

      final MediaType mt = MediaType.valueOf(FileUtils.getMimeType(file));
      final FileRepresentation ret = new FileRepresentation(file, mt);
      ret.setDisposition(new Disposition(Disposition.TYPE_ATTACHMENT));

      return ret;
    }
  }

  protected Attribute getThumbnailAttribute(
      final File file, final File child, final String referenceString) {
    final String thpath =
        file.getAbsolutePath() + File.separator + ".thumbnails" + File.separator + child.getName();

    final File th = new File(thpath);
    if (th.exists()) {
      return new Attribute(
          "thumbnail", referenceString.replaceFirst("FileSystem", "Thumbnails") + child.getName());
    }
    return null;
  }

  private TransactionBenchmark benchmark;

  private TransactionBenchmark getBenchmark() {
    if (this.benchmark == null) {
      this.benchmark = TransactionBenchmark.getSubBenchmark();
    }
    return this.benchmark;
  }

  Element getFileElement(final String directory, final File file) throws Exception {
    final Element celem = new Element("file");
    celem.setAttribute(
        "id",
        getEntityID((directory.endsWith("/") ? directory : directory + "/") + file.getName()));
    celem.setAttribute("name", file.getName());
    return celem;
  }

  protected String getEntityID(final String path) throws Exception {
    return getEntity(path).getId().toString();
  }

  private Entity getEntity(final String path) throws Exception {
    final long t1 = System.currentTimeMillis();
    final TransactionContainer c = new TransactionContainer();
    final Entity e = new RetrieveEntity(0);
    final FileProperties fp = new FileProperties(null, path, null);
    e.setFileProperties(fp);
    c.add(e);
    final Transaction<?> t = new RetrieveSparseEntityByPath(c);
    t.execute();
    final long t2 = System.currentTimeMillis();
    getBenchmark().addBenchmark(this.getClass().getSimpleName() + ".getEntity", t2 - t1);
    return e;
  }

  protected File getFile(final String path) throws Exception {
    final File ret = getFromFileSystem(path);
    if (ret != null && ret.isFile()) {
      checkPermissions(path);
    }
    return ret;
  }

  private final void checkPermissions(final String path) throws Exception {
    final long t1 = System.currentTimeMillis();
    getEntity(path).checkPermission(EntityPermission.RETRIEVE_FILE);
    final long t2 = System.currentTimeMillis();
    getBenchmark().addBenchmark(this.getClass().getSimpleName() + ".checkPermissions", t2 - t1);
  }

  @Override
  protected Representation httpPostInChildClass(final Representation entity)
      throws ConnectionException, JDOMException {
    this.setStatus(org.restlet.data.Status.CLIENT_ERROR_METHOD_NOT_ALLOWED);
    return null;
  }
}
