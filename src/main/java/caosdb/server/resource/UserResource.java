/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.resource;

import caosdb.server.CaosDBException;
import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import caosdb.server.accessControl.ACMPermissions;
import caosdb.server.accessControl.UserSources;
import caosdb.server.accessControl.UserStatus;
import caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import caosdb.server.entity.Message;
import caosdb.server.transaction.DeleteUserTransaction;
import caosdb.server.transaction.InsertUserTransaction;
import caosdb.server.transaction.RetrieveUserTransaction;
import caosdb.server.transaction.UpdateUserTransaction;
import caosdb.server.utils.ServerMessages;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;

/**
 * This class handles requests for Users.
 *
 * @author Timm Fitschen
 */
public class UserResource extends AbstractCaosDBServerResource {

  @Override
  protected Representation httpGetInChildClass()
      throws ConnectionException, IOException, SQLException, CaosDBException,
          NoSuchAlgorithmException, Exception {

    final Document doc = new Document();
    final Element rootElem = generateRootElement();

    if (!getRequestedNames().isEmpty()) {
      try {
        final String username = getRequestedNames().get(0);
        final String realm =
            (getRequestAttributes().containsKey("realm")
                ? (String) getRequestAttributes().get("realm")
                : UserSources.guessRealm(username, UserSources.getDefaultRealm()));

        getUser().checkPermission(ACMPermissions.PERMISSION_RETRIEVE_USER_INFO(realm, username));

        final RetrieveUserTransaction t = new RetrieveUserTransaction(realm, username);
        t.execute();

        rootElem.addContent(t.getUserElement());
      } catch (final Message m) {
        if (m == ServerMessages.ACCOUNT_DOES_NOT_EXIST) {
          return error(ServerMessages.ACCOUNT_DOES_NOT_EXIST, Status.CLIENT_ERROR_NOT_FOUND);
        }
        throw m;
      }
    }

    doc.setRootElement(rootElem);
    return new JdomRepresentation(doc, MediaType.TEXT_XML, "  ", getReference(), getXSLScript());
  }

  @Override
  protected Representation httpPutInChildClass(final Representation entity)
      throws ConnectionException, JDOMException, Exception, xmlNotWellFormedException {

    try {
      final Form form = new Form(entity);
      final String username = getRequestedNames().get(0);
      final String realm =
          (getRequestAttributes().containsKey("realm")
              ? (String) getRequestAttributes().get("realm")
              : UserSources.guessRealm(username));
      final String password = form.getFirstValue("password");
      final String email = form.getFirstValue("email");
      final UserStatus status =
          UserStatus.valueOf(
              form.getFirstValue(
                      "status",
                      CaosDBServer.getServerProperty(
                          ServerProperties.KEY_NEW_USER_DEFAULT_ACTIVITY))
                  .toUpperCase());
      Integer userEntity = null;
      if (form.getFirst("entity") != null) {
        if (form.getFirstValue("entity").isEmpty()) {
          userEntity = 0;
        } else {
          userEntity = Integer.parseInt(form.getFirstValue("entity"));
        }
      }

      final UpdateUserTransaction t =
          new UpdateUserTransaction(realm, username, status, email, userEntity, password);
      t.execute();

      final Document doc = new Document();
      final Element rootElem = generateRootElement();

      rootElem.addContent(t.getUserElement());
      doc.setRootElement(rootElem);
      return new JdomRepresentation(doc, MediaType.TEXT_XML, "  ", getReference(), getXSLScript());
    } catch (final Message m) {
      if (m == ServerMessages.ACCOUNT_DOES_NOT_EXIST) {
        return error(m, Status.CLIENT_ERROR_NOT_FOUND);
      } else if (m == ServerMessages.ENTITY_DOES_NOT_EXIST) {
        return error(m, Status.CLIENT_ERROR_CONFLICT);
      } else if (m == ServerMessages.PASSWORD_TOO_WEAK) {
        return error(m, Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
      }
      throw m;
    } catch (final NumberFormatException e) {
      return error(ServerMessages.CANNOT_PARSE_INT_VALUE, Status.CLIENT_ERROR_BAD_REQUEST);
    }
  }

  @Override
  protected Representation httpPostInChildClass(final Representation entity)
      throws ConnectionException, JDOMException, Exception, xmlNotWellFormedException {

    final Form form = new Form(entity);
    final String username = form.getFirstValue("username");
    final String password = form.getFirstValue("password");
    final String email = form.getFirstValue("email");
    final UserStatus status =
        UserStatus.valueOf(
            form.getFirstValue(
                    "status",
                    CaosDBServer.getServerProperty(ServerProperties.KEY_NEW_USER_DEFAULT_ACTIVITY))
                .toUpperCase());
    Integer userEntity = null;
    if (form.getFirst("entity") != null) {
      userEntity = Integer.parseInt(form.getFirstValue("entity"));
    }

    final InsertUserTransaction t =
        new InsertUserTransaction(username, password, email, status, userEntity);
    try {
      t.execute();
    } catch (final Message m) {
      if (m == ServerMessages.ACCOUNT_NAME_NOT_UNIQUE) {
        return error(m, Status.CLIENT_ERROR_CONFLICT);
      } else if (m == ServerMessages.PASSWORD_TOO_WEAK) {
        return error(m, Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
      }
      throw m;
    }

    final Document doc = new Document();
    final Element rootElem = generateRootElement();

    rootElem.addContent(t.getUserElement());
    doc.setRootElement(rootElem);
    return new JdomRepresentation(doc, MediaType.TEXT_XML, "  ", getReference(), getXSLScript());
  }

  @Override
  protected Representation httpDeleteInChildClass()
      throws ConnectionException, SQLException, CaosDBException, IOException,
          NoSuchAlgorithmException, Exception {

    final Document doc = new Document();
    final Element rootElem = generateRootElement();

    final DeleteUserTransaction t = new DeleteUserTransaction(getRequestedNames().get(0));
    try {
      t.execute();
    } catch (final Message m) {
      if (m == ServerMessages.ACCOUNT_DOES_NOT_EXIST) {
        return error(m, Status.CLIENT_ERROR_NOT_FOUND);
      }
      throw m;
    }
    rootElem.addContent(t.getUserElement());

    doc.setRootElement(rootElem);
    return new JdomRepresentation(doc, MediaType.TEXT_XML, "  ", getReference(), getXSLScript());
  }
}
