/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.resource;

import java.io.File;
import java.io.IOException;
import org.restlet.data.Header;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.engine.header.HeaderConstants;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.restlet.util.Series;

public class Webinterface extends ServerResource {

  @Override
  protected void doInit() throws ResourceException {
    super.doInit();
  }

  private final File PUBLIC_DIRECTORY = new File("caosdb-webui/public/").getAbsoluteFile();

  @Get
  public Representation deliver() throws IOException {
    final String path = (String) getRequest().getAttributes().get("path");

    if (path == null) {
      getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
      return null;
    }
    final File file = new File(this.PUBLIC_DIRECTORY.getAbsolutePath() + "/" + path);
    // TODO   if (!FileUtils.isSubDirSymlinkSave(file, this.PUBLIC_DIRECTORY.getParentFile())) {
    //      getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
    //      return null;
    //    }
    if (!file.exists()) {
      getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
      return null;
    }

    Series<Header> headers = getRequest().getHeaders();
    if (headers == null) {
      headers = new Series<Header>(Header.class);
      getResponse().getAttributes().put(HeaderConstants.ATTRIBUTE_HEADERS, headers);
    }
    headers.set("Access-Control-Allow-Origin", getHostRef().toString());

    final MediaType mt =
        path.endsWith(".json")
            ? MediaType.APPLICATION_JSON
            : path.endsWith(".css")
                ? MediaType.TEXT_CSS
                : path.endsWith(".css.map")
                    ? MediaType.APPLICATION_ALL_JSON
                    : path.endsWith(".js")
                        ? MediaType.APPLICATION_JAVASCRIPT
                        : path.endsWith(".png")
                            ? MediaType.IMAGE_PNG
                            : path.endsWith(".html")
                                ? MediaType.TEXT_HTML
                                : path.endsWith(".yaml") ? MediaType.TEXT_YAML : MediaType.TEXT_XML;

    final FileRepresentation ret = new FileRepresentation(file, mt);

    return ret;
  }
}
