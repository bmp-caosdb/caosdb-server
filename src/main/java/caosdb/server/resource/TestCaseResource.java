/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.resource;

import caosdb.datetime.DateTimeFactory2;
import caosdb.datetime.UTCDateTime;
import caosdb.server.CaosDBException;
import caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import caosdb.server.datatype.AbstractCollectionDatatype;
import caosdb.server.datatype.AbstractDatatype;
import caosdb.server.datatype.BooleanDatatype;
import caosdb.server.datatype.BooleanValue;
import caosdb.server.datatype.CollectionValue;
import caosdb.server.datatype.DateTimeDatatype;
import caosdb.server.datatype.DoubleDatatype;
import caosdb.server.datatype.GenericValue;
import caosdb.server.datatype.IntegerDatatype;
import caosdb.server.datatype.ListDatatype;
import caosdb.server.datatype.ReferenceDatatype2;
import caosdb.server.datatype.ReferenceValue;
import caosdb.server.datatype.TextDatatype;
import caosdb.server.datatype.Value;
import caosdb.server.entity.Entity;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.MagicTypes;
import caosdb.server.entity.Message;
import caosdb.server.entity.Message.MessageType;
import caosdb.server.entity.RetrieveEntity;
import caosdb.server.entity.Role;
import caosdb.server.entity.StatementStatus;
import caosdb.server.entity.container.PropertyContainer;
import caosdb.server.entity.wrapper.Parent;
import caosdb.server.entity.wrapper.Property;
import caosdb.server.entity.xml.ToElementable;
import caosdb.server.query.Query;
import caosdb.server.query.Query.ParsingException;
import caosdb.server.utils.ServerMessages;
import caosdb.server.utils.TransactionLogMessage;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.restlet.representation.Representation;

public class TestCaseResource extends AbstractCaosDBServerResource {

  @Override
  protected void doInit() {
    super.doInit();
    setXSLScript("webcaosdb.xsl");
  }

  @Override
  protected Element generateRootElement() {
    final Element retRoot = new Element("Response");

    retRoot.setAttribute("username", "TestUser");
    retRoot.setAttribute("realm", "PAM");
    retRoot.setAttribute("srid", getSRID());
    if (this.getCRID() != null) {
      retRoot.setAttribute("crid", this.getCRID());
    }
    retRoot.setAttribute("timestamp", getTimestamp().toString());
    retRoot.setAttribute("baseuri", getRootRef().toString() + "/TestCase/");
    return retRoot;
  }

  @Override
  protected synchronized Representation httpGetInChildClass()
      throws ConnectionException, IOException, SQLException, CaosDBException,
          NoSuchAlgorithmException, Exception {

    final Element rootElem = generateRootElement();
    final Document doc = new Document();

    final Collection<ToElementable> selectedTestCases = getSelectedTestCase();
    rootElem.setAttribute("count", Integer.toString(selectedTestCases.size()));

    int from = 0;
    int len = Integer.MAX_VALUE;
    if (getFlags().containsKey("query")) {
      final String queryStr = getFlags().get("query");
      if (queryStr != null && !queryStr.isEmpty()) {
        final Query query = new Query(queryStr);
        try {
          query.parse();
          query.addToElement(rootElem);
        } catch (final ParsingException e) {
          query.addToElement(rootElem);
          rootElem.addContent(ServerMessages.QUERY_PARSING_ERROR.toElement());
          doc.setRootElement(rootElem);
          return ok(doc);
        }
      }
    }
    if (getFlags().containsKey("P")) {
      final String[] split = getFlags().get("P").split("L");
      from = Integer.valueOf(split[0]);
      len = Integer.valueOf(split[1]);
    }
    int i = 0;
    for (final ToElementable e : selectedTestCases) {
      if (i++ >= from && i <= from + len) {
        e.addToElement(rootElem);
      }
    }
    // testEntities = null;
    // entityCounter = 1;

    doc.setRootElement(rootElem);
    return ok(doc);
  }

  private Collection<ToElementable> getSelectedTestCase() {
    final HashMap<String, ToElementable> testCases = getTestCase();
    if (getFlags().containsKey("query")) {
      final String queryStr = getFlags().get("query");
      if (queryStr.startsWith("FIND Annotation WHICH REFERENCES")) {
        final LinkedList<ToElementable> ret = new LinkedList<ToElementable>();
        for (final ToElementable e : testCases.values()) {
          if (e instanceof Entity
              && ((Entity) e).hasParents()
              && ((Entity) e).getParents().get(0).getName().equals("Annotation")) {
            ret.add(e);
          }
        }
        return ret;
      }
    }
    if (getFlags().containsKey("all")) {
      final LinkedList<ToElementable> ret = new LinkedList<ToElementable>();
      for (final ToElementable e : testCases.values()) {
        if (e instanceof Entity) {
          if (getFlags().get("all").equalsIgnoreCase("Entity")) {
            ret.add(e);
          } else {
            final Entity entity = (Entity) e;
            if (entity.getRole().toString().equalsIgnoreCase(getFlags().get("all"))) {
              ret.add(e);
            }
          }
        }
      }
      return ret;
    }
    if (!getRequestedIDs().isEmpty() || !getRequestedNames().isEmpty()) {
      final LinkedList<ToElementable> ret = new LinkedList<ToElementable>();
      for (final Integer id : getRequestedIDs()) {
        for (final ToElementable e : testCases.values()) {
          if (e instanceof Entity) {
            if (((Entity) e).getId().equals(id)) {
              ret.add(e);
            }
          }
        }
      }
      for (final String name : getRequestedNames()) {
        for (final ToElementable e : testCases.values()) {
          if (e instanceof Entity) {
            if (((Entity) e).getName().equals(name)) {
              ret.add(e);
            }
          }
        }
      }
      return ret;
    }
    return testCases.values();
  }

  private HashMap<String, ToElementable> getTestCase() {
    if (testEntities == null) {
      testEntities = new HashMap<String, ToElementable>();

      final Entity lp1 =
          createProperty(
              "LIST"
                  + AbstractCollectionDatatype.LEFT_DELIMITER
                  + "TEXT"
                  + AbstractCollectionDatatype.RIGHT_DELIMITER);
      final Entity lp2 =
          createProperty(
              "LIST"
                  + AbstractCollectionDatatype.LEFT_DELIMITER
                  + "INTEGER"
                  + AbstractCollectionDatatype.RIGHT_DELIMITER,
              "meter");
      final Entity lp4 =
          createProperty(
              "LIST"
                  + AbstractCollectionDatatype.LEFT_DELIMITER
                  + "BOOLEAN"
                  + AbstractCollectionDatatype.RIGHT_DELIMITER);
      final Entity tp1 = createProperty("TEXT");
      final Entity dp1 = createProperty("DOUBLE", "meter");
      final Entity dtp1 = createProperty("DATETIME");
      final Entity tp2 = createProperty("TEXT");
      final Entity ip1 = createProperty("INTEGER", "meter");
      final Entity bp1 = createProperty("BOOLEAN");
      final Entity fp1 = createProperty("FILE");

      final Entity rt1 = createRecordType(new Entity[] {}, new Entity[] {});
      final Entity rt2 = createRecordType(new Entity[] {}, new Entity[] {tp2, ip1, bp1});

      final Entity rp1 = createProperty(rt2);
      final Entity lp3 =
          createProperty(
              "LIST"
                  + AbstractCollectionDatatype.LEFT_DELIMITER
                  + rt2.getName()
                  + AbstractCollectionDatatype.RIGHT_DELIMITER);
      final Entity rt3 =
          createRecordType(
              new Entity[] {rt1}, new Entity[] {lp1, tp1, dp1, dtp1, rp1, lp2, lp3, lp4});

      createRecordFromRecordType(rt2);
      createRecordFromRecordType(rt3);

      createFileRecord("lorem_ipsum.txt");
      createFileRecord("testsubfolder/lorem_ipsum.txt");
      final Entity pngFile = createFileRecord("testimg.png");
      createFileRecord("testimg.jpg");
      createFileRecord("testimg.svg");
      createFileRecord("testimg.gif");

      createRecordWithPngFile(pngFile, fp1, rt1);

      /* Test Error, Warning, Info */
      createError();
      createWarning();
      createInfo();

      /* Entities with Error, Warning, Info */
      createRecordFromRecordType(rt2)
          .addError(
              new Message(
                  MessageType.Error,
                  666,
                  "Description of this error",
                  "Further information about this error."));
      createRecordFromRecordType(rt2)
          .addWarning(
              new Message(
                  MessageType.Warning,
                  346,
                  "Description of this warning",
                  "Further information about this warning."));
      createRecordFromRecordType(rt2)
          .addInfo(
              new Message(
                  MessageType.Info,
                  0,
                  "Description of this info",
                  "Further information about this info."));

      createAnnotation();
    }
    return testEntities;
  }

  private void createAnnotation() {
    final Entity annotationOf = createEntity(Role.Property, "annotationOf");
    annotationOf.setDatatype("REFERENCE");
    final Entity comment = createEntity(Role.Property, "comment");
    comment.setDatatype("TEXT");
    final Entity annotation = createEntity(Role.RecordType, "Annotation");
    annotation.addProperty(new Property(annotationOf));
    annotation.addProperty(new Property(comment));

    final Entity rec = createRecordFromRecordType(annotation);
    rec.addTransactionLog(
        new TransactionLogMessage(
            "INSERT",
            rec,
            "Adam",
            UTCDateTime.SystemMillisToUTCDateTime(System.currentTimeMillis())));
  }

  private Entity createRecordWithPngFile(
      final Entity pngFile, final Entity property, final Entity parent) {
    final Entity entity = createRecordFromRecordType(parent);
    final Property newP = new Property(property.toElement());
    newP.setValue(new ReferenceValue(pngFile));
    entity.addProperty(newP);
    return entity;
  }

  private Entity createFileRecord(final String path) {
    final Entity entity = createEntity(Role.File, "TestFile");
    entity.setFileProperties(TestCaseFileSystemResource.getFileProperties(path));
    return entity;
  }

  private Entity createProperty(final Entity rt2) {
    final Entity entity = createProperty("REFERENCE");
    entity.setDatatype(rt2.getName());
    return entity;
  }

  private Value generateValue(final AbstractDatatype dt) {
    if (dt instanceof IntegerDatatype) {
      return new GenericValue(1337);
    } else if (dt instanceof DoubleDatatype) {
      return new GenericValue(3.14);
    } else if (dt instanceof DateTimeDatatype) {
      return DateTimeFactory2.valueOf(new GregorianCalendar());
    } else if (dt instanceof BooleanDatatype) {
      return BooleanValue.valueOf(entityCounter % 2 == 0);
    } else if (dt instanceof TextDatatype) {
      return new GenericValue("Don't panic.");
    } else if (dt instanceof ReferenceDatatype2) {
      final ReferenceDatatype2 dt2 = (ReferenceDatatype2) dt;
      dt2.getId();
      for (final ToElementable e : testEntities.values()) {
        if (e instanceof Entity && ((Entity) e).getRole() == Role.Record) {
          return new ReferenceValue((Entity) e);
        }
      }
    } else if (dt instanceof ListDatatype) {
      final AbstractDatatype dt2 = ((ListDatatype) dt).getDatatype();
      final CollectionValue v = new CollectionValue();
      for (int i = 0; i < 25; i++) {
        v.add(generateValue(dt2));
      }
      return v;
    }
    return null;
  }

  private Entity createRecordFromRecordType(final Entity rt2) {
    final Entity entity = createEntity(Role.Record, "TestRecord");
    addParentsAndProperties(entity, rt2, rt2.getProperties(), "FIX");
    for (final Property p : entity.getProperties()) {
      p.setValue(generateValue(p.getDatatype()));
    }
    return entity;
  }

  private void addParentsAndProperties(
      final Entity entity,
      final Entity parent,
      final PropertyContainer properties,
      final String importance) {
    final List<Entity> par = new LinkedList<Entity>();
    par.add(parent);
    addParentsAndProperties(entity, par, properties, importance);
  }

  private void addParentsAndProperties(
      final Entity entity,
      final Collection<? extends EntityInterface> parents,
      final Collection<? extends EntityInterface> properties,
      final String importance) {
    for (final EntityInterface p : properties) {
      final Property newP = new Property(p.toElement());
      if (importance != null) {
        newP.setStatementStatus(StatementStatus.valueOf(importance));
      }
      entity.addProperty(newP);
    }
    for (final EntityInterface p : parents) {
      entity.addParent(new Parent(p.toElement()));
    }
  }

  private Message createError() {
    return createMessage(
        MessageType.Error.toString(),
        "This is a description of the error",
        "Here is additional info about the error.",
        666);
  }

  private Message createWarning() {
    return createMessage(
        MessageType.Warning.toString(),
        "This is a description of the warning",
        "Here is additional info about the warning.",
        333);
  }

  private Message createInfo() {
    return createMessage(
        MessageType.Info.toString(),
        "Here is useful information about the last activity of the user.",
        "Here is additional information.",
        null);
  }

  private Message createMessage(
      String type, final String description, final String body, final Integer code) {
    final Message m = new Message(type, code, description, body);
    if (testEntities.containsKey(type)) {
      int i = 0;
      while (testEntities.containsKey(type + "-" + i)) {
        i++;
      }
      type += "-" + i;
    }
    testEntities.put(type, m);
    return m;
  }

  private static HashMap<String, ToElementable> testEntities = null;
  private static int entityCounter = 1;

  private Entity createProperty(final String datatype) {
    return createProperty(datatype, null);
  }

  private Entity createRecordType(final Entity[] parents, final Entity[] properties) {
    final Entity entity = createEntity(Role.RecordType, "TestRecordType");
    addParentsAndProperties(entity, parents, properties, "OBLIGATORY");
    return entity;
  }

  private void addParentsAndProperties(
      final Entity entity,
      final EntityInterface[] parents,
      final EntityInterface[] properties,
      final String importance) {
    addParentsAndProperties(entity, Arrays.asList(parents), Arrays.asList(properties), importance);
  }

  private Entity createProperty(final String datatype, final String unit) {
    final Entity entity = createEntity(Role.Property, "Test" + datatype + "Property");
    entity.setDatatype(datatype);
    if (unit != null) {
      final EntityInterface magicUnit = MagicTypes.UNIT.getEntity();
      final Property unitProp = new Property();
      unitProp.setId(magicUnit.getId());
      unitProp.setValue(new GenericValue(unit));
      entity.addProperty(unitProp);
    }
    return entity;
  }

  private Entity createEntity(final Role role, String name) {
    final Entity entity = new RetrieveEntity(entityCounter++);
    entity.setRole(role);
    entity.setDescription(
        "An informative description of the idea, meaning, and purpose of this entity.");
    if (testEntities.containsKey(name)) {
      int i = 0;
      while (testEntities.containsKey(name + "-" + i)) {
        i++;
      }
      name += "-" + i;
    }
    testEntities.put(name, entity);
    entity.setName(name);
    return entity;
  }
}
