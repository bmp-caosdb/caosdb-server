package caosdb.server.resource;

import caosdb.server.CaosDBServer;
import caosdb.server.ServerPropertiesSerializer;
import caosdb.server.accessControl.ACMPermissions;
import org.restlet.data.Form;
import org.restlet.data.Parameter;
import org.restlet.data.Status;
import org.restlet.representation.Representation;

public class ServerPropertiesResource extends AbstractCaosDBServerResource {

  @Override
  protected void doInit() {
    super.doInit();
    setXSLScript("xsl/server_properties.xsl");
  }

  @Override
  protected Representation httpGetInChildClass() {
    if (CaosDBServer.isDebugMode()) {
      getUser().checkPermission(ACMPermissions.PERMISSION_ACCESS_SERVER_PROPERTIES);
      return super.ok(
          new ServerPropertiesSerializer().serialize(CaosDBServer.getServerProperties()));
    }
    return error(Status.CLIENT_ERROR_NOT_FOUND);
  }

  @Override
  protected Representation httpPostInChildClass(Representation entity) {
    if (CaosDBServer.isDebugMode()) {
      getUser().checkPermission(ACMPermissions.PERMISSION_ACCESS_SERVER_PROPERTIES);
      Form form = new Form(entity);
      for (Parameter param : form) {
        CaosDBServer.setProperty(param.getName(), param.getValue());
      }
      return super.ok(
          new ServerPropertiesSerializer().serialize(CaosDBServer.getServerProperties()));
    }
    return error(Status.CLIENT_ERROR_NOT_FOUND);
  }
}
