/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.resource.transaction;

import caosdb.server.CaosDBException;
import caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import caosdb.server.entity.container.DeleteContainer;
import caosdb.server.entity.container.InsertContainer;
import caosdb.server.entity.container.RetrieveContainer;
import caosdb.server.entity.container.UpdateContainer;
import caosdb.server.resource.AbstractCaosDBServerResource;
import caosdb.server.resource.JdomRepresentation;
import caosdb.server.resource.transaction.handlers.FileUploadHandler;
import caosdb.server.resource.transaction.handlers.IDHandler;
import caosdb.server.resource.transaction.handlers.RequestHandler;
import caosdb.server.resource.transaction.handlers.SimpleGetRequestHandler;
import caosdb.server.resource.transaction.handlers.SimpleWriteHandler;
import caosdb.server.transaction.Delete;
import caosdb.server.transaction.Insert;
import caosdb.server.transaction.Retrieve;
import caosdb.server.transaction.Update;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;

public class EntityResource extends AbstractCaosDBServerResource {

  private final boolean get;
  private final boolean post;
  private final boolean put;
  private final boolean delete;

  public EntityResource(
      final boolean get, final boolean post, final boolean put, final boolean delete) {
    this.get = get;
    this.post = post;
    this.put = put;
    this.delete = delete;
  }

  public EntityResource() {
    this(true, true, true, true);
  }

  protected RequestHandler<RetrieveContainer> getGetRequestHandler() {
    return new SimpleGetRequestHandler();
  }

  protected RequestHandler<DeleteContainer> getDeleteRequestHandler() {
    return new IDHandler<DeleteContainer>();
  }

  protected RequestHandler<InsertContainer> getPostRequestHandler() {
    if (getRequest().getEntity().getMediaType() != null
        && getRequest().getEntity().getMediaType().equals(MediaType.MULTIPART_FORM_DATA, true)) {
      return new FileUploadHandler<InsertContainer>();
    } else {
      return new SimpleWriteHandler<InsertContainer>();
    }
  }

  protected RequestHandler<UpdateContainer> getPutRequestHandler() {
    if (getRequest().getEntity().getMediaType() != null
        && getRequest().getEntity().getMediaType().equals(MediaType.MULTIPART_FORM_DATA, true)) {
      return new FileUploadHandler<UpdateContainer>();
    } else {
      return new SimpleWriteHandler<UpdateContainer>();
    }
  }

  @Override
  protected final Representation httpGetInChildClass()
      throws ConnectionException, IOException, SQLException, CaosDBException,
          NoSuchAlgorithmException, Exception {

    if (!this.get) {
      getResponse().setStatus(Status.CLIENT_ERROR_METHOD_NOT_ALLOWED);
      return null;
    }
    final RetrieveContainer entityContainer =
        new RetrieveContainer(getUser(), getTimestamp(), getSRID(), getFlags());
    final Document doc = new Document();

    getGetRequestHandler().handle(this, entityContainer);

    final Retrieve retrieve = new Retrieve(entityContainer);
    retrieve.execute();

    final Element rootElem = generateRootElement();
    entityContainer.addToElement(rootElem);
    doc.setRootElement(rootElem);
    return new JdomRepresentation(doc, MediaType.TEXT_XML, "  ", getReference(), getXSLScript());
  }

  @Override
  protected final Representation httpDeleteInChildClass() throws Exception {

    if (!this.delete) {
      getResponse().setStatus(Status.CLIENT_ERROR_METHOD_NOT_ALLOWED);
      return null;
    }
    final DeleteContainer entityContainer =
        new DeleteContainer(getUser(), getTimestamp(), getSRID(), getFlags());
    final Document doc = new Document();

    getDeleteRequestHandler().handle(this, entityContainer);

    final Delete delete = new Delete(entityContainer);
    delete.execute();

    final Element rootElem = generateRootElement();
    entityContainer.addToElement(rootElem);
    doc.setRootElement(rootElem);

    return new JdomRepresentation(doc, MediaType.TEXT_XML, "  ", getReference(), getXSLScript());
  }

  @Override
  protected final Representation httpPostInChildClass(final Representation entity)
      throws xmlNotWellFormedException, Exception {

    if (!this.post) {
      getResponse().setStatus(Status.CLIENT_ERROR_METHOD_NOT_ALLOWED);
      return null;
    }

    final InsertContainer entityContainer =
        new InsertContainer(getUser(), getTimestamp(), getSRID(), getFlags());
    final Document doc = new Document();

    getPostRequestHandler().handle(this, entityContainer);

    final Insert insert = new Insert(entityContainer);
    insert.execute();

    final Element rootElem = generateRootElement();
    entityContainer.addToElement(rootElem);
    doc.setRootElement(rootElem);
    return new JdomRepresentation(doc, MediaType.TEXT_XML, "  ", getReference(), getXSLScript());
  }

  @Override
  protected final Representation httpPutInChildClass(final Representation entity)
      throws ConnectionException, JDOMException, Exception, xmlNotWellFormedException {

    if (!this.put) {
      getResponse().setStatus(Status.CLIENT_ERROR_METHOD_NOT_ALLOWED);
      return null;
    }

    final UpdateContainer entityContainer =
        new UpdateContainer(getUser(), getTimestamp(), getSRID(), getFlags());
    final Document doc = new Document();

    getPutRequestHandler().handle(this, entityContainer);

    final Update update = new Update(entityContainer);
    update.execute();

    final Element rootElem = generateRootElement();
    entityContainer.addToElement(rootElem);
    doc.setRootElement(rootElem);
    return new JdomRepresentation(doc, MediaType.TEXT_XML, "  ", getReference(), getXSLScript());
  }
}
