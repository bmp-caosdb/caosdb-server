/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.resource.transaction.handlers;

import caosdb.server.FileSystem;
import caosdb.server.entity.FileProperties;
import caosdb.server.entity.container.WritableContainer;
import caosdb.server.resource.transaction.EntityResource;
import caosdb.server.utils.ServerMessages;
import java.io.IOException;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.jdom2.Element;
import org.restlet.data.MediaType;
import org.restlet.ext.fileupload.RestletFileUpload;

public class FileUploadHandler<T extends WritableContainer> extends SimpleWriteHandler<T> {

  @Override
  public void handle(final EntityResource t, final T container) throws Exception {
    if (!t.getRequest().getEntity().getMediaType().equals(MediaType.MULTIPART_FORM_DATA, true)) {
      throw new Exception("Wrong MEDIATYPE");
    }
    final DiskFileItemFactory factory = new DiskFileItemFactory();
    factory.setSizeThreshold(1000240);
    final RestletFileUpload upload = new RestletFileUpload(factory);
    final FileItemIterator iter = upload.getItemIterator(t.getRequest().getEntity());

    FileItemStream item = iter.next();

    final Element element;
    // First part of the multi-part form data entity must be the xml
    // representation of the files.
    // Name of the form field: FileRepresentation
    if (item.isFormField()) {
      if (item.getFieldName().equals("FileRepresentation")) {
        element = t.parseEntity(item.openStream()).getRootElement();
      } else {
        throw ServerMessages.NO_FILE_REPRESENTATION_SUBMITTED;
      }
    } else {
      throw ServerMessages.FORM_CONTAINS_UNSUPPORTED_CONTENT;
    }

    // Get files, store to tmp dir.
    while (iter.hasNext()) {
      item = iter.next();
      try {
        final FileProperties file = FileSystem.upload(item, t.getSRID());
        container.addFile(item.getName(), file);
      } catch (final IOException e) {
        throw new FileUploadException();
      }
    }

    // transform xml elements to entities and add them to the container.
    processEntities(container, element);
  }
}
