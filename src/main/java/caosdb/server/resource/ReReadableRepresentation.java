/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.resource;

import com.google.common.base.Charsets;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.restlet.data.CharacterSet;
import org.restlet.data.ClientInfo;
import org.restlet.data.Digest;
import org.restlet.data.Disposition;
import org.restlet.data.Encoding;
import org.restlet.data.Language;
import org.restlet.data.MediaType;
import org.restlet.data.Range;
import org.restlet.data.Reference;
import org.restlet.data.Tag;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;
import org.restlet.util.ReadingListener;
import org.restlet.util.SelectionRegistration;

/**
 * Wrapper for Representation. Copies (part of) the content of a transient representation while it
 * is read for later reuse (Needed by RequestLog). If the representation is longer than MAX_LENGTH
 * characters, the copy is truncated.
 *
 * @author tf
 */
public class ReReadableRepresentation extends Representation {

  public static final int MAX_LENGTH = 5000;

  public static class ReReadableInputStream extends InputStream {
    private final InputStream wrapped;
    private final ReReadableRepresentation writeTo;

    public ReReadableInputStream(final ReReadableRepresentation writeTo, final InputStream wrapp) {
      wrapped = wrapp;
      this.writeTo = writeTo;
    }

    @Override
    public int read() throws IOException {
      final int r = wrapped.read();
      if (r != -1) {
        writeTo.add((char) r);
      }
      return r;
    }

    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
      final int r = wrapped.read(b, off, len);
      if (r > 0) {
        writeTo.add(Arrays.copyOf(b, r));
      }
      return r;
    }

    @Override
    public int read(final byte[] b) throws IOException {
      final int r = wrapped.read(b);
      if (r > 0) {
        writeTo.add(Arrays.copyOf(b, r));
      }
      return r;
    }

    @Override
    public void close() throws IOException {
      writeTo.close();
      wrapped.close();
    }

    @Override
    public int available() throws IOException {
      return wrapped.available();
    }

    @Override
    public long skip(final long n) throws IOException {
      return wrapped.skip(n);
    }

    @Override
    public synchronized void mark(final int readlimit) {
      wrapped.mark(readlimit);
    }

    @Override
    public boolean markSupported() {
      return wrapped.markSupported();
    }

    @Override
    public synchronized void reset() throws IOException {
      wrapped.reset();
    }
  }

  public static class ReReadableReader extends Reader {

    private final Reader wrapped;
    private final ReReadableRepresentation writeTo;

    public ReReadableReader(final ReReadableRepresentation writeTo, final Reader wrapp) {
      wrapped = wrapp;
      this.writeTo = writeTo;
    }

    @Override
    public void close() throws IOException {
      writeTo.close();
      wrapped.close();
    }

    @Override
    public int read(final char[] cbuf, final int off, final int len) throws IOException {
      final int r = wrapped.read(cbuf, off, len);
      if (r > 0) {
        writeTo.add(Arrays.copyOf(cbuf, r));
      }
      return r;
    }

    @Override
    public int read() throws IOException {
      final int r = wrapped.read();
      if (r != -1) {
        writeTo.add((char) r);
      }
      return r;
    }

    @Override
    public int read(final char[] cbuf) throws IOException {
      final int r = wrapped.read(cbuf);
      if (r > 0) {
        writeTo.add(Arrays.copyOf(cbuf, r));
      }
      return r;
    }

    @Override
    public int read(final CharBuffer target) throws IOException {
      final int r = wrapped.read(target);
      if (r > 0) {
        writeTo.add(target);
      }
      return r;
    }

    @Override
    public void mark(final int readAheadLimit) throws IOException {
      wrapped.mark(readAheadLimit);
    }

    @Override
    public long skip(final long n) throws IOException {
      return wrapped.skip(n);
    }

    @Override
    public boolean markSupported() {
      return wrapped.markSupported();
    }

    @Override
    public boolean ready() throws IOException {
      return wrapped.ready();
    }

    @Override
    public void reset() throws IOException {
      wrapped.reset();
    }
  }

  public static class ReReadableByteChannel implements ReadableByteChannel {

    private final ReadableByteChannel wrapped;
    private final ReReadableRepresentation writeTo;

    public ReReadableByteChannel(
        final ReReadableRepresentation writeTo, final ReadableByteChannel wrapp) {
      wrapped = wrapp;
      this.writeTo = writeTo;
    }

    @Override
    public void close() throws IOException {
      writeTo.close();
      wrapped.close();
    }

    @Override
    public boolean isOpen() {
      return wrapped.isOpen();
    }

    @Override
    public int read(final ByteBuffer dst) throws IOException {
      final int r = wrapped.read(dst);
      if (r > 0) {
        writeTo.add(dst);
      }
      return r;
    }
  }

  private boolean isClosed = false;

  private final Representation wrapped;

  private final StringBuffer rereadabelStuff;

  public ReReadableRepresentation(final Representation requestEntity) {
    wrapped = requestEntity;
    rereadabelStuff = new StringBuffer();
  }

  private void checkClose() {
    if (rereadabelStuff.length() >= MAX_LENGTH) {
      closeEarly();
    }
  }

  public void add(final ByteBuffer dst) {
    if (!isClosed) {
      rereadabelStuff.append(dst);
      checkClose();
    }
  }

  public void add(final CharBuffer target) {
    if (!isClosed) {
      rereadabelStuff.append(target);
      checkClose();
    }
  }

  public void add(final byte[] b) {
    if (!isClosed) {
      final String s =
          new String(b, getCharacterSet() != null ? getCharacterSet().toCharset() : Charsets.UTF_8);
      rereadabelStuff.append(s);
      checkClose();
    }
  }

  public void add(final char r) {
    if (!isClosed) {
      rereadabelStuff.append(r);
      checkClose();
    }
  }

  public void add(final char[] cbuf) {
    if (!isClosed) {
      rereadabelStuff.append(cbuf);
      checkClose();
    }
  }

  @Override
  public ReadableByteChannel getChannel() throws IOException {
    final ReadableByteChannel wrapp = wrapped.getChannel();
    if (wrapp != null) {
      return new ReReadableByteChannel(this, wrapp);
    }
    return null;
  }

  @Override
  public Reader getReader() throws IOException {
    final Reader wrapp = wrapped.getReader();
    if (wrapp != null) {
      return new ReReadableReader(this, wrapp);
    }
    return null;
  }

  @Override
  public InputStream getStream() throws IOException {
    final InputStream wrapp = wrapped.getStream();
    if (wrapp != null) {
      return new ReReadableInputStream(this, wrapp);
    }
    return null;
  }

  @Override
  public void write(final Writer writer) throws IOException {
    wrapped.write(writer);
  }

  @Override
  public void write(final WritableByteChannel writableChannel) throws IOException {
    wrapped.write(writableChannel);
  }

  @Override
  public void write(final OutputStream outputStream) throws IOException {
    wrapped.write(outputStream);
  }

  public void closeEarly() {
    rereadabelStuff.append("[...]");
    close();
  }

  public void close() {
    isClosed = true;
  }

  public String getString() {
    return rereadabelStuff.toString();
  }

  @Override
  public boolean isTransient() {
    return wrapped.isTransient();
  }

  @Override
  public long getAvailableSize() {
    return wrapped.getAvailableSize();
  }

  @Override
  public void append(final Appendable appendable) throws IOException {
    wrapped.append(appendable);
  }

  @Override
  public ClientInfo createClientInfo() {
    return wrapped.createClientInfo();
  }

  @Override
  public boolean equals(final Object other) {
    return wrapped.equals(other);
  }

  @Override
  public long exhaust() throws IOException {
    return wrapped.exhaust();
  }

  @Override
  public CharacterSet getCharacterSet() {
    return wrapped.getCharacterSet();
  }

  @Override
  public Digest getDigest() {
    return wrapped.getDigest();
  }

  @Override
  public Disposition getDisposition() {
    return wrapped.getDisposition();
  }

  @Override
  public List<Encoding> getEncodings() {
    return wrapped.getEncodings();
  }

  @Override
  public Date getExpirationDate() {
    return wrapped.getExpirationDate();
  }

  @Override
  public List<Language> getLanguages() {
    return wrapped.getLanguages();
  }

  @Override
  public Reference getLocationRef() {
    return wrapped.getLocationRef();
  }

  @Override
  public MediaType getMediaType() {
    return wrapped.getMediaType();
  }

  @Override
  public Date getModificationDate() {
    return wrapped.getModificationDate();
  }

  @Override
  public Range getRange() {
    return wrapped.getRange();
  }

  @Override
  public SelectionRegistration getRegistration() throws IOException {
    return wrapped.getRegistration();
  }

  @Override
  public long getSize() {
    return wrapped.getSize();
  }

  @Override
  public Tag getTag() {
    return wrapped.getTag();
  }

  @Override
  public String getText() throws IOException {
    return wrapped.getText();
  }

  @Override
  public boolean hasKnownSize() {
    return wrapped.hasKnownSize();
  }

  @Override
  public boolean includes(final Variant other) {
    return wrapped.includes(other);
  }

  @Override
  public boolean isAvailable() {
    return wrapped.isAvailable();
  }

  @Override
  public boolean isCompatible(final Variant other) {
    return wrapped.isCompatible(other);
  }

  @Override
  public boolean isEmpty() {
    return wrapped.isEmpty();
  }

  @Override
  public boolean isSelectable() {
    return wrapped.isSelectable();
  }

  @Override
  public void release() {
    wrapped.release();
  }

  @Override
  public void setAvailable(final boolean available) {
    wrapped.setAvailable(available);
  }

  @Override
  public void setCharacterSet(final CharacterSet characterSet) {
    wrapped.setCharacterSet(characterSet);
  }

  @Override
  public void setDigest(final Digest digest) {
    wrapped.setDigest(digest);
  }

  @Override
  public void setDisposition(final Disposition disposition) {
    wrapped.setDisposition(disposition);
  }

  @Override
  public void setEncodings(final List<Encoding> encodings) {
    wrapped.setEncodings(encodings);
  }

  @Override
  public void setExpirationDate(final Date expirationDate) {
    wrapped.setExpirationDate(expirationDate);
  }

  @Override
  public void setLanguages(final List<Language> languages) {
    wrapped.setLanguages(languages);
  }

  @Override
  public void setListener(final ReadingListener readingListener) {
    wrapped.setListener(readingListener);
  }

  @Override
  public void setLocationRef(final Reference location) {
    wrapped.setLocationRef(location);
  }

  @Override
  public void setLocationRef(final String locationUri) {
    wrapped.setLocationRef(locationUri);
  }

  @Override
  public void setMediaType(final MediaType mediaType) {
    wrapped.setMediaType(mediaType);
  }

  @Override
  public void setModificationDate(final Date modificationDate) {
    wrapped.setModificationDate(modificationDate);
  }

  @Override
  public void setSize(final long expectedSize) {
    wrapped.setSize(expectedSize);
  }

  @Override
  public void setRange(final Range range) {
    wrapped.setRange(range);
  }

  @Override
  public void setTag(final Tag tag) {
    wrapped.setTag(tag);
  }

  @Override
  public void setTransient(final boolean isTransient) {
    wrapped.setTransient(isTransient);
  }

  @Override
  public String toString() {
    return wrapped.toString();
  }
}
