/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.resource;

import caosdb.server.accessControl.Principal;
import caosdb.server.entity.FileProperties;
import caosdb.server.utils.FileUtils;
import java.io.File;
import org.jdom2.Element;

public class TestCaseFileSystemResource extends FileSystemResource {

  @Override
  protected void doInit() {
    super.doInit();
    setXSLScript("webcaosdb.xsl");
  }

  private static final String BASEPATH = "./testfiles/";

  @Override
  protected File getFile(final String path) throws Exception {
    final File f = new File(BASEPATH + path);
    return (f != null && f.exists() ? f : null);
  }

  @Override
  protected String getEntityID(final String path) throws Exception {
    return "123412341234";
  }

  @Override
  protected Element generateRootElement() {
    final Element retRoot = new Element("Response");

    if (getUser() != null && getUser().isAuthenticated()) {
      retRoot.setAttribute("username", ((Principal) getUser().getPrincipal()).getUsername());
      retRoot.setAttribute("realm", ((Principal) getUser().getPrincipal()).getRealm());
    }
    retRoot.setAttribute("srid", getSRID());
    if (this.getCRID() != null) {
      retRoot.setAttribute("crid", this.getCRID());
    }
    retRoot.setAttribute("timestamp", getTimestamp().toString());
    retRoot.setAttribute("baseuri", getRootRef().toString() + "/TestCase/");
    return retRoot;
  }

  public static FileProperties getFileProperties(final String path) {
    final File f = new File(BASEPATH + path);
    return new FileProperties(FileUtils.getChecksum(f), path, f.length());
  }
}
