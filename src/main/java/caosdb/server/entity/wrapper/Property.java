/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.entity.wrapper;

import caosdb.server.database.proto.FlatProperty;
import caosdb.server.datatype.AbstractCollectionDatatype;
import caosdb.server.datatype.GenericValue;
import caosdb.server.entity.Entity;
import caosdb.server.entity.EntityInterface;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.subject.Subject;
import org.jdom2.Element;

public class Property extends EntityWrapper {

  public Property(final Integer id) {
    super(new Entity(id));
  }

  public Property(final Element e) {
    super(new Entity(e));
  }

  public Property(final EntityInterface prop) {
    super(prop);
  }

  public Property() {
    super(new Entity());
  }

  public int getPIdx() {
    return this.pIdx;
  }

  private int pIdx = 0;
  private EntityInterface domain = null;
  private boolean isName;

  public void setPIdx(final int i) {
    this.pIdx = i;
  }

  public Property setDomain(final EntityInterface domain) {
    this.domain = domain;
    return this;
  }

  @Override
  public Integer getDomain() {
    if (this.domain != null) {
      return this.domain.getId();
    }
    return null;
  }

  @Override
  public Property linkIdToEntity(final EntityInterface link) {
    this.entity.linkIdToEntity(link);
    return this;
  }

  public Property parseFlatProperty(final FlatProperty fp) {
    setId(fp.id);
    setStatementStatus(fp.status);
    setPIdx(fp.idx);
    if (fp.name != null) {
      setName(fp.name);
      setNameOverride(true);
    }
    if (fp.desc != null) {
      setDescription(fp.desc);
      setDescOverride(true);
    }
    if (fp.type != null) {
      if (fp.collection != null) {
        this.setDatatype(
            AbstractCollectionDatatype.collectionDatatypeFactory(fp.collection, fp.type));
      } else {
        this.setDatatype(fp.type);
      }
      setDatatypeOverride(true);
    }
    if (fp.value != null) {
      setValue(new GenericValue(fp.value));
    }
    return this;
  }

  @Override
  public void checkPermission(final Permission permission) {
    throw new AuthorizationException("This code should never be reached");
  }

  @Override
  public void checkPermission(final Subject subject, final Permission permission) {
    throw new AuthorizationException("This code should never be reached");
  }

  @Override
  public String toString() {
    return "IMPLPROPERTY " + this.entity.toString();
  }

  public void setIsName(final boolean b) {
    this.isName = b;
  }

  public boolean isName() {
    return this.isName;
  }

  public EntityInterface getDomainEntity() {
    return this.domain;
  }
}
