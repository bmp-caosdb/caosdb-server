/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.entity.wrapper;

import caosdb.server.datatype.AbstractDatatype;
import caosdb.server.datatype.Value;
import caosdb.server.entity.Entity;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.Role;
import caosdb.server.entity.container.PropertyContainer;

public class Domain extends Entity {

  private boolean descO;

  public Domain(
      final PropertyContainer properties,
      final AbstractDatatype datatype,
      final Value value,
      final caosdb.server.entity.StatementStatus statementStatus) {

    setRole(Role.Domain);
    setProperties(properties);
    setDatatype(datatype);
    setValue(value);
    setStatementStatus(statementStatus);
  }

  @Override
  public EntityInterface setDescOverride(final boolean b) {
    this.descO = b;
    return this;
  }

  @Override
  public boolean isDescOverride() {
    return this.descO;
  }
}
