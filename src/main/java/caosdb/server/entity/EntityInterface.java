/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.entity;

import caosdb.server.database.proto.SparseEntity;
import caosdb.server.database.proto.VerySparseEntity;
import caosdb.server.datatype.AbstractDatatype;
import caosdb.server.datatype.Value;
import caosdb.server.entity.container.ParentContainer;
import caosdb.server.entity.container.PropertyContainer;
import caosdb.server.entity.wrapper.Domain;
import caosdb.server.entity.wrapper.Parent;
import caosdb.server.entity.wrapper.Property;
import caosdb.server.entity.xml.ToElementable;
import caosdb.server.jobs.JobTarget;
import caosdb.server.permissions.EntityACL;
import caosdb.server.utils.Observable;
import caosdb.server.utils.TransactionLogMessage;
import caosdb.unit.Unit;
import java.util.List;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.subject.Subject;

public interface EntityInterface
    extends JobTarget, Observable, ToElementable, WriteEntity, TransactionEntity {

  public abstract VerySparseEntity getVerySparseEntity();

  public abstract SparseEntity getSparseEntity();

  public abstract Integer getId();

  public abstract void setId(Integer id);

  public abstract boolean hasId();

  public abstract StatementStatus getStatementStatus();

  public abstract void setStatementStatus(StatementStatus statementStatus);

  public abstract void setStatementStatus(String statementStatus);

  public abstract boolean hasStatementStatus();

  public abstract void addProperty(Property property);

  public abstract boolean hasProperties();

  public abstract PropertyContainer getProperties();

  public abstract void addParent(Parent parent);

  public abstract boolean hasParents();

  public abstract ParentContainer getParents();

  public abstract void setName(String name);

  public abstract String getName();

  public abstract boolean hasName();

  public abstract String getDescription();

  public abstract void setDescription(String description);

  public abstract boolean hasDescription();

  public abstract Role getRole();

  public abstract void setRole(Role role);

  public abstract void setRole(String role) throws IllegalArgumentException;

  public abstract boolean hasRole();

  public abstract Value getValue();

  public abstract void setValue(Value value);

  public abstract boolean hasValue();

  public abstract AbstractDatatype getDatatype();

  public abstract void setDatatype(AbstractDatatype datatype);

  public abstract void setDatatype(String datatype);

  public abstract boolean hasDatatype();

  public abstract void print();

  public abstract void print(String indent);

  public abstract FileProperties getFileProperties();

  public abstract void setFileProperties(FileProperties fileProperties);

  public abstract boolean hasFileProperties();

  public abstract void parseValue() throws Message;

  @Override
  public abstract String toString();

  public abstract void addProperty(List<Property> properties);

  public abstract void setProperties(PropertyContainer properties);

  public abstract void setReplacement(Domain d);

  public abstract boolean hasReplacement();

  public abstract Domain getReplacement();

  public abstract EntityInterface setDescOverride(boolean b);

  public abstract boolean isDescOverride();

  public abstract EntityInterface setNameOverride(boolean b);

  public abstract boolean isNameOverride();

  public abstract EntityInterface setDatatypeOverride(boolean b);

  public abstract boolean isDatatypeOverride();

  public abstract void addTransactionLog(TransactionLogMessage transactionLogMessage);

  public abstract List<TransactionLogMessage> getTransactionLogMessages();

  public abstract boolean hasTransactionLogMessages();

  public abstract void setUnit(Unit unit);

  public abstract Unit getUnit();

  public abstract boolean hasUnit();

  public abstract Integer getDomain();

  public abstract EntityInterface parseSparseEntity(SparseEntity spe);

  public abstract EntityInterface linkIdToEntity(EntityInterface link);

  public abstract EntityACL getEntityACL();

  public abstract void setEntityACL(EntityACL acl);

  public abstract void checkPermission(Subject subject, Permission permission);

  public abstract void checkPermission(Permission permission);

  public abstract boolean hasEntityACL();

  public abstract String getQueryTemplateDefinition();

  public abstract void setQueryTemplateDefinition(String query);
}
