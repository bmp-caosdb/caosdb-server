/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.entity.container;

import caosdb.server.entity.Entity;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.wrapper.Property;
import caosdb.server.entity.xml.PropertyToElementStrategy;
import caosdb.server.entity.xml.SetFieldStrategy;
import caosdb.server.entity.xml.ToElementStrategy;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.Observable;
import caosdb.server.utils.Observer;
import caosdb.server.utils.ServerMessages;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import org.jdom2.Element;

public class PropertyContainer extends Container<Property> {

  private final ToElementStrategy s;

  private static final long serialVersionUID = 1L;

  private final EntityInterface domain;

  public PropertyContainer(final EntityInterface domain) {
    this.domain = domain;
    this.s = new PropertyToElementStrategy();
  }

  public void sort() {
    Collections.sort(
        this,
        new Comparator<EntityInterface>() {
          @Override
          public int compare(final EntityInterface o1, final EntityInterface o2) {
            if (o1 instanceof Property && o2 instanceof Property) {
              return ((Property) o1).getPIdx() - ((Property) o2).getPIdx();
            }
            return 0;
          }
        });
  }

  public Element addToElement(final Element element, final SetFieldStrategy setFieldStrategy) {
    sort();
    for (final EntityInterface property : this) {
      if (setFieldStrategy.isToBeSet(property.getName())) {
        this.s.addToElement(property, element, setFieldStrategy.forProperty(property.getName()));
      }
    }
    return element;
  }

  @Override
  public boolean addAll(final Collection<? extends Property> c) {
    for (final Property p : c) {
      addStatusObserver(this.domain, p);
      p.setDomain(this.domain);
    }
    return super.addAll(c);
  }

  @Override
  public boolean add(final Property p) {
    p.setDomain(this.domain);
    addStatusObserver(this.domain, p);
    return super.add(p);
  }

  private static boolean doCheck(final EntityInterface entity, final Property prop) {
    if (entity.getEntityStatus() == EntityStatus.UNQUALIFIED) {
      return false;
    }
    switch (prop.getEntityStatus()) {
      case UNQUALIFIED:
      case DELETED:
      case NONEXISTENT:
        entity.addError(ServerMessages.ENTITY_HAS_UNQUALIFIED_PROPERTIES);
        entity.setEntityStatus(EntityStatus.UNQUALIFIED);
        return false;
      case VALID:
        return false;
      default:
        return true;
    }
  }

  private static void addStatusObserver(final EntityInterface entity, final Property property) {
    if (doCheck(entity, property)) {
      property.acceptObserver(
          new Observer() {

            @Override
            public boolean notifyObserver(final String e, final Observable sender) {
              if (e == Entity.ENTITY_STATUS_CHANGED_EVENT) {
                if (entity.getEntityStatus() == EntityStatus.QUALIFIED) {
                  return doCheck(entity, property);
                }
              }
              return true;
            }
          });
    }
  }
}
