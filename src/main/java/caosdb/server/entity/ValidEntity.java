/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.entity;

import caosdb.server.database.proto.SparseEntity;
import caosdb.server.datatype.AbstractCollectionDatatype;

public class ValidEntity extends Entity {

  public ValidEntity(final int id) {
    super(id);
  }

  public ValidEntity parseSparseEntity(final SparseEntity spe) {
    setId(spe.id);
    this.setRole(spe.role);
    setEntityACL(spe.acl);

    if (!isNameOverride()) {
      setName(spe.name);
    }
    if (!isDescOverride()) {
      setDescription(spe.description);
    }
    if (!isDatatypeOverride()) {
      final String dt = spe.datatype;
      final String col = spe.collection;

      if (dt != null
          && !dt.equalsIgnoreCase("null")
          && (!hasDatatype() || !dt.equalsIgnoreCase(getDatatype().toString()))) {
        if (col != null && !col.equalsIgnoreCase("null")) {
          this.setDatatype(AbstractCollectionDatatype.collectionDatatypeFactory(col, dt));
        } else {
          this.setDatatype(dt);
        }
      }
    }

    if (spe.filePath != null) {
      setFileProperties(new FileProperties(spe.fileHash, spe.filePath, spe.fileSize));
    } else {
      setFileProperties(null);
    }

    return this;
  }
}
