/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.entity;

import caosdb.server.CaosDBException;
import caosdb.server.database.proto.SparseEntity;
import caosdb.server.database.proto.VerySparseEntity;
import caosdb.server.datatype.AbstractCollectionDatatype;
import caosdb.server.datatype.AbstractDatatype;
import caosdb.server.datatype.CollectionValue;
import caosdb.server.datatype.GenericValue;
import caosdb.server.datatype.Value;
import caosdb.server.entity.Message.MessageType;
import caosdb.server.entity.container.ParentContainer;
import caosdb.server.entity.container.PropertyContainer;
import caosdb.server.entity.wrapper.Domain;
import caosdb.server.entity.wrapper.Parent;
import caosdb.server.entity.wrapper.Property;
import caosdb.server.entity.xml.EntityToElementStrategy;
import caosdb.server.entity.xml.SetFieldStrategy;
import caosdb.server.entity.xml.ToElementStrategy;
import caosdb.server.entity.xml.ToElementable;
import caosdb.server.permissions.EntityACL;
import caosdb.server.query.Query.Selection;
import caosdb.server.utils.AbstractObservable;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;
import caosdb.server.utils.TransactionLogMessage;
import caosdb.unit.Unit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.subject.Subject;
import org.jdom2.Element;

public class Entity extends AbstractObservable implements EntityInterface {

  public static final String DATATYPE_CHANGED_EVENT = "DatatypeChangedEvent";
  public static final String ENTITY_STATUS_CHANGED_EVENT = "EntityStatusChangedEvent";
  private Unit unit = null;
  private Role role = null;
  private EntityStatus entityStatus = null;
  private FileProperties fileProperties = null;
  private String name = null;
  private String description = null;
  private Value value = null;
  private final IntegerWrapper id = new IntegerWrapper();
  private AbstractDatatype datatype = null;
  private final ParentContainer parents = new ParentContainer(this);
  private final PropertyContainer properties = new PropertyContainer(this);
  private final List<TransactionLogMessage> history = new ArrayList<>();
  private List<Selection> selections = null;
  private EntityACL entityACL = null;

  @Override
  public boolean hasEntityACL() {
    return this.entityACL != null;
  }

  @Override
  public EntityACL getEntityACL() {
    return this.entityACL;
  }

  @Override
  public void setEntityACL(final EntityACL acl) {
    this.entityACL = acl;
  }

  public void setEntityACL(final String acl) {
    setEntityACL(EntityACL.deserialize(acl));
  }

  @Override
  public void checkPermission(final Subject subject, final Permission permission) {
    try {
      if (!this.entityACL.isPermitted(subject, permission)) {
        throw new AuthorizationException(
            subject.getPrincipal().toString()
                + " doesn't have permission "
                + permission.toString());
      }
    } catch (final NullPointerException e) {
      throw new AuthorizationException("This entity doesn't have an ACL!");
    }
  }

  @Override
  public void checkPermission(final Permission permission) {
    final Subject subject = SecurityUtils.getSubject();
    checkPermission(subject, permission);
  }

  @Override
  public final boolean hasProperties() {
    return !this.properties.isEmpty();
  }

  @Override
  public final PropertyContainer getProperties() {
    return this.properties;
  }

  @Override
  public void addProperty(final Property property) {
    this.properties.add(property);
  }

  @Override
  public void addProperty(final List<Property> properties) {
    this.properties.addAll(properties);
  }

  @Override
  public void setProperties(final PropertyContainer properties) {
    this.properties.addAll(properties);
  }

  @Override
  public final ParentContainer getParents() {
    return this.parents;
  }

  @Override
  public final boolean hasParents() {
    return !this.parents.isEmpty();
  }

  @Override
  public void addParent(final Parent parent) {
    this.parents.add(parent);
  }

  @Override
  public final AbstractDatatype getDatatype() {
    return this.datatype;
  }

  @Override
  public final void setDatatype(final AbstractDatatype datatype) {
    this.datatype = datatype;
    notifyObservers(DATATYPE_CHANGED_EVENT);
  }

  @Override
  public void setDatatype(final String datatype) {
    if (datatype == null) {
      setDatatype((AbstractDatatype) null);
    } else {
      setDatatype(AbstractDatatype.datatypeFactory(datatype));
    }
    notifyObservers(DATATYPE_CHANGED_EVENT);
  }

  @Override
  public boolean hasDatatype() {
    return this.datatype != null;
  }

  private class IntegerWrapper {
    Integer i = null;
    EntityInterface link = null;

    void setId(final Integer i) {
      this.i = i;
    }

    public Integer toInteger() {
      if (this.link != null) {
        return this.link.getId();
      }
      return this.i;
    }

    @Override
    public String toString() {
      return toInteger().toString();
    }

    boolean hasId() {
      return this.i != null || (this.link != null && this.link.hasId());
    }
  }

  @Override
  public final Integer getId() {
    return this.id.toInteger();
  }

  @Override
  public final void setId(final Integer id) {
    this.id.setId(id);
  }

  @Override
  public final boolean hasId() {
    return this.id.hasId();
  }

  @Override
  public EntityInterface linkIdToEntity(final EntityInterface link) {
    this.id.link = link;
    return this;
  }

  @Override
  public final Value getValue() {
    return this.value;
  }

  @Override
  public final void setValue(final Value value) {
    this.value = value;
  }

  @Override
  public final boolean hasValue() {
    return this.value != null;
  }

  @Override
  public final String getDescription() {
    return this.description;
  }

  @Override
  public final void setDescription(final String description) {
    this.description = description;
  }

  @Override
  public final boolean hasDescription() {
    return !(this.description == null || this.description.isEmpty() || this.description.equals(""));
  }

  @Override
  public final void setName(final String name) {
    this.name = name;
  }

  @Override
  public final String getName() {
    return this.name;
  }

  @Override
  public final boolean hasName() {
    if (this.name == null || this.name.isEmpty() || this.name.equals("")) {
      return false;
    }
    return true;
  }

  @Override
  public final FileProperties getFileProperties() {
    return this.fileProperties;
  }

  @Override
  public final void setFileProperties(final FileProperties fileProperties) {
    this.fileProperties = fileProperties;
  }

  @Override
  public final boolean hasFileProperties() {
    return this.fileProperties != null;
  }

  @Override
  public final EntityStatus getEntityStatus() {
    return this.entityStatus;
  }

  @Override
  public final void setEntityStatus(final EntityStatus entityStatus) {
    if (hasEntityStatus() && getEntityStatus() == entityStatus) {
      return;
    }
    if (this.entityStatus == EntityStatus.UNQUALIFIED) {
      throw new CaosDBException(
          "It is not allowed to change the state again, once an UNQUALIFIED state has been reached.");
    }
    this.entityStatus = entityStatus;
    notifyObservers(ENTITY_STATUS_CHANGED_EVENT);
  }

  @Override
  public final boolean hasEntityStatus() {
    return this.entityStatus != null;
  }

  @Override
  public final Role getRole() {
    return this.role;
  }

  @Override
  public final void setRole(final Role role) {
    this.role = role;
  }

  @Override
  public final void setRole(final String role) {
    if (role != null) {
      this.role = Role.parse(role);
    } else {
      this.role = null;
    }
  }

  @Override
  public final boolean hasRole() {
    return this.role != null;
  }

  @Override
  public void setUnit(final Unit unit) {
    this.unit = unit;
  }

  @Override
  public boolean hasUnit() {
    return this.unit != null;
  }

  @Override
  public Unit getUnit() {
    return this.unit;
  }

  @Override
  public VerySparseEntity getVerySparseEntity() {
    return getSparseEntity();
  }

  @Override
  public SparseEntity getSparseEntity() {
    final SparseEntity ret = new SparseEntity();
    ret.id = getId();
    ret.name = getName();
    ret.description = getDescription();
    ret.acl = getEntityACL().serialize();
    if (hasRole()) {
      ret.role = getRole().toString();
    }
    if (hasDatatype()) {
      if (getDatatype() instanceof AbstractCollectionDatatype) {
        ret.collection = ((AbstractCollectionDatatype) getDatatype()).getCollectionName();
        ret.datatype = ((AbstractCollectionDatatype) getDatatype()).getDatatype().getName();
      } else {
        ret.datatype = getDatatype().getName();
      }
    }
    if (hasFileProperties()) {
      ret.fileHash = getFileProperties().getChecksum();
      ret.filePath = getFileProperties().getPath();
      ret.fileSize = getFileProperties().getSize();
    }
    return ret;
  }

  public Entity() {
    setEntityStatus(EntityStatus.QUALIFIED);
  }

  public Entity(final Integer id) {
    this();
    setId(id);
  }

  public Entity(final Element e) {
    try {
      setRole(e.getName().toUpperCase());
      parseFromElement(e);
    } catch (final NoSuchRoleException exc) {
      parseFromElement(e);
      setEntityStatus(EntityStatus.UNQUALIFIED);
      addError(ServerMessages.NO_SUCH_ENTITY_ROLE(e.getName()));
    }
  }

  public Entity(final Integer id, final Role role) {
    this(id);
    if (role != null) {
      setRole(role);
      setToElementStragegy(role.getToElementStrategy());
    }
  }

  public Entity(final String name, final Role role) {
    this(name);
    if (role != null) {
      setRole(role);
      setToElementStragegy(role.getToElementStrategy());
    }
  }

  public Entity(final Element e, final Role role) throws Exception {
    parseFromElement(e);
    if (role != null) {
      setRole(role);
      setToElementStragegy(role.getToElementStrategy());
    }
  }

  public Entity(final String name) {
    this();
    setName(name);
  }

  private StatementStatus statementStatus = null;

  /**
   * statementStatus getter.
   *
   * @return
   */
  @Override
  public StatementStatus getStatementStatus() {
    return this.statementStatus;
  }

  /**
   * statementStatus setter.
   *
   * @return
   */
  @Override
  public void setStatementStatus(final StatementStatus statementStatus) {
    this.statementStatus = statementStatus;
  }

  @Override
  public void setStatementStatus(final String statementStatus) {
    this.statementStatus = StatementStatus.valueOf(statementStatus.toUpperCase());
  }

  /**
   * statementStatus hasser.
   *
   * @return true id statementStatus!= null, false otherwise.
   */
  @Override
  public final boolean hasStatementStatus() {
    return this.statementStatus != null;
  }

  /**
   * cuid - a client unique identifier which is specified by the client. It MUST NOT be changed on
   * server-side. Thus, it can be used by the client to identify objects uniquely and quickly.
   */
  private String cuid = null;

  /**
   * cuid hasser. Does this wrapped entity have a cuid?
   *
   * @return true if cuid != null, false otherwise.
   */
  @Override
  public final boolean hasCuid() {
    return this.cuid != null;
  }

  /**
   * cuid getter.
   *
   * @return cuid.
   */
  @Override
  public final String getCuid() {
    return this.cuid;
  }

  /**
   * cuid setter.
   *
   * @param cuid
   * @throws CaosDBException if one tried to set it once again.
   */
  @Override
  public final void setCuid(final String cuid) {
    if (!hasCuid()) {
      this.cuid = cuid;
    }
  }

  private ToElementStrategy toElementStrategy = null;

  @Override
  public void setToElementStragegy(final ToElementStrategy s) {
    this.toElementStrategy = s;
  }

  @Override
  public final Element toElement() {
    return getToElementStrategy().toElement(this, new SetFieldStrategy(getSelections()));
  }

  @Override
  public final void addToElement(final Element element) {
    getToElementStrategy().addToElement(this, element, new SetFieldStrategy(getSelections()));
  }

  /**
   * Print this entity to the standard outputs. Just for debugging.
   *
   * @throws CaosDBException
   */
  @Override
  public void print() {
    print("");
  }

  @Override
  public Integer getDomain() {
    return 0;
  }

  @Override
  public void print(final String indent) {
    System.out.println(
        indent
            + "+---| "
            + this.getClass().getSimpleName()
            + " |----------------------------------");
    if (getDomain() != 0) {
      System.out.println(indent + "|      Domain: " + Integer.toString(getDomain()));
    }
    if (hasId()) {
      System.out.println(indent + "|          ID: " + Integer.toString(getId()));
    }
    if (hasCuid()) {
      System.out.println(indent + "|        Cuid: " + getCuid());
    }
    if (hasName()) {
      System.out.println(indent + "|        Name: " + getName());
    }
    if (hasDescription()) {
      System.out.println(indent + "| Description: " + getDescription());
    }
    if (hasRole()) {
      System.out.println(indent + "|        Role: " + getRole());
    }
    if (hasStatementStatus()) {
      System.out.println(indent + "|   Statement: " + getStatementStatus().toString());
    }
    if (hasDatatype()) {
      System.out.println(indent + "|    Datatype: " + getDatatype().toString());
    }
    if (hasValue()) {
      System.out.println(indent + "|       Value: " + getValue().toString());
    }
    if (hasEntityStatus()) {
      System.out.println(indent + "|      Entity: " + getEntityStatus().toString());
    }
    if (hasFileProperties()) {
      getFileProperties().print(indent);
    }
    System.out.println(indent + "+-----------------------------------");
    for (final ToElementable m : getMessages()) {
      if (m instanceof Message) {
        ((Message) m).print(indent + "|   ");
      }
    }
    for (final EntityInterface p : getParents()) {
      // p.print(indent + "| ");
      System.out.println(indent + "|      Parent: " + p.getName());
    }
    for (final EntityInterface s : getProperties()) {
      s.print(indent + "|   ");
    }
    if (indent.equals("")) {
      System.out.println(indent + "+------------------------------------");
    }
  }

  /** Errors, Warnings and Info messages for this entity. */
  private final Set<ToElementable> messages = new HashSet<>();

  @Override
  public final Set<ToElementable> getMessages() {
    return this.messages;
  }

  @Override
  public boolean hasMessages() {
    return !this.messages.isEmpty();
  }

  @Override
  public final boolean hasMessage(final String type) {
    for (final ToElementable m : this.messages) {
      if (m instanceof Message && ((Message) m).getType().equalsIgnoreCase(type)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void removeMessage(final Message m) {
    this.messages.remove(m);
  }

  @Override
  public final List<Message> getMessages(final String type) {
    final LinkedList<Message> ret = new LinkedList<>();
    for (final ToElementable m : this.messages) {
      if (m instanceof Message && ((Message) m).getType().equalsIgnoreCase(type)) {
        ret.add((Message) m);
      }
    }
    return ret;
  }

  @Override
  public final Message getMessage(final String type, final Integer code) {
    for (final ToElementable m : this.messages) {
      if (m instanceof Message
          && ((Message) m).getType().equalsIgnoreCase(type)
          && ((Message) m).getCode() == code) {
        return (Message) m;
      }
    }
    return null;
  }

  @Override
  public final void addMessage(final ToElementable m) {
    this.messages.add(m);
  }

  @Override
  public final void addError(final Message m) {
    setEntityStatus(EntityStatus.UNQUALIFIED);
    if (m.getType().equalsIgnoreCase(MessageType.Error.toString())) {
      addMessage(m);
    } else {
      addMessage(new Message(MessageType.Error, m.getCode(), m.getDescription(), m.getBody()));
    }
  }

  @Override
  public void addInfo(final String description) {
    final Message m = new Message(MessageType.Info, 0, description);
    addMessage(m);
  }

  @Override
  public void addInfo(final Message m) {
    if (m.getType().equalsIgnoreCase(MessageType.Info.toString())) {
      addMessage(m);
    } else {
      addMessage(new Message(MessageType.Info, m.getCode(), m.getDescription(), m.getBody()));
    }
  }

  boolean isParsed = false;
  private String queryTemplateDefinition = null;

  @Override
  public void parseValue() throws Message {
    if (!this.isParsed) {
      this.isParsed = true;
      setValue(getDatatype().parseValue(getValue()));
      this.isParsed = true;
    }
  }

  @Override
  public void setQueryTemplateDefinition(final String query) {
    this.queryTemplateDefinition = query;
  }

  @Override
  public String getQueryTemplateDefinition() {
    return this.queryTemplateDefinition;
  }

  @Override
  public void parseFromElement(final Element element) {

    // Check if this element has any attributes at all.
    if (!element.hasAttributes() && element.getChildren().isEmpty()) {
      setEntityStatus(EntityStatus.IGNORE);
      addError(ServerMessages.ENTITY_IS_EMPTY);
      return;
    }

    // This should be the status after parsing. It will be redefined if
    // any error occurs.
    setEntityStatus(EntityStatus.QUALIFIED);

    // Parse CUID.
    if (element.getAttribute("cuid") != null) {
      setCuid(element.getAttributeValue("cuid"));
    }

    // Parse ID. Generate error if it isn't an integer.
    if (element.getAttribute("id") != null && !element.getAttributeValue("id").equals("")) {
      try {
        setId(Integer.parseInt(element.getAttributeValue("id")));
      } catch (final NumberFormatException e) {
        addInfo("Id was " + element.getAttributeValue("id") + ".");
        addError(ServerMessages.PARSING_FAILED);
        setEntityStatus(EntityStatus.UNQUALIFIED);
      }
    }

    // Parse NAME.
    if (element.getAttribute("name") != null && !element.getAttributeValue("name").equals("")) {
      setName(element.getAttributeValue("name"));
    }

    // Parse DESCRIPTION.
    if (element.getAttribute("description") != null
        && !element.getAttributeValue("description").equals("")) {
      setDescription(element.getAttributeValue("description"));
    }

    // Parse DATA TYPE. Generate error if it doesn't fit the Datatype enum.
    if (element.getAttribute("datatype") != null
        && !element.getAttributeValue("datatype").equals("")) {
      try {
        this.setDatatype(element.getAttributeValue("datatype"));
      } catch (final IllegalArgumentException e) {
        addError(ServerMessages.UNKNOWN_DATATYPE);
      }
    }

    // Parse UNIT.
    if (element.getAttribute("unit") != null && !element.getAttributeValue("unit").equals("")) {
      final EntityInterface magicUnit = MagicTypes.UNIT.getEntity();
      final Property unit = new Property();
      unit.setDescription(magicUnit.getDescription());
      unit.setName(magicUnit.getName());
      unit.setId(magicUnit.getId());
      unit.setDatatype(magicUnit.getDatatype());
      unit.setStatementStatus(StatementStatus.FIX);
      unit.setValue(new GenericValue(element.getAttribute("unit").getValue()));
      unit.setEntityStatus(EntityStatus.QUALIFIED);
      addProperty(unit);
    }

    // parse IMPORTANCE. Generate error if it doesn't fit the importance
    // Enum.
    if (element.getAttribute("importance") != null
        && !element.getAttributeValue("importance").equals("")) {
      try {
        setStatementStatus(element.getAttributeValue("importance"));
      } catch (final IllegalArgumentException e) {
        addError(ServerMessages.UNKNOWN_IMPORTANCE);
        setEntityStatus(EntityStatus.UNQUALIFIED);
      }
    }

    final CollectionValue vals = new CollectionValue();
    int pidx = 0;
    for (final Element pe : element.getChildren()) {
      if (pe.getName().equalsIgnoreCase("Value")) {
        // Parse sub-elements which represent VALUES of this entity.

        if (pe.getText() != null && pe.getTextTrim() != "") {
          vals.add(new GenericValue(pe.getTextTrim()));
        } else {
          vals.add(null);
        }
      } else if (pe.getName().equalsIgnoreCase("Property")) {
        // Parse sub elements which represent PROPERTIES of this
        // record.

        final Property property = new Property(pe);
        property.setPIdx(pidx++);
        addProperty(property);
      } else if (pe.getName().equalsIgnoreCase("Parent")) {
        // Parse sub elements which represent PARENTS of this
        // record.
        final Parent parent = new Parent(pe);
        addParent(parent);
      } else if (pe.getName().equalsIgnoreCase("EntityACL")) {
        // Parse and concatenate EntityACL
        try {
          final EntityACL newACL = EntityACL.parseFromElement(pe);
          if (hasEntityACL()) {
            setEntityACL(EntityACL.combine(getEntityACL(), newACL));
          } else {
            this.setEntityACL(newACL);
          }
        } catch (final IllegalArgumentException exc) {
          setEntityStatus(EntityStatus.UNQUALIFIED);
          addError(ServerMessages.CANNOT_PARSE_ENTITY_ACL);
        }
      } else if (getRole() == Role.QueryTemplate && pe.getName().equalsIgnoreCase("Query")) {
        setQueryTemplateDefinition(pe.getTextNormalize());
      } else {
        final String type = pe.getName();
        Integer code = null;
        String localDescription = null;
        String body = null;

        // Parse MESSAGE CODE.
        if (pe.getAttribute("code") != null && !pe.getAttributeValue("code").equals("")) {
          try {
            code = Integer.parseInt(pe.getAttributeValue("code"));
          } catch (final NumberFormatException e) {
            addInfo("Message code was " + pe.getAttributeValue("code") + ".");
            addError(ServerMessages.PARSING_FAILED);
            setEntityStatus(EntityStatus.UNQUALIFIED);
          }
        }

        // Parse MESSAGE DESCRIPTION.
        if (pe.getAttribute("description") != null
            && !pe.getAttributeValue("description").equals("")) {
          localDescription = pe.getAttributeValue("description");
        }

        // Parse MESSAGE BODY.
        if (pe.getTextTrim() != null && !pe.getTextTrim().equals("")) {
          body = pe.getTextTrim();
        }

        addMessage(new Message(type, code, localDescription, body));
      }
    }

    // Parse VALUE.
    if (vals.size() != 0) {
      setValue(vals);
    } else if (element.getTextTrim() != null && !element.getTextTrim().equals("")) {
      setValue(new GenericValue(element.getTextTrim()));
    }

    // Parse PATH.
    String path = null;
    if (element.getAttribute("path") != null && !element.getAttributeValue("path").equals("")) {
      path = element.getAttributeValue("path");
    }
    if (element.getAttribute("destination") != null
        && !element.getAttributeValue("destination").equals("")) {
      path = element.getAttributeValue("destination");
    }

    // Parse CHECKSUM
    String checksum = null;
    if (element.getAttribute("checksum") != null
        && !element.getAttributeValue("checksum").equals("")) {
      checksum = element.getAttributeValue("checksum");
    }

    // Parse SIZE
    Long size = null;
    if (element.getAttribute("size") != null && !element.getAttributeValue("size").equals("")) {
      size = Long.parseLong(element.getAttributeValue("size"));
    }

    // Parse TMPIDENTIFYER.
    String tmpIdentifyer = null;
    boolean pickup = false;
    if (element.getAttribute("pickup") != null && !element.getAttributeValue("pickup").equals("")) {
      tmpIdentifyer = element.getAttributeValue("pickup");
      pickup = true;
    } else if (element.getAttribute("upload") != null
        && !element.getAttributeValue("upload").equals("")) {
      tmpIdentifyer = element.getAttributeValue("upload");
    }
    if (tmpIdentifyer != null && tmpIdentifyer.endsWith("/")) {
      tmpIdentifyer = tmpIdentifyer.substring(0, tmpIdentifyer.length() - 1);
    }

    // Store PATH, HASH, SIZE, TMPIDENTIFYER
    if (tmpIdentifyer != null || checksum != null || path != null || size != null) {
      setFileProperties(
          new FileProperties(checksum, path, size, tmpIdentifyer).setPickupable(pickup));
    }

    // Parse flags
    if (element.getAttribute("flag") != null && !element.getAttributeValue("flag").equals("")) {
      if (element.getAttributeValue("flag").contains(",")) {
        final String[] flags = element.getAttributeValue("flag").split(",");
        for (final String f : flags) {
          if (f.length() > 0) {
            if (f.contains(":")) {
              final String[] kv = f.split(":", 2);
              this.flags.put(kv[0].trim(), kv[1].trim());
            } else {
              this.flags.put(f.trim(), null);
            }
          }
        }
      } else {
        final String f = element.getAttributeValue("flag");
        if (f.contains(":")) {
          final String[] kv = f.split(":", 2);
          this.flags.put(kv[0].trim(), kv[1].trim());
        } else {
          this.flags.put(f.trim(), null);
        }
      }
    }
  }

  @Override
  public String toString() {
    return (hasId() ? "(" + getId().toString() + ")" : "()")
        + (hasCuid() ? "[" + getCuid() + "]" : "[]")
        + (hasName() ? "(" + getName() + ")" : "()")
        + (hasDatatype() ? "{" + getDatatype().toString() + "}" : "{}");
  }

  @Override
  public void addWarning(final Message m) {
    if (m.getType().equalsIgnoreCase(MessageType.Warning.toString())) {
      addMessage(m);
    } else {
      addMessage(new Message(MessageType.Warning, m.getCode(), m.getDescription(), m.getBody()));
    }
  }

  @Override
  public void setReplacement(final Domain d) {
    this.replacement = d;
  }

  @Override
  public boolean hasReplacement() {
    return this.replacement != null;
  }

  @Override
  public Domain getReplacement() {
    return this.replacement;
  }

  private Domain replacement = null;

  private final HashMap<String, String> flags = new HashMap<String, String>();

  @Override
  public HashMap<String, String> getFlags() {
    return this.flags;
  }

  @Override
  public void setFlag(final String key, final String value) {
    this.flags.put(key, value);
  }

  @Override
  public String getFlag(final String key) {
    return getFlags().get(key);
  }

  private boolean descOverride = false;

  @Override
  public EntityInterface setDescOverride(final boolean b) {
    this.descOverride = b;
    return this;
  }

  @Override
  public boolean isDescOverride() {
    return this.descOverride;
  }

  private boolean nameOverride = false;

  @Override
  public EntityInterface setNameOverride(final boolean b) {
    this.nameOverride = b;
    return this;
  }

  @Override
  public boolean isNameOverride() {
    return this.nameOverride;
  }

  private boolean datatypeOverride = false;

  @Override
  public EntityInterface setDatatypeOverride(final boolean b) {
    this.datatypeOverride = b;
    return this;
  }

  @Override
  public boolean isDatatypeOverride() {
    return this.datatypeOverride;
  }

  @Override
  public void addTransactionLog(final TransactionLogMessage transactionLogMessage) {
    this.history.add(transactionLogMessage);
  }

  @Override
  public List<TransactionLogMessage> getTransactionLogMessages() {
    return this.history;
  }

  @Override
  public boolean hasTransactionLogMessages() {
    return !this.history.isEmpty();
  }

  @Override
  public ToElementStrategy getToElementStrategy() {
    if (this.toElementStrategy == null) {
      if (hasRole()) {
        return getRole().getToElementStrategy();
      } else {
        return new EntityToElementStrategy("Entity");
      }
    }
    return this.toElementStrategy;
  }

  @Override
  public EntityInterface parseSparseEntity(final SparseEntity spe) {
    setId(spe.id);
    this.setRole(spe.role);
    setEntityACL(spe.acl);

    if (!isNameOverride()) {
      setName(spe.name);
    }
    if (!isDescOverride()) {
      setDescription(spe.description);
    }
    if (!isDatatypeOverride()) {
      final String dt = spe.datatype;
      final String col = spe.collection;

      if (dt != null
          && !dt.equalsIgnoreCase("null")
          && (!hasDatatype() || !dt.equalsIgnoreCase(getDatatype().toString()))) {
        if (col != null && !col.equalsIgnoreCase("null")) {
          this.setDatatype(AbstractCollectionDatatype.collectionDatatypeFactory(col, dt));
        } else {
          this.setDatatype(dt);
        }
      }
    }

    if (spe.filePath != null) {
      setFileProperties(new FileProperties(spe.fileHash, spe.filePath, spe.fileSize));
    } else {
      setFileProperties(null);
    }

    return this;
  }

  public EntityInterface addSelections(final List<Selection> selections) {
    if (this.selections == null) {
      this.selections = new LinkedList<>();
    }
    this.selections.addAll(selections);
    return this;
  }

  @Override
  public List<Selection> getSelections() {
    return this.selections;
  }

  @Override
  public boolean skipJob() {
    return false;
  }
}
