/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.entity.xml;

import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.Message;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.TransactionLogMessage;
import java.util.Comparator;
import org.apache.shiro.SecurityUtils;
import org.jdom2.Content;
import org.jdom2.Content.CType;
import org.jdom2.Element;

public class EntityToElementStrategy implements ToElementStrategy {

  private final String tagName;

  public EntityToElementStrategy(final String tagName) {
    this.tagName = tagName;
  }

  public static Element sparseEntityToElement(
      final String tagName, final EntityInterface entity, final SetFieldStrategy setFieldStrategy) {
    final Element element = new Element(tagName);

    if (entity.getEntityACL() != null) {
      element.addContent(entity.getEntityACL().getPermissionsFor(SecurityUtils.getSubject()));
    }
    if (setFieldStrategy.isToBeSet("id") && entity.hasId()) {
      element.setAttribute("id", Integer.toString(entity.getId()));
    }
    if (setFieldStrategy.isToBeSet("cuid") && entity.hasCuid()) {
      element.setAttribute("cuid", entity.getCuid());
    }
    if (setFieldStrategy.isToBeSet("name") && entity.hasName()) {
      element.setAttribute("name", entity.getName());
    }
    if (setFieldStrategy.isToBeSet("description") && entity.hasDescription()) {
      element.setAttribute("description", entity.getDescription());
    }
    if (setFieldStrategy.isToBeSet("datatype") && entity.hasDatatype()) {
      if (entity.getDatatype().getName() != null) {
        element.setAttribute("datatype", entity.getDatatype().getName());
      } else {
        element.setAttribute("datatype", entity.getDatatype().getId().toString());
      }
    }
    if (setFieldStrategy.isToBeSet("message") && entity.hasMessages()) {
      for (final ToElementable m : entity.getMessages()) {
        m.addToElement(element);
      }
    }
    if (setFieldStrategy.isToBeSet("query") && entity.getQueryTemplateDefinition() != null) {
      final Element q = new Element("Query");
      q.setText(entity.getQueryTemplateDefinition());
      element.addContent(q);
    }

    return element;
  }

  @Override
  public Element toElement(final EntityInterface entity, final SetFieldStrategy setFieldStrategy) {
    final Element element = sparseEntityToElement(this.tagName, entity, setFieldStrategy);

    if (setFieldStrategy.isToBeSet("importance") && entity.hasStatementStatus()) {
      element.setAttribute("importance", entity.getStatementStatus().toString());
    }
    if (setFieldStrategy.isToBeSet("parent") && entity.hasParents()) {
      entity.getParents().addToElement(element);
    }
    if (entity.hasProperties()) {
      entity.getProperties().addToElement(element, setFieldStrategy);
    }
    if (setFieldStrategy.isToBeSet("history") && entity.hasTransactionLogMessages()) {
      for (final TransactionLogMessage t : entity.getTransactionLogMessages()) {
        t.xmlAppendTo(element);
      }
    }
    if (setFieldStrategy.isToBeSet("value") && entity.hasValue()) {
      if (entity.hasDatatype()) {
        try {

          entity.getDatatype().parseValue(entity.getValue()).addToElement(element);
        } catch (final Message e) {
          //
        }
      } else {
        entity.getValue().addToElement(element);
      }
      // put value at first position
      element.sortContent(
          new Comparator<Content>() {

            @Override
            public int compare(final Content o1, final Content o2) {
              if (o1.getCType() == CType.CDATA || o1.getCType() == CType.Text) {
                return -1;
              }
              if (o2.getCType() == CType.CDATA || o2.getCType() == CType.Text) {
                return 1;
              }
              return 0;
            }
          });
    }
    return element;
  }

  @Override
  public Element addToElement(
      final EntityInterface entity, final Element parent, final SetFieldStrategy setFieldStrategy) {
    if (entity.getEntityStatus() != EntityStatus.IGNORE) {
      parent.addContent(toElement(entity, setFieldStrategy));
    }
    return parent;
  }
}
