/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.entity.xml;

import caosdb.server.entity.EntityInterface;
import caosdb.server.query.Query.Selection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class SetFieldStrategy {

  private final List<Selection> selections = new LinkedList<Selection>();
  private HashMap<String, Boolean> cache = null;
  private static final SetFieldStrategy defaultSelections =
      new SetFieldStrategy(null) {
        @Override
        public boolean isToBeSet(final String field) {
          return true;
        }
      };

  public SetFieldStrategy(final List<Selection> selections) {
    if (selections != null) {
      this.selections.addAll(selections);
    }
  }

  public SetFieldStrategy() {
    this(null);
  }

  public SetFieldStrategy addSelection(final Selection selection) {
    // ignore null
    if (selection == null) {
      return this;
    }

    if (this.cache != null) {
      this.cache.clear();
      this.cache = null;
    }
    this.selections.add(selection);
    return this;
  }

  public SetFieldStrategy forProperty(final EntityInterface property) {
    return forProperty(property.getName());
  }

  public SetFieldStrategy forProperty(final String name) {
    // if property is to be omitted.
    if (!isToBeSet(name)) {
      return new SetFieldStrategy() {
        @Override
        public boolean isToBeSet(final String field) {
          return false;
        }
      };
    }

    final LinkedList<Selection> subselections = new LinkedList<Selection>();
    for (final Selection s : this.selections) {
      if (s.getSelector().equalsIgnoreCase(name) && s.getSubselection() != null) {
        subselections.add(s.getSubselection());
      }
    }
    return new SetFieldStrategy(subselections);
  }

  public boolean isToBeSet(final String field) {
    // default values
    if (this.selections == null || this.selections.isEmpty()) {
      return defaultSelections.isToBeSet(field);
    }

    if (this.cache == null) {
      this.cache = new HashMap<String, Boolean>();
      // fill cache
      this.cache.put("id", true);
      for (final Selection selection : this.selections) {
        if (!selection.getSelector().equals("id")) {
          this.cache.put("name", true);
        }
        this.cache.put(selection.getSelector().toLowerCase(), true);
      }
    }

    // return from cache or false
    return (this.cache.containsKey(field.toLowerCase())
        ? this.cache.get(field.toLowerCase())
        : false);
  }
}
