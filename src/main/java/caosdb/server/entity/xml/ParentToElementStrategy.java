/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.entity.xml;

import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.wrapper.Parent;
import caosdb.server.utils.EntityStatus;
import org.jdom2.Element;

public class ParentToElementStrategy implements ToElementStrategy {

  @Override
  public Element toElement(final EntityInterface entity, final SetFieldStrategy setFieldStrategy) {
    final Element e =
        EntityToElementStrategy.sparseEntityToElement("Parent", entity, setFieldStrategy);
    final Parent parent = (Parent) entity;
    if (parent.getAffiliation() != null) {
      e.setAttribute("affiliation", parent.getAffiliation().toString());
    }
    return e;
  }

  @Override
  public Element addToElement(
      final EntityInterface entity,
      final Element element,
      final SetFieldStrategy setFieldStrategy) {
    if (entity.getEntityStatus() != EntityStatus.IGNORE) {
      element.addContent(toElement(entity, setFieldStrategy));
    }
    return element;
  }
}
