/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.transaction;

import caosdb.server.CaosDBException;
import caosdb.server.database.Database;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.transaction.RetrieveFullEntity;
import caosdb.server.database.backend.transaction.UpdateEntity;
import caosdb.server.entity.Entity;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.FileProperties;
import caosdb.server.entity.RetrieveEntity;
import caosdb.server.entity.container.TransactionContainer;
import caosdb.server.entity.container.UpdateContainer;
import caosdb.server.entity.wrapper.Parent;
import caosdb.server.entity.wrapper.Property;
import caosdb.server.permissions.EntityPermission;
import caosdb.server.permissions.Permission;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;
import com.google.common.base.Objects;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;
import org.apache.shiro.authz.AuthorizationException;

public class Update extends WriteTransaction<UpdateContainer> {

  public Update(final UpdateContainer container) {
    super(container);
  }

  @Override
  protected void preCheck() throws Exception {}

  @Override
  protected void init() throws Exception {
    // collect all ids of the entities which are to be updated.
    final TransactionContainer oldContainer = new TransactionContainer();
    for (final Entity entity : getContainer()) {
      // entity has no id -> it cannot be updated.
      if (!entity.hasId()) {
        entity.addError(ServerMessages.ENTITY_HAS_NO_ID);
        entity.setEntityStatus(EntityStatus.UNQUALIFIED);
        continue;
      }

      // add entity with this id to the updateContainer
      if (entity.getEntityStatus() == EntityStatus.QUALIFIED) {
        final Entity oldEntity = new RetrieveEntity(entity.getId());
        oldContainer.add(oldEntity);
      }
    }

    // allocate strong (write) access. Only one thread can do this
    // at a time. But weak access can still be acquired by other
    // thread until the allocated strong access is actually
    // acquired.
    setAccess(getMonitor().allocateStrongAccess(this));

    // retrieve a container which contains all id of those entities
    // which are to be updated.
    Database.execute(new RetrieveFullEntity(oldContainer), getAccess());

    // Check if any updates are to be processed.
    for (final EntityInterface newEntity : getContainer()) {
      if (newEntity.getEntityStatus() == EntityStatus.QUALIFIED) {
        innerLoop:
        for (final EntityInterface oldEntity : oldContainer) {
          if (oldEntity.getId().equals(newEntity.getId())) {
            if (oldEntity.getEntityStatus() == EntityStatus.NONEXISTENT) {
              newEntity.addError(ServerMessages.ENTITY_DOES_NOT_EXIST);
              newEntity.setEntityStatus(EntityStatus.UNQUALIFIED);
            } else {
              // dereference files (upload only)
              if (newEntity.hasFileProperties() && !newEntity.getFileProperties().isPickupable()) {

                if (newEntity.getFileProperties().getTmpIdentifyer() != null) {
                  // get file by tmpIdentifier
                  final FileProperties f =
                      getContainer()
                          .getFiles()
                          .get(newEntity.getFileProperties().getTmpIdentifyer());

                  // is it there?
                  if (f != null) {
                    newEntity.getFileProperties().setFile(f.getFile());

                    // has it a thumbnail?
                    if (f.getThumbnail() != null) {
                      newEntity.getFileProperties().setThumbnail(f.getThumbnail());
                    } else {
                      final FileProperties thumbnail =
                          getContainer()
                              .getFiles()
                              .get(newEntity.getFileProperties().getTmpIdentifyer() + ".thumbnail");
                      if (thumbnail != null) {
                        newEntity.getFileProperties().setThumbnail(thumbnail.getFile());
                      } else {
                        newEntity.addWarning(ServerMessages.THUMBNAIL_HAS_NOT_BEEN_UPLOAED);
                      }
                    }
                  } else {
                    newEntity.addError(ServerMessages.FILE_HAS_NOT_BEEN_UPLOAED);
                    newEntity.setEntityStatus(EntityStatus.UNQUALIFIED);
                  }

                } else {
                  // in case no file has been uploaded,
                  // the file is expected to stay
                  // unchanged. Therefore we let the file
                  // object point to the original file in
                  // the file system.
                  newEntity
                      .getFileProperties()
                      .setFile(oldEntity.getFileProperties().retrieveFromFileSystem());
                }
              }

              try {
                checkPermissions(newEntity, deriveUpdate(newEntity, oldEntity));
              } catch (final AuthorizationException exc) {
                newEntity.setEntityStatus(EntityStatus.UNQUALIFIED);
                newEntity.addError(ServerMessages.AUTHORIZATION_ERROR);
                newEntity.addInfo(exc.getMessage());
              }
            }
            break innerLoop;
          }
        }
      }
    }

    // make schedule of those entities which are to be updated.
    makeSchedule();
  }

  @Override
  protected void postCheck() {}

  @Override
  protected void postTransaction() {}

  @Override
  public void transaction() throws Exception {
    // write new entities to database
    update(getContainer(), getAccess());
  }

  public static void update(final TransactionContainer container, final Access access)
      throws Exception {
    if (container.getStatus().ordinal() >= EntityStatus.QUALIFIED.ordinal()) {
      Database.execute(new UpdateEntity(container), access);
    }
  }

  /** Check if the user has all permissions */
  public void checkPermissions(final EntityInterface entity, final Set<Permission> permissions) {
    for (final Permission p : permissions) {
      entity.checkPermission(p);
    }
  }

  /**
   * The entity is set to VALID iff there are no updates to be processed. The entity is set to
   * QUALIFIED otherwise.
   *
   * @param newEntity
   * @param oldEntity
   * @throws CaosDBException
   * @throws IOException
   * @throws NoSuchAlgorithmException
   */
  public static HashSet<Permission> deriveUpdate(
      final EntityInterface newEntity, final EntityInterface oldEntity)
      throws NoSuchAlgorithmException, IOException, CaosDBException {
    final HashSet<Permission> needPermissions = new HashSet<>();
    boolean updatetable = false;

    // new acl?
    if (newEntity.hasEntityACL() && !newEntity.getEntityACL().equals(oldEntity.getEntityACL())) {
      oldEntity.checkPermission(EntityPermission.EDIT_ACL);
      if (!newEntity
          .getEntityACL()
          .getPriorityEntityACL()
          .equals(oldEntity.getEntityACL().getPriorityEntityACL())) {
        // priority acl is to be changed?
        oldEntity.checkPermission(Permission.EDIT_PRIORITY_ACL);
      }
      updatetable = true;
    } else if (!newEntity.hasEntityACL()) {
      newEntity.setEntityACL(oldEntity.getEntityACL());
    }

    // new query template definition?
    if (!Objects.equal(
        newEntity.getQueryTemplateDefinition(), oldEntity.getQueryTemplateDefinition())) {
      needPermissions.add(EntityPermission.UPDATE_QUERY_TEMPLATE_DEFINITION);
      updatetable = true;
    }

    // new datatype?
    if (newEntity.hasDatatype()
            && oldEntity.hasDatatype()
            && !newEntity.getDatatype().equals(oldEntity.getDatatype())
        || newEntity.hasDatatype() ^ oldEntity.hasDatatype()) {
      needPermissions.add(EntityPermission.UPDATE_DATA_TYPE);
      updatetable = true;
    }

    // entity role
    if (newEntity.hasRole()
            && oldEntity.hasRole()
            && !newEntity.getRole().equals(oldEntity.getRole())
        || newEntity.hasRole() ^ oldEntity.hasRole()) {
      needPermissions.add(EntityPermission.UPDATE_ROLE);
      updatetable = true;
    }

    // entity value
    if (newEntity.hasValue()
            && oldEntity.hasValue()
            && !newEntity.getValue().equals(oldEntity.getValue())
        || newEntity.hasValue() ^ oldEntity.hasValue()) {
      needPermissions.add(EntityPermission.UPDATE_VALUE);
      updatetable = true;
    }

    // entity name
    if (newEntity.hasName()
            && oldEntity.hasName()
            && !newEntity.getName().equals(oldEntity.getName())
        || newEntity.hasName() ^ oldEntity.hasName()) {
      needPermissions.add(EntityPermission.UPDATE_NAME);
      updatetable = true;
    }

    // entity description
    if (newEntity.hasDescription()
            && oldEntity.hasDescription()
            && !newEntity.getDescription().equals(oldEntity.getDescription())
        || newEntity.hasDescription() ^ oldEntity.hasDescription()) {
      needPermissions.add(EntityPermission.UPDATE_DESCRIPTION);
      updatetable = true;
    }

    // file properties
    if (newEntity.hasFileProperties() || oldEntity.hasFileProperties()) {
      if (newEntity.hasFileProperties() && !oldEntity.hasFileProperties()) {
        // add a file
        needPermissions.add(EntityPermission.UPDATE_ADD_FILE);
        updatetable = true;
      } else if (!newEntity.hasFileProperties() && oldEntity.hasFileProperties()) {
        // remove a file
        needPermissions.add(EntityPermission.UPDATE_REMOVE_FILE);
        updatetable = true;
      } else {
        // change file
        final FileProperties newFile = newEntity.getFileProperties();
        final FileProperties oldFile = oldEntity.getFileProperties();

        // file path
        if (newFile.hasPath() && oldFile.hasPath() && !newFile.getPath().equals(oldFile.getPath())
            || newFile.hasPath() ^ oldFile.hasPath()) {
          // this means, the location of the file is to be changed
          needPermissions.add(EntityPermission.UPDATE_MOVE_FILE);
          updatetable = true;
        }

        // change actual file (different byte code!)
        if (!oldFile.retrieveFromFileSystem().equals(newFile.getFile())) {
          // CHANGE A FILE is like REMOVE AND ADD
          needPermissions.add(EntityPermission.UPDATE_REMOVE_FILE);
          needPermissions.add(EntityPermission.UPDATE_ADD_FILE);
          updatetable = true;
        }
      }
    }

    // properties
    outerLoop:
    for (final EntityInterface newProperty : newEntity.getProperties()) {

      // find corresponding oldProperty for this new property and make a
      // diff.
      if (newProperty.hasId()) {
        for (final EntityInterface oldProperty : oldEntity.getProperties()) {
          if (newProperty.getId().equals(oldProperty.getId())) {
            // do not check again.
            oldEntity.getProperties().remove(oldProperty);

            if (((Property) oldProperty).getPIdx() != ((Property) newProperty).getPIdx()) {
              // change order of properties
              needPermissions.add(EntityPermission.UPDATE_ADD_PROPERTY);
              needPermissions.add(EntityPermission.UPDATE_REMOVE_PROPERTY);
              updatetable = true;
            }

            deriveUpdate(newProperty, oldProperty);
            if (newProperty.getEntityStatus() == EntityStatus.QUALIFIED) {
              needPermissions.add(EntityPermission.UPDATE_ADD_PROPERTY);
              needPermissions.add(EntityPermission.UPDATE_REMOVE_PROPERTY);
              updatetable = true;
            }

            continue outerLoop;
          }
        }
      } else {
        newProperty.setEntityStatus(EntityStatus.UNQUALIFIED);
        newProperty.addError(ServerMessages.ENTITY_HAS_NO_ID);
        newProperty.addInfo("On updates, allways specify the id not just the name.");
        newEntity.addError(ServerMessages.ENTITY_HAS_UNQUALIFIED_PROPERTIES);
        newEntity.setEntityStatus(EntityStatus.UNQUALIFIED);
        return needPermissions;
      }

      // no corresponding property found -> this property is new.
      needPermissions.add(EntityPermission.UPDATE_ADD_PROPERTY);
      updatetable = true;
    }

    // some old properties left (and not matched with new ones) -> there are
    // properties to be deleted.
    if (!oldEntity.getProperties().isEmpty()) {
      needPermissions.add(EntityPermission.UPDATE_REMOVE_PROPERTY);
      updatetable = true;
    }

    // update parents
    outerLoop:
    for (final Parent newParent : newEntity.getParents()) {

      // find corresponding oldParent
      if (newParent.hasId()) {
        for (final Parent oldParent : oldEntity.getParents()) {
          if (oldParent.getId().equals(newParent.getId())) {
            // still there! do not check this one again
            oldEntity.getParents().remove(oldParent);
            continue outerLoop;
          }
        }
      } else {
        newParent.setEntityStatus(EntityStatus.UNQUALIFIED);
        newParent.addError(ServerMessages.ENTITY_HAS_NO_ID);
        newParent.addInfo("On updates, allways specify the id not just the name.");
        newEntity.addError(ServerMessages.ENTITY_HAS_UNQUALIFIED_PROPERTIES);
        newEntity.setEntityStatus(EntityStatus.UNQUALIFIED);
        return needPermissions;
      }

      // no corresponding parent found -> this parent is new.
      needPermissions.add(EntityPermission.UPDATE_ADD_PARENT);
      updatetable = true;
    }

    // some old parents left (and not matched with new ones) -> there are
    // parents to be deleted.
    if (!oldEntity.getParents().isEmpty()) {
      needPermissions.add(EntityPermission.UPDATE_REMOVE_PARENT);
      updatetable = true;
    }

    // nothing to be updated
    if (!updatetable) {
      newEntity.setEntityStatus(EntityStatus.VALID);
      newEntity.addInfo("Nothing to be updated.");
    }

    return needPermissions;
  }

  @Override
  public boolean logHistory() {
    return true;
  }
}
