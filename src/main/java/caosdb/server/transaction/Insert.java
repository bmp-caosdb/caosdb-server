/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.transaction;

import caosdb.server.database.Database;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.transaction.InsertEntity;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.FileProperties;
import caosdb.server.entity.container.InsertContainer;
import caosdb.server.entity.container.TransactionContainer;
import caosdb.server.permissions.EntityACL;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;
import org.apache.shiro.SecurityUtils;

public class Insert extends WriteTransaction<InsertContainer> {

  public Insert(final InsertContainer container) {
    super(container);
  }

  @Override
  protected void init() throws Exception {
    // allocate strong (write) access. Only one thread can do this
    // at a time. But weak access can still be acquired by other
    // thread until the allocated strong access is actually
    // acquired.
    setAccess(getMonitor().allocateStrongAccess(this));

    // make schedule for all parsed entities;
    makeSchedule();

    // dereference files (file upload only)
    for (final EntityInterface entity : getContainer()) {
      if (entity.hasFileProperties() && !entity.getFileProperties().isPickupable()) {
        if (entity.getFileProperties().getTmpIdentifyer() != null) {
          final FileProperties f =
              getContainer().getFiles().get(entity.getFileProperties().getTmpIdentifyer());
          if (f != null) {
            entity.getFileProperties().setFile(f.getFile());
            if (f.getThumbnail() != null) {
              entity.getFileProperties().setThumbnail(f.getThumbnail());
            } else {
              final FileProperties thumbnail =
                  getContainer()
                      .getFiles()
                      .get(entity.getFileProperties().getTmpIdentifyer() + ".thumbnail");
              if (thumbnail != null) {
                entity.getFileProperties().setThumbnail(thumbnail.getFile());
              }
            }
          } else {
            entity.addError(ServerMessages.FILE_HAS_NOT_BEEN_UPLOAED);
            entity.setEntityStatus(EntityStatus.UNQUALIFIED);
          }
        }
      }
    }
  }

  @Override
  protected void preCheck() throws InterruptedException {
    for (final EntityInterface entity : getContainer()) {
      // set default EntityACL if none present
      if (entity.getEntityACL() == null) {
        entity.setEntityACL(EntityACL.getOwnerACLFor(SecurityUtils.getSubject()));
      }
    }
  }

  @Override
  protected void postCheck() {}

  @Override
  protected void postTransaction() {}

  @Override
  public void transaction() throws Exception {
    // write new entities to database
    insert(getContainer(), getAccess());
  }

  public static void insert(final TransactionContainer container, final Access access)
      throws Exception {
    if (container.getStatus().ordinal() >= EntityStatus.QUALIFIED.ordinal()) {
      Database.execute(new InsertEntity(container), access);
    }
  }

  @Override
  public boolean logHistory() {
    return true;
  }
}
