/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.transaction;

import caosdb.server.accessControl.ACMPermissions;
import caosdb.server.accessControl.Principal;
import caosdb.server.accessControl.UserSources;
import caosdb.server.database.Database;
import caosdb.server.database.backend.transaction.RetrieveRole;
import caosdb.server.database.backend.transaction.UpdateUserRoles;
import caosdb.server.utils.ServerMessages;
import java.util.HashSet;
import java.util.Set;
import org.apache.shiro.SecurityUtils;
import org.jdom2.Element;

public class UpdateUserRolesTransaction extends AccessControlTransaction {

  private final HashSet<String> roles;
  private final String user;
  private final String realm;

  public UpdateUserRolesTransaction(
      final String realm, final String user, final HashSet<String> roles) {
    this.realm = realm;
    this.user = user;
    this.roles = roles;
  }

  @Override
  protected void transaction() throws Exception {
    SecurityUtils.getSubject()
        .checkPermission(ACMPermissions.PERMISSION_UPDATE_USER_ROLES(this.realm, this.user));

    for (final String role : this.roles) {
      SecurityUtils.getSubject().checkPermission(ACMPermissions.PERMISSION_ASSIGN_ROLE(role));

      // test if role is existent
      if (Database.execute(new RetrieveRole(role), getAccess()).getRole() == null) {
        throw ServerMessages.ROLE_DOES_NOT_EXIST;
      }
    }

    if (!UserSources.isUserExisting(new Principal(this.realm, this.user))) {
      throw ServerMessages.ACCOUNT_DOES_NOT_EXIST;
    }

    Database.execute(new UpdateUserRoles(this.realm, this.user, this.roles), getAccess());
  }

  public Element getUserRolesElement() {
    final Set<String> resulting_roles = UserSources.resolve(this.realm, this.user);
    final Element rolesElem = RetrieveUserRolesTransaction.getUserRolesElement(resulting_roles);
    if (!this.roles.equals(resulting_roles) && resulting_roles != null) {
      final Element warning = new Element("Warning");
      warning.setAttribute(
          "description",
          "Some roles cannot be removed. They are built-in or statically configured in the server configs.");
      rolesElem.addContent(0, warning);
    }
    return rolesElem;
  }

  public HashSet<String> getRoles() {
    return this.roles;
  }
}
