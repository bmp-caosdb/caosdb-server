/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.transaction;

import caosdb.server.database.misc.RollBackHandler;
import caosdb.server.entity.FileProperties;
import caosdb.server.entity.container.TransactionContainer;
import caosdb.server.utils.ChecksumUpdater;

public abstract class WriteTransaction<C extends TransactionContainer> extends Transaction<C> {

  protected WriteTransaction(final C container) {
    super(container);
  }

  @Override
  protected final void preTransaction() throws InterruptedException {
    // acquire strong access. No other thread can have access until
    // it this strong access is released.
    setAccess(getMonitor().acquireStrongAccess(this));
  }

  @Override
  protected void commit() throws Exception {
    getAccess().commit();
  }

  @Override
  protected void rollBack() {
    final RollBackHandler handler = (RollBackHandler) getAccess().getHelper("RollBack");
    handler.rollBack();
    super.rollBack();
  }

  @Override
  protected final void cleanUp() {
    // release strong access. Other threads can acquire or allocate
    // access again.
    getAccess().release();
    ChecksumUpdater.start();
    // remove unused tmp files
    for (final FileProperties f : getContainer().getFiles().values()) {
      if (f != null) {
        f.cleanUpTmpDir();
      }
    }
  }
}
