/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.transaction;

import caosdb.server.accessControl.Principal;
import caosdb.server.accessControl.UserSources;
import caosdb.server.accessControl.UserStatus;
import caosdb.server.database.Database;
import caosdb.server.database.backend.transaction.RetrieveUser;
import caosdb.server.database.proto.ProtoUser;
import caosdb.server.utils.ServerMessages;
import java.util.Set;
import org.jdom2.Element;

public class RetrieveUserTransaction extends AccessControlTransaction {

  private final Principal principal;
  private ProtoUser user;

  public RetrieveUserTransaction(final String realm, final String name) {
    this.principal = new Principal(realm, name);
  }

  @Override
  protected void transaction() throws Exception {
    if (!UserSources.isUserExisting(this.principal)) {
      throw ServerMessages.ACCOUNT_DOES_NOT_EXIST;
    }
    this.user = Database.execute(new RetrieveUser(this.principal), getAccess()).getUser();

    if (this.user == null) {
      this.user = new ProtoUser();
      this.user.name = this.principal.getUsername();
      this.user.realm = this.principal.getRealm();
    }
    if (this.user.status == null) {
      this.user.status = UserSources.getDefaultUserStatus(this.principal);
    }
    if (this.user.email == null) {
      this.user.email = UserSources.getDefaultUserEmail(this.principal);
    }
  }

  public static Element getUserElement(final ProtoUser user) {
    final Element ret = new Element("User");
    ret.setAttribute("name", user.name);
    if (user.realm != null) {
      ret.setAttribute("realm", user.realm);
    }
    if (user.email != null) {
      ret.setAttribute("email", user.email);
    }
    if (user.entity != null) {
      ret.setAttribute("entity", Integer.toString(user.entity));
    }
    if (user.status != null) {
      ret.setAttribute("status", user.status.toString());
    }
    return ret;
  }

  public Element getUserElement() {
    return getUserElement(this.user);
  }

  public Set<String> getRoles() {
    return this.user.roles;
  }

  public boolean isActive() {
    return this.user.status == UserStatus.ACTIVE;
  }
}
