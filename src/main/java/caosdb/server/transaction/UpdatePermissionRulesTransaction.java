/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.transaction;

import caosdb.server.accessControl.ACMPermissions;
import caosdb.server.database.Database;
import caosdb.server.database.backend.transaction.RetrieveRole;
import caosdb.server.database.backend.transaction.SetPermissionRules;
import caosdb.server.permissions.PermissionRule;
import caosdb.server.utils.ServerMessages;
import java.util.Set;
import org.apache.shiro.SecurityUtils;

public class UpdatePermissionRulesTransaction extends AccessControlTransaction {

  private final Set<PermissionRule> rules;
  private final String role;

  public UpdatePermissionRulesTransaction(final String role, final Set<PermissionRule> rules) {
    this.role = role;
    this.rules = rules;
  }

  @Override
  protected void transaction() throws Exception {
    SecurityUtils.getSubject()
        .checkPermission(ACMPermissions.PERMISSION_UPDATE_ROLE_PERMISSIONS(this.role));

    if (Database.execute(new RetrieveRole(this.role), getAccess()).getRole() == null) {
      throw ServerMessages.ROLE_DOES_NOT_EXIST;
    }

    Database.execute(new SetPermissionRules(this.role, this.rules), getAccess());
  }
}
