/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.transaction;

import caosdb.server.accessControl.ACMPermissions;
import caosdb.server.database.Database;
import caosdb.server.database.backend.transaction.DeleteRole;
import caosdb.server.database.backend.transaction.RetrieveRole;
import caosdb.server.database.backend.transaction.SetPermissionRules;
import caosdb.server.utils.ServerMessages;
import org.apache.shiro.SecurityUtils;

public class DeleteRoleTransaction extends AccessControlTransaction {

  private final String name;

  public DeleteRoleTransaction(final String name) {
    this.name = name;
  }

  @Override
  protected void transaction() throws Exception {
    SecurityUtils.getSubject().checkPermission(ACMPermissions.PERMISSION_DELETE_ROLE(this.name));

    if (Database.execute(new RetrieveRole(this.name), getAccess()).getRole() == null) {
      throw ServerMessages.ROLE_DOES_NOT_EXIST;
    }
    Database.execute(new SetPermissionRules(this.name, null), getAccess());
    Database.execute(new DeleteRole(this.name), getAccess());
  }
}
