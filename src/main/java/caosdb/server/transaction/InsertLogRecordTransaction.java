/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.transaction;

import caosdb.server.database.Database;
import caosdb.server.database.DatabaseMonitor;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.transaction.InsertLogRecord;
import java.util.List;
import java.util.logging.LogRecord;

public class InsertLogRecordTransaction implements TransactionInterface {

  private final List<LogRecord> toBeFlushed;

  public InsertLogRecordTransaction(final List<LogRecord> toBeFlushed) {
    this.toBeFlushed = toBeFlushed;
  }

  @Override
  public void execute() throws Exception {
    final Access access = DatabaseMonitor.getInstance().acquiredWeakAccess(this);
    try {
      Database.execute(new InsertLogRecord(this.toBeFlushed), access);
    } finally {
      access.release();
    }
  }
}
