/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.transaction;

import caosdb.server.database.Database;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.transaction.RetrieveFullEntity;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.container.Container;
import caosdb.server.entity.container.RetrieveContainer;
import caosdb.server.entity.xml.SetFieldStrategy;
import caosdb.server.entity.xml.ToElementStrategy;
import caosdb.server.entity.xml.ToElementable;
import caosdb.server.jobs.core.Mode;
import caosdb.server.jobs.core.RemoveDuplicates;
import caosdb.server.jobs.core.ResolveNames;
import caosdb.server.permissions.EntityPermission;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;
import org.apache.shiro.authz.AuthorizationException;
import org.jdom2.Element;

public class Retrieve extends Transaction<RetrieveContainer> {

  public Retrieve(final RetrieveContainer container) {
    super(container);
  }

  @Override
  protected void init() throws Exception {
    // acquire weak access
    setAccess(getMonitor().acquiredWeakAccess(this));

    // resolve names
    final ResolveNames r = new ResolveNames();
    r.init(Mode.SHOULD, null, this);
    getSchedule().add(r);
    getSchedule().runJob(r);

    final RemoveDuplicates job = new RemoveDuplicates();
    job.init(Mode.MUST, null, this);
    getSchedule().add(job);
    getSchedule().runJob(job);

    // make schedule for all parsed entities
    makeSchedule();
  }

  @Override
  protected void preCheck() throws InterruptedException {}

  @Override
  protected void postCheck() {}

  @Override
  protected void preTransaction() {}

  @Override
  protected void postTransaction() {
    // generate Error for missing RETRIEVE:ENTITY Permission.
    for (final EntityInterface e : getContainer()) {
      if (e.getEntityACL() != null) {
        try {
          e.checkPermission(EntityPermission.RETRIEVE_ENTITY);
        } catch (final AuthorizationException exc) {
          e.setToElementStragegy(
              new ToElementStrategy() {

                @Override
                public Element toElement(
                    final EntityInterface entity, final SetFieldStrategy setFieldStrategy) {
                  Element ret;
                  if (entity.hasRole()) {
                    ret = new Element(entity.getRole().toString());
                  } else {
                    ret = new Element("Entity");
                  }
                  ret.setAttribute("id", entity.getId().toString());
                  for (final ToElementable m : entity.getMessages()) {
                    m.addToElement(ret);
                  }
                  return ret;
                }

                @Override
                public Element addToElement(
                    final EntityInterface entity,
                    final Element parent,
                    final SetFieldStrategy setFieldStrategy) {
                  parent.addContent(toElement(entity, setFieldStrategy));
                  return parent;
                }
              });
          e.setEntityStatus(EntityStatus.UNQUALIFIED);
          e.addError(ServerMessages.AUTHORIZATION_ERROR);
          e.addInfo(exc.getMessage());
        }
      }
    }
    // generate Error for non-existent entities.
    for (final EntityInterface wrappedEntity : getContainer()) {
      if (wrappedEntity.getEntityStatus() == EntityStatus.NONEXISTENT) {
        wrappedEntity.addError(ServerMessages.ENTITY_DOES_NOT_EXIST);
      }
    }
  }

  @Override
  protected void cleanUp() {
    // release weak access
    getAccess().release();
  }

  @Override
  protected void transaction() throws Exception {
    // retrieve entities from mysql database
    retrieveFullEntities(getContainer(), getAccess());
  }

  private static void retrieveFullEntities(
      final Container<? extends EntityInterface> container, final Access access) throws Exception {
    Database.execute(new RetrieveFullEntity(container), access);
  }

  @Override
  public boolean logHistory() {
    return false;
  }
}
