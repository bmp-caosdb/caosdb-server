/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.transaction;

import caosdb.server.accessControl.Role;
import caosdb.server.database.Database;
import caosdb.server.database.backend.transaction.RetrieveRole;
import caosdb.server.utils.ServerMessages;

public class RetrieveRoleTransaction extends AccessControlTransaction {

  private final String name;
  private Role role;

  public RetrieveRoleTransaction(final String name) {
    this.name = name;
  }

  @Override
  protected void transaction() throws Exception {
    this.role = Database.execute(new RetrieveRole(this.name), getAccess()).getRole();
    if (this.role == null) {
      throw ServerMessages.ROLE_DOES_NOT_EXIST;
    }
  }

  public Role getRole() {
    return this.role;
  }
}
