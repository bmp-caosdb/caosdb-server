/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.transaction;

import caosdb.server.accessControl.ACMPermissions;
import caosdb.server.accessControl.UserSources;
import caosdb.server.accessControl.UserStatus;
import caosdb.server.database.Database;
import caosdb.server.database.backend.transaction.RetrievePasswordValidator;
import caosdb.server.database.backend.transaction.SetPassword;
import caosdb.server.database.backend.transaction.UpdateUser;
import caosdb.server.database.proto.ProtoUser;
import caosdb.server.utils.ServerMessages;
import caosdb.server.utils.Utils;
import org.apache.shiro.SecurityUtils;
import org.jdom2.Element;

public class InsertUserTransaction extends AccessControlTransaction {

  ProtoUser user = new ProtoUser();
  private final String password;

  public InsertUserTransaction(
      final String username,
      final String password,
      final String email,
      final UserStatus status,
      final Integer entity) {
    this.user.realm = UserSources.getInternalRealm().getName();
    this.user.name = username;
    this.user.email = email;
    this.user.status = status;
    this.user.entity = entity;
    this.password = password;
  }

  @Override
  protected void transaction() throws Exception {
    SecurityUtils.getSubject()
        .checkPermission(ACMPermissions.PERMISSION_INSERT_USER(this.user.realm));

    if (this.user.email != null && !Utils.isRFC822Compliant(this.user.email)) {
      throw ServerMessages.EMAIL_NOT_WELL_FORMED;
    }

    if (this.user.entity != null) {
      UpdateUserTransaction.checkEntityExists(this.user.entity);
    }

    if (Database.execute(new RetrievePasswordValidator(this.user.name), getAccess()).getValidator()
        == null) {
      if (this.password != null) {
        Utils.checkPasswordStrength(this.password);
      }

      Database.execute(new SetPassword(this.user.name, this.password), getAccess());
      Database.execute(new UpdateUser(this.user), getAccess());
    } else {
      throw ServerMessages.ACCOUNT_NAME_NOT_UNIQUE;
    }
  }

  public Element getUserElement() {
    return RetrieveUserTransaction.getUserElement(this.user);
  }
}
