/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.transaction;

import caosdb.server.database.Database;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.transaction.DeleteEntity;
import caosdb.server.database.backend.transaction.RetrieveFullEntity;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.container.DeleteContainer;
import caosdb.server.entity.container.TransactionContainer;
import caosdb.server.jobs.JobExecutionTime;
import caosdb.server.permissions.EntityPermission;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;
import org.apache.shiro.authz.AuthorizationException;

public class Delete extends WriteTransaction<DeleteContainer> {

  public Delete(final DeleteContainer container) {
    super(container);
  }

  @Override
  protected void init() throws Exception {}

  @Override
  protected void preCheck() throws InterruptedException, Exception {
    // allocate strong (write) access. Only one thread can do this
    // at a time. But weak access can still be acquired by other
    // thread until the allocated strong access is actually
    // acquired.
    setAccess(getMonitor().allocateStrongAccess(this));

    // retrieve all entities which are to be deleted.
    Database.execute(new RetrieveFullEntity(getContainer()), getAccess());

    for (final EntityInterface e : getContainer()) {
      if (e.getEntityStatus() == EntityStatus.NONEXISTENT) {
        e.addError(ServerMessages.ENTITY_DOES_NOT_EXIST);
        continue;
      }

      // check permissions
      try {
        e.checkPermission(EntityPermission.DELETE);
      } catch (final AuthorizationException exc) {
        e.setEntityStatus(EntityStatus.UNQUALIFIED);
        e.addError(ServerMessages.AUTHORIZATION_ERROR);
        e.addInfo(exc.getMessage());
      }

      // no standard entities are to be deleted.
      if (e.hasId() && e.getId() < 100) {
        e.setEntityStatus(EntityStatus.UNQUALIFIED);
        e.addInfo("This entity cannot be deleted");
      }
    }

    // make schedule of existing entities which are to be deleted.
    makeSchedule();
    getSchedule().runJobs(JobExecutionTime.INIT);
  }

  @Override
  protected void postCheck() {}

  @Override
  protected void postTransaction() {
    // set entityStatus to DELETED and add deletion info message
    if (getContainer().getStatus().ordinal() >= EntityStatus.QUALIFIED.ordinal()) {
      for (final EntityInterface entity : getContainer()) {
        if (entity.getEntityStatus() == EntityStatus.VALID) {
          entity.setEntityStatus(EntityStatus.DELETED);
          entity.addInfo(ServerMessages.ENTITY_HAS_BEEN_DELETED_SUCCESSFULLY);
        }
      }
    }
  }

  @Override
  public void transaction() throws Exception {
    // delete entities from database
    delete(getContainer(), getAccess());
  }

  private static void delete(final TransactionContainer container, final Access access)
      throws Exception {
    if (container.getStatus().ordinal() >= EntityStatus.QUALIFIED.ordinal()) {
      Database.execute(new DeleteEntity(container), access);
    }
  }

  @Override
  public boolean logHistory() {
    return true;
  }
}
