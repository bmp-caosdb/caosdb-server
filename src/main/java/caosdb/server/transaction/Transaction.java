/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.transaction;

import caosdb.datetime.UTCDateTime;
import caosdb.server.accessControl.AuthenticationUtils;
import caosdb.server.accessControl.Principal;
import caosdb.server.database.Database;
import caosdb.server.database.DatabaseMonitor;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.transaction.InsertTransactionHistory;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.Message;
import caosdb.server.entity.Message.MessageType;
import caosdb.server.entity.container.TransactionContainer;
import caosdb.server.jobs.Job;
import caosdb.server.jobs.JobExecutionTime;
import caosdb.server.jobs.Schedule;
import caosdb.server.jobs.core.AccessControl;
import caosdb.server.jobs.core.CheckDatatypePresent;
import caosdb.server.jobs.core.CheckEntityACLRoles;
import caosdb.server.jobs.core.Mode;
import caosdb.server.jobs.core.PickUp;
import caosdb.server.utils.AbstractObservable;
import caosdb.server.utils.Info;
import caosdb.server.utils.Observer;
import java.util.HashMap;
import java.util.List;
import org.apache.shiro.subject.Subject;

public abstract class Transaction<C extends TransactionContainer> extends AbstractObservable
    implements TransactionInterface {

  public static final Message ERROR_INTEGRITY_VIOLATION =
      new Message(MessageType.Error, 0, "This entity caused an unexpected integrity violation.");

  private static final DatabaseMonitor monitor = DatabaseMonitor.getInstance();

  public static final String CLEAN_UP = "TransactionCleanUp";
  private final C container;
  private Access access = null;
  private final Schedule schedule = new Schedule();

  protected Transaction(final C container) {
    this(container, Info.getInstance());
  }

  protected Transaction(C container, Observer o) {
    this.container = container;
    if (o != null) acceptObserver(o);
  }

  public static DatabaseMonitor getMonitor() {
    return monitor;
  }

  public C getContainer() {
    return this.container;
  }

  protected void makeSchedule() throws Exception {
    // load flag jobs
    final Job loadContainerFlags = Job.getJob("LoadContainerFlagJobs", Mode.MUST, null, this);
    this.schedule.add(loadContainerFlags);
    this.schedule.runJob(loadContainerFlags);

    // AccessControl
    this.schedule.add(Job.getJob(AccessControl.class.getSimpleName(), Mode.MUST, null, this));
    this.schedule.add(Job.getJob(CheckEntityACLRoles.class.getSimpleName(), Mode.MUST, null, this));

    // load permanent container jobs
    this.schedule.addAll(Job.loadPermanentContainerJobs(this));

    for (final EntityInterface e : getContainer()) {
      final List<Job> loadJobs = Job.loadJobs(e, this);
      this.schedule.addAll(loadJobs);

      // additionally load datatype job
      if (e.hasValue()) {
        boolean found = false;
        for (final Job j : loadJobs) {

          if (CheckDatatypePresent.class.isInstance(j)
              && ((CheckDatatypePresent) j).getEntity() == e) {
            found = true;
          }
        }
        if (!found) {
          this.schedule.add(new CheckDatatypePresent().init(Mode.MUST, e, this));
        }
      }

      // load pickup job if necessary
      if (e.hasFileProperties() && e.getFileProperties().isPickupable()) {
        this.schedule.add(new PickUp().init(Mode.MUST, e, this));
      }
    }
  }

  @Override
  public final void execute() throws Exception {
    long t1 = System.currentTimeMillis();
    try {
      init();
      this.schedule.runJobs(JobExecutionTime.INIT);
      getContainer()
          .getTransactionBenchmark()
          .addBenchmark(this.getClass().getSimpleName() + ".init", System.currentTimeMillis() - t1);

      t1 = System.currentTimeMillis();
      preCheck();
      this.schedule.runJobs(JobExecutionTime.PRE_CHECK);
      getContainer()
          .getTransactionBenchmark()
          .addBenchmark(
              this.getClass().getSimpleName() + ".pre_check", System.currentTimeMillis() - t1);

      t1 = System.currentTimeMillis();
      check();
      getContainer()
          .getTransactionBenchmark()
          .addBenchmark(
              this.getClass().getSimpleName() + ".check", System.currentTimeMillis() - t1);

      t1 = System.currentTimeMillis();
      this.schedule.runJobs(JobExecutionTime.POST_CHECK);
      postCheck();
      getContainer()
          .getTransactionBenchmark()
          .addBenchmark(
              this.getClass().getSimpleName() + ".post_check", System.currentTimeMillis() - t1);

      t1 = System.currentTimeMillis();
      preTransaction();
      this.schedule.runJobs(JobExecutionTime.PRE_TRANSACTION);
      getContainer()
          .getTransactionBenchmark()
          .addBenchmark(
              this.getClass().getSimpleName() + ".pre_transaction",
              System.currentTimeMillis() - t1);

      t1 = System.currentTimeMillis();
      transaction();
      getContainer()
          .getTransactionBenchmark()
          .addBenchmark(
              this.getClass().getSimpleName() + ".transaction", System.currentTimeMillis() - t1);

      t1 = System.currentTimeMillis();
      this.schedule.runJobs(JobExecutionTime.POST_TRANSACTION);
      postTransaction();
      getContainer()
          .getTransactionBenchmark()
          .addBenchmark(
              this.getClass().getSimpleName() + ".post_transaction",
              System.currentTimeMillis() - t1);

      t1 = System.currentTimeMillis();
      writeHistory();
      getContainer()
          .getTransactionBenchmark()
          .addBenchmark(
              this.getClass().getSimpleName() + ".history", System.currentTimeMillis() - t1);

      t1 = System.currentTimeMillis();
      commit();
      getContainer()
          .getTransactionBenchmark()
          .addBenchmark(
              this.getClass().getSimpleName() + ".commit", System.currentTimeMillis() - t1);

    } catch (final Exception e) {
      e.printStackTrace();
      rollBack();
      throw e;
    } finally {
      t1 = System.currentTimeMillis();
      cleanUp();
      getContainer()
          .getTransactionBenchmark()
          .addBenchmark(
              this.getClass().getSimpleName() + ".cleanup", System.currentTimeMillis() - t1);
      notifyObservers(CLEAN_UP);
    }
  }

  public Schedule getSchedule() {
    return this.schedule;
  }

  public Subject getTransactor() {
    return getContainer().getOwner();
  }

  public abstract boolean logHistory();

  public UTCDateTime getTimestamp() {
    return UTCDateTime.SystemMillisToUTCDateTime(getContainer().getTimestamp());
  }

  // TODO move to post-transaction job
  private void writeHistory() throws TransactionException, Message {
    if (logHistory()) {
      String realm =
          getTransactor().getPrincipal() == AuthenticationUtils.ANONYMOUS_USER.getPrincipal()
              ? ""
              : ((Principal) getTransactor().getPrincipal()).getRealm();
      String username =
          getTransactor().getPrincipal() == AuthenticationUtils.ANONYMOUS_USER.getPrincipal()
              ? "anonymous"
              : ((Principal) getTransactor().getPrincipal()).getUsername();
      Database.execute(
          new InsertTransactionHistory(
              getContainer(), this.getClass().getSimpleName(), realm, username, getTimestamp()),
          getAccess());
    }
  }

  protected void rollBack() {
    this.schedule.runJobs(JobExecutionTime.ROLL_BACK);
  }

  protected abstract void init() throws Exception;

  protected abstract void preCheck() throws InterruptedException, Exception;

  protected final void check() {
    this.schedule.runJobs(JobExecutionTime.CHECK);
  }

  protected abstract void postCheck();

  protected abstract void preTransaction() throws InterruptedException;

  protected abstract void transaction() throws Exception;

  protected abstract void postTransaction() throws Exception;

  protected abstract void cleanUp();

  protected void commit() throws Exception {}

  public boolean useCache() {
    return getContainer().getFlags() != null
        && !getContainer().getFlags().containsKey("disableCache");
  }

  public final Access getAccess() {
    return this.access;
  }

  protected void setAccess(final Access a) {
    this.access = a;
  }

  @SuppressWarnings("unchecked")
  public <S, T> HashMap<S, T> getCache(final String name) {
    if (!this.caches.containsKey(name)) {
      this.caches.put(name, new HashMap<S, T>());
    }
    return this.caches.get(name);
  }

  @SuppressWarnings("rawtypes")
  private final HashMap<String, HashMap> caches = new HashMap<>();
}
