/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.transaction;

import caosdb.server.accessControl.ACMPermissions;
import caosdb.server.database.Database;
import caosdb.server.database.DatabaseMonitor;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.transaction.RetrieveLogRecord;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import org.apache.shiro.SecurityUtils;

public class RetrieveLogRecordTransaction implements TransactionInterface {

  private List<LogRecord> logRecords;
  private final String logger;
  private final Level level;
  private final String message;

  public RetrieveLogRecordTransaction(
      final String logger, final Level level, final String message) {
    this.level = level;
    if (message != null && message.isEmpty()) {
      this.message = null;
    } else if (message != null) {
      this.message = message.replaceAll("\\*", "%");
    } else {
      this.message = null;
    }
    if (logger != null && logger.isEmpty()) {
      this.logger = null;
    } else if (logger != null) {
      this.logger = logger.replaceAll("\\*", "%");
    } else {
      this.logger = logger;
    }
  }

  @Override
  public void execute() throws Exception {
    SecurityUtils.getSubject().checkPermission(ACMPermissions.PERMISSION_RETRIEVE_SERVERLOGS);
    final Access access = DatabaseMonitor.getInstance().acquiredWeakAccess(this);
    try {
      this.logRecords =
          Database.execute(new RetrieveLogRecord(this.logger, this.level, this.message), access)
              .getLogRecords();
    } finally {
      access.release();
    }
  }

  public List<LogRecord> getLogRecords() {
    return this.logRecords;
  }
}
