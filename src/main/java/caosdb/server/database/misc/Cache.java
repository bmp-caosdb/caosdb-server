/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.misc;

import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import caosdb.server.terminal.StatLabel;
import caosdb.server.terminal.StatsPanel;
import caosdb.server.utils.AbstractObservable;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;
import org.apache.commons.jcs.JCS;
import org.apache.commons.jcs.access.CacheAccess;

public class Cache {

  static {
    final Properties p = new Properties();
    try {
      final InputStream is =
          new FileInputStream(CaosDBServer.getServerProperty(ServerProperties.KEY_CACHE_CONF_LOC));
      p.load(is);
      is.close();
    } catch (final FileNotFoundException e) {
      e.printStackTrace();
    } catch (final IOException e) {
      e.printStackTrace();
    }
    JCS.setConfigProperties(p);
  }

  public static <K, V extends Serializable> CacheAccess<K, V> getCache(
      final String name, final int capacity) {
    if (capacity > 0) {
      final CacheAccess<K, V> cache = JCS.getInstance(name);
      cache.getCacheAttributes().setMaxObjects(capacity);
      StatsPanel.addStat(
          name,
          new AbstractObservable() {
            @Override
            public String toString() {
              return cache.getStats();
            };
          });
      StatsPanel.addStat(
          name,
          new StatLabel(
              "Configuration", cache.getCacheAttributes().toString().replaceAll(",", "\n")));

      return cache;
    }
    return null;
  }
}
