/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.misc;

import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import caosdb.server.utils.CronJob;
import caosdb.server.utils.Info;
import caosdb.server.utils.ServerStat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map.Entry;
import org.jdom2.Element;

public class TransactionBenchmark implements ServerStat {

  private static final long serialVersionUID = -8916163825450491067L;
  private long since = System.currentTimeMillis();
  private final HashMap<String, Integer> counts = new HashMap<String, Integer>();
  private final HashMap<String, Long> acc = new HashMap<String, Long>();
  private transient boolean synced = false;
  private static final TransactionBenchmark instance = new TransactionBenchmark();
  private static final boolean isActive =
      Boolean.valueOf(
          CaosDBServer.getServerProperty(ServerProperties.KEY_TRANSACTION_BENCHMARK_ENABLED)
              .toLowerCase());

  static {
    if (isActive) {
      instance.init();
    }
  }

  /**
   * Fetch old data (from before last shutdown) and fill it into this instance.
   *
   * @return
   */
  private TransactionBenchmark init() {
    synchronized (this.counts) {
      final Runnable updater =
          new Runnable() {

            @Override
            public void run() {
              try {
                synchronized (TransactionBenchmark.this.counts) {
                  Info.syncDatabase(TransactionBenchmark.this);
                }
              } catch (final Exception e) {
                e.printStackTrace();
              }
            }
          };

      updater.run();

      new CronJob("SyncTransactionBenchmark", updater, 3600, true); // hourly
    }
    return this;
  }

  /**
   * Add a benchmark for a certain object. The object will be toString()'ed. The string serves as a
   * key.
   *
   * @param object
   * @param time
   */
  public void addBenchmark(final Object object, final long time) {
    if (isActive) {
      addBenchmark(instance, object.toString(), time, 1);
    }
  }

  private static void addBenchmark(
      final TransactionBenchmark b, final String name, final long time, final int count) {
    synchronized (b.counts) {
      if (b.counts.containsKey(name)) {
        int c = b.counts.get(name);
        c += count;
        b.counts.put(name, c);
        long a = b.acc.get(name);
        a += time;
        b.acc.put(name, a);
      } else {
        b.counts.put(name, count);
        b.acc.put(name, time);
      }
    }
  }

  public static TransactionBenchmark getInstance() {
    return instance;
  }

  @Override
  public String toString() {
    if (isActive) {
      final StringBuilder sb = new StringBuilder();
      sb.append(
          "\nNAME: (execution count, accumulated execution time [ms], avg time per execution [ms])\n");
      synchronized (this.counts) {
        for (final Entry<String, Integer> e : this.counts.entrySet()) {
          final int c = e.getValue();
          final long acc = this.acc.get(e.getKey());
          final String name = e.getKey();
          final double avg = (double) acc / c;
          sb.append('\n');
          sb.append('#');
          sb.append(name);
          sb.append(": ");
          sb.append('(');
          sb.append(c);
          sb.append(", ");
          sb.append(acc);
          sb.append(", ");
          sb.append(String.format(Locale.US, "%3.2f", avg));
          sb.append(')');
        }
      }
      return sb.toString();
    } else {
      return "TransactionBenchmark is disabled.";
    }
  }

  public Element toElememt() {
    final Element ret = new Element("TransactionBenchmark");
    if (isActive) {
      ret.setAttribute("since", new Date(this.since).toString());
      synchronized (this.counts) {
        for (final Entry<String, Integer> e : this.counts.entrySet()) {
          final int c = e.getValue();
          final long acc = TransactionBenchmark.this.acc.get(e.getKey());
          final String name = e.getKey();
          final double avg = (double) acc / c;

          final Element b = new Element("Benchmark");
          b.setAttribute("name", name);
          b.setAttribute("execution_count", Integer.toString(c));
          b.setAttribute("accumulated_execution_time", Long.toString(acc) + "ms");
          b.setAttribute("avg_execution_time", String.format(Locale.US, "%3.2f", avg) + "ms");

          ret.addContent(b);
        }
      }
    } else {
      ret.setAttribute("info", "TransactionBenchmark is disabled.");
    }
    return ret;
  }

  @Override
  public String getName() {
    return this.getClass().getSimpleName();
  }

  /**
   * Add data from s to this instance
   *
   * @param s another TransactionBenchmark instance
   */
  @Override
  public void update(final ServerStat s) {
    final TransactionBenchmark t = (TransactionBenchmark) s;
    if (!this.synced) {
      this.synced = true;
      this.since = t.since;
      for (final String key : t.counts.keySet()) {
        addBenchmark(this, key, t.acc.get(key), t.counts.get(key));
      }
    }
  }

  /**
   * Get a SubBenchmark which can be used to produce a separate benchmark (e.g. for single
   * transactions). The SubBenchmark's data will also available to the main benchmark.
   *
   * @return A SubBenchmark
   */
  public static TransactionBenchmark getSubBenchmark() {
    return new SubBenchmark();
  }

  private static class SubBenchmark extends TransactionBenchmark {

    private static final long serialVersionUID = 1L;

    @Override
    public void addBenchmark(final Object object, final long time) {
      if (isActive) {
        super.addBenchmark(object, time);
        TransactionBenchmark.addBenchmark(this, object.toString(), time, 1);
      }
    }
  }
}
