/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database;

import caosdb.server.database.access.Access;
import caosdb.server.database.backend.implementation.MySQL.MySQLDeleteEntityProperties;
import caosdb.server.database.backend.implementation.MySQL.MySQLDeletePassword;
import caosdb.server.database.backend.implementation.MySQL.MySQLDeleteRole;
import caosdb.server.database.backend.implementation.MySQL.MySQLDeleteSparseEntity;
import caosdb.server.database.backend.implementation.MySQL.MySQLDeleteUser;
import caosdb.server.database.backend.implementation.MySQL.MySQLGetChildren;
import caosdb.server.database.backend.implementation.MySQL.MySQLGetDependentEntities;
import caosdb.server.database.backend.implementation.MySQL.MySQLGetFileRecordByPath;
import caosdb.server.database.backend.implementation.MySQL.MySQLGetIDByName;
import caosdb.server.database.backend.implementation.MySQL.MySQLGetInfo;
import caosdb.server.database.backend.implementation.MySQL.MySQLGetUpdateableChecksums;
import caosdb.server.database.backend.implementation.MySQL.MySQLInsertEntityDatatype;
import caosdb.server.database.backend.implementation.MySQL.MySQLInsertEntityProperties;
import caosdb.server.database.backend.implementation.MySQL.MySQLInsertLinCon;
import caosdb.server.database.backend.implementation.MySQL.MySQLInsertLogRecord;
import caosdb.server.database.backend.implementation.MySQL.MySQLInsertParents;
import caosdb.server.database.backend.implementation.MySQL.MySQLInsertRole;
import caosdb.server.database.backend.implementation.MySQL.MySQLInsertSparseEntity;
import caosdb.server.database.backend.implementation.MySQL.MySQLInsertTransactionHistory;
import caosdb.server.database.backend.implementation.MySQL.MySQLIsSubType;
import caosdb.server.database.backend.implementation.MySQL.MySQLRegisterSubDomain;
import caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveAll;
import caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveAllUncheckedFiles;
import caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveDatatypes;
import caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveLogRecord;
import caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveParents;
import caosdb.server.database.backend.implementation.MySQL.MySQLRetrievePasswordValidator;
import caosdb.server.database.backend.implementation.MySQL.MySQLRetrievePermissionRules;
import caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveProperties;
import caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveQueryTemplateDefinition;
import caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveRole;
import caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveSparseEntity;
import caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveTransactionHistory;
import caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveUser;
import caosdb.server.database.backend.implementation.MySQL.MySQLRuleLoader;
import caosdb.server.database.backend.implementation.MySQL.MySQLSetFileCheckedTimestampImpl;
import caosdb.server.database.backend.implementation.MySQL.MySQLSetPassword;
import caosdb.server.database.backend.implementation.MySQL.MySQLSetPermissionRules;
import caosdb.server.database.backend.implementation.MySQL.MySQLSetQueryTemplateDefinition;
import caosdb.server.database.backend.implementation.MySQL.MySQLSyncStats;
import caosdb.server.database.backend.implementation.MySQL.MySQLUpdateSparseEntity;
import caosdb.server.database.backend.implementation.MySQL.MySQLUpdateUser;
import caosdb.server.database.backend.implementation.MySQL.MySQLUpdateUserRoles;
import caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemCheckHash;
import caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemCheckSize;
import caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemFileExists;
import caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemFileWasModifiedAfter;
import caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemGetFileIterator;
import caosdb.server.database.backend.interfaces.BackendTransactionImpl;
import caosdb.server.database.backend.interfaces.DeleteEntityPropertiesImpl;
import caosdb.server.database.backend.interfaces.DeletePasswordImpl;
import caosdb.server.database.backend.interfaces.DeleteRoleImpl;
import caosdb.server.database.backend.interfaces.DeleteSparseEntityImpl;
import caosdb.server.database.backend.interfaces.DeleteUserImpl;
import caosdb.server.database.backend.interfaces.FileCheckHash;
import caosdb.server.database.backend.interfaces.FileCheckSize;
import caosdb.server.database.backend.interfaces.FileExists;
import caosdb.server.database.backend.interfaces.FileWasModifiedAfter;
import caosdb.server.database.backend.interfaces.GetChildrenImpl;
import caosdb.server.database.backend.interfaces.GetDependentEntitiesImpl;
import caosdb.server.database.backend.interfaces.GetFileIteratorImpl;
import caosdb.server.database.backend.interfaces.GetFileRecordByPathImpl;
import caosdb.server.database.backend.interfaces.GetIDByNameImpl;
import caosdb.server.database.backend.interfaces.GetInfoImpl;
import caosdb.server.database.backend.interfaces.GetUpdateableChecksumsImpl;
import caosdb.server.database.backend.interfaces.InsertEntityDatatypeImpl;
import caosdb.server.database.backend.interfaces.InsertEntityPropertiesImpl;
import caosdb.server.database.backend.interfaces.InsertLinConImpl;
import caosdb.server.database.backend.interfaces.InsertLogRecordImpl;
import caosdb.server.database.backend.interfaces.InsertParentsImpl;
import caosdb.server.database.backend.interfaces.InsertRoleImpl;
import caosdb.server.database.backend.interfaces.InsertSparseEntityImpl;
import caosdb.server.database.backend.interfaces.InsertTransactionHistoryImpl;
import caosdb.server.database.backend.interfaces.IsSubTypeImpl;
import caosdb.server.database.backend.interfaces.RegisterSubDomainImpl;
import caosdb.server.database.backend.interfaces.RetrieveAllImpl;
import caosdb.server.database.backend.interfaces.RetrieveAllUncheckedFilesImpl;
import caosdb.server.database.backend.interfaces.RetrieveDatatypesImpl;
import caosdb.server.database.backend.interfaces.RetrieveLogRecordImpl;
import caosdb.server.database.backend.interfaces.RetrieveParentsImpl;
import caosdb.server.database.backend.interfaces.RetrievePasswordValidatorImpl;
import caosdb.server.database.backend.interfaces.RetrievePermissionRulesImpl;
import caosdb.server.database.backend.interfaces.RetrievePropertiesImpl;
import caosdb.server.database.backend.interfaces.RetrieveQueryTemplateDefinitionImpl;
import caosdb.server.database.backend.interfaces.RetrieveRoleImpl;
import caosdb.server.database.backend.interfaces.RetrieveSparseEntityImpl;
import caosdb.server.database.backend.interfaces.RetrieveTransactionHistoryImpl;
import caosdb.server.database.backend.interfaces.RetrieveUserImpl;
import caosdb.server.database.backend.interfaces.RuleLoaderImpl;
import caosdb.server.database.backend.interfaces.SetFileCheckedTimestampImpl;
import caosdb.server.database.backend.interfaces.SetPasswordImpl;
import caosdb.server.database.backend.interfaces.SetPermissionRulesImpl;
import caosdb.server.database.backend.interfaces.SetQueryTemplateDefinitionImpl;
import caosdb.server.database.backend.interfaces.SyncStatsImpl;
import caosdb.server.database.backend.interfaces.UpdateSparseEntityImpl;
import caosdb.server.database.backend.interfaces.UpdateUserImpl;
import caosdb.server.database.backend.interfaces.UpdateUserRolesImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.misc.TransactionBenchmark;
import caosdb.server.utils.UndoHandler;
import caosdb.server.utils.Undoable;
import java.util.HashMap;

public abstract class BackendTransaction implements Undoable {

  private final UndoHandler undoHandler = new UndoHandler();
  private Access access;
  BackendTransaction parent = null;
  private static HashMap<
          Class<? extends BackendTransactionImpl>, Class<? extends BackendTransactionImpl>>
      impl = new HashMap<>();

  protected abstract void execute();

  final void executeTransaction() {
    final long t1 = System.currentTimeMillis();
    execute();
    final long t2 = System.currentTimeMillis();
    TransactionBenchmark.getInstance().addBenchmark(this, t2 - t1);
  }

  private static void init() {
    if (impl.isEmpty()) {
      setImpl(DeleteEntityPropertiesImpl.class, MySQLDeleteEntityProperties.class);
      setImpl(DeleteSparseEntityImpl.class, MySQLDeleteSparseEntity.class);
      setImpl(GetChildrenImpl.class, MySQLGetChildren.class);
      setImpl(GetDependentEntitiesImpl.class, MySQLGetDependentEntities.class);
      setImpl(GetIDByNameImpl.class, MySQLGetIDByName.class);
      setImpl(GetInfoImpl.class, MySQLGetInfo.class);
      setImpl(InsertEntityPropertiesImpl.class, MySQLInsertEntityProperties.class);
      setImpl(InsertLinConImpl.class, MySQLInsertLinCon.class);
      setImpl(InsertParentsImpl.class, MySQLInsertParents.class);
      setImpl(InsertSparseEntityImpl.class, MySQLInsertSparseEntity.class);
      setImpl(InsertTransactionHistoryImpl.class, MySQLInsertTransactionHistory.class);
      setImpl(IsSubTypeImpl.class, MySQLIsSubType.class);
      setImpl(UpdateSparseEntityImpl.class, MySQLUpdateSparseEntity.class);
      setImpl(RetrieveAllImpl.class, MySQLRetrieveAll.class);
      setImpl(RegisterSubDomainImpl.class, MySQLRegisterSubDomain.class);
      setImpl(RetrieveDatatypesImpl.class, MySQLRetrieveDatatypes.class);
      setImpl(RetrieveTransactionHistoryImpl.class, MySQLRetrieveTransactionHistory.class);
      setImpl(RetrieveUserImpl.class, MySQLRetrieveUser.class);
      setImpl(RetrieveParentsImpl.class, MySQLRetrieveParents.class);
      setImpl(GetFileRecordByPathImpl.class, MySQLGetFileRecordByPath.class);
      setImpl(RetrievePropertiesImpl.class, MySQLRetrieveProperties.class);
      setImpl(RetrieveSparseEntityImpl.class, MySQLRetrieveSparseEntity.class);
      setImpl(RuleLoaderImpl.class, MySQLRuleLoader.class);
      setImpl(SyncStatsImpl.class, MySQLSyncStats.class);
      setImpl(FileExists.class, UnixFileSystemFileExists.class);
      setImpl(FileWasModifiedAfter.class, UnixFileSystemFileWasModifiedAfter.class);
      setImpl(FileCheckHash.class, UnixFileSystemCheckHash.class);
      setImpl(GetFileIteratorImpl.class, UnixFileSystemGetFileIterator.class);
      setImpl(SetFileCheckedTimestampImpl.class, MySQLSetFileCheckedTimestampImpl.class);
      setImpl(RetrieveAllUncheckedFilesImpl.class, MySQLRetrieveAllUncheckedFiles.class);
      setImpl(UpdateUserImpl.class, MySQLUpdateUser.class);
      setImpl(DeleteUserImpl.class, MySQLDeleteUser.class);
      setImpl(SetPasswordImpl.class, MySQLSetPassword.class);
      setImpl(RetrievePasswordValidatorImpl.class, MySQLRetrievePasswordValidator.class);
      setImpl(DeletePasswordImpl.class, MySQLDeletePassword.class);
      setImpl(GetUpdateableChecksumsImpl.class, MySQLGetUpdateableChecksums.class);
      setImpl(FileCheckSize.class, UnixFileSystemCheckSize.class);
      setImpl(InsertRoleImpl.class, MySQLInsertRole.class);
      setImpl(RetrieveRoleImpl.class, MySQLRetrieveRole.class);
      setImpl(DeleteRoleImpl.class, MySQLDeleteRole.class);
      setImpl(SetPermissionRulesImpl.class, MySQLSetPermissionRules.class);
      setImpl(RetrievePermissionRulesImpl.class, MySQLRetrievePermissionRules.class);
      setImpl(UpdateUserRolesImpl.class, MySQLUpdateUserRoles.class);
      setImpl(InsertLogRecordImpl.class, MySQLInsertLogRecord.class);
      setImpl(RetrieveLogRecordImpl.class, MySQLRetrieveLogRecord.class);
      setImpl(SetQueryTemplateDefinitionImpl.class, MySQLSetQueryTemplateDefinition.class);
      setImpl(
          RetrieveQueryTemplateDefinitionImpl.class, MySQLRetrieveQueryTemplateDefinition.class);
      setImpl(InsertEntityDatatypeImpl.class, MySQLInsertEntityDatatype.class);
    }
  }

  protected <K extends BackendTransaction> K execute(final K t) {
    assert t != this;
    this.undoHandler.append(t);
    t.setAccess(this.access);
    t.parent = this;
    final long t1 = System.currentTimeMillis();
    t.execute();
    final long t2 = System.currentTimeMillis();
    TransactionBenchmark.getInstance().addBenchmark(t, t2 - t1);
    return t;
  }

  private static <K extends BackendTransactionImpl, L extends K> void setImpl(
      final Class<K> k, final Class<L> l) {
    impl.put(k, l);
  }

  void setAccess(final Access access) {
    this.access = access;
  }

  @SuppressWarnings("unchecked")
  protected <T extends BackendTransactionImpl> T getImplementation(final Class<T> clz) {
    init();
    try {
      final T ret = (T) impl.get(clz).getConstructor(Access.class).newInstance(this.access);
      if (ret instanceof Undoable) {
        this.undoHandler.append((Undoable) ret);
      }
      return ret;
    } catch (final Exception e) {
      throw new TransactionException(e);
    }
  }

  protected UndoHandler getUndoHandler() {
    return this.undoHandler;
  }

  @Override
  public final void undo() {
    this.undoHandler.undo();
  }

  @Override
  public final void cleanUp() {
    this.undoHandler.cleanUp();
  }

  boolean useCache() {
    return this.access.useCache();
  }

  @Override
  public String toString() {
    return (this.parent != null ? this.parent.toString() + " -> " : "")
        + this.getClass().getSimpleName();
  }
}
