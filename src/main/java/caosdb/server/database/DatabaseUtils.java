/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database;

import caosdb.server.database.proto.FlatProperty;
import caosdb.server.database.proto.ProtoProperty;
import caosdb.server.database.proto.SparseEntity;
import caosdb.server.database.proto.VerySparseEntity;
import caosdb.server.datatype.AbstractCollectionDatatype;
import caosdb.server.datatype.CollectionValue;
import caosdb.server.datatype.IndexedSingleValue;
import caosdb.server.datatype.ReferenceValue;
import caosdb.server.datatype.SingleValue;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.Message;
import caosdb.server.entity.Role;
import caosdb.server.entity.StatementStatus;
import caosdb.server.entity.wrapper.Domain;
import caosdb.server.entity.wrapper.Parent;
import caosdb.server.entity.wrapper.Property;
import com.google.common.base.Objects;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DatabaseUtils {

  public static final String bytes2UTF8(final byte[] bytes) {
    if (bytes == null) {
      return null;
    } else {
      return new String(bytes);
    }
  }

  private static Domain makeReplacement(final EntityInterface p) {
    // add a new helper domain.
    final Domain d =
        new Domain(p.getProperties(), p.getDatatype(), p.getValue(), p.getStatementStatus());
    d.setDescOverride(p.isDescOverride());
    d.setNameOverride(p.isNameOverride());
    d.setDatatypeOverride(p.isDatatypeOverride());
    d.setName(p.getName());
    d.setDescription(p.getDescription());
    p.setReplacement(d);
    return d;
  }

  public static void deriveStage1Inserts(
      final List<EntityInterface> stage1Inserts, final EntityInterface e) {
    // entity value
    if (e.hasValue()) {
      final Property p = new Property();
      p.setId(e.getId());
      p.setStatementStatus(StatementStatus.FIX);
      p.setPIdx(0);
      p.setDatatype(e.getDatatype());
      p.setValue(e.getValue());
      processPropertiesStage1(stage1Inserts, p, e);
    }

    for (final EntityInterface p : e.getProperties()) {
      processPropertiesStage1(stage1Inserts, p, e);
    }
  }

  private static void processPropertiesStage1(
      final List<EntityInterface> stage1Inserts, final EntityInterface p, final EntityInterface e) {
    stage1Inserts.add(p);
    if (!p.isDescOverride()
        && !p.isNameOverride()
        && !p.isDatatypeOverride()
        && (!p.hasProperties() || hasUniquePropertyId(p, e))
        && !(p.getDatatype() instanceof AbstractCollectionDatatype)) {
      // if p has no sub-properties, just add it
    } else {
      stage1Inserts.add(makeReplacement(p));
    }
    processSubPropertiesStage1(stage1Inserts, p);
  }

  public static int deriveStage2Inserts(
      final List<EntityInterface> stage2Inserts, final List<EntityInterface> stage1Inserts) {
    int domainCount = 0;
    for (final EntityInterface p : stage1Inserts) {
      if (p.hasRole() && p.getRole() == Role.Domain) {
        domainCount++;
      }
      if (!p.hasReplacement() && p.hasProperties()) {
        stage2Inserts.addAll(p.getProperties());
      }
    }
    return domainCount;
  }

  private static boolean hasUniquePropertyId(final EntityInterface p, final EntityInterface e) {
    final Integer id = p.getId();
    for (final EntityInterface p2 : e.getProperties()) {
      if (Objects.equal(p2.getId(), id) && p2 != p) {
        return false;
      }
    }
    return true;
  }

  private static void processSubPropertiesStage1(
      final List<EntityInterface> stage1Inserts, final EntityInterface p) {
    for (final EntityInterface subP : p.getProperties()) {
      if (subP.hasProperties()) {
        stage1Inserts.add(makeReplacement(subP));
        processSubPropertiesStage1(stage1Inserts, subP);
      }
    }
  }

  public static void parseFromSparseEntities(final EntityInterface e, final SparseEntity spe) {
    if (spe != null) {
      e.parseSparseEntity(spe);
    }
  }

  public static void parseOverrides(final List<FlatProperty> properties, final ResultSet rs)
      throws SQLException {
    while (rs.next()) {
      final int property_id = rs.getInt("property_id");
      for (final FlatProperty p : properties) {
        if (p.id == property_id) {
          final String name = bytes2UTF8(rs.getBytes("name_override"));
          if (name != null) {
            p.name = name;
          }
          final String desc = bytes2UTF8(rs.getBytes("desc_override"));
          if (desc != null) {
            p.desc = desc;
          }
          final String type = bytes2UTF8(rs.getBytes("type_override"));
          if (type != null) {
            p.type = type;
          }
          final String coll = bytes2UTF8(rs.getBytes("collection_override"));
          if (coll != null) {
            p.collection = coll;
          }
        }
      }
    }
  }

  public static List<FlatProperty> parsePropertyResultset(final ResultSet rs) throws SQLException {
    final ArrayList<FlatProperty> ret = new ArrayList<FlatProperty>();
    while (rs.next()) {
      final FlatProperty fp = new FlatProperty();
      fp.id = rs.getInt("PropertyID");

      final String v = bytes2UTF8(rs.getBytes("PropertyValue"));
      if (v != null) {
        fp.value = v;
      }

      fp.status = bytes2UTF8(rs.getBytes("PropertyStatus"));
      fp.idx = rs.getInt("PropertyIndex");
      ret.add(fp);
    }
    return ret;
  }

  public static SparseEntity parseEntityResultSet(final ResultSet rs) throws SQLException {
    if (rs.next()) {
      final SparseEntity ret = new SparseEntity();
      ret.id = rs.getInt("EntityID");
      ret.name = bytes2UTF8(rs.getBytes("EntityName"));
      ret.description = bytes2UTF8(rs.getBytes("EntityDesc"));
      ret.role = bytes2UTF8(rs.getBytes("EntityRole"));
      ret.datatype = bytes2UTF8(rs.getBytes("Datatype"));
      ret.collection = bytes2UTF8(rs.getBytes("Collection"));
      ret.acl = bytes2UTF8(rs.getBytes("ACL"));

      final String path = bytes2UTF8(rs.getBytes("FilePath"));
      if (!rs.wasNull()) {
        ret.filePath = path;
        ret.fileSize = rs.getLong("FileSize");
        ret.fileHash = bytes2UTF8(rs.getBytes("FileHash"));
      }
      return ret;
    }
    return null;
  }

  public static ArrayList<VerySparseEntity> parseParentResultSet(final ResultSet rs)
      throws SQLException {
    final ArrayList<VerySparseEntity> ret = new ArrayList<VerySparseEntity>();
    while (rs.next()) {
      final VerySparseEntity vsp = new VerySparseEntity();
      vsp.id = rs.getInt("ParentID");
      vsp.name = bytes2UTF8(rs.getBytes("ParentName"));
      vsp.description = bytes2UTF8(rs.getBytes("ParentDescription"));
      vsp.role = bytes2UTF8(rs.getBytes("ParentRole"));
      vsp.acl = bytes2UTF8(rs.getBytes("ACL"));
      ret.add(vsp);
    }
    return ret;
  }

  public static <K extends EntityInterface> K parseEntityFromVerySparseEntity(
      final K entity, final VerySparseEntity vse) {
    entity.setId(vse.id);
    entity.setName(vse.name);
    entity.setRole(vse.role);
    entity.setDescription(vse.description);
    return entity;
  }

  public static void parseParentsFromVerySparseEntity(
      final EntityInterface entity, final List<VerySparseEntity> pars) {
    for (final VerySparseEntity vsp : pars) {
      final Parent p = new Parent();
      p.setId(vsp.id);
      p.setName(vsp.name);
      p.setDescription(vsp.description);
      entity.addParent(p);
    }
  }

  private static void replace(final Property p, final HashMap<Integer, Property> domainMap) {
    // ... find the corresponding domain and replace it
    ReferenceValue ref;
    try {
      ref = ReferenceValue.parseReference(p.getValue());
    } catch (final Message e) {
      throw new RuntimeException("This should never happen.");
    }
    final EntityInterface replacement = domainMap.get((ref.getId()));
    if (replacement.isDescOverride()) {
      p.setDescOverride(true);
      p.setDescription(replacement.getDescription());
    }
    if (replacement.isNameOverride()) {
      p.setNameOverride(true);
      p.setName(replacement.getName());
    }
    if (replacement.isDatatypeOverride()) {
      p.setDatatypeOverride(true);
      p.setDatatype(replacement.getDatatype());
    }
    p.setProperties(replacement.getProperties());
    p.setStatementStatus(replacement.getStatementStatus());
    p.setValue(replacement.getValue());
  }

  public static void transformToDeepPropertyTree(
      final EntityInterface e, final List<Property> protoProperties) {
    // here we will store every domain we can find and we will index it by
    // its id.
    final HashMap<Integer, Property> domainMap = new HashMap<Integer, Property>();

    // loop over all properties and collect the domains
    for (final Property p : protoProperties) {
      // if this is a domain it represents a deeper branch of the property
      // tree.
      if (p.getRole() == Role.Domain) {
        if (domainMap.containsKey(p.getId())) {
          // aggregate the multiple values.
          final Property domain = domainMap.get(p.getId());
          if (!(domain.getValue() instanceof CollectionValue)) {
            final SingleValue singleValue = (SingleValue) domain.getValue();
            final CollectionValue vals = new CollectionValue();
            final IndexedSingleValue iSingleValue =
                new IndexedSingleValue(domain.getPIdx(), singleValue);
            vals.add(iSingleValue);
            domain.setValue(vals);
          }
          ((CollectionValue) domain.getValue()).add(p.getPIdx(), p.getValue());
        } else {
          domainMap.put(p.getId(), p);
        }
      }
    }

    // loop over all properties
    for (final Property p : protoProperties) {
      // if this is a replacement
      if (p.getStatementStatus() == StatementStatus.REPLACEMENT) {
        replace(p, domainMap);
      }

      for (final Property subP : p.getProperties()) {
        if (subP.getStatementStatus() == StatementStatus.REPLACEMENT) {
          replace(subP, domainMap);
        }
      }

      if (p.getId().equals(e.getId())) {
        // this is the value of an abstract property.
        e.setValue(p.getValue());
      } else if (p.getRole() != Role.Domain) {
        // if this is not a domain it is to be added to the properties
        // of e.
        e.addProperty(p);
      }
    }
  }

  public static ArrayList<Property> parseFromProtoProperties(final List<ProtoProperty> protos) {
    final ArrayList<Property> ret = new ArrayList<Property>();
    for (final ProtoProperty pp : protos) {
      final Property property = parseFlatProperty(pp.property);
      parseFromFlatProperties(property.getProperties(), pp.subProperties);
      ret.add(property);
    }
    return ret;
  }

  private static void parseFromFlatProperties(
      final List<Property> properties, final List<FlatProperty> props) {
    for (final FlatProperty fp : props) {
      final Property property = parseFlatProperty(fp);
      properties.add(property);
    }
  }

  private static Property parseFlatProperty(final FlatProperty fp) {
    final Property property = new Property();
    property.parseFlatProperty(fp);
    return property;
  }
}
