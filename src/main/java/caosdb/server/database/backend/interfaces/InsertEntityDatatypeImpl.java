package caosdb.server.database.backend.interfaces;

import caosdb.server.database.proto.SparseEntity;
import caosdb.server.utils.Undoable;

public interface InsertEntityDatatypeImpl extends BackendTransactionImpl, Undoable {
  public abstract void execute(SparseEntity entity);
}
