/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.transaction;

import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import caosdb.server.database.CacheableBackendTransaction;
import caosdb.server.database.backend.interfaces.RuleLoaderImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.misc.Cache;
import caosdb.server.database.proto.Rule;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.container.TransactionContainer;
import caosdb.server.jobs.Job;
import caosdb.server.transaction.Transaction;
import java.util.ArrayList;
import org.apache.commons.jcs.access.CacheAccess;

public class RuleLoader extends CacheableBackendTransaction<String, ArrayList<Rule>> {

  private final Transaction<? extends TransactionContainer> transaction;
  private final EntityInterface e;
  private final Integer entity;
  private final Integer domain;
  private ArrayList<Job> jobs;

  public RuleLoader(
      final Integer domain,
      final Integer entity,
      final EntityInterface e,
      final Transaction<? extends TransactionContainer> transaction) {
    this.domain = domain;
    this.entity = entity;
    this.e = e;
    this.transaction = transaction;
  }

  @Override
  protected String getKey() {
    return "<"
        + this.domain
        + ","
        + this.entity
        + ","
        + this.transaction.getClass().getSimpleName()
        + ">";
  }

  @Override
  protected CacheAccess<String, ArrayList<Rule>> getCache() {
    return rulesCache;
  }

  private static CacheAccess<String, ArrayList<Rule>> rulesCache =
      Cache.getCache(
          "RulesCache",
          Integer.valueOf(
              CaosDBServer.getServerProperty(ServerProperties.KEY_RULES_CACHE_CAPACITY)));

  public ArrayList<Job> getJobs() {
    return this.jobs;
  }

  @Override
  public ArrayList<Rule> executeNoCache() throws TransactionException {
    final RuleLoaderImpl t = getImplementation(RuleLoaderImpl.class);
    return t.executeNoCache(this.domain, this.entity, this.transaction.getClass().getSimpleName());
  }

  @Override
  protected void process(final ArrayList<Rule> t) throws TransactionException {
    this.jobs = new ArrayList<Job>();
    for (final Rule r : t) {
      this.jobs.add(Job.getJob(r.job, r.mode, this.e, this.transaction));
    }
  }
}
