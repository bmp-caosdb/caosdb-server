/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.transaction;

import caosdb.server.database.BackendTransaction;
import caosdb.server.database.backend.interfaces.RetrieveLogRecordImpl;
import caosdb.server.database.exceptions.TransactionException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public class RetrieveLogRecord extends BackendTransaction {

  private final Level level;
  private final String logger;
  private List<LogRecord> logRecords;
  private final String message;

  public RetrieveLogRecord(final String logger, final Level level, final String message) {
    this.logger = logger;
    this.level = level;
    this.message = message;
  }

  @Override
  protected void execute() throws TransactionException {
    final RetrieveLogRecordImpl t = getImplementation(RetrieveLogRecordImpl.class);
    this.logRecords = t.retrieve(this.logger, this.level, this.message);
  }

  public List<LogRecord> getLogRecords() {
    return this.logRecords;
  }
}
