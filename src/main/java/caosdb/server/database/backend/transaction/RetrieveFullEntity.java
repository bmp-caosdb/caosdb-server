/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.transaction;

import caosdb.server.database.BackendTransaction;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.RetrieveEntity;
import caosdb.server.entity.Role;
import caosdb.server.entity.container.Container;
import caosdb.server.utils.EntityStatus;

public class RetrieveFullEntity extends BackendTransaction {

  private final Container<? extends EntityInterface> container;

  public RetrieveFullEntity(final EntityInterface entity) {
    final Container<EntityInterface> c = new Container<>();
    c.add(entity);
    this.container = c;
  }

  public RetrieveFullEntity(final Container<? extends EntityInterface> container) {
    this.container = container;
  }

  public RetrieveFullEntity(Integer id) {
    this(new RetrieveEntity(id));
  }

  @Override
  public void execute() {
    for (final EntityInterface e : this.container) {
      if (e.hasId() && e.getId() > 0 && e.getEntityStatus() == EntityStatus.QUALIFIED) {

        execute(new RetrieveSparseEntity(e));
        if (e.getEntityStatus() == EntityStatus.VALID) {
          if (e.getRole() == Role.QueryTemplate) {
            execute(new RetrieveQueryTemplateDefinition(e));
          }
          execute(new RetrieveParents(e));

          execute(new RetrieveProperties(e));
        }
      }
    }
  }

  public Container<? extends EntityInterface> getContainer() {
    return container;
  }
}
