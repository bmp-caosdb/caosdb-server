/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.transaction;

import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import caosdb.server.accessControl.Principal;
import caosdb.server.database.CacheableBackendTransaction;
import caosdb.server.database.backend.interfaces.RetrieveUserImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.misc.Cache;
import caosdb.server.database.proto.ProtoUser;
import org.apache.commons.jcs.access.CacheAccess;

public class RetrieveUser extends CacheableBackendTransaction<Principal, ProtoUser> {

  private ProtoUser user;
  private static final CacheAccess<Principal, ProtoUser> cache =
      Cache.getCache(
          "UserAccountCache",
          Integer.valueOf(
              CaosDBServer.getServerProperty(ServerProperties.KEY_USER_ACCOUNT_CACHE_CAPACITY)));
  private final Principal principal;

  /**
   * To be called by DeleteSparseEntity, SetPassword, and UpdateSparseEntity on execution.
   *
   * @param u
   */
  public static void removeCached(final Principal principal) {
    if (principal != null && cache != null) {
      cache.remove(principal);
    }
  }

  @Override
  protected CacheAccess<Principal, ProtoUser> getCache() {
    return cache;
  }

  public RetrieveUser(final Principal principal) {
    this.principal = principal;
  }

  @Override
  protected Principal getKey() {
    return this.principal;
  }

  @Override
  public ProtoUser executeNoCache() throws TransactionException {
    final RetrieveUserImpl t = getImplementation(RetrieveUserImpl.class);
    return t.execute(this.principal);
  }

  @Override
  protected void process(final ProtoUser t) throws TransactionException {
    this.user = t;
  }

  public ProtoUser getUser() {
    return this.user;
  }
}
