/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.transaction;

import caosdb.server.database.BackendTransaction;
import caosdb.server.database.DatabaseUtils;
import caosdb.server.database.backend.interfaces.InsertEntityPropertiesImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.proto.FlatProperty;
import caosdb.server.datatype.AbstractCollectionDatatype;
import caosdb.server.datatype.AbstractDatatype.Table;
import caosdb.server.datatype.CollectionValue;
import caosdb.server.datatype.SingleValue;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.Role;
import caosdb.server.entity.StatementStatus;
import caosdb.server.entity.wrapper.Property;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class InsertEntityProperties extends BackendTransaction {

  private final EntityInterface entity;

  public InsertEntityProperties(final EntityInterface entity) {
    this.entity = entity;
  }

  @Override
  public void execute() throws TransactionException {

    final InsertEntityPropertiesImpl t = getImplementation(InsertEntityPropertiesImpl.class);

    final ArrayList<EntityInterface> stage1Inserts = new ArrayList<EntityInterface>();
    final ArrayList<EntityInterface> stage2Inserts = new ArrayList<EntityInterface>();

    DatabaseUtils.deriveStage1Inserts(stage1Inserts, this.entity);

    final int domainCount = DatabaseUtils.deriveStage2Inserts(stage2Inserts, stage1Inserts);

    final Deque<Integer> domainIds = execute(new RegisterSubDomain(domainCount)).getDomains();

    insertStages(t, domainIds, stage1Inserts, this.entity.getDomain(), this.entity.getId());
    insertStages(t, domainIds, stage2Inserts, this.entity.getId(), null);
  }

  private void insertStages(
      final InsertEntityPropertiesImpl t,
      final Deque<Integer> domainIds,
      final List<EntityInterface> stage1Inserts,
      final Integer domain,
      final Integer entity)
      throws TransactionException {

    for (final EntityInterface property : stage1Inserts) {
      if (property.hasRole() && property.getRole() == Role.Domain && property.getId() == null) {
        property.setId(domainIds.removeFirst());
      }
      int pIdx;
      if (property instanceof Property) {
        // this is a normal property
        pIdx = ((Property) property).getPIdx();
      } else {
        // this is a replacement
        pIdx = 0;
      }

      // prepare flat property
      final FlatProperty fp = new FlatProperty();
      Table table = Table.null_data;
      Long unit_sig = null;
      fp.id = property.getId();
      fp.idx = pIdx;

      if (property.hasReplacement()) {
        if (property.getReplacement().getId() == null) {
          property.getReplacement().setId(domainIds.removeFirst());
        }

        fp.value = property.getReplacement().getId().toString();
        fp.status = StatementStatus.REPLACEMENT.toString();

        table = Table.reference_data;
      } else {
        if (property.hasUnit()) {
          unit_sig = property.getUnit().getSignature();
        }
        fp.status = property.getStatementStatus().toString();
        if (property.hasValue()) {
          if (property.getValue() instanceof CollectionValue) {
            // insert collection of values
            final CollectionValue v = (CollectionValue) property.getValue();

            // insert 2nd to nth item
            for (int i = 1; i < v.size(); i++) {
              final SingleValue vi = v.get(i);
              table = (vi != null ? vi.getTable() : Table.null_data);
              fp.idx = i;
              fp.value = (vi != null ? vi.toDatabaseString() : null);
              t.execute(
                  domain, (entity != null ? entity : property.getDomain()), fp, table, unit_sig);
            }

            // insert first item
            final SingleValue vi = v.get(0);
            fp.idx = 0;
            fp.value = vi.toDatabaseString();
            table = (vi != null ? vi.getTable() : Table.null_data);

          } else {
            // insert single value
            fp.value = ((SingleValue) property.getValue()).toDatabaseString();
            if (property instanceof Property && ((Property) property).isName()) {
              table = Table.name_data;
            } else {
              table = ((SingleValue) property.getValue()).getTable();
            }
          }
        }
        if (property.isNameOverride()) {
          fp.name = property.getName();
        }
        if (property.isDescOverride()) {
          fp.desc = property.getDescription();
        }
        if (property.isDatatypeOverride()) {
          if (property.getDatatype() instanceof AbstractCollectionDatatype) {
            fp.type =
                ((AbstractCollectionDatatype) property.getDatatype())
                    .getDatatype()
                    .getId()
                    .toString();
            fp.collection =
                ((AbstractCollectionDatatype) property.getDatatype()).getCollectionName();
          } else {
            fp.type = property.getDatatype().getId().toString();
          }
        }
      }

      t.execute(domain, (entity != null ? entity : property.getDomain()), fp, table, unit_sig);
    }
  }
}
