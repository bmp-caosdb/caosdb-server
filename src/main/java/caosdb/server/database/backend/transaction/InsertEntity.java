/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.transaction;

import caosdb.server.database.BackendTransaction;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.container.TransactionContainer;
import caosdb.server.utils.EntityStatus;

public class InsertEntity extends BackendTransaction {

  private final TransactionContainer container;

  public InsertEntity(final TransactionContainer container) {
    this.container = container;
  }

  @Override
  public void execute() {
    for (final EntityInterface newEntity : this.container) {
      if (newEntity.getEntityStatus() == EntityStatus.QUALIFIED) {
        execute(new InsertSparseEntity(newEntity));
      }
    }
    for (final EntityInterface newEntity : this.container) {
      if (newEntity.getEntityStatus() == EntityStatus.QUALIFIED && newEntity.hasDatatype()) {
        execute(new InsertEntityDatatype(newEntity));
      }
    }
    for (final EntityInterface newEntity : this.container) {
      if (newEntity.getEntityStatus() == EntityStatus.QUALIFIED) {
        if (newEntity.getQueryTemplateDefinition() != null) {
          execute(new SetQueryTemplateDefinition(newEntity));
        }
        if (newEntity.hasFileProperties()) {
          execute(new InsertFile(newEntity));
        }
        execute(new InsertEntityProperties(newEntity));
        execute(new InsertParents(newEntity));

        if (newEntity.getEntityStatus() == EntityStatus.QUALIFIED) {
          newEntity.setEntityStatus(EntityStatus.VALID);
        }
      }
    }
  }
}
