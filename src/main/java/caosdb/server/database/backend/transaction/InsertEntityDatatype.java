package caosdb.server.database.backend.transaction;

import static caosdb.server.transaction.Transaction.ERROR_INTEGRITY_VIOLATION;

import caosdb.server.database.BackendTransaction;
import caosdb.server.database.backend.interfaces.InsertEntityDatatypeImpl;
import caosdb.server.database.exceptions.IntegrityException;
import caosdb.server.database.proto.SparseEntity;
import caosdb.server.entity.EntityInterface;

public class InsertEntityDatatype extends BackendTransaction {

  private final EntityInterface entity;

  public InsertEntityDatatype(final EntityInterface entity) {
    this.entity = entity;
  }

  @Override
  public void execute() {
    final InsertEntityDatatypeImpl t = getImplementation(InsertEntityDatatypeImpl.class);

    final SparseEntity e = this.entity.getSparseEntity();

    try {
      t.execute(e);
    } catch (final IntegrityException exc) {
      this.entity.addError(ERROR_INTEGRITY_VIOLATION);
      throw exc;
    }

    this.entity.setId(e.id);
  }
}
