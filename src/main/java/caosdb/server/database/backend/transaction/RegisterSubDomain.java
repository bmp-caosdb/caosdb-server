/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.transaction;

import caosdb.server.database.BackendTransaction;
import caosdb.server.database.backend.interfaces.RegisterSubDomainImpl;
import caosdb.server.database.exceptions.TransactionException;
import java.util.Deque;

public class RegisterSubDomain extends BackendTransaction {

  private final int domainCount;
  private Deque<Integer> list;

  public RegisterSubDomain(final int domainCount) {
    this.domainCount = domainCount;
  }

  @Override
  public void execute() throws TransactionException {
    final RegisterSubDomainImpl t = getImplementation(RegisterSubDomainImpl.class);
    this.list = t.execute(this.domainCount);
  }

  public Deque<Integer> getDomains() {
    return this.list;
  }
}
