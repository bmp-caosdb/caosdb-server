/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.transaction;

import caosdb.datetime.UTCDateTime;
import caosdb.server.database.BackendTransaction;
import caosdb.server.database.backend.interfaces.InsertTransactionHistoryImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.container.TransactionContainer;
import caosdb.server.utils.EntityStatus;

public class InsertTransactionHistory extends BackendTransaction {

  private final TransactionContainer container;
  private final String transaction;
  private final UTCDateTime datetime;
  private final String user;
  private final String realm;

  public InsertTransactionHistory(
      final TransactionContainer container,
      final String transaction,
      final String realm,
      final String user,
      final UTCDateTime timestamp) {
    this.container = container;
    this.transaction = transaction;
    this.user = user;
    this.datetime = timestamp;
    this.realm = realm;
  }

  @Override
  public void execute() throws TransactionException {
    final InsertTransactionHistoryImpl t = getImplementation(InsertTransactionHistoryImpl.class);

    for (final EntityInterface e : this.container) {
      if (e.getEntityStatus() == EntityStatus.DELETED
          || e.getEntityStatus() == EntityStatus.VALID) {

        t.execute(
            this.transaction,
            this.realm,
            this.user,
            this.datetime.getUTCSeconds(),
            this.datetime.getNanoseconds(),
            e.getId());
      }
    }
  }
}
