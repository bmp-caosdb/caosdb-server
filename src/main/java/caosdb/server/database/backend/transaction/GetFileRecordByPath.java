/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.transaction;

import caosdb.server.database.CacheableBackendTransaction;
import caosdb.server.database.backend.interfaces.GetFileRecordByPathImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.proto.SparseEntity;

public class GetFileRecordByPath extends CacheableBackendTransaction<String, SparseEntity> {

  private final String path;
  private SparseEntity entity;

  public GetFileRecordByPath(final String path) {
    this.path = path;
  }

  @Override
  protected String getKey() {
    return this.path;
  }

  @Override
  protected void process(final SparseEntity t) throws TransactionException {
    this.entity = t;
  }

  @Override
  public SparseEntity executeNoCache() throws TransactionException {
    final GetFileRecordByPathImpl t = getImplementation(GetFileRecordByPathImpl.class);
    return t.execute(getKey());
  }

  public Integer getId() {
    return this.entity.id;
  }

  public Long getSize() {
    return this.entity.fileSize;
  }

  public String getHash() {
    return this.entity.fileHash;
  }

  public Long getLastConsistencyCheck() {
    return this.entity.fileChecked;
  }

  public SparseEntity getEntity() {
    return this.entity;
  }
}
