/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.transaction;

import caosdb.server.database.BackendTransaction;
import caosdb.server.database.backend.interfaces.RetrieveAllImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.entity.RetrieveEntity;
import caosdb.server.entity.container.TransactionContainer;
import java.util.List;

public class RetrieveAll extends BackendTransaction {

  private final TransactionContainer container;
  private final String role;

  public RetrieveAll(final TransactionContainer container, final String role) {
    this.container = container;
    this.role = role;
  }

  @Override
  public void execute() throws TransactionException {
    final RetrieveAllImpl t = getImplementation(RetrieveAllImpl.class);

    final List<Integer> list = t.execute(this.role);

    for (final Integer id : list) {
      this.container.add(new RetrieveEntity(id));
    }
  }
}
