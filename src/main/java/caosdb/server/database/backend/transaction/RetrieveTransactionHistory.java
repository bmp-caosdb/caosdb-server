/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.transaction;

import caosdb.datetime.UTCDateTime;
import caosdb.server.database.CacheableBackendTransaction;
import caosdb.server.database.backend.interfaces.RetrieveTransactionHistoryImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.proto.ProtoTransactionLogMessage;
import caosdb.server.entity.EntityInterface;
import caosdb.server.utils.TransactionLogMessage;
import java.util.ArrayList;
import org.apache.commons.jcs.access.CacheAccess;

public class RetrieveTransactionHistory
    extends CacheableBackendTransaction<Integer, ArrayList<ProtoTransactionLogMessage>> {

  private final EntityInterface entity;

  public RetrieveTransactionHistory(final EntityInterface entity) {
    this.entity = entity;
  }

  @Override
  protected Integer getKey() {
    return this.entity.getId();
  }

  @Override
  protected CacheAccess<Integer, ArrayList<ProtoTransactionLogMessage>> getCache() {
    return null;
  }

  @Override
  public ArrayList<ProtoTransactionLogMessage> executeNoCache() throws TransactionException {
    final RetrieveTransactionHistoryImpl t =
        getImplementation(RetrieveTransactionHistoryImpl.class);
    return t.execute(getKey());
  }

  @Override
  protected void process(final ArrayList<ProtoTransactionLogMessage> l)
      throws TransactionException {
    for (final ProtoTransactionLogMessage t : l) {
      final UTCDateTime dateTime = UTCDateTime.UTCSeconds(t.seconds, t.nanos);

      this.entity.addTransactionLog(
          new TransactionLogMessage(t.transaction, this.entity, t.username, dateTime));
    }
  }
}
