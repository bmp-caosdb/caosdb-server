/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.transaction;

import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import caosdb.server.database.CacheableBackendTransaction;
import caosdb.server.database.DatabaseUtils;
import caosdb.server.database.backend.interfaces.RetrieveSparseEntityImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.misc.Cache;
import caosdb.server.database.proto.SparseEntity;
import caosdb.server.entity.Entity;
import caosdb.server.entity.EntityInterface;
import caosdb.server.utils.EntityStatus;
import org.apache.commons.jcs.access.CacheAccess;

public class RetrieveSparseEntity extends CacheableBackendTransaction<Integer, SparseEntity> {

  private final EntityInterface entity;
  private static final CacheAccess<Integer, SparseEntity> cache =
      Cache.getCache(
          "SparseEntityCache",
          Integer.valueOf(
              CaosDBServer.getServerProperty(ServerProperties.KEY_SPARSE_ENTITY_CACHE_CAPACITY)));

  /**
   * To be called by UpdateSparseEntity and DeleteEntity on execution.
   *
   * @param id
   */
  public static void removeCached(final Integer id) {
    if (id != null && cache != null) {
      cache.remove(id);
    }
  }

  @Override
  protected CacheAccess<Integer, SparseEntity> getCache() {
    return cache;
  }

  public RetrieveSparseEntity(final EntityInterface entity) {
    this.entity = entity;
  }

  public RetrieveSparseEntity(final int id) {
    this.entity = new Entity(id);
  }

  @Override
  public SparseEntity executeNoCache() throws TransactionException {
    final RetrieveSparseEntityImpl t = getImplementation(RetrieveSparseEntityImpl.class);
    final SparseEntity ret = t.execute(getKey());
    if (ret == null) {
      this.entity.setEntityStatus(EntityStatus.NONEXISTENT);
    }
    return ret;
  }

  @Override
  protected void process(final SparseEntity t) throws TransactionException {
    DatabaseUtils.parseFromSparseEntities(this.entity, t);
    this.entity.setEntityStatus(EntityStatus.VALID);
  }

  @Override
  protected Integer getKey() {
    return this.entity.getId();
  }

  public EntityInterface getEntity() {
    return this.entity;
  }
}
