/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.transaction;

import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import caosdb.server.database.CacheableBackendTransaction;
import caosdb.server.database.DatabaseUtils;
import caosdb.server.database.backend.interfaces.RetrievePropertiesImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.misc.Cache;
import caosdb.server.database.proto.ProtoProperty;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.Role;
import caosdb.server.entity.wrapper.Property;
import java.util.ArrayList;
import org.apache.commons.jcs.access.CacheAccess;

public class RetrieveProperties
    extends CacheableBackendTransaction<Integer, ArrayList<ProtoProperty>> {

  private final EntityInterface entity;
  private static final CacheAccess<Integer, ArrayList<ProtoProperty>> cache =
      Cache.getCache(
          "PropertiesCache",
          Integer.valueOf(
              CaosDBServer.getServerProperty(ServerProperties.KEY_PROPERTIES_CACHE_CAPACITY)));

  /**
   * To be called by DeleteEntityProperties on execution.
   *
   * @param id
   */
  protected static void removeCached(final Integer id) {
    if (id != null && cache != null) {
      cache.remove(id);
    }
  }

  @Override
  protected CacheAccess<Integer, ArrayList<ProtoProperty>> getCache() {
    return cache;
  }

  public RetrieveProperties(final EntityInterface entity) {
    this.entity = entity;
  }

  @Override
  public ArrayList<ProtoProperty> executeNoCache() throws TransactionException {
    final RetrievePropertiesImpl t = getImplementation(RetrievePropertiesImpl.class);
    return t.execute(getKey());
  }

  @Override
  protected void process(final ArrayList<ProtoProperty> t) throws TransactionException {
    this.entity.getProperties().clear();

    final ArrayList<Property> props = DatabaseUtils.parseFromProtoProperties(t);

    for (final Property p : props) {
      // retrieve sparse properties stage 1
      final RetrieveSparseEntity t1 = new RetrieveSparseEntity(p);
      execute(t1);

      // add default data type for record types
      if (!p.hasDatatype() && p.getRole() == Role.RecordType) {
        p.setDatatype(p.getName());
      }

      // retrieve sparse properties stage 2
      for (final EntityInterface subP : p.getProperties()) {
        final RetrieveSparseEntity t2 = new RetrieveSparseEntity(subP);
        execute(t2);

        // add default data type for record types
        if (!subP.hasDatatype() && subP.getRole() == Role.RecordType) {
          subP.setDatatype(subP.getName());
        }
      }
    }

    DatabaseUtils.transformToDeepPropertyTree(this.entity, props);
  }

  @Override
  protected Integer getKey() {
    return this.entity.getId();
  }
}
