/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.transaction;

import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import caosdb.server.database.CacheableBackendTransaction;
import caosdb.server.database.DatabaseUtils;
import caosdb.server.database.backend.interfaces.RetrieveParentsImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.misc.Cache;
import caosdb.server.database.proto.VerySparseEntity;
import caosdb.server.entity.EntityInterface;
import java.util.ArrayList;
import org.apache.commons.jcs.access.CacheAccess;

public class RetrieveParents
    extends CacheableBackendTransaction<Integer, ArrayList<VerySparseEntity>> {

  private static final CacheAccess<Integer, ArrayList<VerySparseEntity>> cache =
      Cache.getCache(
          "ParentsCache",
          Integer.valueOf(
              CaosDBServer.getServerProperty(ServerProperties.KEY_SPARSE_ENTITY_CACHE_CAPACITY)));

  /**
   * To be called by DeleteEntityProperties on execution.
   *
   * @param id
   */
  public static void removeCached(final Integer id) {
    if (id != null && cache != null) {
      cache.remove(id);
    }
  }

  @Override
  protected CacheAccess<Integer, ArrayList<VerySparseEntity>> getCache() {
    return cache;
  }

  private final EntityInterface entity;

  public RetrieveParents(final EntityInterface entity) {
    this.entity = entity;
  }

  @Override
  public ArrayList<VerySparseEntity> executeNoCache() throws TransactionException {
    final RetrieveParentsImpl t = getImplementation(RetrieveParentsImpl.class);
    final Integer key = getKey();
    return t.execute(key);
  }

  @Override
  protected void process(final ArrayList<VerySparseEntity> t) throws TransactionException {
    this.entity.getParents().clear();
    DatabaseUtils.parseParentsFromVerySparseEntity(this.entity, t);
  }

  @Override
  protected Integer getKey() {
    return this.entity.getId();
  }
}
