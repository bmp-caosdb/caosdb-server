package caosdb.server.database.backend.implementation.MySQL;

import caosdb.server.database.access.Access;
import caosdb.server.database.backend.interfaces.InsertEntityDatatypeImpl;
import caosdb.server.database.exceptions.IntegrityException;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.proto.SparseEntity;
import java.sql.PreparedStatement;
import java.sql.SQLIntegrityConstraintViolationException;

public class MySQLInsertEntityDatatype extends MySQLTransaction
    implements InsertEntityDatatypeImpl {

  public MySQLInsertEntityDatatype(Access access) {
    super(access);
  }

  public static final String STMT_INSERT_ENTITY_DATATYPE =
      "INSERT INTO data_type (domain_id, entity_id, property_id, datatype) SELECT 0, 0, ?, ( SELECT id from entities where name = ? LIMIT 1);";
  public static final String STMT_INSERT_ENTITY_COLLECTION =
      "INSERT INTO collection_type (domain_id, entity_id, property_id, collection) SELECT 0, 0, ?, ?;";

  @Override
  public void execute(final SparseEntity entity) {
    try {
      final PreparedStatement insertEntityDatatypeStmt =
          prepareStatement(STMT_INSERT_ENTITY_DATATYPE);

      insertEntityDatatypeStmt.setInt(1, entity.id);
      insertEntityDatatypeStmt.setString(2, entity.datatype);

      insertEntityDatatypeStmt.execute();

      if (entity.collection != null) {
        final PreparedStatement insertEntityCollectionStmt =
            prepareStatement(STMT_INSERT_ENTITY_COLLECTION);

        insertEntityCollectionStmt.setInt(1, entity.id);
        insertEntityCollectionStmt.setString(2, entity.collection);

        insertEntityCollectionStmt.execute();
      }

    } catch (final SQLIntegrityConstraintViolationException exc) {
      throw new IntegrityException(exc);
    } catch (final TransactionException exc) {
      throw exc;
    } catch (final Exception exc) {
      throw new TransactionException(exc);
    }
  }
}
