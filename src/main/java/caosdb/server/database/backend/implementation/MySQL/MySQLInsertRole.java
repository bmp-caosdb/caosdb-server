/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.implementation.MySQL;

import caosdb.server.accessControl.Role;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.interfaces.InsertRoleImpl;
import caosdb.server.database.exceptions.TransactionException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MySQLInsertRole extends MySQLTransaction implements InsertRoleImpl {

  public MySQLInsertRole(final Access access) {
    super(access);
  }

  public static final String STMT_INSERT_ROLE =
      "INSERT INTO roles (name, description) VALUES (?,?) ON DUPLICATE KEY UPDATE description=?;";

  @Override
  public void insertRole(final Role role) throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_INSERT_ROLE);
      stmt.setString(1, role.name);
      stmt.setString(2, role.description);
      stmt.setString(3, role.description);
      stmt.execute();
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
