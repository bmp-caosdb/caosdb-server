/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.implementation.MySQL;

import caosdb.server.database.access.Access;
import caosdb.server.database.backend.interfaces.InsertLinConImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.proto.LinCon;
import java.sql.PreparedStatement;

public class MySQLInsertLinCon extends MySQLTransaction implements InsertLinConImpl {

  public MySQLInsertLinCon(final Access access) {
    super(access);
  }

  public static final String STMT_INSERT_LIN_CON = "call insertLinCon(?,?,?,?,?,?)";

  @Override
  public void execute(final LinCon linCon) throws TransactionException {
    try {
      final PreparedStatement statement = prepareStatement(STMT_INSERT_LIN_CON);

      statement.setLong(1, linCon.signature_from);
      statement.setLong(2, linCon.signature_to);
      statement.setDouble(3, linCon.a);
      statement.setLong(4, linCon.b_dividend);
      statement.setLong(5, linCon.b_divisor);
      statement.setDouble(6, linCon.c);
      statement.execute();
    } catch (final Exception e) {
      throw new TransactionException(e);
    }
  }
}
