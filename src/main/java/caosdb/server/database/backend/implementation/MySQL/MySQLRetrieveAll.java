/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.implementation.MySQL;

import caosdb.server.database.access.Access;
import caosdb.server.database.backend.interfaces.RetrieveAllImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.entity.Role;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySQLRetrieveAll extends MySQLTransaction implements RetrieveAllImpl {

  public MySQLRetrieveAll(final Access access) {
    super(access);
  }

  public static final String STMT_GET_ALL_HEAD = "Select id from entities where ";
  public static final String STMT_ENTITY_WHERE_CLAUSE =
      " ( role=? OR role='"
          + Role.RecordType
          + "' OR role='"
          + Role.Property
          + "' OR role='"
          + Role.File
          + "'"
          + " ) AND ( NOT name=role OR name IS NULL)";
  public static final String STMT_OTHER_ROLES = " role=? AND ( NOT name=role OR name IS NULL)";

  @Override
  public List<Integer> execute(final String role) throws TransactionException {
    try {
      final String STMT_GET_ALL =
          STMT_GET_ALL_HEAD
              + (role.equalsIgnoreCase("ENTITY") ? STMT_ENTITY_WHERE_CLAUSE : STMT_OTHER_ROLES);
      final PreparedStatement stmt = prepareStatement(STMT_GET_ALL);

      if (role.equalsIgnoreCase("ENTITY")) {
        stmt.setString(1, Role.Record.toString());

      } else {
        stmt.setString(1, role);
      }

      final ResultSet rs = stmt.executeQuery();
      try {
        final ArrayList<Integer> ret = new ArrayList<Integer>();
        while (rs.next()) {
          ret.add(rs.getInt(1));
        }
        return ret;
      } finally {
        rs.close();
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
