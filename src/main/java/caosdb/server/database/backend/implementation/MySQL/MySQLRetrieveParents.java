/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.implementation.MySQL;

import caosdb.server.database.DatabaseUtils;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.interfaces.RetrieveParentsImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.proto.VerySparseEntity;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MySQLRetrieveParents extends MySQLTransaction implements RetrieveParentsImpl {

  public MySQLRetrieveParents(final Access access) {
    super(access);
  }

  private static final String stmtStr = "call retrieveEntityParents(?)";

  @Override
  public ArrayList<VerySparseEntity> execute(final Integer id) throws TransactionException {
    try {
      ResultSet rs = null;
      try {
        final PreparedStatement prepareStatement = prepareStatement(stmtStr);

        prepareStatement.setInt(1, id);
        rs = prepareStatement.executeQuery();
        return DatabaseUtils.parseParentResultSet(rs);
      } finally {
        if (rs != null && !rs.isClosed()) {
          rs.close();
        }
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
