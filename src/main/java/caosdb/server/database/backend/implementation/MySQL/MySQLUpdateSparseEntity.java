/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.implementation.MySQL;

import caosdb.server.database.access.Access;
import caosdb.server.database.backend.interfaces.UpdateSparseEntityImpl;
import caosdb.server.database.exceptions.IntegrityException;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.proto.SparseEntity;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Types;

public class MySQLUpdateSparseEntity extends MySQLTransaction implements UpdateSparseEntityImpl {

  public MySQLUpdateSparseEntity(final Access access) {
    super(access);
  }

  public static final String STMT_UPDATE_ENTITY = "call updateEntity(?,?,?,?,?,?,?)";
  public static final String STMT_UPDATE_FILE_PROPS =
      "INSERT INTO files (hash, size, path, file_id) VALUES (unhex(?),?,?,?) ON DUPLICATE KEY UPDATE hash=unhex(?), size=?, path=?;";

  @Override
  public void execute(final SparseEntity spe) throws TransactionException {
    try {
      final PreparedStatement updateEntityStmt = prepareStatement(STMT_UPDATE_ENTITY);

      // very sparse entity
      updateEntityStmt.setInt(1, spe.id);
      updateEntityStmt.setString(2, spe.name);
      updateEntityStmt.setString(3, spe.description);
      updateEntityStmt.setString(4, spe.role);
      if (spe.datatype != null) {
        updateEntityStmt.setString(5, spe.datatype);
      } else {
        updateEntityStmt.setNull(5, Types.VARCHAR);
      }
      updateEntityStmt.setString(6, spe.collection);
      updateEntityStmt.setString(7, spe.acl);
      updateEntityStmt.execute();

      // file properties;
      if (spe.filePath != null) {
        final PreparedStatement updateFilePropsStmt = prepareStatement(STMT_UPDATE_FILE_PROPS);
        if (spe.fileHash != null) {
          updateFilePropsStmt.setString(1, spe.fileHash);
          updateFilePropsStmt.setString(5, spe.fileHash);
        } else {
          updateFilePropsStmt.setNull(1, Types.VARCHAR);
          updateFilePropsStmt.setNull(5, Types.VARCHAR);
        }
        updateFilePropsStmt.setLong(2, spe.fileSize);
        updateFilePropsStmt.setLong(6, spe.fileSize);

        updateFilePropsStmt.setString(3, spe.filePath);
        updateFilePropsStmt.setString(7, spe.filePath);

        updateFilePropsStmt.setInt(4, spe.id);

        updateFilePropsStmt.execute();
      }
    } catch (final SQLIntegrityConstraintViolationException e) {
      throw new IntegrityException(e);
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
