/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.implementation.MySQL;

import caosdb.server.database.DatabaseUtils;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.interfaces.RetrievePropertiesImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.proto.FlatProperty;
import caosdb.server.database.proto.ProtoProperty;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySQLRetrieveProperties extends MySQLTransaction implements RetrievePropertiesImpl {

  public MySQLRetrieveProperties(final Access access) {
    super(access);
  }

  private static final String stmtStr = "call retrieveEntityProperties(?,?)";
  private static final String stmtStr2 = "call retrieveOverrides(?,?)";

  @Override
  public ArrayList<ProtoProperty> execute(final Integer entity) throws TransactionException {
    try {
      final PreparedStatement prepareStatement = prepareStatement(stmtStr);

      final List<FlatProperty> props = retrieveFlatPropertiesStage1(0, entity, prepareStatement);
      final ArrayList<ProtoProperty> protos = new ArrayList<ProtoProperty>();
      for (final FlatProperty p : props) {
        final ProtoProperty proto = new ProtoProperty();
        proto.property = p;

        final List<FlatProperty> subProps =
            retrieveFlatPropertiesStage1(entity, p.id, prepareStatement);
        proto.subProperties = subProps;

        protos.add(proto);
      }
      return protos;
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }

  private List<FlatProperty> retrieveFlatPropertiesStage1(
      final Integer domain, final Integer entity, final PreparedStatement stmt)
      throws SQLException, ConnectionException {
    ResultSet rs = null;
    try {
      if (domain != null && domain >= 0) {
        stmt.setInt(1, domain);
      } else {
        stmt.setInt(1, 0);
      }

      stmt.setInt(2, entity);
      rs = stmt.executeQuery();

      final List<FlatProperty> props = DatabaseUtils.parsePropertyResultset(rs);

      final PreparedStatement stmt2 = prepareStatement(stmtStr2);

      retrieveOverrides(domain, entity, stmt2, props);

      return props;
    } finally {
      if (rs != null && !rs.isClosed()) {
        rs.close();
      }
    }
  }

  private void retrieveOverrides(
      final Integer domain,
      final Integer entity,
      final PreparedStatement stmt2,
      final List<FlatProperty> props)
      throws SQLException {

    ResultSet rs = null;
    try {
      stmt2.setInt(1, domain);
      stmt2.setInt(2, entity);
      rs = stmt2.executeQuery();
      DatabaseUtils.parseOverrides(props, rs);
    } finally {
      if (rs != null && !rs.isClosed()) {
        rs.close();
      }
    }
  }
}
