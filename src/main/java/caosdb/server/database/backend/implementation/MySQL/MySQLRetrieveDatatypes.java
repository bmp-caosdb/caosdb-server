/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.implementation.MySQL;

import caosdb.server.database.DatabaseUtils;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.interfaces.RetrieveDatatypesImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.proto.VerySparseEntity;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MySQLRetrieveDatatypes extends MySQLTransaction implements RetrieveDatatypesImpl {

  public MySQLRetrieveDatatypes(final Access access) {
    super(access);
  }

  private static final String STMT_GET_DATATYPE =
      "select id AS ParentID, name AS ParentName, description as ParentDescription, role as ParentRole, (SELECT acl FROM entity_acl as a WHERE a.id=e.acl) as ACL from entities as e where e.role='DATATYPE'";

  @Override
  public ArrayList<VerySparseEntity> execute() throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_GET_DATATYPE);

      final ResultSet rs = stmt.executeQuery();
      try {
        return DatabaseUtils.parseParentResultSet(rs);
      } finally {
        rs.close();
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
