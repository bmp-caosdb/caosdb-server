/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.implementation.MySQL;

import caosdb.server.database.access.Access;
import caosdb.server.database.backend.interfaces.GetIDByNameImpl;
import caosdb.server.database.exceptions.TransactionException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class MySQLGetIDByName extends MySQLTransaction implements GetIDByNameImpl {

  public MySQLGetIDByName(final Access access) {
    super(access);
  }

  public static final String STMT_GET_ID_BY_NAME = "Select id from entities where name=?";
  public static final String STMT_AND_ROLE = " AND role=?";
  public static final String STMT_NOT_ROLE = " AND role!='ROLE'";
  public static final String STMT_LIMIT = " LIMIT ";

  @Override
  public List<Integer> execute(final String name, final String role, final String limit)
      throws TransactionException {
    try {
      final String stmtStr =
          STMT_GET_ID_BY_NAME
              + (role != null ? STMT_AND_ROLE : STMT_NOT_ROLE)
              + (limit != null ? STMT_LIMIT + limit : "");
      final PreparedStatement stmt = prepareStatement(stmtStr);

      stmt.setString(1, name);
      if (role != null) {
        stmt.setString(2, role);
      }
      ResultSet rs = null;
      try {
        rs = stmt.executeQuery();
        final ArrayList<Integer> ret = new ArrayList<Integer>();
        while (rs.next()) {
          ret.add(rs.getInt("id"));
        }

        return ret;
      } finally {
        rs.close();
      }
    } catch (final Exception e) {
      throw new TransactionException(e);
    }
  }
}
