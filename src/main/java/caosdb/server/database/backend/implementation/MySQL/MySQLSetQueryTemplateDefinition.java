/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.implementation.MySQL;

import caosdb.server.database.access.Access;
import caosdb.server.database.backend.interfaces.SetQueryTemplateDefinitionImpl;
import caosdb.server.database.exceptions.TransactionException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MySQLSetQueryTemplateDefinition extends MySQLTransaction
    implements SetQueryTemplateDefinitionImpl {

  public MySQLSetQueryTemplateDefinition(final Access access) {
    super(access);
  }

  public static final String STMT_INSERT_QUERY_TEMPLATE_DEF =
      "INSERT INTO query_template_def (id, definition) VALUES (?,?) ON DUPLICATE KEY UPDATE definition=?;";

  @Override
  public void insert(final Integer id, final String definition) {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_INSERT_QUERY_TEMPLATE_DEF);
      stmt.setInt(1, id);
      stmt.setString(2, definition);
      stmt.setString(3, definition);
      stmt.execute();
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
