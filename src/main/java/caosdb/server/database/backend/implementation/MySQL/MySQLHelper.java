/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.implementation.MySQL;

import caosdb.server.database.misc.DBHelper;
import caosdb.server.transaction.TransactionInterface;
import caosdb.server.transaction.WriteTransaction;
import caosdb.server.utils.Info;
import caosdb.server.utils.Initialization;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Provides cached statements for a MySQL back-end.
 *
 * @author tf
 */
public class MySQLHelper implements DBHelper {

  private Connection connection = null;

  public Connection getConnection() throws SQLException, ConnectionException {
    if (this.connection == null) {
      this.connection = DatabaseConnectionPool.getConnection();
      if (this.transaction instanceof WriteTransaction) {
        this.connection.setReadOnly(false);
        this.connection.setAutoCommit(false);
      } else if (this.transaction instanceof Initialization || this.transaction instanceof Info) {
        this.connection.setReadOnly(false);
        this.connection.setAutoCommit(true);
      } else {
        this.connection.setReadOnly(false);
        this.connection.setAutoCommit(true);
      }
    }
    return this.connection;
  }

  public PreparedStatement prepareStatement(final String statement)
      throws SQLException, ConnectionException {
    if (this.stmtCache.containsKey(statement)) {
      final PreparedStatement ret = this.stmtCache.get(statement);
      if (!ret.isClosed()) {
        return ret;
      }
    }

    final PreparedStatement stmt = getConnection().prepareStatement(statement);
    this.stmtCache.put(statement, stmt);
    return stmt;
  }

  private HashMap<String, PreparedStatement> stmtCache = new HashMap<String, PreparedStatement>();

  @Override
  public void setHelped(final TransactionInterface transaction) {
    this.transaction = transaction;
  }

  private TransactionInterface transaction = null;

  @Override
  public void commit() throws SQLException {
    if (this.connection != null
        && !this.connection.isClosed()
        && !this.connection.getAutoCommit()) {
      this.connection.commit();
    }
  }

  @Override
  public void cleanUp() {

    // close all statements (if necessary), roll back to last save point (if
    // possible) and close connection (if necessary).
    try {
      if (this.connection != null && !this.connection.isClosed()) {
        // close all cached statements (if possible)
        for (final PreparedStatement stmt : this.stmtCache.values()) {
          try {
            if (!stmt.isClosed()) {
              stmt.close();
            }
          } catch (final SQLException e) {
            e.printStackTrace();
          }
        }

        try {
          if (!this.connection.getAutoCommit()) {
            this.connection.rollback();
          }
        } catch (final SQLException r) {
          r.printStackTrace();
        }
        this.connection.close();
      }
    } catch (final SQLException e) {
      e.printStackTrace();
    }

    // clear everything
    this.stmtCache.clear();
    this.stmtCache = null;
    this.connection = null;
  }
}
