/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.implementation.MySQL;

import static caosdb.server.database.DatabaseUtils.bytes2UTF8;

import caosdb.server.database.access.Access;
import caosdb.server.database.backend.interfaces.RuleLoaderImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.proto.Rule;
import caosdb.server.jobs.core.Mode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MySQLRuleLoader extends MySQLTransaction implements RuleLoaderImpl {

  public MySQLRuleLoader(final Access access) {
    super(access);
  }

  public static final String STMT_GET_RULES =
      "SELECT rules.transaction, rules.criterion, rules.modus from rules where rules.domain_id=? AND rules.entity_id=? AND rules.transaction=?";

  @Override
  public ArrayList<Rule> executeNoCache(
      final Integer domain, final Integer entity, final String transaction)
      throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_GET_RULES);

      stmt.setInt(1, domain);
      stmt.setInt(2, entity);
      stmt.setString(3, transaction);
      stmt.execute();

      final ResultSet rs = stmt.executeQuery();
      try {
        final ArrayList<Rule> ret = new ArrayList<Rule>();
        while (rs.next()) {
          final Rule r = new Rule();
          r.mode = Mode.valueOf(bytes2UTF8(rs.getBytes("modus")));
          r.job = bytes2UTF8(rs.getBytes("criterion"));
          r.domain = domain;
          r.entity = entity;
          r.transaction = transaction;
          ret.add(r);
        }
        return ret;
      } finally {
        rs.close();
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
