/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.implementation.MySQL;

import caosdb.server.database.access.Access;
import caosdb.server.database.backend.interfaces.RetrieveLogRecordImpl;
import caosdb.server.database.exceptions.TransactionException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public class MySQLRetrieveLogRecord extends MySQLTransaction implements RetrieveLogRecordImpl {

  public MySQLRetrieveLogRecord(final Access access) {
    super(access);
  }

  public static final String STMT_RETRIEVE_LOG = "SELECT logRecord FROM logging";
  public static final String WHERE = " WHERE ";
  public static final String AND = " AND ";
  public static final String LOGGER_COND = "logger LIKE ?";
  public static final String MESSAGE_COND = "message LIKE ?";

  public static final String LEVEL_COND(final Level level) {
    return "level=" + level.intValue();
  }

  private static String getWhereClause(
      final String logger, final Level level, final String message) {

    if (logger != null || level != null || message != null) {
      final StringBuilder sb = new StringBuilder();

      if (logger != null) {
        sb.append(LOGGER_COND);
      }
      if (level != null) {
        if (sb.length() > 0) {
          sb.append(AND);
        }
        sb.append(LEVEL_COND(level));
      }
      if (message != null) {
        if (sb.length() > 0) {
          sb.append(AND);
        }
        sb.append(MESSAGE_COND);
      }
      return WHERE + sb.toString();
    }
    return "";
  }

  @Override
  public List<LogRecord> retrieve(final String logger, final Level level, final String message)
      throws TransactionException {
    final List<LogRecord> ret = new LinkedList<LogRecord>();

    final String stmtStr = STMT_RETRIEVE_LOG + getWhereClause(logger, level, message);

    try {
      final PreparedStatement stmt = prepareStatement(stmtStr);
      int index = 1;
      if (logger != null) {
        stmt.setString(index++, logger);
      }
      if (message != null) {
        stmt.setString(index++, message);
      }
      ResultSet rs = null;
      try {
        rs = stmt.executeQuery();

        while (rs.next()) {
          final byte[] bytes = rs.getBytes("logRecord");

          final ObjectInputStream s = new ObjectInputStream(new ByteArrayInputStream(bytes));
          final Object o = s.readObject();
          ret.add((LogRecord) o);
        }
      } finally {
        if (rs != null && !rs.isClosed()) {
          rs.close();
        }
      }
    } catch (final ClassNotFoundException e) {
      throw new TransactionException(e);
    } catch (final IOException e) {
      throw new TransactionException(e);
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }

    return ret;
  }
}
