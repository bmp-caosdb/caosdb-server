/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.implementation.MySQL;

import static caosdb.server.database.DatabaseUtils.bytes2UTF8;

import caosdb.server.accessControl.Principal;
import caosdb.server.accessControl.UserStatus;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.interfaces.RetrieveUserImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.proto.ProtoUser;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;

public class MySQLRetrieveUser extends MySQLTransaction implements RetrieveUserImpl {

  public MySQLRetrieveUser(final Access access) {
    super(access);
  }

  public static final String STMT_RETRIEVE_USER_ACCOUNT =
      "SELECT email, status, entity FROM user_info WHERE realm=? and name=?";
  public static final String STMT_RETRIEVE_USER_ROLES =
      "SELECT role FROM user_roles WHERE realm=? AND user=?";

  @Override
  public ProtoUser execute(final Principal principal) throws TransactionException {
    try {
      ProtoUser ret = null;

      final PreparedStatement stmt = prepareStatement(STMT_RETRIEVE_USER_ACCOUNT);
      stmt.setString(1, principal.getRealm());
      stmt.setString(2, principal.getUsername());

      final ResultSet rs = stmt.executeQuery();
      try {
        if (rs.next()) {
          ret = new ProtoUser();
          ret.realm = principal.getRealm();
          ret.name = principal.getUsername();
          ret.email = bytes2UTF8(rs.getBytes("email"));
          ret.status = UserStatus.valueOf(bytes2UTF8(rs.getBytes("status")));
          if (rs.getString("entity") == null) {
            ret.entity = null;
          } else {
            ret.entity = rs.getInt("entity");
          }
        }
      } finally {
        rs.close();
      }

      final PreparedStatement roles_stmt = prepareStatement(STMT_RETRIEVE_USER_ROLES);
      roles_stmt.setString(1, principal.getRealm());
      roles_stmt.setString(2, principal.getUsername());
      final ResultSet roles_rs = roles_stmt.executeQuery();
      try {
        while (roles_rs.next()) {
          if (ret == null) {
            ret = new ProtoUser();
          }
          if (ret.roles == null) {
            ret.roles = new HashSet<String>();
          }
          ret.roles.add(bytes2UTF8(roles_rs.getBytes("role")));
        }
      } finally {
        roles_rs.close();
      }
      return ret;
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
