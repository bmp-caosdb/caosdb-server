/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.implementation.MySQL;

import static caosdb.server.database.DatabaseUtils.bytes2UTF8;

import caosdb.server.database.access.Access;
import caosdb.server.database.backend.interfaces.RetrieveTransactionHistoryImpl;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.proto.ProtoTransactionLogMessage;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MySQLRetrieveTransactionHistory extends MySQLTransaction
    implements RetrieveTransactionHistoryImpl {

  public MySQLRetrieveTransactionHistory(final Access access) {
    super(access);
  }

  public static final String STMT_RETRIEVE_HISTORY =
      "SELECT transaction, realm, username, seconds, nanos FROM transaction_log WHERE entity_id=? ";

  @Override
  public ArrayList<ProtoTransactionLogMessage> execute(final Integer id)
      throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_RETRIEVE_HISTORY);

      final ArrayList<ProtoTransactionLogMessage> ret = new ArrayList<ProtoTransactionLogMessage>();
      stmt.setInt(1, id);
      final ResultSet rs = stmt.executeQuery();
      try {
        while (rs.next()) {
          final String transaction = bytes2UTF8(rs.getBytes("transaction"));
          final String realm = bytes2UTF8(rs.getBytes("realm"));
          final String username = bytes2UTF8(rs.getBytes("username"));
          final Integer seconds = rs.getInt("seconds");
          final Integer nanos = rs.getInt("nanos");

          ret.add(new ProtoTransactionLogMessage(transaction, realm, username, seconds, nanos));
        }
        return ret;
      } finally {
        rs.close();
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
