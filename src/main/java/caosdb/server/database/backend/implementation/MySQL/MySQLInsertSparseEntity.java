/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.implementation.MySQL;

import caosdb.server.database.access.Access;
import caosdb.server.database.backend.interfaces.InsertSparseEntityImpl;
import caosdb.server.database.exceptions.IntegrityException;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.proto.SparseEntity;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Types;

public class MySQLInsertSparseEntity extends MySQLTransaction implements InsertSparseEntityImpl {

  public MySQLInsertSparseEntity(final Access access) {
    super(access);
  }

  public static final String STMT_INSERT_SPARSE_ENTITY = "call insertEntity(?,?,?,?)";
  public static final String STMT_INSERT_FILE_PROPERTIES =
      "INSERT INTO files (file_id, hash, size, path) VALUES (?, unhex(?), ?, ?);";

  @Override
  public void execute(final SparseEntity entity) {
    try {
      final PreparedStatement insertEntityStmt = prepareStatement(STMT_INSERT_SPARSE_ENTITY);
      final PreparedStatement insertFilePropsStmt = prepareStatement(STMT_INSERT_FILE_PROPERTIES);

      insertEntityStmt.setString(1, entity.name);
      insertEntityStmt.setString(2, entity.description);
      insertEntityStmt.setString(3, entity.role);
      insertEntityStmt.setString(4, entity.acl);

      try (final ResultSet rs = insertEntityStmt.executeQuery()) {
        if (rs.next()) {
          entity.id = rs.getInt("EntityID");
        } else {
          throw new TransactionException("Didn't get new EntityID back.");
        }
      }

      if (entity.filePath != null) {
        insertFilePropsStmt.setInt(1, entity.id);
        if (entity.fileHash != null) {
          insertFilePropsStmt.setString(2, entity.fileHash);
        } else {
          insertFilePropsStmt.setNull(2, Types.VARCHAR);
        }
        insertFilePropsStmt.setLong(3, entity.fileSize);
        insertFilePropsStmt.setString(4, entity.filePath);
        insertFilePropsStmt.execute();
      }
    } catch (final SQLIntegrityConstraintViolationException exc) {
      throw new IntegrityException(exc);
    } catch (final TransactionException exc) {
      throw exc;
    } catch (final Exception exc) {
      throw new TransactionException(exc);
    }
  }
}
