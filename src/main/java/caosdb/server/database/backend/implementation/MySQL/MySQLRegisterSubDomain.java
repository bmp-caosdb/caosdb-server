/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.implementation.MySQL;

import caosdb.server.database.access.Access;
import caosdb.server.database.backend.interfaces.RegisterSubDomainImpl;
import caosdb.server.database.exceptions.TransactionException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.Deque;

public class MySQLRegisterSubDomain extends MySQLTransaction implements RegisterSubDomainImpl {

  public MySQLRegisterSubDomain(final Access access) {
    super(access);
  }

  public static final String STMT_REGISTER_SUBDOMAIN = "call registerSubdomain(?)";

  @Override
  public Deque<Integer> execute(final int domainCount) throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_REGISTER_SUBDOMAIN);
      stmt.setInt(1, domainCount);
      final ResultSet rs = stmt.executeQuery();
      try {
        final ArrayDeque<Integer> ret = new ArrayDeque<Integer>();
        while (rs.next()) {
          ret.add(rs.getInt(1));
        }
        return ret;
      } finally {
        rs.close();
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
