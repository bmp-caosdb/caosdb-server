/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database.backend.implementation.MySQL;

import caosdb.server.CaosDBException;
import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import com.google.common.base.Objects;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import snaq.db.ConnectionPool;

/**
 * DatabaseConnectionPool provides reusable MySQL Connections. They are configured and ready to use.
 *
 * <p>Singleton
 *
 * @author Timm Fitschen
 */
class DatabaseConnectionPool {

  public static Logger logger = LoggerFactory.getLogger(DatabaseConnectionPool.class);

  static Connection getConnection() throws ConnectionException {
    try {
      if (instance == null) {
        instance = createConnectionPool();
      }
      Connection ret = instance.getConnection();
      while (!ret.isValid(1000)) {
        ret.close();
        ret = instance.getConnection();
      }
      ret.setReadOnly(true);
      ret.setAutoCommit(true);
      return ret;
    } catch (final Exception e) {
      if (instance != null) {
        instance.release();
      }
      instance = null;
      throw new ConnectionException(e);
    }
  }

  /** Singleton - Hidden Constructor. */
  private DatabaseConnectionPool() {}

  private static ConnectionPool instance = null;

  private static synchronized ConnectionPool createConnectionPool()
      throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException,
          ConnectionException, CaosDBException {

    final String url =
        "jdbc:mysql://"
            + CaosDBServer.getServerProperty(ServerProperties.KEY_MYSQL_HOST)
            + ":"
            + CaosDBServer.getServerProperty(ServerProperties.KEY_MYSQL_PORT)
            + "/"
            + CaosDBServer.getServerProperty(ServerProperties.KEY_MYSQL_DATABASE_NAME)
            + "?noAccessToProcedureBodies=true&cacheCallableStmts=true&autoReconnect=true&useUnicode=true&characterEncoding=UTF-8&connectionCollation=utf8_unicode_ci&characterSetResults=utf8&serverTimezone=CET";
    // + "?profileSQL=true&characterSetResults=utf8";
    final String user = CaosDBServer.getServerProperty(ServerProperties.KEY_MYSQL_USER_NAME);
    final String pwd = CaosDBServer.getServerProperty(ServerProperties.KEY_MYSQL_USER_PASSWORD);
    final ConnectionPool pool = new ConnectionPool("MySQL Pool", 2, 5, 0, 0, url, user, pwd);
    pool.removeShutdownHook();
    CaosDBServer.addPostShutdownHook(
        new Thread("SHUTDOWN_MYSQL_CONNECTION_POOL") {
          @Override
          public void run() {
            try {
              final Thread t =
                  new Thread() {
                    @Override
                    public void run() {
                      pool.release();
                    }
                  };
              t.start();
              t.join(5000);
              if (!pool.isReleased()) {
                pool.releaseImmediately();
              }
              logger.debug("Stopping MySQL connection pool [OK]\n");
            } catch (final Exception e) {
              logger.error("Stopping MySQL connection pool [failed]\n", e);
            }
          }
        });
    pool.init();

    checkVersion(pool.getConnection());
    printSettings(pool);

    return pool;
  }

  static void printSettings(final ConnectionPool pool) throws SQLException {
    final Connection connection = pool.getConnection();
    Statement statement = connection.createStatement();
    ResultSet query = statement.executeQuery("SHOW VARIABLES LIKE '%char%'");
    int columnCount = query.getMetaData().getColumnCount();
    while (query.next()) {
      String p = "|";
      for (int i = 1; i <= columnCount; i++) {
        p += query.getString(i);
        p += "|";
      }
      logger.debug(p);
    }
    query.close();
    statement.close();

    statement = connection.createStatement();
    query = statement.executeQuery("SHOW VARIABLES LIKE '%coll%'");
    columnCount = query.getMetaData().getColumnCount();
    while (query.next()) {
      String p = "|";
      for (int i = 1; i <= columnCount; i++) {
        p += query.getString(i);
        p += "|";
      }
      logger.debug(p);
    }
    query.close();
    statement.close();

    statement = connection.createStatement();
    query = statement.executeQuery("SHOW VARIABLES LIKE '%max%'");
    columnCount = query.getMetaData().getColumnCount();
    while (query.next()) {
      String p = "|";
      for (int i = 1; i <= columnCount; i++) {
        p += query.getString(i);
        p += "|";
      }
      logger.debug(p);
    }
    query.close();
    statement.close();
    connection.close();
  }

  static void printStatus() {
    logger.debug("###### POOL ########");
    logger.debug("#free   : " + instance.getFreeCount());
    logger.debug("#checked: " + instance.getCheckedOut());
    logger.debug("#params : " + instance.getParametersString());
    logger.debug("####################");
  }

  private static void checkVersion(final Connection con)
      throws SQLException, ConnectionException, CaosDBException {
    try {
      con.setReadOnly(false);
      final PreparedStatement prepareStatement = con.prepareStatement("SELECT CaosDBVersion()");
      try {
        final ResultSet executeQuery = prepareStatement.executeQuery();
        if (executeQuery.next()) {
          final String v_e =
              CaosDBServer.getServerProperty(ServerProperties.KEY_MYSQL_SCHEMA_VERSION)
                  .toLowerCase();
          final String v_a = executeQuery.getString(1).toLowerCase();
          if (!Objects.equal(v_a, v_e)) {
            logger.error(
                "Version of the MySQL schema is wrong.\n\tExpected: {}\n\tActual: {}n\nPlease upgrade the mysql backend.\n\n",
                v_e,
                v_a);
            System.exit(1);
          }
        }
      } catch (final SQLException e) {
        logger.error("Could not check the version of the MySQL schema.", e);
        System.exit(1);
      }

      // set auto_increment on table entities to the maximum entity_id of
      // table transaction_log;
      // Why? MYSQL seems to reset the
      // auto_increment value each time the MySQL server is being
      // restarted to the maximum of ids in the entities table. But if
      // some of the ids had been used yet, we don't want to use them
      // again.
      final CallableStatement prepareCall = con.prepareCall("call initAutoIncrement()");
      try {
        prepareCall.execute();
      } catch (final SQLException e) {
        logger.error("Could inititialize the autoincrement value for the entities table.", e);
        System.exit(1);
      }
    } finally {
      con.close();
    }
  }
}
