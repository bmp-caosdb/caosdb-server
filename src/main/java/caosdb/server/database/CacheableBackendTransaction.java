/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database;

import caosdb.server.database.exceptions.TransactionException;
import java.io.Serializable;
import org.apache.commons.jcs.access.CacheAccess;

public abstract class CacheableBackendTransaction<K, V extends Serializable>
    extends BackendTransaction {

  public abstract V executeNoCache() throws TransactionException;

  private Boolean cached = null;

  @Override
  public final void execute() throws TransactionException {
    final V v = execute(getKey());
    if (v != null) {
      process(v);
    }
  }

  private final V execute(final K key) throws TransactionException {
    // get from cache if possible...
    if (cacheIsEnabled() && key != null) {
      final V cached = getCache().get(getKey());
      if (cached != null) {
        this.cached = true;
        return cached;
      }
    }

    // ... or executeNoCache()
    final V notCached = executeNoCache();
    this.cached = false;
    if (notCached != null) {
      if (cacheIsEnabled() && key != null) {
        // now cache if possible
        getCache().put(getKey(), notCached);
      }
    }
    return notCached;
  }

  protected abstract void process(V t) throws TransactionException;

  protected abstract K getKey();

  protected CacheAccess<K, V> getCache() {
    return null;
  }

  private final boolean cacheIsEnabled() {
    return useCache() && getCache() != null;
  }

  @Override
  public String toString() {
    return super.toString() + " (cached=" + this.cached + ")";
  }
}
