/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database;

import caosdb.server.database.access.Access;
import caosdb.server.database.access.AccessControlAccess;
import caosdb.server.database.access.InfoAccess;
import caosdb.server.database.access.InitAccess;
import caosdb.server.database.access.TransactionAccess;
import caosdb.server.entity.container.TransactionContainer;
import caosdb.server.transaction.AccessControlTransaction;
import caosdb.server.transaction.TransactionInterface;
import caosdb.server.transaction.WriteTransaction;
import caosdb.server.utils.Info;
import caosdb.server.utils.Initialization;
import caosdb.server.utils.Observable;
import caosdb.server.utils.Observer;
import caosdb.server.utils.Releasable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Acquire and release weak access. DatabaseMonitor uses this class for managing access to entities
 * during processing updates, inserts, deletions and retrievals. Weak access will be granted
 * immediately for every thread that requests it, unless one or more threads requested for blocking
 * the weak access (usually due to requesting for strong access). If this happens, all threads that
 * already have weak access will proceed and release their weak access as usual but no NEW permits
 * will be granted.
 *
 * @author Timm Fitschen
 */
class WeakAccessSemaphore extends Semaphore implements Observable, Releasable {

  private static final long serialVersionUID = 4999687434687029136L;
  private int acquired = 0; // how many thread have weak access
  Semaphore block = new Semaphore(1, true);

  public WeakAccessSemaphore() {
    // this is a fair semaphore with no initial permit.
    super(1, true);
  }

  /**
   * Acquires a weak access permit if and only if it has not been blocked via block(). If the
   * WeakAccess is currently blocked, the thread wait until the unblock() method is invoked by any
   * thread.
   */
  @Override
  public void acquire() throws InterruptedException {
    super.acquire();
    if (this.acquired == 0) {
      this.block.acquire();
    }
    this.acquired++;
    notifyObservers(null);
    super.release();
  }

  /** Releases a weak access permit. */
  @Override
  public void release() {
    this.acquired--;
    notifyObservers(null);
    if (this.acquired <= 0) {
      this.acquired = 0;
      if (this.block.availablePermits() <= 0) {
        this.block.release();
      }
    }
  }

  /**
   * Acquires the permit of a block of WeakAccess if no thread currently has a WeakAccess and no
   * thread currently has a block. I.e. it blocks the further permission of weak access for any
   * thread. Every thread that has a weak access yet can proceed. The current thread waits until any
   * thread has released its weak access. If another thread has invoked this method yet the current
   * thread waits until the unblock() method is called.
   *
   * @throws InterruptedException
   */
  public void block() throws InterruptedException {
    super.reducePermits(1);
    this.block.acquire();
  }

  /**
   * Unblock WeakAccess.
   *
   * @throws InterruptedException
   */
  public void unblock() {
    if (this.block.availablePermits() <= 0) {
      this.block.release();
    }
    super.release();
  }

  public int waitingAquireAccess() {
    return getQueueLength();
  }

  public int acquiredAccess() {
    return this.acquired;
  }

  LinkedList<Observer> observers = new LinkedList<Observer>();

  @Override
  public boolean acceptObserver(final Observer o) {
    return this.observers.add(o);
  }

  @Override
  public void notifyObservers(final String e) {
    final Iterator<Observer> it = this.observers.iterator();
    while (it.hasNext()) {
      final Observer o = it.next();
      if (!o.notifyObserver(e, this)) {
        it.remove();
      }
    }
  }
}

/**
 * Acquire and release strong access. DatabaseMonitor uses this class for managing access to
 * entities during processing updates, inserts, deletions and retrievals. Strong access will be
 * granted to one and only one thread if no other thread yet holds weak or strong access permits.
 * The strong access has to be allocated before requesting it to be permitted. See below.
 *
 * @author Timm Fitschen
 */
class StrongAccessLock extends ReentrantLock implements Observable, Releasable {

  private static final long serialVersionUID = 1918369324107546576L;
  private WeakAccessSemaphore wa = null;
  private Thread allocator = null;
  private Thread acquirer = null;

  public StrongAccessLock(final WeakAccessSemaphore wa) {
    super();
    this.wa = wa;
  }

  /**
   * Allocates the strong access permits. While a strong access is allocated but not yet acquired
   * any weak access may still be granted. When a strong access is yet granted or another allocation
   * is still active, the thread waits until the strong access has been released.
   *
   * @throws InterruptedException
   */
  public void allocate() throws InterruptedException {
    super.lock();
    this.allocator = Thread.currentThread();
    notifyObservers(null);
  }

  @Override
  public void lockInterruptibly() throws InterruptedException {
    if (!super.isHeldByCurrentThread()) {
      super.lock();
    }
    this.acquirer = Thread.currentThread();
    notifyObservers(null);
    this.wa.block();
  }

  @Override
  public void unlock() {
    if (super.isHeldByCurrentThread()) {
      this.wa.unblock();
      this.allocator = null;
      this.acquirer = null;
      notifyObservers(null);
      super.unlock();
    }
  }

  @Override
  public void release() {
    unlock();
  }

  LinkedList<Observer> observers = new LinkedList<Observer>();

  @Override
  public boolean acceptObserver(final Observer o) {
    return this.observers.add(o);
  }

  @Override
  public void notifyObservers(final String e) {
    final Iterator<Observer> it = this.observers.iterator();
    while (it.hasNext()) {
      final Observer o = it.next();
      if (!o.notifyObserver(e, this)) {
        it.remove();
      }
    }
  }

  public Thread whoHasAllocatedAccess() {
    return this.allocator;
  }

  public Thread whoHasAcquiredAccess() {
    return this.acquirer;
  }

  public int waitingAllocateAccess() {
    return getQueueLength();
  }
}

/**
 * Manages the read and write access to the database.
 *
 * @author tf
 */
public class DatabaseMonitor {

  private DatabaseMonitor() {}

  private static final DatabaseMonitor instance = new DatabaseMonitor();
  private final WeakAccessSemaphore wa = new WeakAccessSemaphore();
  private final StrongAccessLock sa = new StrongAccessLock(this.wa);

  public static DatabaseMonitor getInstance() {
    return instance;
  }

  public void acquireWeakAccess() throws InterruptedException {
    this.wa.acquire();
  }

  public void releaseWeakAccess() {
    this.wa.release();
  }

  public void allocateStrongAccess() throws InterruptedException {
    this.sa.allocate();
  }

  public void acquireStrongAccess() throws InterruptedException {
    this.sa.lockInterruptibly();
  }

  public void releaseStrongAccess() {
    this.sa.unlock();
  }

  public static final void acceptWeakAccessObserver(final Observer o) {
    instance.wa.acceptObserver(o);
  }

  public static final void acceptStrongAccessObserver(final Observer o) {
    instance.sa.acceptObserver(o);
  }

  public static int waitingAllocateStrongAccess() {
    return instance.sa.waitingAllocateAccess();
  }

  public static int waitingAcquireWeakAccess() {
    return instance.wa.waitingAquireAccess();
  }

  public static int acquiredWeakAccess() {
    return instance.wa.acquiredAccess();
  }

  public static Thread whoHasAllocatedStrongAccess() {
    return instance.sa.whoHasAllocatedAccess();
  }

  public static Thread whoHasAcquiredStrongAccess() {
    return instance.sa.whoHasAcquiredAccess();
  }

  public Access acquiredWeakAccess(final TransactionInterface t) {
    acquiredWeakAccess();
    return new TransactionAccess(t, this.wa);
  }

  public Access allocateStrongAccess(final WriteTransaction<? extends TransactionContainer> wt)
      throws InterruptedException {
    allocateStrongAccess();
    return new TransactionAccess(wt, this.sa);
  }

  public Access acquireStrongAccess(final WriteTransaction<? extends TransactionContainer> wt)
      throws InterruptedException {
    acquireStrongAccess();
    return wt.getAccess();
  }

  public static Access getInfoAccess(final Info i) {
    return new InfoAccess(i);
  }

  public static Access getInitAccess(final Initialization initialization) {
    return new InitAccess(initialization);
  }

  public static Access getAccountAccess(final AccessControlTransaction t) {
    return new AccessControlAccess(t);
  }
}
