/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.datatype;

import caosdb.server.datatype.AbstractDatatype.Table;
import com.google.common.base.Objects;
import org.jdom2.Attribute;
import org.jdom2.Element;

public abstract class AbstractEnumValue implements SingleValue {

  private final EnumElement value;

  public AbstractEnumValue(final EnumElement e) {
    this.value = e;
  }

  @Override
  public final void addToElement(final Element e) {
    e.addContent(this.value.toString());
  }

  @Override
  public final Table getTable() {
    return Table.enum_data;
  }

  @Override
  public final String toDatabaseString() {
    return this.value.toString();
  }

  @Override
  public final void addToAttribute(final Attribute a) {
    a.setValue(this.value.toString());
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof AbstractEnumValue) {
      final AbstractEnumValue that = (AbstractEnumValue) obj;
      return Objects.equal(that.value, this.value);
    }
    return false;
  }
}
