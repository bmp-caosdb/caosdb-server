/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.datatype;

import caosdb.server.datatype.AbstractDatatype.Table;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.Message;
import caosdb.server.utils.ServerMessages;
import org.jdom2.Attribute;
import org.jdom2.Element;

public class ReferenceValue implements SingleValue {
  private EntityInterface entity = null;
  private String name = null;
  private Integer id = null;

  public static ReferenceValue parseReference(final Object reference) throws Message {
    if (reference == null) {
      return null;
    }
    if (reference instanceof EntityInterface) {
      return new ReferenceValue((EntityInterface) reference);
    } else if (reference instanceof ReferenceValue) {
      return (ReferenceValue) reference;
    } else if (reference instanceof GenericValue) {
      try {
        return new ReferenceValue(Integer.parseInt(((GenericValue) reference).toDatabaseString()));
      } catch (final NumberFormatException e) {
        return new ReferenceValue(((GenericValue) reference).toDatabaseString());
      }
    } else if (reference instanceof CollectionValue) {
      throw ServerMessages.DATA_TYPE_DOES_NOT_ACCEPT_COLLECTION_VALUES;
    } else {
      try {
        return new ReferenceValue(Integer.parseInt(reference.toString()));
      } catch (final NumberFormatException e) {
        return new ReferenceValue(reference.toString());
      }
    }
  }

  @Override
  public String toString() {
    if (this.entity != null) {
      return this.entity.getId().toString();
    } else if (this.id == null && this.name != null) {
      return this.name;
    }
    return this.id.toString();
  }

  public ReferenceValue(final EntityInterface entity) {
    this.entity = entity;
  }

  public ReferenceValue(final Integer id) {
    this.id = id;
  }

  public ReferenceValue(final String name) {
    this.name = name;
  }

  public final EntityInterface getEntity() {
    return this.entity;
  }

  public final void setEntity(final EntityInterface entity) {
    this.entity = entity;
  }

  public final String getName() {
    if (this.entity != null && this.entity.hasName()) {
      return this.entity.getName();
    }
    return this.name;
  }

  public final Integer getId() {
    if (this.entity != null && this.entity.hasId()) {
      return this.entity.getId();
    }
    return this.id;
  }

  public final void setId(final Integer id) {
    this.id = id;
  }

  @Override
  public void addToElement(final Element e) {
    e.addContent(toString());
  }

  @Override
  public Table getTable() {
    return Table.reference_data;
  }

  @Override
  public String toDatabaseString() {
    return toString();
  }

  @Override
  public void addToAttribute(final Attribute a) {
    a.setValue(toString());
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof ReferenceValue) {
      final ReferenceValue that = (ReferenceValue) obj;
      if (that.getId() != null && getId() != null) {
        return that.getId().equals(getId());
      } else if (that.getName() != null && getName() != null) {
        return that.getName().equals(getName());
      }
    }
    return false;
  }
}
