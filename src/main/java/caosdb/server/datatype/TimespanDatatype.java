/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.datatype;

import caosdb.server.datatype.AbstractDatatype.Table;
import caosdb.server.entity.Message;
import org.jdom2.Attribute;
import org.jdom2.Element;

@DatatypeDefinition(name = "Timespan")
public class TimespanDatatype extends AbstractDatatype {

  public static final Message TIMESPAN_NOT_PARSABLE = new Message(219, "Timespan not parsable.");

  @Override
  public Timespan parseValue(final Object value) throws Message {
    Timespan ret = null;
    try {
      ret = Timespan.parse(value.toString());
    } catch (final NumberFormatException e) {
      // TODO define correct error message
      throw TIMESPAN_NOT_PARSABLE;
    }
    return ret;
  }
}

class Timespan implements SingleValue {
  public enum Unit {
    YEARS,
    MONTHS,
    WEEKS,
    DAYS,
    HOURS,
    MINUTES,
    SECONDS,
    USERDEFINED
  };

  static Timespan parse(final String str) throws NumberFormatException {

    final Timespan ts = new Timespan();
    String[] parts;
    if (str.matches("^\\s*[0-9]+(\\.[0-9]+)?([eE]-?[0-9]+)?\\s*[A-Za-z]+\\s*$")) {
      parts = new String[2];
      parts[0] = str.split("\\s*?[A-Za-z]*$")[0];
      parts[1] = str.split("^[0-9]+(\\.[0-9]+)?([eE]-?[0-9]+)?\\s*")[1];

      if (parts[1].equalsIgnoreCase(Unit.YEARS.toString()) || parts[1].equalsIgnoreCase("y")) {
        ts.setUnit(Unit.YEARS);
        ts.factor = 31556925.261;
      } else if (parts[1].equalsIgnoreCase(Unit.MONTHS.toString()) || parts[1].equals("M")) {
        ts.setUnit(Unit.MONTHS);
        ts.factor = 31556925.261 / 12;
      } else if (parts[1].equalsIgnoreCase(Unit.WEEKS.toString())
          || parts[1].equalsIgnoreCase("w")) {
        ts.setUnit(Unit.WEEKS);
        ts.factor = 7 * 86400.0;
      } else if (parts[1].equalsIgnoreCase(Unit.DAYS.toString())
          || parts[1].equalsIgnoreCase("d")) {
        ts.setUnit(Unit.DAYS);
        ts.factor = 86400.0;
      } else if (parts[1].equalsIgnoreCase(Unit.HOURS.toString())
          || parts[1].equalsIgnoreCase("h")) {
        ts.setUnit(Unit.HOURS);
        ts.factor = 3600.0;
      } else if (parts[1].equalsIgnoreCase(Unit.MINUTES.toString()) || parts[1].equals("m")) {
        ts.setUnit(Unit.MINUTES);
        ts.factor = 60.0;
      } else if (parts[1].equalsIgnoreCase(Unit.SECONDS.toString())
          || parts[1].equalsIgnoreCase("s")) {
        ts.setUnit(Unit.SECONDS);
      } else {
        ts.setUnit(Unit.USERDEFINED);
      }
      ts.value = Double.parseDouble(parts[0]);
    } else if (str.matches("^\\s*[0-9]+(\\.[0-9]+)?([eE]-?[0-9]+)?\\s*$")) {
      ts.setUnit(null);
      ts.value = Double.parseDouble(str);

    } else {
      throw new NumberFormatException("Couldn't parse String to Timespan.");
    }
    return ts;
  }

  private double value = 0;
  private double factor = 1;

  private Unit unit = null;

  public double toSeconds() {
    return value * factor;
  }

  @Override
  public String toString() {
    return Double.toString(value);
  }

  public String getUnit() {
    return unit.toString();
  }

  private void setUnit(final Unit unit) {
    this.unit = unit;
  }

  @Override
  public void addToElement(Element e) {
    e.addContent(toString());
  }

  @Override
  public Table getTable() {
    return Table.double_data;
  }

  @Override
  public String toDatabaseString() {
    return toString();
  }

  @Override
  public void addToAttribute(Attribute a) {
    a.setValue(toString());
  }
}
