/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.datatype;

import java.util.LinkedList;
import org.jdom2.Element;

public class CollectionValue extends LinkedList<IndexedSingleValue> implements Value {

  private static final long serialVersionUID = 1L;

  @Override
  public void addToElement(final Element e) {
    for (final SingleValue v : this) {
      final Element valueElem = new Element("Value");
      if (v != null) {
        v.addToElement(valueElem);
      }
      e.addContent(valueElem);
    }
  }

  @Override
  public boolean add(final IndexedSingleValue e) {
    if (e == null) {
      return super.add(new IndexedSingleValue(null));
    }
    return super.add(e);
  }

  public void add(final SingleValue v) {
    this.add(new IndexedSingleValue(v));
  }

  public void add(final int i, final SingleValue v) {
    this.add(new IndexedSingleValue(i, v));
  }

  public void add(final Value v) {
    add(0, v);
  }

  public void add(final int i, final Value v) {
    if (v == null || v instanceof SingleValue) {
      add(i, (SingleValue) v);
    } else {
      throw new IllegalArgumentException("Expected a SingleValue.");
    }
  }
}
