/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.logging;

import caosdb.server.resource.ReReadableRepresentation;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import org.restlet.Message;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.engine.header.HeaderConstants;
import org.restlet.representation.Representation;
import org.restlet.util.Series;

public class RequestErrorLogMessage implements Serializable {

  /** */
  private static final long serialVersionUID = 4683559833384175003L;

  public String request = null;
  public String response = null;
  public String request_headers = null;
  public String response_headers = null;
  public String request_entity = null;
  public String response_entity = null;

  public RequestErrorLogMessage() {}

  public RequestErrorLogMessage(final Request request, final Response response) {
    if (request != null) {
      this.request = request.toString();
      this.request_headers = getHeaders(request);
      this.request_entity = getEntity(request);
    }
    if (response != null) {
      this.response = response.toString();
      this.response_headers = getHeaders(response);
      this.response_entity = getEntity(response);
    }
  }

  private static String getEntity(final Message r) {
    final Representation entity = r.getEntity();
    if (entity != null && !entity.isEmpty()) {
      if (!entity.isTransient()) {
        try {
          return entity.getText();
        } catch (final IOException e) {
          e.printStackTrace();
        }
      } else if (entity instanceof ReReadableRepresentation) {
        return ((ReReadableRepresentation) entity).getString();
      }
      return "entity was transient";
    }
    return null;
  }

  private static String getHeaders(final Message r) {
    if (r.getAttributes().get(HeaderConstants.ATTRIBUTE_HEADERS) != null
        && r.getAttributes().get(HeaderConstants.ATTRIBUTE_HEADERS) instanceof Series<?>) {
      final Series<?> headers =
          (Series<?>) r.getAttributes().get(HeaderConstants.ATTRIBUTE_HEADERS);
      final StringBuilder sb = new StringBuilder();
      for (final Object h : headers) {
        try {
          sb.append(URLDecoder.decode(h.toString(), "UTF-8"));
        } catch (final UnsupportedEncodingException e) {
          e.printStackTrace();
        }
      }
      return sb.toString();
    }
    return null;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();

    sb.append("REQUEST: ");
    sb.append(this.request);
    if (this.request_headers != null) {
      sb.append("\nREQUEST HEADERS: ");
      sb.append(this.request_headers);
    }
    if (this.request_entity != null) {
      sb.append("\nREQUEST ENTITY: ");
      sb.append(this.request_entity);
    }
    sb.append("\nRESPONSE: ");
    sb.append(this.response);
    if (this.response_headers != null) {
      sb.append("\nRESPONSE HEADERS: ");
      sb.append(this.response_headers);
    }
    if (this.response_entity != null) {
      sb.append("\nRESPONSE ENTITY:");
      sb.append(this.response_entity);
    }
    return sb.toString();
  }
}
