/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server;

import caosdb.server.accessControl.AuthenticationUtils;
import caosdb.server.accessControl.OneTimeAuthenticationToken;
import caosdb.server.accessControl.SessionToken;
import caosdb.server.resource.DefaultResource;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.subject.Subject;
import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.security.Authenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CaosAuthenticator extends Authenticator {

  private static final Logger logger = LoggerFactory.getLogger(CaosAuthenticator.class.getName());

  public CaosAuthenticator(final Context context) {
    super(context);
  }

  @Override
  protected boolean authenticate(final Request request, final Response response) {
    final Subject subject = SecurityUtils.getSubject();

    return attemptOneTimeTokenLogin(subject, request) || attemptSessionValidation(subject, request);
  }

  private static boolean attemptSessionValidation(final Subject subject, final Request request) {
    try {
      final SessionToken sessionToken =
          AuthenticationUtils.parseSessionTokenCookie(
              request.getCookies().getFirst(AuthenticationUtils.SESSION_TOKEN_COOKIE), null);

      if (sessionToken != null) {
        subject.login(sessionToken);
      }
    } catch (AuthenticationException e) {
      logger.info("LOGIN_FAILED", e);
    }
    // anonymous users
    if (!subject.isAuthenticated()
        && CaosDBServer.getServerProperty(ServerProperties.KEY_AUTH_OPTIONAL)
            .equalsIgnoreCase("TRUE")) {
      subject.login(AuthenticationUtils.ANONYMOUS_USER);
    }
    return subject.isAuthenticated();
  }

  private static boolean attemptOneTimeTokenLogin(final Subject subject, final Request request) {
    try {
      OneTimeAuthenticationToken oneTimeToken = null;

      // try and parse from the query segment of the uri
      oneTimeToken =
          AuthenticationUtils.parseOneTimeTokenQuerySegment(
              request.getResourceRef().getQueryAsForm().getFirstValue("AuthToken"), null);

      // try and parse from cookie
      if (oneTimeToken == null) {
        oneTimeToken =
            AuthenticationUtils.parseOneTimeTokenCookie(
                request.getCookies().getFirst(AuthenticationUtils.ONE_TIME_TOKEN_COOKIE), null);
      }

      if (oneTimeToken != null) {
        subject.login(oneTimeToken);
      }
    } catch (final AuthenticationException e) {
      logger.info("LOGIN_FAILED", e);
    }
    return subject.isAuthenticated();
  }

  @Override
  protected int unauthenticated(final Request request, final Response response) {
    final DefaultResource defaultResource =
        new DefaultResource(AuthenticationUtils.UNAUTHENTICATED.toElement());
    defaultResource.init(getContext(), request, response);
    defaultResource.handle();
    response.setStatus(org.restlet.data.Status.CLIENT_ERROR_UNAUTHORIZED);
    return super.unauthenticated(request, response);
  }
}
