/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.scripting;

import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import caosdb.server.entity.Message;
import caosdb.server.utils.ConfigurationException;
import caosdb.server.utils.ServerMessages;
import caosdb.server.utils.Utils;
import java.io.File;
import java.nio.file.Path;

public class ScriptingUtils {

  private File bin;
  private File working;

  public ScriptingUtils() {
    this.bin =
        new File(
            CaosDBServer.getServerProperty(ServerProperties.KEY_SERVER_SIDE_SCRIPTING_BIN_DIR));
    this.working =
        new File(
            CaosDBServer.getServerProperty(ServerProperties.KEY_SERVER_SIDE_SCRIPTING_WORKING_DIR));
    if (!bin.exists()) {
      bin.mkdirs();
    }
    if (!bin.isDirectory()) {
      throw new ConfigurationException(
          "The ServerProperty `"
              + ServerProperties.KEY_SERVER_SIDE_SCRIPTING_BIN_DIR
              + "` must point to a directory");
    }

    if (!working.exists()) {
      working.mkdirs();
    }
    if (!working.isDirectory()) {
      throw new ConfigurationException(
          "The ServerProperty `"
              + ServerProperties.KEY_SERVER_SIDE_SCRIPTING_WORKING_DIR
              + "` must point to a directory");
    }
  }

  public File getScriptFile(final String command) {
    final Path script = bin.toPath().resolve(command);
    return script.toFile();
  }

  public void checkScriptExists(final String command) throws Message {
    if (!getScriptFile(command).exists()) {
      throw ServerMessages.SERVER_SIDE_SCRIPT_DOES_NOT_EXIST;
    }
  }

  public void checkScriptExecutable(final String command) throws Message {
    if (!getScriptFile(command).canExecute()) {
      throw ServerMessages.SERVER_SIDE_SCRIPT_NOT_EXECUTABLE;
    }
  }

  public File getTmpWorkingDir() {
    String uid = Utils.getUID();
    return working.toPath().resolve(uid).toFile();
  }

  public File getStdOutFile(File workingDir) {
    return workingDir.toPath().resolve(".STDOUT").toFile();
  }

  public File getStdErrFile(File workingDir) {
    return workingDir.toPath().resolve(".STDERR").toFile();
  }
}
