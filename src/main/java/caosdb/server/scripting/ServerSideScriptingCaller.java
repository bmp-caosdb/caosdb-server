/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.scripting;

import caosdb.server.CaosDBException;
import caosdb.server.entity.FileProperties;
import caosdb.server.entity.Message;
import caosdb.server.utils.ServerMessages;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.util.Collection;
import java.util.List;
import org.apache.commons.io.FileUtils;

public class ServerSideScriptingCaller {

  public static final String UPLOAD_FILES_DIR = ".upload_files";
  public static final Integer STARTED = -1;
  private final String[] commandLine;
  private final int timeoutMs;
  private ScriptingUtils utils;
  private List<FileProperties> files;
  private File workingDir;
  private Object authToken;
  private File stdOutFile;
  private File stdErrFile;
  private String stdErr = null;
  private String stdOut;
  private Integer code = null;

  public Integer getCode() {
    return code;
  }

  File getUploadFilesDir() {
    return getTmpWorkingDir().toPath().resolve(UPLOAD_FILES_DIR).toFile();
  }

  public ServerSideScriptingCaller(
      String[] commandLine, Integer timeoutMs, List<FileProperties> files, Object authToken) {
    this(commandLine, timeoutMs, files, authToken, new ScriptingUtils());
  }

  public ServerSideScriptingCaller(
      String[] commandLine,
      Integer timeoutMs,
      List<FileProperties> files,
      Object authToken,
      ScriptingUtils utils) {
    this(commandLine, timeoutMs, files, authToken, utils, utils.getTmpWorkingDir());
  }

  public ServerSideScriptingCaller(
      String[] commandLine,
      Integer timeoutMs,
      List<FileProperties> files,
      Object authToken,
      ScriptingUtils utils,
      File workingDir) {
    this.utils = utils;
    this.files = files;
    this.commandLine = commandLine;
    this.timeoutMs = timeoutMs;
    this.authToken = authToken;
    this.workingDir = workingDir;
    this.stdOutFile = utils.getStdOutFile(workingDir);
    this.stdErrFile = utils.getStdErrFile(workingDir);
  }

  public int invoke() throws Message {
    try {
      checkCommandLine(commandLine);
      try {
        createWorkingDir();
        putFilesInWorkingDir(files);
      } catch (final Exception e) {
        e.printStackTrace();
        throw ServerMessages.SERVER_SIDE_SCRIPT_SETUP_ERROR;
      }

      try {
        return callScript();
      } catch (TimeoutException e) {
        throw ServerMessages.SERVER_SIDE_SCRIPT_TIMEOUT;
      } catch (final Throwable e) {
        e.printStackTrace();
        throw ServerMessages.SERVER_SIDE_SCRIPT_ERROR;
      }
    } finally {
      cleanup();
    }
  }

  void checkCommandLine(String[] commandLine) throws Message {
    utils.checkScriptExists(commandLine[0]);
    utils.checkScriptExecutable(commandLine[0]);
  }

  void putFilesInWorkingDir(final Collection<FileProperties> files)
      throws FileNotFoundException, CaosDBException {
    if (files == null) {
      return;
    }

    // create files dir
    if (!getUploadFilesDir().mkdir()) {
      throw new FileNotFoundException("Could not crete the FILE_UPLOAD_DIR.");
    }
    for (final FileProperties f : files) {
      if (f.getPath() == null || f.getPath().isEmpty()) {
        throw new CaosDBException("The path must not be null or empty!");
      }
      caosdb.server.utils.FileUtils.createSymlink(
          getUploadFilesDir().toPath().resolve(f.getPath()).toFile(), f.getFile());
    }
  }

  void createWorkingDir() throws Exception {
    if (getTmpWorkingDir().exists()) {
      throw new Exception("The working directory must be non-existing when the caller is invoked.");
    }

    // create working dir
    getTmpWorkingDir().mkdirs();
  }

  void cleanup() {
    // catch exception and throw afterwards,
    IOException e1 = null;
    try {
      // cache script outputs
      getStdErr();
      getStdOut();
    } catch (final IOException e2) {
      e1 = e2;
    }
    try {
      deleteWorkingDir(getTmpWorkingDir());
    } catch (final IOException e2) {
      if (e1 == null) {
        e1 = e2;
      } else {
        e1.addSuppressed(e2);
      }
    }
    if (e1 != null) throw new RuntimeException("Cleanup failed.", e1);
  }

  void deleteWorkingDir(final File pwd) throws IOException {
    if (pwd.exists()) FileUtils.forceDelete(pwd);
  }

  String makeCallAbsolute(String call) {
    return utils.getScriptFile(call).getAbsolutePath();
  }

  String[] injectAuthToken(String[] commandLine) {
    String[] newCommandLine = new String[commandLine.length + 1];
    newCommandLine[0] = commandLine[0];
    newCommandLine[1] = "--auth-token=" + authToken.toString();
    for (int i = 2; i < newCommandLine.length; i++) {
      newCommandLine[i] = commandLine[i - 1];
    }
    return newCommandLine;
  }

  int callScript() throws IOException, InterruptedException, TimeoutException {
    String[] effectiveCommandLine = injectAuthToken(getCommandLine());
    effectiveCommandLine[0] = makeCallAbsolute(effectiveCommandLine[0]);
    final ProcessBuilder pb = new ProcessBuilder(effectiveCommandLine);
    pb.redirectOutput(Redirect.to(getStdOutFile()));
    pb.redirectError(Redirect.to(getStdErrFile()));
    pb.directory(getTmpWorkingDir());

    code = STARTED;
    final TimeoutProcess process = new TimeoutProcess(pb.start(), getTimeoutMs());

    code = process.waitFor();
    return code;
  }

  public File getStdOutFile() {
    return stdOutFile;
  }

  public File getStdErrFile() {
    return stdErrFile;
  }

  public String[] getCommandLine() {
    return this.commandLine;
  }

  File getTmpWorkingDir() {
    return this.workingDir;
  }

  int getTimeoutMs() {
    return this.timeoutMs;
  }

  public String getStdErr() throws IOException {
    if (stdErr == null && getStdErrFile().exists()) {
      stdErr = FileUtils.readFileToString(getStdErrFile(), "UTF-8");
    }
    return stdErr;
  }

  public String getStdOut() throws IOException {
    if (stdOut == null && getStdOutFile().exists()) {
      stdOut = FileUtils.readFileToString(getStdOutFile(), "UTF-8");
    }
    return stdOut;
  }
}
