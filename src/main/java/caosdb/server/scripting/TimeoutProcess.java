/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.scripting;

import java.io.InputStream;

public class TimeoutProcess {

  private final Process process;
  private boolean wasTimeouted = false;

  public TimeoutProcess(final Process process, final int ms) {
    this.process = process;
    if (ms > 0) {
      watchTimeout(this, ms).start();
    }
  }

  public static Thread watchTimeout(final TimeoutProcess p, final int ms) {
    return new Thread(
        new Runnable() {

          @Override
          public void run() {
            try {
              Thread.sleep(ms);
            } catch (final InterruptedException e) {
              e.printStackTrace();
            }
            p.timeout();
          }
        },
        "Timeout " + p.toString());
  }

  protected void timeout() {
    if (this.process.isAlive()) {
      this.wasTimeouted = true;
      this.process.destroyForcibly();
    }
  }

  public boolean wasTimeouted() {
    return this.wasTimeouted;
  }

  public int waitFor() throws InterruptedException, TimeoutException {
    int code = this.process.waitFor();
    if (wasTimeouted) {
      throw new TimeoutException("The process was terminated due to a timeout.");
    } else {
      return code;
    }
  }

  public InputStream getErrorStream() {
    return this.process.getErrorStream();
  }

  public int exitValue() {
    return this.process.exitValue();
  }
}
