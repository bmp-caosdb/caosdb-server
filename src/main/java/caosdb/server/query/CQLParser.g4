/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
parser grammar CQLParser;

options { tokenVocab = CQLLexer; }

@header {
    import java.util.LinkedList;
    import java.util.List;
}

cq returns [Query.Type t, List<Query.Selection> s, Query.Pattern e, Query.Role r, EntityFilterInterface filter]
	@init{
	    $s = null;
		$e = null;
		$r = null;
		$filter = null;
	}
:

	(
	   SELECT prop_sel {$s = $prop_sel.s;} FROM {$t = Query.Type.FIND;}
	   | FIND {$t = Query.Type.FIND;}
	   | COUNT {$t = Query.Type.COUNT;})
	(
		(
			role {$r = $role.r;}
			(entity {$e = $entity.ep;})?
		) | entity {$e = $entity.ep;}
	)
	(
		entity_filter {$filter = $entity_filter.filter;}
	)??
	EOF
;

prop_sel returns [List<Query.Selection> s]
    @init{
        $s = new LinkedList<Query.Selection>();
    }
:
    prop_subsel {$s.add($prop_subsel.sub);}
    (SELECT_COMMA prop_subsel {$s.add($prop_subsel.sub);})*
;

prop_subsel returns [Query.Selection sub]:
    selector_txt {$sub = new Query.Selection($selector_txt.text);}(SELECT_DOT s=prop_subsel {$sub.setSubSelection($s.sub);})?
;

selector_txt:
    SELECTOR_TXT+
;

role returns [Query.Role r]: 
	RECORDTYPE {$r = Query.Role.RECORDTYPE;}
	| RECORD {$r = Query.Role.RECORD;}
	| PROPERTY {$r = Query.Role.PROPERTY;}
	| FILE {$r = Query.Role.FILE;}
	| QUERYTEMPLATE {$r = Query.Role.QUERYTEMPLATE;}
	| ENTITY {$r = Query.Role.ENTITY;}
;

entity_filter returns [EntityFilterInterface filter]
	@init{
		$filter = null;
	}
:
	which_exp
	(
		(
			LPAREN
			( 
				filter_expression {$filter = $filter_expression.efi;}
				| conjunction {$filter = $conjunction.c;} 
				| disjunction {$filter = $disjunction.d;}
			)
			RPAREN
		) | (
			filter_expression {$filter = $filter_expression.efi;}
			| conjunction {$filter = $conjunction.c;}
			| disjunction {$filter = $disjunction.d;}
		)
	)?
;

which_exp:
	WHICH (HAS_A (PROPERTY)?)?
	| HAS_A (PROPERTY)?
	| WITH (A (PROPERTY)?)?
	| WHERE
	| DOT
;

filter_expression returns [EntityFilterInterface efi]
:
	backreference (subproperty {((Backreference) $backreference.ref).setSubProperty($subproperty.subp);})? {$efi = $backreference.ref;}
	| idfilter {$efi=$idfilter.filter;}
	| storedat {$efi=$storedat.filter;}
	| transaction {$efi=$transaction.filter;}
	| ( pov (subproperty {((POV) $pov.filter).setSubProperty($subproperty.subp);})? {$efi = $pov.filter;} )
	| subproperty {$efi=$subproperty.subp;}
	| negation {$efi=$negation.n;}
;

idfilter returns [IDFilter filter] locals [String o, String v, String a]
@init{
	$a = null;
	$o = null;
	$v = null;
} 
@after{
	$filter = new IDFilter($o,$v,$a);
}
:
	(minmax {$a=$minmax.agg;})??
	ID 
	(
		OPERATOR {$o = $OPERATOR.text;}
		value {$v = $value.str;}
	)?
;

transaction returns [TransactionFilter filter] locals [String type, TransactionFilter.Transactor user, String time]
@init{
	$time = null;
	$user = null;
	$type = null;
}
@after{
	$filter = new TransactionFilter($type,$user,$time);
}
:
	(
		( INSERTED | CREATED ) {$type = TransactionFilter.INSERTION;}
		| ( UPDATED ) {$type = TransactionFilter.UPDATE;}
	)
	
	(
		transactor (transaction_time {$time = $transaction_time.tqp;})? {$user = $transactor.t;}
		| transaction_time (transactor {$user = $transactor.t;})? {$time = $transaction_time.tqp;}
	)
;

transactor returns [TransactionFilter.Transactor t]
:
	BY 
	( 
		SOMEONE ELSE BUT ME {$t = TransactionFilter.neq_currentTransactor();}
		| SOMEONE ELSE BUT entity {$t = TransactionFilter.neq_foreignTransactor($entity.ep);}
		| SOMEONE ELSE BUT username {$t = TransactionFilter.neq_foreignTransactor($username.ep);}
		| SOMEONE ELSE {$t = TransactionFilter.neq_currentTransactor();}
		| ME {$t = TransactionFilter.eq_currentTransactor();}
		| entity {$t = TransactionFilter.eq_foreignTransactor($entity.ep);}
		| username {$t = TransactionFilter.eq_foreignTransactor($username.ep);}
	)
;

username returns [Query.Pattern ep] locals [int type]
@init{
    $type = Query.Pattern.TYPE_NORMAL;
}
@after{
    $ep = new Query.Pattern($text, Query.Pattern.TYPE_NORMAL);
}
:
    ( STAR {$type = Query.Pattern.TYPE_LIKE;} | ESC_STAR | TXT | DOT | ESC_REGEXP_END | COLON  )+
;

transaction_time returns [String tqp]
:
	(
		(ON | IN) 
		(datetime {$tqp = $datetime.text;} 
		| entity {$tqp = $entity.ep.toString();})
	) | TODAY {$tqp = TransactionFilter.TODAY;} 
;

/*
* not fully compliant with iso 8601 (TODO) 
*/
datetime
:
	NUM // year 
	(
		HYPHEN NUM // mon
		( 
			HYPHEN NUM // day of mon
			(
				(m=TXT {$m.text.equals("T")}?)?// compliance with iso datetime
				NUM // hour
				(
					COLON NUM // minut 
					(
						COLON NUM // sec 
						(
							DOT NUM // millisec
						)?
					)?
				)?
			)?
		)?
	)?	
;

pov returns [POV filter] locals [Query.Pattern p, String o, String v, String a]
	@init{
		$p = null;
		$o = null;
		$v = null;
		$a = null;
	}
	@after{
		$filter = new POV($p,$o,$v,$a);
	}
:
	(
		property {$p = $property.pp; $a=$property.agg;} 
		(
			( 
			  LIKE {$o = $LIKE.text;}
			  ( like_pattern {$v = $like_pattern.ep.toString();}
			    | value {$v = $value.str;} )
			  | OPERATOR {$o = $OPERATOR.text;} value {$v = $value.str;}
			)
			| IS_NULL {$o = "0";}
			| IS_NOT_NULL {$o = "!0";}
			| IN datetime {$o = "("; $v=$datetime.text;}
			| NEGATION IN datetime {$o = "!("; $v=$datetime.text;}
		)?
	) 
	| 
	(
		( LIKE {$o = $LIKE.text;}
		  ( like_pattern {$v = $like_pattern.ep.toString();}
		    | value {$v = $value.str;} )
		)
		| ( OPERATOR {$o = $OPERATOR.text;} value {$v = $value.str;}
		  ( AS_A
			property {$p = $property.pp;} )?
		)
	)
	
;


subproperty returns [SubProperty subp] locals [String p]
@init{
	$p = null;
	$subp = null;
}
:
	entity_filter {$subp = new SubProperty($entity_filter.filter);}
;

backreference returns [Backreference ref] locals [Query.Pattern e, Query.Pattern p]
	@init{
		$e = null;
		$p = null;
	}
	@after{
		$ref = new Backreference($e, $p); 
	}
:
	IS_REFERENCED
	(BY A? entity {$e=$entity.ep;})?
	( 
		AS_A
		property {$p=$property.pp;}
	)?
;

storedat returns [StoredAt filter] locals [String loc]
	@init{
		$loc = null;
	}
	@after{
		$filter = new StoredAt($loc);
	}
:
	IS_STORED_AT
	location {$loc = $location.str;}
;

conjunction returns [Conjunction c] locals [Conjunction dummy]
	@init{
		$c = new Conjunction();
	}
:
	(
		f1 = filter_expression {$c.add($f1.efi);}
		|
		LPAREN
		f4 = filter_expression {$c.add($f4.efi);}
		RPAREN
	) 
	( 
		AND
		(
			( which_exp | A (PROPERTY)?? )
		)? 
		( 
			f2 = filter_expression {$c.add($f2.efi);}
			| (
				LPAREN
				( 
					f3 = filter_expression {$c.add($f3.efi);}
					| disjunction {$c.add($disjunction.d);}
					| c2=conjunction {$c.addAll($c2.c);} 
				)
				RPAREN
			)
		)
	)+
;

disjunction returns [Disjunction d]
	@init{
		$d = new Disjunction();
	}
:
	(
		f1 = filter_expression {$d.add($f1.efi);}
		|
		LPAREN
		f4 = filter_expression {$d.add($f4.efi);}
		RPAREN
	)
	(
		OR
		(
			( which_exp | A (PROPERTY)? )
		)?
		(
			f2 = filter_expression {$d.add($f2.efi);}
			| (
				LPAREN
				( 
					f3 = filter_expression {$d.add($f3.efi);}
					| conjunction {$d.add($conjunction.c);}
					| d2 = disjunction {$d.addAll($d2.d);} 
				)
				RPAREN
			)
		)
	)+
;

negation returns [Negation n]
	@init{
	}
:	
	NEGATION
	(
		f1 = filter_expression {$n = new Negation($f1.efi);}
		| ( 
			LPAREN
			(	
				f2 = filter_expression {$n = new Negation($f2.efi);}
				| disjunction {$n = new Negation($disjunction.d);}
				| conjunction {$n = new Negation($conjunction.c);}
			)
			RPAREN
		)
	)
;

entity returns [Query.Pattern ep]
: 
	regexp_pattern {$ep = $regexp_pattern.ep;}
	| like_pattern {$ep = $like_pattern.ep;}
	| ( double_quoted {$ep = $double_quoted.ep;} ) 
	| ( single_quoted {$ep = $single_quoted.ep;} )
	| (~(SINGLE_QUOTE_START|DOUBLE_QUOTE_START|WHITE_SPACE|WHICH|DOT|WHERE|HAS_A|WITH|STAR|AND|OR))+? {$ep = new Query.Pattern((String) $text, Query.Pattern.TYPE_NORMAL);}
;

regexp_pattern returns [Query.Pattern ep] locals [StringBuffer sb]
	@init{
		$sb = new StringBuffer();
	}
:
	REGEXP_BEGIN  
	(ESC_REGEXP_END {$sb.append(">>");} |m=. {$sb.append($m.text);})*? 
	REGEXP_END {$ep = new Query.Pattern((String) $sb.toString(), Query.Pattern.TYPE_REGEXP);}
;

like_pattern returns [Query.Pattern ep] locals [StringBuffer sb]
	@init{
		$sb = new StringBuffer();
	}
:
	
	(m=~(WHITE_SPACE|WHICH|DOT|WHERE|HAS_A|WITH|STAR|LIKE|OPERATOR|AS_A|AND|OR|IS_STORED_AT|IS_REFERENCED) {$sb.append($m.text);})*?
	(
		STAR {$sb.append('*');}
		(m=~(WHITE_SPACE|WHICH|DOT|WHERE|HAS_A|WITH|STAR|LIKE|OPERATOR|AS_A|AND|OR) {$sb.append($m.text);})*?
	)+? {$ep = new Query.Pattern((String) $sb.toString(), Query.Pattern.TYPE_LIKE);}
;

property returns [Query.Pattern pp, String agg]locals [StringBuffer sb]
	@init{
		$sb = new StringBuffer();
		$agg = null;
	}
:
	(minmax {$agg=$minmax.agg;})??
	(
        regexp_pattern {$pp = $regexp_pattern.ep;}
        | like_pattern {$pp = $like_pattern.ep;}
        | ( double_quoted {$pp = $double_quoted.ep;} ) 
        | ( single_quoted {$pp = $single_quoted.ep;} )
        | ((m=TXT | m=NUM | m=REGEXP_MARKER | m=ENTITY){$sb.append($m.text);})+  {$pp = new Query.Pattern($sb.toString(), Query.Pattern.TYPE_NORMAL);}
	)
;

minmax returns [String agg]
:
	(THE?? (
		GREATEST {$agg="max";} 
		| SMALLEST {$agg="min";} 
	))
;

value returns [String str]
:
	number {$str = $text;}
	| datetime {$str = $text;}
	| atom {$str = $atom.str;}
;

number
:
	HYPHEN?? NUM* (DOT NUM+)?
;

location returns [String str]
:
    atom {$str = $atom.str;}
    |
	SLASHES ?
	((WHICH | WITH)+ SLASHES |( TXT | COLON | HYPHEN | NUM | DOT | ESC_STAR | ESC_BS | ESC_REGEXP_END | STAR )+ SLASHES ?)* {$str = $text; }
;

atom returns [String str]
	@init {
		$str = null;
	}
:
    double_quoted {$str = $double_quoted.ep.toString();}
	| single_quoted {$str = $single_quoted.ep.toString();} 
	| (TXT | NUM | REGEXP_MARKER | STAR | A )+  {$str = $text;}
;

single_quoted returns [Query.Pattern ep] locals [StringBuffer sb, int patternType]
	@init{
		$sb = new StringBuffer();
		$patternType = Query.Pattern.TYPE_NORMAL;
	}
	@after{
		$ep = new Query.Pattern($sb.toString(),$patternType);
	}
:
	SINGLE_QUOTE_START
	(
		t = SINGLE_QUOTE_ESCAPED_CHAR {$sb.append($t.text.substring(1,$t.text.length()));}
	|
		r = SINGLE_QUOTE_STAR {$sb.append($r.text); $patternType = Query.Pattern.TYPE_LIKE;}
	|
		s = ~SINGLE_QUOTE_END {$sb.append($s.text);}
	)+?
	SINGLE_QUOTE_END
;

double_quoted returns [Query.Pattern ep] locals [StringBuffer sb, int patternType]
	@init{
		$sb = new StringBuffer();
		$patternType = Query.Pattern.TYPE_NORMAL;
	}
	@after{
		$ep = new Query.Pattern($sb.toString(),$patternType);
	}
:
	DOUBLE_QUOTE_START
	(
		t = DOUBLE_QUOTE_ESCAPED_CHAR {$sb.append($t.text.substring(1,$t.text.length()));}
	|
		r = DOUBLE_QUOTE_STAR {$sb.append($r.text); $patternType = Query.Pattern.TYPE_LIKE;}
	|
		s = ~DOUBLE_QUOTE_END {$sb.append($s.text);}
	)+?
	DOUBLE_QUOTE_END
;
