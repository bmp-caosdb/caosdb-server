/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.query;

import static java.sql.Types.CHAR;
import static java.sql.Types.INTEGER;
import static java.sql.Types.VARCHAR;

import caosdb.server.query.Query.QueryException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import org.jdom2.Element;

public class IDFilter implements EntityFilterInterface {

  private final String operator;
  private final String value;
  private final String aggregate;
  private final Integer vInt;

  public IDFilter(final String o, final String v, final String a) {
    this.operator = o;
    this.value = v;
    this.aggregate = a;
    Integer i;
    try {
      i = Integer.valueOf(v);
    } catch (final NumberFormatException e) {
      i = null;
    }
    this.vInt = i;
  }

  @Override
  public void apply(final QueryInterface query) throws QueryException {
    final long t1 = System.currentTimeMillis();
    try {
      final Connection connection = query.getConnection();
      // applyIDFilter(sourceSet, targetSet, o, vInt, agg)
      final CallableStatement callIDFilter =
          connection.prepareCall("call applyIDFilter(?,?,?,?,?)");

      callIDFilter.setString(1, query.getSourceSet()); // sourceSet
      if (query.getTargetSet() != null) { // targetSet
        callIDFilter.setString(2, query.getTargetSet());
      } else {
        callIDFilter.setNull(2, VARCHAR);
      }

      // operator
      if (getOperator() == null) {
        callIDFilter.setNull(3, CHAR);
      } else {
        callIDFilter.setString(3, getOperator());
      }

      // value
      if (getValue() == null) {
        callIDFilter.setNull(4, INTEGER);
      } else {
        callIDFilter.setInt(4, this.vInt);
      }

      // aggregate
      if (getAggregate() == null) {
        callIDFilter.setNull(5, CHAR);
      } else {
        callIDFilter.setString(5, getAggregate());
      }

      callIDFilter.execute();
      callIDFilter.close();
    } catch (final SQLException e) {
      throw new QueryException(e);
    }
    query.addBenchmark(this.getClass().getSimpleName(), System.currentTimeMillis() - t1);
  }

  @Override
  public Element toElement() {
    final Element ret = new Element("IDFilter");
    if (getOperator() != null) {
      ret.setAttribute("operator", getOperator());
    }
    if (getValue() != null) {
      ret.setAttribute("value", getValue());
    }
    if (getAggregate() != null) {
      ret.setAttribute("aggregate", getAggregate());
    }
    return ret;
  }

  public String getOperator() {
    return this.operator;
  }

  public String getValue() {
    return this.value;
  }

  public String getAggregate() {
    return this.aggregate;
  }
}
