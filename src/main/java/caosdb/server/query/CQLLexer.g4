/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
lexer grammar CQLLexer;

AS_A:
	[Aa][Ss] (EMPTY_SPACE? A)?
;

IS_REFERENCED:
	(IS_f EMPTY_SPACE?)? [Rr][Ee][Ff][Ee][Rr][Ee][Nn][Cc][Ee][Dd]
;

BY:
	[Bb][Yy]
;

SELECT:
    [Ss][Ee][Ll][Ee][Cc][Tt] -> pushMode(SELECT_MODE)
;

INSERTED:
	[Ii][Nn][Ss][Ee][Rr][Tt][Ee][Dd]
;

CREATED:
	[Cc][Rr][Ee][Aa][Tt][Ee][Dd]
;

UPDATED:
	[Uu][Pp][Dd][Aa][Tt][Ee][Dd]
;

ON:
	[Oo][Nn]
;

IN:
	[Ii][Nn]
;

IS_STORED_AT:
	(IS_f EMPTY_SPACE?)? [Ss][Tt][Oo][Rr][Ee][Dd] (EMPTY_SPACE? AT)?
;

AT:
	[Aa][Tt]
;

FIND:
	[Ff][Ii][Nn][Dd]
;

COUNT:
	[Cc][Oo][Uu][Nn][Tt]
;
	
AND:
	( 
		[Aa][Nn][Dd] 
	)
	| '&'
;

OR:
	( 
		[Oo][Rr] 
	)
	| '|'
;

LPAREN:
	'('
;

RPAREN:
	')'
;

SINGLE_QUOTE_START:
	'\'' -> pushMode(SINGLE_QUOTE_MODE)
;

DOUBLE_QUOTE_START:
	'"' -> pushMode(DOUBLE_QUOTE_MODE)
;

OPERATOR:
	'='
	| '<'
	| '<='
	| '>='
	| '>'
	| '!='
	| '->'
	| [Rr][Ee][Ff][Ee][Rr][Ee][Nn][Cc][Ee]([Ss]|EMPTY_SPACE? [Tt][Oo]) (EMPTY_SPACE? A {_input.LA(1) == ' '}?)? {setText("->");}
;

LIKE:
	[Ll][Ii][Kk][Ee]
;

IS_NULL:
	IS_f EMPTY_SPACE NULL_f
;

IS_NOT_NULL:
	IS_f EMPTY_SPACE NOT_f EMPTY_SPACE NULL_f
;

fragment
NULL_f:
	[Nn][Uu][Ll][Ll]
;

fragment
DOES_f:
	[Dd][Oo][Ee][Ss]
;

fragment
NOT_f:
	[Nn][Oo][Tt]
;

fragment
DOESNT_f:
	DOES_f EMPTY_SPACE? NOT_f
	| DOES_f [Nn] SINGLE_QUOTE [Tt]
;

fragment
ISNT_f:
	IS_f [Nn] SINGLE_QUOTE [Tt]
;

fragment
WERE_f:
	[Ww][Ee][Rr][Ee]
;

fragment
WERENT_f:
	WERE_f [Nn] SINGLE_QUOTE [Tt]
;

fragment
HAVENT_f:
	HAVE_f [Nn] SINGLE_QUOTE [Tt]
;

fragment
HADNT_f:
	HAD_f [Nn] SINGLE_QUOTE [Tt]
;

fragment
HAD_f:
	[Hh][Aa][Dd]
;
	
fragment
HAVE_f:
	[Hh][Aa][Vv][Ee]
;

fragment
HAS_f:
	[Hh][Aa][Ss]
;

fragment
HASNT_f:
	HAS_f [Nn] SINGLE_QUOTE [Tt]
;

fragment
BEEN_f:
	[Bb][Ee][Ee][Nn]
;

fragment
HAVE_A_f:
	HAVE_f (WHITE_SPACE? A)? 
;

fragment
DO_f:
	[Dd][Oo]
;

fragment
DONT_f:
	DO_f NOT_f
	| DO_f [Nn] SINGLE_QUOTE [Tt]
;

fragment
WAS_f:
	[Ww][Aa][Ss]
;

fragment
WASNT_f:
	WAS_f [Nn] SINGLE_QUOTE [Tt]
;

NEGATION:
	'!'
	| DOESNT_f (WHITE_SPACE? HAVE_A_f)?
	| DONT_f (WHITE_SPACE? HAVE_A_f)?
	| HASNT_f (WHITE_SPACE? BEEN_f)?
	| ISNT_f (WHITE_SPACE? BEEN_f)?
	| NOT_f (WHITE_SPACE? BEEN_f)?
	| WERENT_f (WHITE_SPACE? BEEN_f)?
	| WASNT_f (WHITE_SPACE? BEEN_f)?
	| HAVENT_f (WHITE_SPACE? BEEN_f)?
	| HADNT_f (WHITE_SPACE? BEEN_f)?
;	

WITH:
	[Ww][Ii][Tt][Hh]
;

THE: 
	[Tt][Hh][Ee]
;

GREATEST:
	[Gg][Rr][Ee][Aa][Tt][Ee][Ss][Tt]
;

SMALLEST:
	[Ss][Mm][Aa][Ll][Ll][Ee][Ss][Tt]
;

A:
	[Aa][Nn]?
;

ME:
	[Mm][Ee]
;

SOMEONE:
	[Ss][Oo][Mm][Ee][Oo][Nn][Ee]
;

ELSE:
	[Ee][Ll][Ss][Ee]
;

WHERE:
	[Ww][Hh][Ee][Rr][Ee]
;

WHICH:
	[Ww][Hh][Ii][Cc][Hh]
;

HAS_A:
	(HAS_f | HAD_f | HAVE_f | WERE_f | WAS_f | IS_f)
	(
		(EMPTY_SPACE? A)
		| (EMPTY_SPACE? BEEN_f)
	)?
;

PROPERTY:
	[Pp][Rr][Oo][Pp][Ee][Rr][Tt]([Yy]|[Ii][Ee][Ss])
;

RECORDTYPE:	
	[Rr][Ee][Cc][Oo][Rr][Dd][Tt][Yy][Pp][Ee]([Ss])?
;

RECORD:
	[Rr][Ee][Cc][Oo][Rr][Dd]([Ss])?
;

FILE:
	[Ff][Ii][Ll][Ee]([Ss])?
;

ENTITY:
    [Ee][Nn][Tt][Ii][Tt]([Yy]|[Ii][Ee][Ss])
;

QUERYTEMPLATE:
	[Qq][Uu][Ee][Rr][yY][Tt][Ee][Mm][Pp][Ll][Aa][Tt][Ee]
;

fragment
IS_f:
	[Ii][Ss]
;

fragment
EMPTY_SPACE:
	[ \t\n\r]+
;

WHITE_SPACE:
	[ \t\n\r]+ -> channel(HIDDEN)
;

fragment
DOUBLE_QUOTE:
	'"'
;

fragment
SINGLE_QUOTE:
	'\''
;

REGEXP_MARKER:
	'#'
;

REGEXP_BEGIN:
	'<<'
;

REGEXP_END:
	'>>'
;

ID: 
	[Ii][Dd]
;

// Multiple slashes should be allowed in paths instead of a single slash.
SLASHES:
	'/'+
;

STAR:
	'*'
;

DOT:
	'.'
;

QMARK:
	'?'
;

BUT:
	[Bb][Uu][Tt]
;

ESC_REGEXP_END:
	ESC_MARKER
	'>>'
;

ESC_STAR:
	ESC_MARKER
	'*'
;

ESC_BS:
	ESC_MARKER
	'\\'
;

fragment 
ESC_MARKER:
	'\\'
;

TODAY:
	[Tt][Oo][Dd][Aa][Yy]
;

HYPHEN:
	'-'
;

COLON:
	':'
;

NUM:
	('0'..'9')+
;

TXT:
	('a'..'z' | 'A'..'Z' | '_' | '-' {_input.LA(1) != '>'}? | '+' | '&' | ';' | ',' | '$' | ':' | '%' | '^' | '~' {_input.LA(1) != '='}? | '`' | '´' | 'ö' | 'ä' | 'ß' | 'ü' | 'Ö' | 'Ä' | 'Ü' | '@' | '[' | ']' | '{' | '}' )+
;

UNKNOWN_CHAR: . ;

mode SINGLE_QUOTE_MODE;

	SINGLE_QUOTE_ESCAPED_CHAR:
		ESC_MARKER
		( '\'' | '\\' | '*' )
	;

	SINGLE_QUOTE_END:
		'\'' -> mode(DEFAULT_MODE)
	;
	
	SINGLE_QUOTE_STAR:
		'*'
	;

	SINGLE_QUOTE_ANY_CHAR:
		~('\''|'\\'|'*')+
	;
	
mode DOUBLE_QUOTE_MODE;

	DOUBLE_QUOTE_ESCAPED_CHAR:
		ESC_MARKER
		( '"' | '\\' | '*' )
	;

	DOUBLE_QUOTE_END:
		'"' -> mode(DEFAULT_MODE)
	;
	
	DOUBLE_QUOTE_STAR:
		'*'
	;

	DOUBLE_QUOTE_ANY_CHAR:
		~('"'|'\\'|'*')+
	;
	

mode SELECT_MODE;

    FROM:
         [Ff][Rr][Oo][Mm] -> mode(DEFAULT_MODE)
    ;

    SELECT_DOT:
        '.'
    ;
    
    SELECT_COMMA:
        ','
    ;
    
    SELECTOR_TXT:
        . 
    ;
    
    
    
