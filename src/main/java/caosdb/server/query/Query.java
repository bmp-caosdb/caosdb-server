/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.query;

import static caosdb.server.database.DatabaseUtils.bytes2UTF8;

import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import caosdb.server.database.Database;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import caosdb.server.database.backend.implementation.MySQL.MySQLHelper;
import caosdb.server.database.backend.transaction.RetrieveSparseEntity;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.misc.DBHelper;
import caosdb.server.database.misc.TransactionBenchmark;
import caosdb.server.entity.Entity;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.Message;
import caosdb.server.entity.Message.MessageType;
import caosdb.server.entity.RetrieveEntity;
import caosdb.server.entity.container.TransactionContainer;
import caosdb.server.entity.xml.ToElementable;
import caosdb.server.permissions.EntityACL;
import caosdb.server.permissions.EntityPermission;
import caosdb.server.query.CQLParser.CqContext;
import caosdb.server.query.CQLParsingErrorListener.ParsingError;
import caosdb.server.transaction.TransactionInterface;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.shiro.subject.Subject;
import org.jdom2.Element;

public class Query implements QueryInterface, ToElementable, TransactionInterface {

  public static class Selection {
    private final String selector;
    private Selection subselection = null;

    public Selection setSubSelection(final Selection sub) {
      if (this.subselection != null) {
        throw new UnsupportedOperationException("subselection is immutble!");
      }
      this.subselection = sub;
      return this;
    }

    public Selection(final String selector) {
      this.selector = selector.trim();
    }

    public String getSelector() {
      return this.selector;
    }

    @Override
    public String toString() {
      final StringBuilder sb = new StringBuilder(this.selector);
      if (this.subselection != null) {
        sb.append(".");
        sb.append(this.subselection.toString());
      }
      return sb.toString();
    }

    public Selection getSubselection() {
      return this.subselection;
    }

    public Element toElement() {
      final Element ret = new Element("Selector");
      ret.setAttribute("name", toString());
      return ret;
    }
  }

  public enum Role {
    RECORD,
    RECORDTYPE,
    PROPERTY,
    ENTITY,
    FILE,
    QUERYTEMPLATE
  }

  public enum Type {
    FIND,
    COUNT
  };

  public static final class Pattern {
    public static final int TYPE_NORMAL = 0;
    public static final int TYPE_LIKE = 1;
    public static final int TYPE_REGEXP = 2;
    public final int type;
    private final String str;

    public Pattern(final String str, final int type) {
      this.type = type;
      this.str = str;
    }

    @Override
    public String toString() {
      switch (this.type) {
        case TYPE_LIKE:
          return this.str.replaceAll("\\*", "%").replace("_", "\\_");
        default:
          return this.str;
      }
    }
  }

  public static final class ParsingException extends Query.QueryException {

    public ParsingException(final String string) {
      super(string);
    }

    /** */
    private static final long serialVersionUID = 8984839198803429114L;
  }

  public static class QueryException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public QueryException(final String string) {
      super(string);
    }

    public QueryException(final Throwable t) {
      super(t);
    }
  }

  private static boolean filterEntitiesWithoutRetrievePermisions =
      !CaosDBServer.getServerProperty(
              ServerProperties.KEY_QUERY_FILTER_ENTITIES_WITHOUT_RETRIEVE_PERMISSIONS)
          .equalsIgnoreCase("FALSE");

  List<Integer> resultSet = null;
  private final String query;
  private Pattern entity = null;
  private Role role = null;
  private String sourceSet = null;
  private String targetSet = null;
  private List<Selection> selections = null;
  private String parseTree = null;
  private final TransactionContainer container;
  private EntityFilterInterface filter = null;
  private CQLParsingErrorListener el = null;
  private final Subject user;
  private Type type = null;
  private final ArrayList<ToElementable> messages = new ArrayList<>();
  private Access access;

  public Type getType() {
    return this.type;
  }

  public Query(final String query) {
    this(query, null);
  }

  public Query(final String query, final Subject user) {
    this(query, user, null);
  }

  public Query(final String query, final Subject user, final TransactionContainer container) {
    this.container = container;
    this.query = query;
    this.user = user;
  };

  /**
   * Fill the initially empty resultSet with all entities which match the name, or the id. Then
   * calculate the transitive closure of child entities and add it to the resultSet.
   *
   * @throws SQLException
   */
  private void initResultSetWithNameIDAndChildren() throws SQLException {
    final CallableStatement callInitEntity =
        getConnection().prepareCall("call initEntity(?,?,?,?,?)");

    try {
      callInitEntity.setInt(1, Integer.parseInt(this.entity.toString()));
    } catch (final NumberFormatException e) {
      callInitEntity.setNull(1, Types.INTEGER);
    }
    switch (this.entity.type) {
      case Pattern.TYPE_NORMAL:
        callInitEntity.setString(2, this.entity.toString());
        callInitEntity.setNull(3, Types.VARCHAR);
        callInitEntity.setNull(4, Types.VARCHAR);
        break;
      case Pattern.TYPE_LIKE:
        callInitEntity.setNull(2, Types.VARCHAR);
        callInitEntity.setString(3, this.entity.toString());
        callInitEntity.setNull(4, Types.VARCHAR);
        break;
      case Pattern.TYPE_REGEXP:
        callInitEntity.setNull(2, Types.VARCHAR);
        callInitEntity.setNull(3, Types.VARCHAR);
        callInitEntity.setString(4, this.entity.toString());
        break;
      default:
        break;
    }
    callInitEntity.setString(5, this.sourceSet);
    callInitEntity.execute();
    callInitEntity.close();
  }

  /**
   * @return The result set table name.
   * @throws QueryException
   */
  private String sourceStrategy(final String sourceSet) throws QueryException {
    try {
      this.sourceSet = sourceSet;
      initResultSetWithNameIDAndChildren();

      if (this.role != Role.QUERYTEMPLATE) {
        applyQueryTemplates(this, getSourceSet());
      }

      if (this.role != null) {
        final RoleFilter roleFilter = new RoleFilter(this.role, "=");
        roleFilter.apply(this);
      }

      if (this.filter != null) {
        this.filter.apply(this);
      }

      return this.sourceSet;
    } catch (final SQLException e) {
      e.printStackTrace();
      throw new TransactionException(e);
    }
  }

  /**
   * Finds all QueryTemplates in the resultSet and applies them to the same resultSet. The ids of
   * the QueryTemplates themselves are then removed from the resultSet. If the current user doesn't
   * have the RETRIEVE:ENTITY permission for a particular QueryTemplate it will be ignored.
   *
   * @param resultSet
   * @throws SQLException
   * @throws QueryException
   */
  public static void applyQueryTemplates(final QueryInterface query, final String resultSet)
      throws QueryException {
    try {
      final Map<Integer, String> queryTemplates = getQueryTemplates(query, resultSet);

      final PreparedStatement removeQTStmt =
          query.getConnection().prepareStatement("DELETE FROM `" + resultSet + "` WHERE id=?");

      // Run thru all QTs found...
      for (final Entry<Integer, String> q : queryTemplates.entrySet()) {
        // ... remove the QT from resultSet...
        removeQTStmt.setInt(1, q.getKey());
        removeQTStmt.execute();

        // ... check for RETRIEVE:ENTITY permission...
        final EntityInterface e =
            Database.execute(new RetrieveSparseEntity(q.getKey()), query.getAccess()).getEntity();
        final EntityACL entityACL = e.getEntityACL();
        if (!entityACL.isPermitted(query.getUser(), EntityPermission.RETRIEVE_ENTITY)) {
          // ... and ignore if not.
          continue;
        }

        // ... apply them...
        final Query subQuery = new Query(q.getValue(), query.getUser());
        subQuery.setAccess(query.getAccess());
        subQuery.parse();
        final String subResultSet = subQuery.executeStrategy();

        // ... and merge the resultSets.
        union(query, resultSet, subResultSet);
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    }
  }

  private static void union(final QueryInterface query, final String target, final String source)
      throws SQLException {
    final PreparedStatement unionStmt =
        query.getConnection().prepareCall("call calcUnion('" + target + "','" + source + "')");
    unionStmt.execute();
  }

  /**
   * Return the definitions of all QueryTemplates in the resultSet as a List.
   *
   * @param resultSet
   * @return A list of query strings.
   * @throws SQLException
   */
  private static Map<Integer, String> getQueryTemplates(
      final QueryInterface query, final String resultSet) throws SQLException {
    ResultSet rs = null;
    try {
      final HashMap<Integer, String> ret = new HashMap<Integer, String>();
      final CallableStatement stmt =
          query
              .getConnection()
              .prepareCall(
                  "SELECT q.id, q.definition FROM query_template_def AS q INNER JOIN `"
                      + resultSet
                      + "` AS r ON (r.id=q.id);");
      rs = stmt.executeQuery();
      while (rs.next()) {
        ret.put(rs.getInt("id"), rs.getString("definition"));
      }
      return ret;
    } finally {
      if (rs != null) {
        try {
          rs.close();
        } catch (final SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * @return The result set table name.
   * @throws QueryException
   */
  private String targetStrategy(final String targetSet) throws QueryException {
    this.targetSet = targetSet;

    // set entities table as source
    this.sourceSet = "entities";

    if (this.filter != null) {

      // apply filters
      this.filter.apply(this);

      this.sourceSet = null;
    }

    // filter by role
    if (this.role != null && this.role != Role.ENTITY) {
      final RoleFilter roleFilter = new RoleFilter(this.role, "=");
      roleFilter.apply(this);
    }

    return this.targetSet;
  }

  private MySQLHelper getMySQLHelper(final Access access) {
    final DBHelper h = access.getHelper("MySQL");
    if (h == null) {
      final MySQLHelper ret = new MySQLHelper();
      access.setHelper("MySQL", ret);
      return ret;
    } else {
      return (MySQLHelper) h;
    }
  }

  private String initQuery() throws QueryException {
    try (final CallableStatement callInitQuery = getConnection().prepareCall("call initQuery()")) {
      ResultSet initQueryResult = null;
      initQueryResult = callInitQuery.executeQuery();
      if (!initQueryResult.next()) {
        throw new QueryException("No resultSet table created.");
      }
      return bytes2UTF8(initQueryResult.getBytes("tablename"));
    } catch (final SQLException e) {
      throw new QueryException(e);
    }
  }

  public void parse() throws ParsingException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    parser.removeErrorListeners();
    this.el = new CQLParsingErrorListener(CQLLexer.UNKNOWN_CHAR);
    parser.addErrorListener(this.el);
    final CqContext cq = parser.cq();
    if (this.el.hasErrors()) {
      throw new ParsingException("Parsing finished with errors.");
    }

    this.entity = cq.e;
    this.role = cq.r;
    this.parseTree = cq.toStringTree(parser);
    this.type = cq.t;
    this.filter = cq.filter;
    this.selections = cq.s;
  }

  private String executeStrategy() throws QueryException {
    if (this.entity != null) {
      return sourceStrategy(initQuery());
    } else {
      return targetStrategy(initQuery());
    }
  }

  private LinkedList<Integer> getResultSet(final String resultSetTableName) throws QueryException {
    ResultSet finishResultSet = null;
    try {
      final PreparedStatement finish =
          getConnection().prepareStatement("Select id from `" + resultSetTableName + "`");
      finishResultSet = finish.executeQuery();
      final LinkedList<Integer> rs = new LinkedList<Integer>();
      while (finishResultSet.next()) {
        rs.add(finishResultSet.getInt("id"));
      }
      return rs;
    } catch (final SQLException e) {
      throw new QueryException(e);
    } finally {
      if (finishResultSet != null) {
        try {
          finishResultSet.close();
        } catch (final SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public Query execute(final Access access) throws ParsingException {
    setAccess(access);
    parse();

    try {

      this.resultSet = getResultSet(executeStrategy());

      filterEntitiesWithoutRetrievePermission(this.resultSet);

      // Fill resulting entities into container
      if (this.container != null && this.type == Type.FIND) {
        for (final int id : this.resultSet) {

          final Entity e = new RetrieveEntity(id);

          // if query has select-clause:
          if (this.selections != null && !this.selections.isEmpty()) {
            e.addSelections(this.selections);
          }
          this.container.add(e);
        }
      }
      return this;

    } finally {
      cleanUp();
    }
  }

  private void addWarning(final String w) {
    this.messages.add(new Message(MessageType.Warning, 0, w));
  }

  private void cleanUp() {
    if (getConnection() != null) {
      ResultSet rs = null;
      try {
        rs = getConnection().prepareCall("call cleanUpQuery()").executeQuery();
        while (rs.next()) {
          addWarning(bytes2UTF8(rs.getBytes("warning")));
        }
      } catch (final SQLException e) {
        throw new QueryException(e);
      } finally {
        try {
          if (rs != null) {
            rs.close();
          }
        } catch (final SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  private void setAccess(final Access access) {
    this.access = access;
  }

  private final TransactionBenchmark benchmark = TransactionBenchmark.getSubBenchmark();

  /**
   * Filter out all entities which may not be retrieved by this user due to a missing RETRIEVE
   * permission. This one is also designed for filtering of intermediate results.
   *
   * @param query
   * @param resultSet
   * @throws SQLException
   * @throws TransactionException
   */
  public static void filterEntitiesWithoutRetrievePermission(
      final QueryInterface query, final String resultSet)
      throws SQLException, TransactionException {
    if (!filterEntitiesWithoutRetrievePermisions) {
      return;
    }
    try (final Statement stmt = query.getConnection().createStatement()) {
      final ResultSet rs = stmt.executeQuery("SELECT id from `" + resultSet + "`");
      final List<Integer> toBeDeleted = new LinkedList<Integer>();
      while (rs.next()) {
        final long t1 = System.currentTimeMillis();
        final Integer id = rs.getInt("id");
        if (!Database.execute(new RetrieveSparseEntity(id), query.getAccess())
            .getEntity()
            .getEntityACL()
            .isPermitted(query.getUser(), EntityPermission.RETRIEVE_ENTITY)) {
          toBeDeleted.add(id);
        }
        final long t2 = System.currentTimeMillis();
        query.addBenchmark("filterEntitiesWithoutRetrievePermission", t2 - t1);
      }
      rs.close();
      for (final Integer id : toBeDeleted) {
        stmt.execute("DELETE FROM `" + resultSet + "` WHERE id = " + id);
      }
    }
  }

  /**
   * Filter out all entities which may not be retrieved by this user due to a missing RETRIEVE
   * permission. This one is for the filtering of the final result set and not for the filtering of
   * any intermediate results.
   *
   * @param entities
   * @throws TransactionException
   */
  private void filterEntitiesWithoutRetrievePermission(final List<Integer> entities)
      throws TransactionException {
    if (!filterEntitiesWithoutRetrievePermisions) {
      return;
    }
    final Iterator<Integer> iterator = entities.iterator();
    while (iterator.hasNext()) {
      final long t1 = System.currentTimeMillis();
      final Integer id = iterator.next();
      if (!Database.execute(new RetrieveSparseEntity(id), getAccess())
          .getEntity()
          .getEntityACL()
          .isPermitted(getUser(), EntityPermission.RETRIEVE_ENTITY)) {
        iterator.remove();
      }
      final long t2 = System.currentTimeMillis();
      addBenchmark("filterEntitiesWithoutRetrievePermission", t2 - t1);
    }
  }

  @Override
  public String toString() {
    return this.query;
  }

  public List<Integer> getResultSet() {
    return this.resultSet;
  }

  @Override
  public String getSourceSet() {
    return this.sourceSet;
  }

  @Override
  public Connection getConnection() {
    try {
      return getMySQLHelper(getAccess()).getConnection();
    } catch (final SQLException e) {
      e.printStackTrace();
    } catch (final ConnectionException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public Access getAccess() {
    return this.access;
  }

  /** @return the number of entities in the resultset. Might be updated by the filters. */
  @Override
  public int getTargetSetCount() {
    return this.targetSetCount;
  }

  public void setTargetSetCount(final Integer c) {
    this.targetSetCount = c;
  }

  private int targetSetCount = -1;

  @Override
  public void addToElement(final Element parent) {
    final Element ret = new Element("Query");
    if (this.query == null) {
      parent.addContent(ret);
      return;
    }
    ret.setAttribute("string", this.query);
    if (this.resultSet != null) {
      ret.setAttribute("results", Integer.toString(this.resultSet.size()));
    } else {
      ret.setAttribute("results", "0");
    }

    final Element parseTreeElem = new Element("ParseTree");
    if (this.el.hasErrors()) {
      for (final ParsingError m : this.el.getErrors()) {
        parseTreeElem.addContent(m.toElement());
      }
    } else {
      parseTreeElem.setText(this.parseTree);
    }
    ret.addContent(parseTreeElem);

    final Element roleElem = new Element("Role");
    if (this.role != null) {
      roleElem.setText(this.role.toString());
    }
    ret.addContent(roleElem);

    final Element entityElem = new Element("Entity");
    try {
      entityElem.setText(this.entity.toString());
    } catch (final NullPointerException exc) {
    }
    ret.addContent(entityElem);

    if (this.filter != null) {
      final Element filterElem = new Element("Filter");
      filterElem.addContent(this.filter.toElement());
      ret.addContent(filterElem);
    }

    for (final ToElementable m : this.messages) {
      m.addToElement(ret);
    }
    if (getSelections() != null && !getSelections().isEmpty()) {
      final Element selection = new Element("Selection");
      for (final Selection s : getSelections()) {
        selection.addContent(s.toElement());
      }
      ret.addContent(selection);
    }

    ret.addContent(this.benchmark.toElememt());

    parent.addContent(ret);
  }

  @Override
  public Subject getUser() {
    return this.user;
  }

  @Override
  public Query getQuery() {
    return this;
  }

  @Override
  public String getTargetSet() {
    return this.targetSet;
  }

  @Override
  public void execute() throws Exception {
    execute(getAccess());
  }

  public List<Selection> getSelections() {
    return this.selections;
  }

  @Override
  public void addBenchmark(final String str, final long time) {
    this.benchmark.addBenchmark(this.getClass().getSimpleName().toString() + "." + str, time);
  }
}
