/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServerProperties extends Properties {

  private static final long serialVersionUID = 1L;
  private static Logger logger = LoggerFactory.getLogger(ServerProperties.class.getName());

  public static final String KEY_FILE_SYSTEM_ROOT = "FILE_SYSTEM_ROOT";
  public static final String KEY_DROP_OFF_BOX = "DROP_OFF_BOX";
  public static final String KEY_TMP_FILES = "TMP_FILES";
  public static final String KEY_USER_FOLDERS = "USER_FOLDERS";
  public static final String KEY_CHOWN_SCRIPT = "CHOWN_SCRIPT";
  public static final String KEY_AUTH_OPTIONAL = "AUTH_OPTIONAL";

  public static final String KEY_MYSQL_HOST = "MYSQL_HOST";
  public static final String KEY_MYSQL_PORT = "MYSQL_PORT";
  public static final String KEY_MYSQL_DATABASE_NAME = "MYSQL_DATABASE_NAME";
  public static final String KEY_MYSQL_USER_NAME = "MYSQL_USER_NAME";
  public static final String KEY_MYSQL_USER_PASSWORD = "MYSQL_USER_PASSWORD";
  public static final String KEY_MYSQL_SCHEMA_VERSION = "MYSQL_SCHEMA_VERSION";

  public static final String KEY_BASE_PATH = "BASE_PATH";
  public static final String KEY_FILE_POLICY = "FILE_POLICY";
  public static final String KEY_FILE_MESSAGES = "FILE_MESSAGES";
  public static final String KEY_CONTEXT_ROOT = "CONTEXT_ROOT";
  public static final String KEY_POLICY_COMPONENT = "POLICY_COMPONENT";

  public static final String KEY_SERVER_PORT_HTTPS = "SERVER_PORT_HTTPS";
  public static final String KEY_SERVER_PORT_HTTP = "SERVER_PORT_HTTP";
  public static final String KEY_REDIRECT_HTTP_TO_HTTPS_PORT = "REDIRECT_HTTP_TO_HTTPS_PORT";

  public static final String KEY_HTTPS_ENABLED_PROTOCOLS = "HTTPS_ENABLED_PROTOCOLS";
  public static final String KEY_HTTPS_DISABLED_PROTOCOLS = "HTTPS_DISABLED_PROTOCOLS";
  public static final String KEY_HTTPS_ENABLED_CIPHER_SUITES = "HTTPS_ENABLED_CIPHER_SUITES";
  public static final String KEY_HTTPS_DISABLED_CIPHER_SUITES = "HTTPS_DISABLED_CIPHER_SUITES";

  public static final String KEY_CERTIFICATES_KEY_STORE_PATH = "CERTIFICATES_KEY_STORE_PATH";
  public static final String KEY_CERTIFICATES_KEY_STORE_PASSWORD =
      "CERTIFICATES_KEY_STORE_PASSWORD";
  public static final String KEY_CERTIFICATES_KEY_PASSWORD = "CERTIFICATES_KEY_PASSWORD";

  public static final String KEY_INITIAL_CONNECTIONS = "INITIAL_CONNECTIONS";

  public static final String KEY_MAX_CONNECTIONS = "MAX_CONNECTIONS";

  public static final String KEY_MAIL_HANDLER_CLASS = "MAIL_HANDLER_CLASS";
  public static final String KEY_MAIL_TO_FILE_HANDLER_LOC = "MAIL_TO_FILE_HANDLER_LOC";

  public static final String KEY_ADMIN_NAME = "ADMIN_NAME";
  public static final String KEY_ADMIN_EMAIL = "ADMIN_EMAIL";
  public static final String KEY_BUGTRACKER_URI = "BUGTRACKER_URI";

  public static final String KEY_SESSION_TIMEOUT_MS = "SESSION_TIMEOUT_MS";
  public static final String KEY_ACTIVATION_TIMEOUT_MS = "ACTIVATION_TIMEOUT_MS";

  public static final String KEY_CACHE_CONF_LOC = "CACHE_CONF_LOC";

  public static final String KEY_RULES_CACHE_CAPACITY = "RULES_CACHE_CAPACITY";
  public static final String KEY_SPARSE_ENTITY_CACHE_CAPACITY = "SPARSE_ENTITY_CACHE_CAPACITY";
  public static final String KEY_PROPERTIES_CACHE_CAPACITY = "PROPERTIES_CACHE_CAPACITY";
  public static final String KEY_PARENTS_CACHE_CAPACITY = "PARENTS_CACHE_CAPACITY";
  public static final String KEY_USER_ACCOUNT_CACHE_CAPACITY = "USER_ACCOUNT_CACHE_CAPACITY";
  public static final String KEY_GROUP_CACHE_CAPACITY = "GROUP_CACHE_CAPACITY";

  public static final String KEY_TRANSACTION_BENCHMARK_ENABLED = "TRANSACTION_BENCHMARK_ENABLED";

  public static final String KEY_INSERT_FILES_IN_DIR_ALLOWED_DIRS =
      "INSERT_FILES_IN_DIR_ALLOWED_DIRS"; // see
  // server/jobs/core/InsertFilesInDir.java

  public static final String KEY_SUDO_PASSWORD = "SUDO_PASSWORD";

  public static final String KEY_USER_SOURCES_INI_FILE = "USER_SOURCES_INI_FILE";

  public static final String KEY_NEW_USER_DEFAULT_ACTIVITY = "NEW_USER_DEFAULT_ACTIVITY";

  public static final String KEY_PASSWORD_STRENGTH_REGEX = "PASSWORD_STRENGTH_REGEX";

  public static final String KEY_QUERY_FILTER_ENTITIES_WITHOUT_RETRIEVE_PERMISSIONS =
      "QUERY_FILTER_ENTITIES_WITHOUT_RETRIEVE_PERMISSIONS";

  public static final String KEY_SERVER_NAME = "SERVER_NAME";

  public static final String KEY_SERVER_OWNER = "SERVER_OWNER";

  public static final String KEY_SERVER_SIDE_SCRIPTING_BIN_DIR = "SERVER_SIDE_SCRIPTING_BIN_DIR";
  public static final String KEY_SERVER_SIDE_SCRIPTING_WORKING_DIR =
      "SERVER_SIDE_SCRIPTING_WORKING_DIR";

  public static final String KEY_NO_REPLY_EMAIL = "NO_REPLY_EMAIL";

  public static final String KEY_NO_REPLY_NAME = "NO_REPLY_NAME";

  public static final String KEY_CHECK_ENTITY_ACL_ROLES_MODE = "CHECK_ENTITY_ACL_ROLES_MODE";

  public static final String KEY_GLOBAL_ENTITY_PERMISSIONS_FILE = "GLOBAL_ENTITY_PERMISSIONS_FILE";
  public static final String KEY_TIMEZONE = "TIMEZONE";

  /**
   * Read the config files and initialize the server properties.
   *
   * @throws IOException
   */
  public static Properties initServerProperties() throws IOException {
    final Properties serverProperties = new Properties();
    final String basepath = System.getProperty("user.dir");

    // load standard config
    loadConfigFile(serverProperties, new File(basepath + "/conf/core/server.conf"));

    // load ext/server.conf
    loadConfigFile(serverProperties, new File(basepath + "/conf/ext/server.conf"));

    // load ext/server.conf.d/ (ordering is determined by system collation)
    File confDir = new File(basepath + "/conf/ext/server.conf.d");
    if (confDir.exists() && confDir.isDirectory()) {
      String[] confFiles = confDir.list();
      Arrays.sort(confFiles, Comparator.naturalOrder());
      for (String confFile : confFiles) {
        // prevent backup files from being read
        if (confFile.endsWith(".conf")) {
          loadConfigFile(serverProperties, new File(confDir, confFile));
        }
      }
    }

    // load env vars
    for (java.util.Map.Entry<String, String> envvar : System.getenv().entrySet()) {
      if (envvar.getKey().startsWith("CAOSDB_CONFIG_") && envvar.getKey().length() > 14) {
        serverProperties.setProperty(envvar.getKey().substring(14), envvar.getValue());
      }
    }

    // log configuration alphabetically
    if (logger.isInfoEnabled()) {
      ArrayList<String> names = new ArrayList<>(serverProperties.stringPropertyNames());
      Collections.sort(names);
      for (String name : names) {
        String val = name.contains("PASSW") ? "****" : serverProperties.getProperty(name);
        logger.info(name + "=" + val);
      }
    }
    return serverProperties;
  }

  private static void loadConfigFile(Properties serverProperties, File confFile)
      throws IOException {
    if (confFile.exists() && confFile.isFile()) {
      logger.info("Reading configuration from " + confFile.getAbsolutePath());
      final BufferedInputStream sp_in = new BufferedInputStream(new FileInputStream(confFile));
      serverProperties.load(sp_in);
      sp_in.close();
    }
  }
}
