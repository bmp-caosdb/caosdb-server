/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.accessControl;

import com.google.common.base.Objects;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;

public class CaosDBAuthorizingRealm extends AuthorizingRealm {

  private static class PermissionPrincipalCollection extends SimplePrincipalCollection {

    private final List<String> permissions;

    public PermissionPrincipalCollection(
        final PrincipalCollection principals, final String[] permissions) {
      super(principals);
      this.permissions = Arrays.asList(permissions);
    }

    private static final long serialVersionUID = 5585425107072564933L;

    @Override
    public boolean equals(final Object obj) {
      if (obj == this) {
        return true;
      } else if (obj instanceof PermissionPrincipalCollection) {
        final PermissionPrincipalCollection that = (PermissionPrincipalCollection) obj;
        return Objects.equal(that.permissions, this.permissions) && super.equals(that);
      } else {
        return false;
      }
    }

    @Override
    public int hashCode() {
      return super.hashCode() + 28 * this.permissions.hashCode();
    }
  }

  static class PermissionAuthenticationInfo implements AuthenticationInfo {

    private static final long serialVersionUID = -3714484164124767976L;
    private final PermissionPrincipalCollection principalCollection;

    public PermissionAuthenticationInfo(
        final PrincipalCollection principals, final String... permissions) {
      this.principalCollection = new PermissionPrincipalCollection(principals, permissions);
    }

    @Override
    public PrincipalCollection getPrincipals() {
      return this.principalCollection;
    }

    @Override
    public Object getCredentials() {
      return null;
    }

    @Override
    public boolean equals(final Object obj) {
      if (obj == this) {
        return true;
      } else if (obj instanceof PermissionAuthenticationInfo) {
        final PermissionAuthenticationInfo that = (PermissionAuthenticationInfo) obj;
        return Objects.equal(that.principalCollection, this.principalCollection);
      } else {
        return false;
      }
    }

    @Override
    public int hashCode() {
      return this.principalCollection.hashCode();
    }
  }

  private static final CaosDBRolePermissionResolver role_permission_resolver =
      new CaosDBRolePermissionResolver();

  @Override
  protected AuthorizationInfo doGetAuthorizationInfo(final PrincipalCollection principals) {
    if (principals instanceof PermissionPrincipalCollection) {
      // the PrincialsCollection carries the permissions.
      final SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
      info.addStringPermissions(((PermissionPrincipalCollection) principals).permissions);
      return info;
    }

    final SimpleAuthorizationInfo authzInfo = new SimpleAuthorizationInfo();

    // find all roles which are associated with this principal in this
    // realm.
    final Set<String> roles = UserSources.resolve(principals);
    if (roles != null) {
      authzInfo.setRoles(roles);

      // find all permissions which are associated with these roles.
      authzInfo.addObjectPermission(role_permission_resolver.resolvePermissionsInRole(roles));
    }

    return authzInfo;
  }

  public CaosDBAuthorizingRealm() {
    setCachingEnabled(false);
    setAuthorizationCachingEnabled(false);
  }

  @Override
  public boolean supports(final AuthenticationToken token) {
    // this is not an authenticating realm - just for authorizing.
    return false;
  }

  @Override
  protected AuthenticationInfo doGetAuthenticationInfo(final AuthenticationToken token) {
    return null;
  }
}
