/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.accessControl;

import static caosdb.server.utils.Utils.URLDecodeWithUTF8;

import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import caosdb.server.entity.Message;
import caosdb.server.entity.Message.MessageType;
import caosdb.server.permissions.ResponsibleAgent;
import caosdb.server.permissions.Role;
import caosdb.server.utils.Utils;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.LinkedList;
import org.apache.shiro.authc.AuthenticationToken;
import org.restlet.data.Cookie;
import org.restlet.data.CookieSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Useful static methods, mainly for parsing and serializing SessionTokens by the means of web
 * cookies and producing session timeout cookies.
 *
 * @author tf
 */
public class AuthenticationUtils {

  private static final Logger logger = LoggerFactory.getLogger(AuthenticationUtils.class);

  public static final String ONE_TIME_TOKEN_COOKIE = "OneTimeToken";
  public static final Message UNAUTHENTICATED =
      new Message(MessageType.Error, 401, "Sign up, please!");
  public static final String SESSION_TOKEN_COOKIE = "SessionToken";
  public static final String SESSION_TIMEOUT_COOKIE = "SessionTimeOut";

  public static final AuthenticationToken ANONYMOUS_USER =
      AnonymousAuthenticationToken.getInstance();

  /**
   * Create a cookie for a {@link SelfValidatingAuthenticationToken}. Returns null if the parameter
   * is null or the token is invalid. The cookie will have the httpOnly and secure flags enabled.
   *
   * @param token
   * @return A Cookie for the token.
   * @see {@link AuthenticationUtils#parseSessionTokenCookie(Cookie, String)}, {@link
   *     SelfValidatingAuthenticationToken}
   */
  private static CookieSetting createTokenCookie(
      final String cookieName, final SelfValidatingAuthenticationToken token) {
    if (token != null && token.isValid()) {
      // new expiration time.
      final int exp_in_sec = (int) Math.ceil(token.getTimeout() / 1000.0);
      return new CookieSetting(
          0,
          cookieName,
          Utils.URLEncodeWithUTF8(token.toString()),
          CaosDBServer.getServerProperty(ServerProperties.KEY_CONTEXT_ROOT) + "/",
          null,
          null,
          exp_in_sec,
          true,
          true);
    }
    return null;
  }

  public static CookieSetting createOneTimeTokenCookie(final OneTimeAuthenticationToken token) {
    return createTokenCookie(AuthenticationUtils.ONE_TIME_TOKEN_COOKIE, token);
  }

  public static CookieSetting createSessionTokenCookie(final SessionToken token) {
    return createTokenCookie(AuthenticationUtils.SESSION_TOKEN_COOKIE, token);
  }

  /**
   * Parse a SessionToken from a cookie with optional cryptographic curry.
   *
   * @param cookie
   * @param curry
   * @return A new SessionToken
   * @see {@link AuthenticationUtils#createSessionTokenCookie(SessionToken)}, {@link SessionToken}
   */
  public static SessionToken parseSessionTokenCookie(final Cookie cookie, final String curry) {
    if (cookie != null) {
      final String tokenString = URLDecodeWithUTF8(cookie.getValue());
      if (tokenString != null && !tokenString.equals("")) {
        try {
          return SessionToken.parse(tokenString, curry);
        } catch (final Exception e) {
          logger.warn("AUTHTOKEN_PARSING_FAILED", e);
        }
      }
    }
    return null;
  }

  private static OneTimeAuthenticationToken parseOneTimeToken(
      final String tokenString, final String curry) {
    if (tokenString != null && !tokenString.equals("")) {
      try {
        return OneTimeAuthenticationToken.parse(tokenString, curry);
      } catch (final Exception e) {
        logger.warn("AUTHTOKEN_PARSING_FAILED", e);
      }
    }
    return null;
  }

  public static OneTimeAuthenticationToken parseOneTimeTokenQuerySegment(
      final String tokenString, final String curry) {
    if (tokenString != null) {
      return parseOneTimeToken(URLDecodeWithUTF8(tokenString), curry);
    }
    return null;
  }

  public static OneTimeAuthenticationToken parseOneTimeTokenCookie(
      final Cookie cookie, final String curry) {
    if (cookie != null) {
      return parseOneTimeToken(URLDecodeWithUTF8(cookie.getValue()), curry);
    }
    return null;
  }

  /**
   * Create a session timeout cookie. The value is a plain UTC timestamp which tells the user how
   * long his session will stay active. This cookie will be ignored by the server and carries only
   * additional information for the user interfaces (E.g. they can remind the user before her
   * session is expiring or do an auto-logout based on this timestamp). Of course, the cookie will
   * be flagged with httpOnly:false (but secure:true).
   *
   * @param token a SessionToken
   * @return
   */
  public static CookieSetting createSessionTimeoutCookie(final SessionToken token) {

    // replace empty space with "T" for iso 8601 compliance
    String t = new Timestamp(0).toString().replaceFirst(" ", "T");
    int exp_in_sec = 0; // expires in zero seconds.

    if (token != null && token.isValid()) {
      t = new Timestamp(token.getExpires()).toString().replaceFirst(" ", "T");
      exp_in_sec = (int) Math.ceil(token.getTimeout() / 1000.0); // new
      // expiration
      // time.
      return new CookieSetting(
          0,
          AuthenticationUtils.SESSION_TIMEOUT_COOKIE,
          Utils.URLEncodeWithUTF8(t),
          CaosDBServer.getServerProperty(ServerProperties.KEY_CONTEXT_ROOT) + "/",
          null,
          null,
          exp_in_sec,
          false,
          false);
    }
    return null;
  }

  public static boolean isResponsibleAgentExistent(final ResponsibleAgent agent) {
    // 1) check OWNER, OTHER
    if (Role.OTHER_ROLE.equals(agent) || Role.OWNER_ROLE.equals(agent)) {
      return true;
    }

    // 2) check role exists in UserSources
    if (UserSources.isRoleExisting(agent.toString())) {
      return true;
    }

    // 3) check user exists in UserSources
    return agent instanceof Principal && UserSources.isUserExisting((Principal) agent);
  }

  public static CookieSetting getLogoutSessionTokenCookie() {
    return new CookieSetting(
        0,
        AuthenticationUtils.SESSION_TOKEN_COOKIE,
        "invalid",
        CaosDBServer.getServerProperty(ServerProperties.KEY_CONTEXT_ROOT) + "/",
        null,
        null,
        0,
        true,
        true);
  }

  public static Collection<? extends CookieSetting> getLogoutCookies() {
    final LinkedList<CookieSetting> ret = new LinkedList<CookieSetting>();
    ret.add(getLogoutSessionTokenCookie());
    ret.add(getLogoutSessionTimeoutCookie());
    return ret;
  }

  private static CookieSetting getLogoutSessionTimeoutCookie() {
    return new CookieSetting(
        0,
        AuthenticationUtils.SESSION_TIMEOUT_COOKIE,
        "invalid",
        CaosDBServer.getServerProperty(ServerProperties.KEY_CONTEXT_ROOT) + "/",
        null,
        null,
        0,
        false,
        false);
  }
}
