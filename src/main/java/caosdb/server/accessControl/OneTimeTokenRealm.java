/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.accessControl;

import caosdb.server.accessControl.CaosDBAuthorizingRealm.PermissionAuthenticationInfo;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.AllowAllCredentialsMatcher;

public class OneTimeTokenRealm extends SessionTokenRealm {

  @Override
  protected AuthenticationInfo doGetAuthenticationInfo(final AuthenticationToken token)
      throws AuthenticationException {

    final AuthenticationInfo info = super.doGetAuthenticationInfo(token);
    if (info != null) {
      return new PermissionAuthenticationInfo(
          info.getPrincipals(), ((OneTimeAuthenticationToken) token).getPermissions());
    }

    return null;
  }

  public OneTimeTokenRealm() {
    setAuthenticationTokenClass(OneTimeAuthenticationToken.class);
    setCredentialsMatcher(new AllowAllCredentialsMatcher());
    setCachingEnabled(false);
    setAuthenticationCachingEnabled(false);
    // setAuthorizationCachingEnabled(false);
  }
}
