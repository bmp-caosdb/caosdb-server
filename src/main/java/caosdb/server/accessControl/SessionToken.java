/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.accessControl;

import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import org.eclipse.jetty.util.ajax.JSON;

public class SessionToken extends SelfValidatingAuthenticationToken {

  /**
   * Cryptographic pepper. Generated when class is loaded and used until the server reboots. Hence
   * all SessionTokens invalidate when the server reboots.
   */
  private static final transient String PEPPER = java.util.UUID.randomUUID().toString();

  public static final String SEP = ":";

  public SessionToken(
      final Principal principal,
      final long date,
      final long timeout,
      final String salt,
      final String curry,
      final String checksum) {
    super(principal, date, timeout, salt, curry, checksum);
  }

  public SessionToken(
      final Principal principal,
      final long date,
      final long timeout,
      final String salt,
      final String curry) {
    super(principal, date, timeout, salt, curry);
  }

  private static final long serialVersionUID = 5887135104218573761L;

  @Override
  public String calcChecksum() {
    return calcChecksum(
        this.date,
        this.timeout,
        this.principal.getRealm(),
        this.principal.getUsername(),
        PEPPER,
        this.salt,
        this.curry);
  }

  @Override
  public String toString() {
    return JSON.toString(
        new Object[] {
          this.principal.getRealm(),
          this.principal.getUsername(),
          this.date,
          this.timeout,
          this.salt,
          this.checksum
        });
  }

  public static SessionToken parse(final String token, final String curry) {
    final Object[] array = (Object[]) JSON.parse(token);
    final Principal principal = new Principal((String) array[0], (String) array[1]);
    final long date = (Long) array[2];
    final long timeout = (Long) array[3];
    final String salt = (String) array[4];
    final String checksum = (String) array[5];
    return new SessionToken(principal, date, timeout, salt, curry, checksum);
  }

  public static SessionToken generate(final Principal principal, final String curry) {
    final SessionToken ret =
        new SessionToken(
            principal,
            System.currentTimeMillis(),
            Long.parseLong(
                CaosDBServer.getServerProperty(ServerProperties.KEY_SESSION_TIMEOUT_MS).trim()),
            java.util.UUID.randomUUID().toString(),
            curry);
    if (!ret.isValid()) {
      throw new RuntimeException("SessionToken not valid!");
    }
    return ret;
  }
}
