/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.accessControl;

import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import caosdb.server.entity.Message;
import caosdb.server.transaction.RetrieveRoleTransaction;
import caosdb.server.transaction.RetrieveUserTransaction;
import caosdb.server.utils.ServerMessages;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.config.Ini;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserSources extends HashMap<String, UserSource> {

  public static final String ANONYMOUS_ROLE = "anonymous";
  private static final Logger logger = LoggerFactory.getLogger(UserSources.class);
  public static final String KEY_DEFAULT_REALM = "defaultRealm";
  public static final String KEY_REALMS = "defaultRealm";
  public static final String KEY_REALM_CLASS = "class";

  private static final long serialVersionUID = 6782744064206400521L;
  private static final UserSource internalRealm = new InternalUserSource();

  private static UserSources instance = new UserSources();

  public static boolean isUserExisting(final Principal principal) {
    return instance.get(principal.getRealm()).isUserExisting(principal.getUsername());
  }

  private UserSources() {
    initMap();
    this.put(getInternalRealm());
    final String[] realms =
        this.map
            .getSectionProperty(Ini.DEFAULT_SECTION_NAME, KEY_REALMS)
            .split("(\\s*,\\s*)|(\\s+)");
    for (final String realm : realms) {
      if (realm != null && !realm.isEmpty()) {
        final String className = this.map.getSectionProperty(realm, KEY_REALM_CLASS);
        if (className != null && !className.isEmpty()) {
          try {
            @SuppressWarnings("unchecked")
            final Class<? extends UserSource> clazz =
                (Class<? extends UserSource>) Class.forName(className);
            this.put(clazz.newInstance());
          } catch (final ClassNotFoundException e) {
            logger.error("LOAD_USER_SOURCE", e);
          } catch (final InstantiationException e) {
            logger.error("LOAD_USER_SOURCE", e);
          } catch (final IllegalAccessException e) {
            logger.error("LOAD_USER_SOURCE", e);
          }
        }
      }
    }
  }

  public UserSource put(final UserSource src) {
    if (src.getName() == null) {
      throw new IllegalArgumentException("A user source's name must not be null.");
    }
    if (containsKey(src.getName())) {
      throw new IllegalArgumentException("This user source's name is already in use.");
    }
    src.setMap(this.map.getSection(src.getName()));
    return put(src.getName(), src);
  }

  public static UserSource add(final UserSource src) {
    return instance.put(src);
  }

  private Ini map = null;

  public void initMap() {
    this.map = null;
    try {
      final FileInputStream f =
          new FileInputStream(
              CaosDBServer.getServerProperty(ServerProperties.KEY_USER_SOURCES_INI_FILE));
      this.map = new Ini();
      this.map.load(f);
      f.close();
    } catch (final FileNotFoundException e) {
      e.printStackTrace();
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Return the roles of a given user.
   *
   * @param realm
   * @param username
   * @return
   */
  public static Set<String> resolve(String realm, final String username) {

    if (realm == null) {
      realm = guessRealm(username);
    }

    // find all roles that are associated with this principal in this realm
    final Set<String> ret = instance.get(realm).resolveRolesForUsername(username);

    return ret;
  }

  public static String guessRealm(final String username) {
    String found = null;
    for (final UserSource u : instance.values()) {
      if (u.isUserExisting(username)) {
        if (found == null) {
          found = u.getName();
        } else {
          throw new AuthenticationException(
              "This user cannot be uniquely identified. This name is used in several realms. ");
        }
      }
    }
    return found;
  }

  public static String guessRealm(final String username, final String defaultRealm) {
    final String found = guessRealm(username);
    if (found != null) {
      return found;
    }

    return defaultRealm;
  }

  public static String getDefaultRealm() {
    return instance.map.getSectionProperty(Ini.DEFAULT_SECTION_NAME, KEY_DEFAULT_REALM);
  }

  public static Set<String> resolve(final PrincipalCollection principals) {
    if (principals.getPrimaryPrincipal() == AuthenticationUtils.ANONYMOUS_USER.getPrincipal()) {
      // anymous has one role
      Set<String> roles = new HashSet<>();
      roles.add(ANONYMOUS_ROLE);
      return roles;
    }

    Principal primaryPrincipal = (Principal) principals.getPrimaryPrincipal();
    return resolve(primaryPrincipal.getRealm(), primaryPrincipal.getUsername());
  }

  public static boolean isRoleExisting(final String role) {
    final RetrieveRoleTransaction t = new RetrieveRoleTransaction(role);
    try {
      t.execute();
      return true;
    } catch (final Message m) {
      if (m != ServerMessages.ROLE_DOES_NOT_EXIST) {
        throw new AuthenticationException(m);
      }
      return false;
    } catch (final Exception e) {
      throw new AuthenticationException(e);
    }
  }

  public static UserStatus getDefaultUserStatus(final Principal p) {
    return instance.get(p.getRealm()).getDefaultUserStatus(p.getUsername());
  }

  public static String getDefaultUserEmail(final Principal p) {
    return instance.get(p.getRealm()).getDefaultUserEmail(p.getUsername());
  }

  public static UserSource getInternalRealm() {
    return internalRealm;
  }

  public static boolean isValid(String realm, final String username, final String password) {
    if (realm == null) {
      realm = guessRealm(username);
    }

    final boolean isValid = instance.get(realm).isValid(username, password);
    return isValid && isActive(realm, username);
  }

  private static boolean isActive(final String realm, final String username) {
    final RetrieveUserTransaction t = new RetrieveUserTransaction(realm, username);
    try {
      t.execute();
      return t.isActive();
    } catch (final Exception e) {
      throw new AuthenticationException(e);
    }
  }
}
