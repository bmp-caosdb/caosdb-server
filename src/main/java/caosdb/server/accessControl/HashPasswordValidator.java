/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.accessControl;

import caosdb.server.utils.Utils;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashPasswordValidator implements CredentialsValidator<String> {

  private final String password;
  private final int it;
  private final String salt;
  private final MessageDigest alg;

  public HashPasswordValidator(
      final String alg, final String password, final String salt, final int it)
      throws NoSuchAlgorithmException {
    this.alg = MessageDigest.getInstance(alg);
    this.password = password;
    this.salt = salt;
    this.it = it;
  }

  @Override
  public boolean isValid(final String password) {
    return this.password != null
        && this.password.equalsIgnoreCase(Utils.hash(this.alg, password, this.salt, this.it));
  }
}
