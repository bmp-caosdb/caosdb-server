/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.accessControl;

import caosdb.server.utils.Utils;
import org.apache.shiro.authc.AuthenticationToken;

public abstract class SelfValidatingAuthenticationToken implements AuthenticationToken {

  private static final long serialVersionUID = -7212039848895531161L;
  protected final long date;
  protected final long timeout;
  protected final String checksum;
  protected final Principal principal;
  protected final String curry;
  protected final String salt;

  public SelfValidatingAuthenticationToken(
      final Principal principal,
      final long date,
      final long timeout,
      final String salt,
      final String curry,
      final String checksum) {
    this.date = date;
    this.timeout = timeout;
    this.principal = principal;
    this.salt = salt;
    this.curry = curry;
    this.checksum = checksum;
  }

  public SelfValidatingAuthenticationToken(
      final Principal principal,
      final long date,
      final long timeout,
      final String salt,
      final String curry) {
    this.date = date;
    this.timeout = timeout;
    this.principal = principal;
    this.salt = salt;
    this.curry = curry;
    this.checksum = calcChecksum();
  }

  @Override
  public Principal getPrincipal() {
    return this.principal;
  }

  @Override
  public Object getCredentials() {
    return null;
  }

  public long getExpires() {
    return this.date + this.timeout;
  }

  public long getTimeout() {
    return this.timeout;
  }

  public boolean isExpired() {
    return System.currentTimeMillis() >= getExpires();
  }

  public boolean isHashValid() {
    final String other = calcChecksum();
    return this.checksum != null && this.checksum.equals(other);
  }

  public boolean isValid() {
    return !isExpired() && isHashValid();
  }

  public abstract String calcChecksum();

  protected static String calcChecksum(final Object... fields) {
    final StringBuilder sb = new StringBuilder();
    for (final Object field : fields) {
      if (field != null) {
        sb.append(field.toString());
      }
      sb.append(":");
    }
    return Utils.sha512(sb.toString(), null, 1);
  }
}
