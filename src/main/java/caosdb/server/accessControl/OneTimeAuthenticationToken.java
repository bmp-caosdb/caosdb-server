/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.accessControl;

import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import java.util.UUID;
import org.eclipse.jetty.util.ajax.JSON;

public class OneTimeAuthenticationToken extends SelfValidatingAuthenticationToken {

  private static final transient String PEPPER = java.util.UUID.randomUUID().toString();
  private final String[] permissions;

  public OneTimeAuthenticationToken(
      final Principal principal,
      final long date,
      final long timeout,
      final String salt,
      final String curry,
      final String checksum,
      final String... permissions) {
    super(principal, date, timeout, salt, curry, checksum);
    this.permissions = permissions;
  }

  public OneTimeAuthenticationToken(
      final Principal principal,
      final long date,
      final long timeout,
      final String salt,
      final String curry,
      final String... permissions) {
    super(
        principal,
        date,
        timeout,
        salt,
        curry,
        calcChecksum(
            principal.getRealm(),
            principal.getUsername(),
            date,
            timeout,
            salt,
            curry,
            calcChecksum((Object[]) permissions),
            PEPPER));
    this.permissions = permissions;
  }

  private static final long serialVersionUID = -1072740888045267613L;

  public String[] getPermissions() {
    return this.permissions;
  }

  @Override
  public String calcChecksum() {
    return calcChecksum(
        this.principal.getRealm(),
        this.principal.getUsername(),
        this.date,
        this.timeout,
        this.salt,
        this.curry,
        calcChecksum((Object[]) this.permissions),
        PEPPER);
  }

  @Override
  public String toString() {
    return JSON.toString(
        new Object[] {
          this.principal.getRealm(),
          this.principal.getUsername(),
          this.date,
          this.timeout,
          this.salt,
          this.permissions,
          this.checksum
        });
  }

  private static String[] toStringArray(final Object[] array) {
    final String[] ret = new String[array.length];
    for (int i = 0; i < ret.length; i++) {
      ret[i] = (String) array[i];
    }
    return ret;
  }

  public static OneTimeAuthenticationToken parse(final String token, final String curry) {
    final Object[] array = (Object[]) JSON.parse(token);
    final Principal principal = new Principal((String) array[0], (String) array[1]);
    final long date = (Long) array[2];
    final long timeout = (Long) array[3];
    final String salt = (String) array[4];
    final String[] permissions = toStringArray((Object[]) array[5]);
    final String checksum = (String) array[6];
    return new OneTimeAuthenticationToken(
        principal, date, timeout, salt, curry, checksum, permissions);
  }

  public static OneTimeAuthenticationToken generate(
      final Principal principal, final String curry, final String... permissions) {
    return new OneTimeAuthenticationToken(
        principal,
        System.currentTimeMillis(),
        Long.parseLong(CaosDBServer.getServerProperty(ServerProperties.KEY_ACTIVATION_TIMEOUT_MS)),
        UUID.randomUUID().toString(),
        curry,
        permissions);
  }
}
