/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.accessControl;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.shiro.authz.AuthorizationException;
import org.jvnet.libpam.PAMException;
import org.jvnet.libpam.UnixUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Pam implements UserSource {

  public static class DefaultPamScriptCaller implements PamScriptCaller {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private final String pam_script;

    public DefaultPamScriptCaller(String pam_script) {
      if (pam_script == null) {
        pam_script = DEFAULT_PAM_SCRIPT;
      }
      this.pam_script = pam_script;
    }

    public Process getProcess(final String username, final String password) throws IOException {

      final File script = new File(this.pam_script);

      final ProcessBuilder pb = new ProcessBuilder(script.getAbsolutePath(), username, password);
      pb.directory(script.getParentFile());
      return pb.start();
    }

    @Override
    public boolean isValid(final String username, final String password) {
      Process pam_authentication;

      try {
        pam_authentication = getProcess(username, password);
        logger.info("call pam script");
        return pam_authentication.waitFor() == 0;
      } catch (final IOException e) {
        throw new RuntimeException(e);
      } catch (final InterruptedException e) {
        throw new RuntimeException(e);
      }
    }
  }

  public static final String DEFAULT_PAM_SCRIPT = "./misc/pam_authentication/pam_authentication.sh";
  public static final String KEY_PAM_SCRIPT = "pam_script";
  public static final String KEY_DEFAULT_USER_STATUS = "default_status";
  public static final String KEY_GROUP = "group";
  public static final String KEY_USER = "user";
  public static final String KEY_EXCLUDE = "exclude";
  public static final String KEY_INCLUDE = "include";
  public static final String KEY_ROLES = "roles";
  public static final String SEPARATOR = ".";
  public static final String REGEX_SPLIT_CSV = "\\s*,\\s*";
  private static final String KEY_EMAIL = "email";
  private String[] EXCLUDE_USERS = null;
  private String[] INCLUDE_USERS = null;
  private String[] EXCLUDE_GROUPS = null;
  private String[] INCLUDE_GROUPS = null;
  private Map<String, String> map = null;
  private PamScriptCaller pamScriptCaller = null;

  @Override
  public void setMap(final Map<String, String> map) {
    this.map = map;
    this.pamScriptCaller = null;
  }

  public Map<String, String> getMap() {
    return this.map;
  }

  @Override
  public Set<String> resolveRolesForUsername(final String username) {
    final Set<String> resulting_roles = new HashSet<String>();

    // resolve roles per unix user
    final String userStr = KEY_USER + SEPARATOR + username + SEPARATOR + KEY_ROLES;
    if (getMap().containsKey(userStr)) {
      final String[] roles = getMap().get(userStr).split(REGEX_SPLIT_CSV);
      for (final String role : roles) {
        resulting_roles.add(role);
      }
    }

    // resolve roles per unix group
    final Set<String> groups = getGroups(username);
    for (final String g : groups) {
      final String groupStr = KEY_GROUP + SEPARATOR + g + SEPARATOR + KEY_ROLES;
      if (getMap().containsKey(groupStr)) {
        final String[] roles = getMap().get(groupStr).split(REGEX_SPLIT_CSV);
        for (final String role : roles) {
          resulting_roles.add(role);
        }
      }
    }

    // filter non existing
    final Iterator<String> iterator = resulting_roles.iterator();
    while (iterator.hasNext()) {
      final String role = iterator.next();
      if (!UserSources.isRoleExisting(role)) {
        iterator.remove();
      }
    }
    return resulting_roles;
  }

  private static Set<String> getGroups(final String username) {
    if (UnixUser.exists(username)) {
      try {
        return new UnixUser(username).getGroups();
      } catch (final PAMException e) {
        throw new AuthorizationException(e);
      }
    }
    return null;
  }

  @Override
  public String getName() {
    return "PAM";
  }

  @Override
  public boolean isUserExisting(final String username) {
    return username != null && UnixUser.exists(username) && isIncorporated(username);
  }

  @Override
  public boolean isValid(final String username, final String password) {
    if (isUserExisting(username)) {
      return isValid(getPamScriptCaller(), username, password);
    }
    return false;
  }

  private PamScriptCaller getPamScriptCaller() {
    if (this.pamScriptCaller == null) {
      this.pamScriptCaller = new DefaultPamScriptCaller(getMap().get(KEY_PAM_SCRIPT));
    }
    return this.pamScriptCaller;
  }

  private boolean isValid(
      final PamScriptCaller caller, final String username, final String password) {
    return caller.isValid(username, password);
  }

  /**
   * Tests if a user is in the allowed groups that should be authenticated and may access this
   * database.
   *
   * @param user
   * @return true iff the user may access this database.
   * @throws PAMException
   */
  private boolean isIncorporated(final String user) {
    final Set<String> groups = getGroups(user);
    boolean ret = false;
    for (final String g : INCLUDE_GROUPS()) {
      if (groups.contains(g)) {
        ret = true;
        break;
      }
    }
    for (final String g : EXCLUDE_GROUPS()) {
      if (groups.contains(g)) {
        ret = false;
        break;
      }
    }
    for (final String n : INCLUDE_USERS()) {
      if (user.equals(n)) {
        ret = true;
        break;
      }
    }
    for (final String n : EXCLUDE_USERS()) {
      if (user.equals(n)) {
        ret = false;
        break;
      }
    }
    return ret;
  }

  private String[] EXCLUDE_USERS() {
    if (this.EXCLUDE_USERS == null) {
      final String s = this.map.get(KEY_EXCLUDE + SEPARATOR + KEY_USER);
      if (s == null) {
        this.EXCLUDE_USERS = new String[0];
      } else {
        this.EXCLUDE_USERS = s.split("(\\s*,\\s*)|(\\s+)");
      }
    }
    return this.EXCLUDE_USERS;
  }

  private String[] INCLUDE_USERS() {
    if (this.INCLUDE_USERS == null) {
      final String s = this.map.get(KEY_INCLUDE + SEPARATOR + KEY_USER);
      if (s == null) {
        this.INCLUDE_USERS = new String[0];
      } else {
        this.INCLUDE_USERS = s.split("(\\s*,\\s*)|(\\s+)");
      }
    }
    return this.INCLUDE_USERS;
  }

  private String[] EXCLUDE_GROUPS() {
    if (this.EXCLUDE_GROUPS == null) {
      final String s = this.map.get(KEY_EXCLUDE + SEPARATOR + KEY_GROUP);
      if (s == null) {
        this.EXCLUDE_GROUPS = new String[0];
      } else {
        this.EXCLUDE_GROUPS = s.split("(\\s*,\\s*)|(\\s+)");
      }
    }
    return this.EXCLUDE_GROUPS;
  }

  private String[] INCLUDE_GROUPS() {
    if (this.INCLUDE_GROUPS == null) {
      final String s = this.map.get(KEY_INCLUDE + SEPARATOR + KEY_GROUP);
      if (s == null) {
        this.INCLUDE_GROUPS = new String[0];
      } else {
        this.INCLUDE_GROUPS = s.split("(\\s*,\\s*)|(\\s+)");
      }
    }
    return this.INCLUDE_GROUPS;
  }

  @Override
  public UserStatus getDefaultUserStatus(final String username) {
    // by groups
    final Set<String> groups = getGroups(username);
    UserStatus ret = null;
    for (final String g : groups) {
      final String groupStr = KEY_GROUP + SEPARATOR + g + SEPARATOR + KEY_DEFAULT_USER_STATUS;
      if (this.map.containsKey(groupStr)) {
        final UserStatus n = UserStatus.valueOf(this.map.get(groupStr).toUpperCase());
        if (ret == null) {
          ret = n;
        } else if (ret != n) {
          // conflict -> ignore, go for pam-wide setting
          break;
        }
      }
    }
    // by pam-wide setting
    if (this.map.containsKey(KEY_DEFAULT_USER_STATUS)) {
      return UserStatus.valueOf(this.map.get(KEY_DEFAULT_USER_STATUS).toUpperCase());
    }

    return UserStatus.INACTIVE;
  }

  @Override
  public String getDefaultUserEmail(final String username) {
    return this.map.get(KEY_USER + SEPARATOR + username + SEPARATOR + KEY_EMAIL);
  }
}
