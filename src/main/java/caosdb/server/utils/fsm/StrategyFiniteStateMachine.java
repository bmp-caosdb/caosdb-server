/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.utils.fsm;

import java.util.Map;

public class StrategyFiniteStateMachine<S extends State, T extends Transition, I>
    extends FiniteStateMachine<S, T> {

  public StrategyFiniteStateMachine(
      final S initial, final Map<S, I> stateImplementations, final Map<S, Map<T, S>> transitions)
      throws MissingImplementationException, StateNotReachableException {
    super(initial, transitions);
    this.stateImplementations = stateImplementations;
    checkImplementationsComplete();
  }

  /**
   * Check if every state has it's implementation.
   *
   * @throws MissingImplementationException
   */
  private void checkImplementationsComplete() throws MissingImplementationException {
    for (final State s : getAllStates()) {
      if (!this.stateImplementations.containsKey(s)) {
        throw new MissingImplementationException(s);
      }
    }
  }

  private final Map<S, I> stateImplementations;

  public I getImplementation() {
    return getImplementation(getCurrentState());
  }

  public I getImplementation(final S state) {
    return this.stateImplementations.get(state);
  }
}
