/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.utils.fsm;

import java.util.List;
import java.util.Map;

public abstract class FiniteStateMachine<S extends State, T extends Transition> {

  public FiniteStateMachine(final S initial, final Map<S, Map<T, S>> transitions)
      throws StateNotReachableException {
    this.currentState = initial;
    this.transitions = transitions;
    checkEveryStateReachable();
  }

  private void checkEveryStateReachable() throws StateNotReachableException {
    for (final State s : getAllStates()) {
      if (!s.equals(this.currentState) && !stateIsReachable(s)) {
        throw new StateNotReachableException(s);
      }
    }
  }

  private boolean stateIsReachable(final State s) {
    for (final Map<T, S> map : this.transitions.values()) {
      if (map.containsValue(s)) {
        return true;
      }
    }
    return false;
  }

  private final Map<S, Map<T, S>> transitions;
  private S currentState = null;

  public void trigger(final T t) throws TransitionNotAllowedException {
    final S old = this.currentState;
    this.currentState = getNextState(t);
    onAfterTransition(old, t, this.currentState);
  }

  S getNextState(final T t) throws TransitionNotAllowedException {
    final Map<T, S> map = this.transitions.get(this.currentState);
    if (map != null && map.containsKey(t)) {
      return map.get(t);
    }
    throw new TransitionNotAllowedException(this.getCurrentState(), t);
  }

  public S getCurrentState() {
    return this.currentState;
  }

  public List<? extends State> getAllStates() {
    return this.currentState.getAllStates();
  }

  /**
   * Override this method in subclasses. The method is called immediately after a transition
   * finished.
   *
   * @param from
   * @param transition
   * @param to
   */
  protected void onAfterTransition(final S from, final T transition, final S to) {}
}
