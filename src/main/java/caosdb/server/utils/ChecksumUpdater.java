/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.utils;

import caosdb.server.CaosDBException;
import caosdb.server.database.Database;
import caosdb.server.database.DatabaseMonitor;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.transaction.GetUpdateableChecksums;
import caosdb.server.database.backend.transaction.RetrieveSparseEntity;
import caosdb.server.database.backend.transaction.UpdateSparseEntity;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.FileProperties;
import caosdb.server.entity.Message;
import caosdb.server.entity.container.TransactionContainer;
import caosdb.server.transaction.WriteTransaction;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

/**
 * Retrieves all file without a checksum, calculates one and stores it to the database (method
 * 'run'). This is meant for asynchronous checksum calculation.
 *
 * @author tf
 */
public class ChecksumUpdater extends WriteTransaction<TransactionContainer> implements Runnable {

  private Boolean running = false;
  private static final ChecksumUpdater instance = new ChecksumUpdater();

  private ChecksumUpdater() {
    super(new TransactionContainer());
  }

  /** Retrieves all file without a checksum, calculates one and stores it to the database */
  @Override
  public void run() {
    try {
      while (true) {
        final EntityInterface fileEntity = getNextUpdateableFileEntity();
        if (fileEntity == null) {
          // there is no next fileEntity
          return;
        }

        if (!isStillToBeUpdated(fileEntity)) {
          continue;
        }

        // get lastModified...
        final long lastModified =
            fileEntity.getFileProperties().retrieveFromFileSystem().lastModified();
        final String checksum = calcChecksum(fileEntity);
        // ... therefore check again:
        Access strongAccess = null;

        try {
          strongAccess = DatabaseMonitor.getInstance().allocateStrongAccess(this);
          if (wasChangedInTheMeantime(fileEntity, strongAccess, lastModified)) {
            continue;
          }

          fileEntity.getFileProperties().setChecksum(checksum);

          DatabaseMonitor.getInstance().acquireStrongAccess(this);

          // update
          Database.execute(new UpdateSparseEntity(fileEntity), strongAccess);
          strongAccess.commit();

        } catch (final InterruptedException e) {
          e.printStackTrace();
        } catch (final TransactionException e) {
          e.printStackTrace();
        } finally {
          if (strongAccess != null) {
            strongAccess.release();
          }
        }
      }
    } catch (final Throwable t) {
      t.printStackTrace();
    }
  }

  private boolean wasChangedInTheMeantime(
      final EntityInterface fileEntity, final Access access, final long lastModified)
      throws NoSuchAlgorithmException, IOException, CaosDBException, Message {
    final EntityInterface checkEntity =
        Database.execute(new RetrieveSparseEntity(fileEntity), access).getEntity();
    final FileProperties thatFP = checkEntity.getFileProperties();
    final FileProperties thisFP = fileEntity.getFileProperties();

    return !(isStillToBeUpdated(checkEntity)
        && thatFP.getPath().equals(thisFP.getPath())
        && thatFP.retrieveFromFileSystem().lastModified() == lastModified);
  }

  private String calcChecksum(final EntityInterface fileEntity) {
    try {
      return FileUtils.getChecksum(fileEntity.getFileProperties().retrieveFromFileSystem());
    } catch (final NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (final IOException e) {
      e.printStackTrace();
    } catch (final CaosDBException e) {
      e.printStackTrace();
    }
    return null;
  }

  private EntityInterface getNextUpdateableFileEntity() {
    final Access weakAccess = DatabaseMonitor.getInstance().acquiredWeakAccess(this);
    try {
      synchronized (instance.running) {

        // test for updatable checksums
        final Integer id = Database.execute(new GetUpdateableChecksums(), weakAccess).getID();
        if (id == null) {
          // nothing to be updated...
          instance.running = false;
          return null;
        }
        return Database.execute(new RetrieveSparseEntity(id), weakAccess).getEntity();
      }
    } catch (final Exception e) {
      e.printStackTrace();
      return null;
    } finally {
      weakAccess.release();
    }
  }

  private static boolean isStillToBeUpdated(final EntityInterface fileEntity) {
    return fileEntity.hasFileProperties() && !fileEntity.getFileProperties().hasChecksum();
  }

  @Override
  public boolean logHistory() {
    return false;
  }

  @Override
  protected void init() throws Exception {}

  @Override
  protected void preCheck() throws InterruptedException, Exception {}

  @Override
  protected void postCheck() {}

  @Override
  protected void transaction() throws Exception {}

  @Override
  protected void postTransaction() throws Exception {}

  public static void start() {
    synchronized (instance.running) {
      if (instance.running) {
        return;
      }
      instance.running = true;

      // run
      new Thread(instance).start();
    }
  }
}
