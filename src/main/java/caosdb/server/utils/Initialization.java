/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.utils;

import caosdb.server.database.DatabaseMonitor;
import caosdb.server.database.access.Access;
import caosdb.server.transaction.TransactionInterface;

public final class Initialization implements TransactionInterface {

  private Access access;
  private static final Initialization instance = new Initialization();

  private Initialization() {
    this.access = DatabaseMonitor.getInitAccess(this);
  }

  public static final Initialization setUp() {
    return instance;
  }

  public final Access getAccess() {
    return this.access;
  }

  public final void release() {
    if (this.access != null) {
      this.access.release();
      this.access = null;
    }
  }

  @Override
  public void execute() throws Exception {}
}
