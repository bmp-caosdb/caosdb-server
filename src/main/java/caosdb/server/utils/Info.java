/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.utils;

import caosdb.server.CaosDBServer;
import caosdb.server.FileSystem;
import caosdb.server.database.Database;
import caosdb.server.database.DatabaseMonitor;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.transaction.GetInfo;
import caosdb.server.database.backend.transaction.SyncStats;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.database.proto.ProtoInfo;
import caosdb.server.entity.Message;
import caosdb.server.transaction.TransactionInterface;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import org.jdom2.Element;

public class Info extends AbstractObservable implements Observer, TransactionInterface {

  private static Info instance = new Info();
  public static final String SYNC_DATABASE_EVENT = "SyncDatabaseEvent";
  private final Access access;

  @Override
  public boolean notifyObserver(final String e, final Observable o) {
    try {
      syncDatabase();
    } catch (final Exception exc) {
      exc.printStackTrace();
    }
    return true;
  }

  static {
    try {
      syncDatabase();
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }

  private Info() {
    this.access = DatabaseMonitor.getInfoAccess(this);
  }

  public static Info getInstance() {
    return instance;
  }

  private static File dropOffBox = null;
  private static int filesCount = -1;
  private static int propertiesCount = -1;
  private static int recordsCount = -1;
  private static int recordTypesCount = -1;
  private static int tmpFilesCount;
  private static String fssize;

  public static Integer getRecordsCount() throws Exception {
    return recordsCount;
  }

  public static final Integer getFilesCount() {
    return filesCount;
  }

  public static final Integer getPropertiesCount() {
    return propertiesCount;
  }

  public static final Integer getRecordTypesCount() {
    return recordTypesCount;
  }

  private static LinkedList<Element> getFlatList(final File[] files) {
    try {
      final LinkedList<Element> ret = new LinkedList<Element>();
      for (final File file : files) {
        if (file.isDirectory()) {
          ret.addAll(getFlatList(file.listFiles()));
        } else {
          final Element element = new Element("file");
          final String tempPath =
              file.getAbsolutePath().substring(dropOffBox.getCanonicalPath().length() + 1);
          element.setAttribute("path", tempPath);
          ret.add(element);
        }
      }
      return ret;
    } catch (final IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  private static LinkedList<Element> getTree(final File[] files) {
    final LinkedList<Element> ret = new LinkedList<Element>();
    for (final File file : files) {
      if (file.isDirectory()) {
        final Element element = new Element("dir");
        element.setAttribute("path", file.getName());
        element.addContent(getTree(file.listFiles()));
        ret.add(element);
      } else {
        final Element element = new Element("file");
        element.setAttribute("path", file.getName());
        ret.add(element);
      }
    }
    return ret;
  }

  public static void syncDatabase() throws Exception {
    final Access access = getInstance().access;

    final ProtoInfo i = new ProtoInfo();
    final GetInfo t = new GetInfo(i);
    Database.execute(t, access);
    filesCount = i.filesCount;
    recordsCount = i.recordsCount;
    recordTypesCount = i.recordTypesCount;
    propertiesCount = i.propertiesCount;
    fssize = Utils.getReadableByteSize(i.fssize);
    tmpFilesCount = countTmpFiles();

    getInstance().notifyObservers(SYNC_DATABASE_EVENT);
  }

  private static int countTmpFiles() throws IOException {
    final File tmpdir = new File(FileSystem.getTmp());
    return tmpdir.list().length;
  }

  public static Element toElement() throws Exception {
    return toElement(false);
  }

  public static Element toElement(final boolean tree) throws Exception, SQLException {
    dropOffBox = new File(FileSystem.getDropOffBox());
    final Element info = new Element("Stats");
    final Element counts = new Element("counts");
    counts.setAttribute("records", Integer.toString(recordsCount));
    counts.setAttribute("properties", Integer.toString(propertiesCount));
    counts.setAttribute("recordTypes", Integer.toString(recordTypesCount));
    counts.setAttribute("files", Integer.toString(filesCount));
    counts.setAttribute("fssize", fssize);
    counts.setAttribute("tmpfiles", Integer.toString(tmpFilesCount));
    if (CaosDBServer.isDebugMode()) {
      counts.setAttribute("debug", "true");
    }
    final Element e = new Element("dropOffBox");
    if (dropOffBox.isDirectory()) {
      if (tree) {
        e.setAttribute("path", dropOffBox.getAbsolutePath());
        e.addContent(getTree(dropOffBox.listFiles()));
      } else {
        e.setAttribute("path", dropOffBox.getAbsolutePath());
        e.addContent(getFlatList(dropOffBox.listFiles()));
      }
    } else {
    }
    info.addContent(counts);
    info.addContent(e);
    return info;
  }

  public static void syncDatabase(final ServerStat stat) throws TransactionException, Message {

    final Access access = getInstance().access;

    final SyncStats t = new SyncStats(stat);
    Database.execute(t, access);
  }

  @Override
  public void execute() throws Exception {
    syncDatabase();
  }
}
