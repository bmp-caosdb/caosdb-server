/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.utils;

/**
 * IGNORE - This instance must be ignored during further processing. <br>
 * VALID - This instance has a final ID and has been synchronized with the database. <br>
 * WRITTEN - This instance has been written successfully to the database. It has a final ID. <br>
 * QUALIFIED - This instance has a provisional or final ID, represents a well-formed entity, and any
 * referenced entities (by a reference property) have status QUALIFIED or VALID. QUALIFIED - This
 * instance has been checked and is not qualified to be processed further (i.e. to be inserted,
 * updated, deleted) DELETED - This instance has been deleted recently. CORRUPT - This instance has
 * been retrieved from the database, but though something turned out to be wrong with it.
 * NONEXISTENT - This instance has been called (via id or something) but it doesn't exist.
 *
 * @author Timm Fitschen
 */
public enum EntityStatus {
  IGNORE,
  UNQUALIFIED,
  DELETED,
  NONEXISTENT,
  QUALIFIED,
  VALID
}
