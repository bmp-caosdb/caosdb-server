/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.utils;

import caosdb.datetime.UTCDateTime;
import caosdb.server.database.Database;
import caosdb.server.database.DatabaseMonitor;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.transaction.FileConsistencyCheck;
import caosdb.server.database.backend.transaction.GetFileIterator;
import caosdb.server.database.backend.transaction.GetFileRecordByPath;
import caosdb.server.database.backend.transaction.RetrieveAllUncheckedFiles;
import caosdb.server.database.backend.transaction.SetFileCheckedTimestamp;
import caosdb.server.database.proto.SparseEntity;
import caosdb.server.entity.Message;
import caosdb.server.entity.xml.ToElementable;
import caosdb.server.transaction.TransactionInterface;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.TimeZone;
import org.jdom2.Element;

public class FileStorageConsistencyCheck extends Thread
    implements ToElementable, TransactionInterface {

  private Access access = null;
  private final HashMap<String, Integer> results = new HashMap<String, Integer>();
  private Exception exception = null;
  private Runnable finishRunnable = null;
  private final String location;
  private Long ts = null;

  public Exception getException() {
    return this.exception;
  }

  public FileStorageConsistencyCheck(final String location) {
    setDaemon(true);
    this.location = location.startsWith("/") ? location.replaceFirst("^/", "") : location;
  }

  @Override
  public void run() {
    this.access = DatabaseMonitor.getInstance().acquiredWeakAccess(this);
    try {

      // test all files in file system.
      final Iterator<String> iterator =
          Database.execute(new GetFileIterator(this.location), this.access).getIterator();

      this.ts = System.currentTimeMillis();
      while (iterator != null && iterator.hasNext()) {
        if (DatabaseMonitor.whoHasAllocatedStrongAccess() != null) {
          // there is a thread waiting to write. pause this one and
          // apply for a new weak access which will be granted when
          // the write thread is ready.
          this.access.release();
          this.access = DatabaseMonitor.getInstance().acquiredWeakAccess(this);
        }

        final String path = iterator.next();
        // FIXME this prevents all files with ".thumbnail" from being checked.
        if (path.contains(".thumbnail")) {
          continue;
        }

        final GetFileRecordByPath t = Database.execute(new GetFileRecordByPath(path), this.access);

        if (t.getEntity() == null) {
          this.results.put(path, FileConsistencyCheck.UNKNOWN_FILE);
          continue;
        }
        final int result =
            Database.execute(
                    new FileConsistencyCheck(
                        path, t.getSize(), t.getHash(), t.getLastConsistencyCheck(), new SHA512()),
                    this.access)
                .getResult();

        if (result != FileConsistencyCheck.OK) {
          this.results.put(path, result);
        }

        Database.execute(new SetFileCheckedTimestamp(t.getId(), this.ts), this.access);
      }

      // test all remaining file records
      final Iterator<SparseEntity> iterator2 =
          Database.execute(new RetrieveAllUncheckedFiles(this.ts, this.location), this.access)
              .getIterator();
      while (iterator2 != null && iterator2.hasNext()) {

        final SparseEntity entity = iterator2.next();
        final int result =
            Database.execute(
                    new FileConsistencyCheck(
                        entity.filePath,
                        entity.fileSize,
                        entity.fileHash,
                        entity.fileChecked,
                        new SHA512()),
                    this.access)
                .getResult();

        if (result != FileConsistencyCheck.OK) {
          this.results.put(entity.filePath, result);
        }

        Database.execute(new SetFileCheckedTimestamp(entity.id, this.ts), this.access);
      }

    } catch (final Exception e) {
      this.exception = e;
    } finally {
      this.access.release();
    }

    synchronized (this.results) {
      if (this.finishRunnable != null) {
        this.finishRunnable.run();
      }
    }
  }

  public HashMap<String, Integer> getResults() {
    return this.results;
  }

  public void setOnFinish(final Runnable r) {
    synchronized (this.results) {
      this.finishRunnable = r;
    }
  }

  @Override
  public void addToElement(final Element e) {
    if (this.ts != null) {
      e.setAttribute(
          "timestamp",
          UTCDateTime.SystemMillisToUTCDateTime(this.ts).toDateTimeString(TimeZone.getDefault()));
    }
    if (this.location != null) {
      e.setAttribute("location", this.location);
    }

    if (getException() != null) {
      final StringBuilder sb = new StringBuilder();
      sb.append(getException().toString());
      for (final StackTraceElement t : getException().getStackTrace()) {
        sb.append('\n').append(t.toString());
      }

      e.addContent(new Message("Error", 0, "An exception was thrown.", sb.toString()).toElement());
    }

    final List<Message> results2Messages = results2Messages(getResults(), this.location);
    for (final Message m : results2Messages) {
      e.addContent(m.toElement());
    }
  }

  public Element toElement() {
    final Element results = new Element("Results");
    addToElement(results);
    return results;
  }

  private static List<Message> results2Messages(
      final HashMap<String, Integer> results, final String location) {
    final ArrayList<Message> ret = new ArrayList<Message>();
    if (results.isEmpty()) {
      if (location.length() > 0) {
        ret.add(new Message("Info", 0, "File system below " + location + " is consistent."));
      } else {
        ret.add(new Message("Info", 0, "File system is consistent."));
      }
    }
    for (final Entry<String, Integer> r : results.entrySet()) {
      switch (r.getValue()) {
        case FileConsistencyCheck.FILE_DOES_NOT_EXIST:
          ret.add(new Message("Error", 0, r.getKey() + ": File does not exist."));
          break;
        case FileConsistencyCheck.FILE_MODIFIED:
          ret.add(new Message("Error", 0, r.getKey() + ": File was modified."));
          break;
        case FileConsistencyCheck.UNKNOWN_FILE:
          ret.add(new Message("Warning", 0, r.getKey() + ": Unknown file."));
          break;
        case FileConsistencyCheck.NONE:
          ret.add(new Message("Warning", 0, r.getKey() + ": Test result not available."));
          break;
        default:
          break;
      }
    }
    return ret;
  }

  @Override
  public void execute() throws Exception {
    run();
  }
}
