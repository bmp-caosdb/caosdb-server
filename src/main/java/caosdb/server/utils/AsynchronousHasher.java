/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
// package caosdb.server.utils;
//
// import java.io.File;
//
// class HasherThread extends Thread {
//
//    private final File file;
//    private String hash = null;
//    private Exception exc = null;
//    private final HashAlgorithm alg;
//
//    HasherThread(final File file, final HashAlgorithm alg) {
//        this.file = file;
//        this.alg = alg;
//    }
//
//    @Override
//    public void run() {
//        try {
//            this.hash = this.alg.calcHash(this.file);
//        } catch (final HashException e) {
//            this.exc = e;
//        }
//    }
//
//    String getHash() throws HashException {
//        if (this.exc != null) {
//            throw new HashException(this.exc);
//        } else if (this.hash == null) {
//            throw new HashNotReadyException();
//        }
//        return this.hash;
//    }
// }
//
// public class AsynchronousHasher implements Hasher<HashAlgorithm> {
//
//    private HasherThread thread = null;
//    private final File file;
//    private final HashAlgorithm alg;
//
//    public AsynchronousHasher(final File file, final HashAlgorithm alg) {
//        this.alg = alg;
//        this.file = file;
//    }
//
//    public boolean isHasher(final File file) {
//        return file.equals(this.file);
//    }
//
//    /**
//     * timeout in milliseconds. 0 means 'wait forever'. Throws
//     * HashTimeoutException on timeout. Returns hash when finished.
//     *
//     * @throws HashException
//     *             when timed out, or error occures.
//     */
//    @Override
//    public String hash(final long timeout) throws InterruptedException, HashException {
//
//        // create hasher thread
//        if (this.thread == null) {
//            this.thread = new HasherThread(this.file, getAlgorithm());
//            this.thread.setDaemon(true);
//            this.thread.start();
//        }
//        this.thread.join(timeout);
//        // ready or timeout
//
//        return this.thread.getHash();
//    }
//
//    @Override
//    public HashAlgorithm getAlgorithm() {
//        return this.alg;
//    }
// }
