/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.core;

import static caosdb.server.entity.MagicTypes.NAME;

import caosdb.server.datatype.AbstractDatatype;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.Message;
import caosdb.server.entity.wrapper.Parent;
import caosdb.server.entity.wrapper.Property;
import caosdb.server.jobs.EntityJob;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * To be called after CheckPropValid.
 *
 * @author tf
 */
public class ProcessNameProperties extends EntityJob {

  @Override
  protected void run() {
    try {
      for (final Property prop : getEntity().getProperties()) {
        final Collection<Object> parents = getNearestValidParents(prop);
        if (parents != null) {
          for (final Object par : parents) {
            if (isValidAndSubTypeOfName(par)) {
              doProcessNameProperty(prop);
              break;
            }
          }
        }
      }
    } catch (final Message m) {
      getEntity().addError(m);
      getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
    }
  }

  private void doProcessNameProperty(final Property prop) throws Message {
    final AbstractDatatype textdt = AbstractDatatype.datatypeFactory("TEXT");
    if (prop.hasDatatype()) {
      if (!prop.getDatatype().equals(textdt)) {
        throw ServerMessages.NAME_PROPERTIES_MUST_BE_TEXT;
      }
    } else {
      prop.setDatatype(textdt);
    }
    prop.setIsName(true);
  }

  private boolean isValidAndSubTypeOfName(final Object par) {
    if (par != null && par instanceof Integer && (Integer) par > 0) {
      final boolean isName = par.equals(NAME.getId());
      return isName || isValidSubType((Integer) par, NAME.getId());
    }
    return false;
  }

  private Set<Object> getNearestValidParents(final EntityInterface entity) {
    final HashSet<Object> set = new HashSet<Object>();
    getNearestValidParents(entity, set);
    return set;
  }

  private void getNearestValidParents(final EntityInterface entity, final Set<Object> set) {
    for (final Parent par : entity.getParents()) {
      if (par.hasParents() && par.hasId()) {
        // parent has parents and id
        if (set.add(par.getId())) {
          getNearestValidParents(par, set);
        }
      } else if (par.hasParents() && par.hasName()) {
        // parent has parents and name
        if (set.add(par.getName())) {
          getNearestValidParents(par, set);
        }
      } else if (par.hasId() && par.getId() > 0) {
        // parent is valid
        set.add(par.getId());
      } else if (par.hasId()) {
        // get parent from container
        final EntityInterface parentEntity = getEntityById(par.getId());
        if (parentEntity != null && parentEntity.hasParents()) {
          if (set.add(parentEntity.getId())) {
            getNearestValidParents(parentEntity, set);
          }
          continue;
        }
      }
      if (par.hasName()) {
        // get parent from container
        final EntityInterface parentEntity = getEntityByName(par.getName());
        if (parentEntity != null && parentEntity.hasParents()) {
          if (set.add(parentEntity.getName())) {
            getNearestValidParents(parentEntity, set);
          }
        }
      }
      // else: parent has no name and no id
    }
  }

  private Collection<Object> getNearestValidParents(final Property prop) {
    if (prop.hasId() && prop.getId() > 0) {
      final ArrayList<Object> ret = new ArrayList<Object>();
      if (prop.hasParents()) {
        for (final Parent par : prop.getParents()) {
          ret.add(par.getId());
        }
      } else {
        ret.add(prop.getId());
      }
      return ret;
    } else if (prop.hasId()) {
      // property is new -> get valid parents of any depth
      final EntityInterface propertyEntity = getEntityById(prop.getId());
      if (propertyEntity != null) {
        return getNearestValidParents(propertyEntity);
      }
      return null;
    } else {
      return null;
    }
  }
}
