/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.core;

import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import caosdb.server.entity.Message;
import caosdb.server.entity.Message.MessageType;
import caosdb.server.jobs.FlagJob;
import caosdb.server.jobs.JobAnnotation;
import caosdb.server.utils.ServerMessages;
import caosdb.server.utils.Utils;
import caosdb.server.utils.mail.Mail;

@JobAnnotation(
    flag = "testMail",
    description =
        "Value may be any mail address. Send a test mail to the given mail address. Works only if server is in debug mode.")
public class TestMail extends FlagJob {

  private static final String NAME =
      CaosDBServer.getServerProperty(ServerProperties.KEY_NO_REPLY_NAME);
  private static final String EMAIL =
      CaosDBServer.getServerProperty(ServerProperties.KEY_NO_REPLY_EMAIL);

  @Override
  protected void job(final String value) {
    if (CaosDBServer.isDebugMode() && value != null) {
      if (Utils.isRFC822Compliant(value)) {
        final Mail m = new Mail(NAME, EMAIL, null, value, "Test mail", "This is a test mail.");
        m.send();
        getContainer()
            .addMessage(new Message(MessageType.Info, 0, "A mail has been send to " + value));
      } else {
        getContainer().addMessage(ServerMessages.EMAIL_NOT_WELL_FORMED);
      }
    }
  }
}
