/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.core;

import caosdb.server.database.exceptions.EntityDoesNotExistException;
import caosdb.server.database.exceptions.EntityWasNotUniqueException;
import caosdb.server.entity.EntityInterface;
import caosdb.server.jobs.FlagJob;
import caosdb.server.jobs.JobAnnotation;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;

@JobAnnotation(flag = "uniquename")
public class UniqueName extends FlagJob {

  private void doCheck(final EntityInterface entity) {
    if (entity.hasName()) {

      // check against data base
      try {
        final Integer foreign = retrieveValidIDByName(entity.getName());
        if (entity.hasId() && !foreign.equals(entity.getId())) {
          throw new EntityWasNotUniqueException();
        }
      } catch (final EntityDoesNotExistException e) {
        // ok
      } catch (final EntityWasNotUniqueException e) {
        entity.addError(ServerMessages.NAME_IS_NOT_UNIQUE);
        entity.setEntityStatus(EntityStatus.UNQUALIFIED);
        return;
      }

      // check against container
      for (final EntityInterface e : getContainer()) {
        if (entity != e && e.hasName() && e.getName().equals(entity.getName())) {
          entity.setEntityStatus(EntityStatus.UNQUALIFIED);
          entity.addError(ServerMessages.NAME_IS_NOT_UNIQUE);
          return;
        }
      }
    }
  }

  @Override
  protected void job(final String value) {
    for (final EntityInterface entity : getContainer()) {
      doCheck(entity);
    }
  }
}
