package caosdb.server.jobs.core;

import caosdb.server.entity.Message;
import caosdb.server.entity.wrapper.Property;
import caosdb.server.jobs.EntityJob;
import caosdb.server.jobs.JobAnnotation;
import caosdb.server.utils.ServerMessages;

@JobAnnotation(
    flag = "noOverrides",
    description = "Fail if this entity has any properties with overrides.")
public class CheckNoOverridesPresent extends EntityJob {

  @Override
  protected void run() {
    runJobFromSchedule(this.getEntity(), CheckPropValid.class);
    for (Property p : getEntity().getProperties()) {
      if (p.isDatatypeOverride()) {
        addMessage(p, ServerMessages.PROPERTY_WITH_DATATYPE_OVERRIDE);
      }
      if (p.isNameOverride()) {
        addMessage(p, ServerMessages.PROPERTY_WITH_NAME_OVERRIDE);
      }
      if (p.isDescOverride()) {
        addMessage(p, ServerMessages.PROPERTY_WITH_DESC_OVERRIDE);
      }
    }
  }

  private void addMessage(Property p, Message message) {
    if (getMode() == Mode.MUST) {
      p.addError(message);
    } else {
      p.addWarning(message);
    }
  }
}
