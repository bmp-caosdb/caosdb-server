/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.core;

import caosdb.server.entity.Message;
import caosdb.server.jobs.FlagJob;
import caosdb.server.jobs.JobAnnotation;
import caosdb.server.jobs.JobExecutionTime;
import caosdb.server.query.Query;
import caosdb.server.query.Query.ParsingException;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;

@JobAnnotation(flag = "query", time = JobExecutionTime.INIT)
public class ExecuteQuery extends FlagJob {

  @Override
  protected void job(final String value) {

    final Query queryInstance = new Query(value, getTransaction().getTransactor(), getContainer());
    try {
      if (value != null) {
        queryInstance.execute(getTransaction().getAccess());
      }
    } catch (final ParsingException e) {
      getContainer().addMessage(ServerMessages.QUERY_PARSING_ERROR);
      getContainer().setStatus(EntityStatus.UNQUALIFIED);
    } catch (final UnsupportedOperationException e) {
      getContainer().addMessage(ServerMessages.QUERY_EXCEPTION);
      getContainer().setStatus(EntityStatus.UNQUALIFIED);
      getContainer().addMessage(new Message(e.getMessage()));
    }
    getContainer().addMessage(queryInstance);
  }
}
