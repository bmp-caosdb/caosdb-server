/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.core;

import caosdb.server.database.Database;
import caosdb.server.database.backend.transaction.GetDependentEntities;
import caosdb.server.entity.EntityInterface;
import caosdb.server.jobs.EntityJob;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;
import java.util.List;

/**
 * Check whether an entity is referenced by other entities. In the event that someone wants to
 * delete this entity, this entity must not be referenced by any other entity which is not deleted
 * along with this entity.
 *
 * @author tf
 */
public class CheckReferenceDependencyExistent extends EntityJob {
  @Override
  public final void run() {
    if (getEntity().getDomain() == null || getEntity().getDomain() == 0) {
      // if (getContainer().contains(getEntity())) {

      // retrieve dependent entities
      final List<Integer> depends =
          Database.execute(
                  new GetDependentEntities(getEntity().getId()), getTransaction().getAccess())
              .getList();

      // loop:
      for (final Integer id : depends) {
        final EntityInterface foreign = getEntityById(id);
        if (foreign == null) {
          // dependent entity is not in the container and will not be
          // deleted. Therefore, this entity cannot be deleted either.
          getEntity().addError(ServerMessages.REQUIRED_BY_PERSISTENT_ENTITY);
          getEntity().addInfo("Required by entity " + id + ".");
          getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
        }
        // for (final EntityInterface e : getContainer()) {
        // if (e.getId().equals(id)) {
        // // entity is in the container.
        // if (e.getEntityStatus() == EntityStatus.UNQUALIFIED) {
        // getEntity().addError(ServerMessages.REQUIRED_BY_UNQUALIFIED);
        // getEntity().addInfo("Required by entity " + id + ".");
        // getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
        // } else {
        // e.acceptObserver(new Observer() {
        // @Override
        // public boolean notifyObserver(final String evt,
        // final Observable o) {
        // if (e == o && evt == Entity.ENTITY_STATUS_CHANGED_EVENT) {
        // if (e.getEntityStatus() == EntityStatus.UNQUALIFIED) {
        // getEntity().addError(
        // ServerMessages.REQUIRED_BY_UNQUALIFIED);
        // getEntity().addInfo("Required by entity " + id + ".");
        // getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
        // return false;
        // }
        // }
        // return true;
        // }
        // });
        // }
        //
        // continue loop;
        // }
        // }

        // // dependent entity is not in the container. That is, it will
        // // not be deleted. Therefore, this entity cannot be deleted
        // // either.
        // getEntity().addError(ServerMessages.REQUIRED_BY_PERSISTENT_ENTITY);
        // getEntity().addInfo("Required by entity " + id + ".");
        // getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
      }
    }
  }
}
