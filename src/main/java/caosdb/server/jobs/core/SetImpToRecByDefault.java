/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.core;

import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.StatementStatus;
import caosdb.server.jobs.EntityJob;

/**
 * Set the statement status of all properties of an entity which do not have a statement status to
 * the value "RECOMMENED" by default. Additionally: Set all statement statuses of all subproperties
 * to their parent values.
 *
 * @author tf
 */
public class SetImpToRecByDefault extends EntityJob {
  @Override
  public final void run() {
    for (final EntityInterface property : getEntity().getProperties()) {
      if (!property.hasStatementStatus()) {
        property.setStatementStatus(StatementStatus.RECOMMENDED);
      }
      inheritStatementStatus(property);
    }
  }

  private static void inheritStatementStatus(final EntityInterface property) {
    for (final EntityInterface subproperty : property.getProperties()) {
      if (!subproperty.hasStatementStatus()) {
        subproperty.setStatementStatus(property.getStatementStatus());
      }
      inheritStatementStatus(subproperty);
    }
  }
}
