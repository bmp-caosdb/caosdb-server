/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.core;

import caosdb.server.database.Database;
import caosdb.server.database.backend.transaction.GetIDByName;
import caosdb.server.database.exceptions.EntityDoesNotExistException;
import caosdb.server.entity.Entity;
import caosdb.server.entity.container.TransactionContainer;
import caosdb.server.jobs.ContainerJob;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;
import java.util.ArrayList;
import java.util.List;

public class ResolveNames extends ContainerJob {

  @Override
  protected void run() {
    resolve(getContainer());
  }

  public void resolve(final TransactionContainer container) {
    final ArrayList<Entity> add = new ArrayList<Entity>();
    for (final Entity e : container) {
      if (e.hasName() && !e.hasId()) {
        try {
          final List<Integer> c =
              Database.execute(new GetIDByName(e.getName(), false), getTransaction().getAccess())
                  .getList();

          e.setId(c.get(0));
          e.setEntityStatus(EntityStatus.QUALIFIED);
          for (int i = 1; i < c.size(); i++) {
            final Entity e2 = new Entity(c.get(i), e.getRole());
            e2.setEntityStatus(EntityStatus.QUALIFIED);
            add.add(e2);
          }
        } catch (final EntityDoesNotExistException exc) {
          e.setEntityStatus(EntityStatus.NONEXISTENT);
          e.addError(ServerMessages.ENTITY_DOES_NOT_EXIST);
        }
      }
    }
    container.addAll(add);
  }
}
