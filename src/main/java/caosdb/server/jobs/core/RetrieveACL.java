/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.core;

import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.xml.ToElementable;
import caosdb.server.jobs.FlagJob;
import caosdb.server.jobs.JobAnnotation;
import caosdb.server.jobs.JobExecutionTime;
import caosdb.server.permissions.EntityPermission;
import caosdb.server.transaction.Retrieve;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;
import org.apache.shiro.authz.AuthorizationException;
import org.jdom2.Element;

@JobAnnotation(
    time = JobExecutionTime.POST_TRANSACTION,
    flag = "ACL",
    description = "Adds an XML representation of the EntityACL to the output of this entity.")
public class RetrieveACL extends FlagJob {

  @Override
  protected void job(final String value) {

    for (final EntityInterface entity : getContainer()) {
      if (entity.getId() != null && entity.getId() > 0 && entity.getEntityACL() != null) {
        try {
          if (getTransaction() instanceof Retrieve) {
            entity.checkPermission(EntityPermission.RETRIEVE_ACL);
          }
          entity.addMessage(
              new ToElementable() {

                @Override
                public void addToElement(final Element ret) {
                  ret.addContent(entity.getEntityACL().toElement());
                }
              });
        } catch (final AuthorizationException e) {
          entity.setEntityStatus(EntityStatus.UNQUALIFIED);
          entity.addError(ServerMessages.AUTHORIZATION_ERROR);
          entity.addInfo(e.getMessage());
        }
      }
    }
  }
}
