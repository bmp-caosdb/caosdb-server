/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.core;

import caosdb.server.database.Database;
import caosdb.server.database.backend.transaction.RetrieveSparseEntity;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.wrapper.Parent;
import caosdb.server.jobs.ContainerJob;
import caosdb.server.jobs.JobAnnotation;
import caosdb.server.jobs.JobExecutionTime;
import caosdb.server.transaction.Retrieve;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

@JobAnnotation(time = JobExecutionTime.INIT)
public class AccessControl extends ContainerJob {

  @Override
  protected void run() {
    final Subject subject = SecurityUtils.getSubject();

    // subject has complete permissions for this kind of transaction
    if (subject.isPermitted("TRANSACTION:" + getTransaction().getClass().getSimpleName())) {
      return;
    }

    if (getTransaction() instanceof Retrieve) {
      return;
    }

    for (final EntityInterface e : getContainer()) {

      // per role permission
      if (subject.isPermitted(
          "TRANSACTION:"
              + getTransaction().getClass().getSimpleName()
              + ":"
              + e.getRole().toString())) {
        continue;
      }

      // special annotations permission
      if (e.hasParents() && e.getParents().size() == 1) {
        final Parent par1 = e.getParents().get(0);
        if (par1.hasId() && par1.getId() > 0) {
          Database.execute(new RetrieveSparseEntity(par1), getTransaction().getAccess());
        }
        if (par1.hasName()
            && par1.getName().equals("CommentAnnotation")
            && subject.isPermitted(
                getTransaction().getClass().getSimpleName() + ":CommentAnnotation")) {
          continue;
        }
      }
      e.setEntityStatus(EntityStatus.UNQUALIFIED);
      e.addMessage(ServerMessages.AUTHORIZATION_ERROR);
    }
  }
}
