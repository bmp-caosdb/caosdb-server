/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.core;

import caosdb.server.jobs.EntityJob;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;

/**
 * Check whether an entity has a description.
 *
 * @author tf
 */
public class CheckDescPresent extends EntityJob {
  @Override
  public final void run() {
    if (!getEntity().hasDescription()) {
      switch (getMode()) {
        case MUST:
          getEntity().addError(ServerMessages.ENTITY_HAS_NO_DESCRIPTION);
          getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
          break;
        case SHOULD:
          getEntity().addWarning(ServerMessages.ENTITY_HAS_NO_DESCRIPTION);
        default:
          break;
      }
    }
  }
}
