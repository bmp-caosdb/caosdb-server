/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.core;

import caosdb.server.database.exceptions.EntityDoesNotExistException;
import caosdb.server.database.exceptions.EntityWasNotUniqueException;
import caosdb.server.datatype.AbstractCollectionDatatype;
import caosdb.server.datatype.AbstractDatatype;
import caosdb.server.datatype.ReferenceDatatype2;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.Message;
import caosdb.server.entity.Role;
import caosdb.server.jobs.EntityJob;
import caosdb.server.jobs.Job;
import caosdb.server.permissions.EntityPermission;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;
import java.util.List;

/**
 * Check whether the entity has a data type. Assign the data type of the abstract property if
 * necessary
 *
 * @author tf
 */
public final class CheckDatatypePresent extends EntityJob {

  @Override
  public final void run() {
    try {

      // inherit datatype
      if (!getEntity().hasDatatype()) {
        resolveId(getEntity());

        inheritDatatypeFromAbstractEntity();

        // still no data type ??? try to get it from parent...
        if (!getEntity().hasDatatype() && getEntity().hasParents()) {
          getDataTypeFromParent();
        }

      } else {

        // check if this data type is an overridden data type
        checkIfOverride();
      }

      // resolve reference data types, run jobs
      if (getEntity().hasDatatype()) {

        if (getEntity().getDatatype() instanceof ReferenceDatatype2) {
          checkReference2((ReferenceDatatype2) getEntity().getDatatype());
        } else if (getEntity().getDatatype() instanceof AbstractCollectionDatatype) {
          final AbstractCollectionDatatype datatype =
              (AbstractCollectionDatatype) getEntity().getDatatype();
          if (datatype.getDatatype() instanceof ReferenceDatatype2) {
            checkReference2((ReferenceDatatype2) datatype.getDatatype());
          }
        }

        // run jobsreturn this.entities;
        final List<Job> datatypeJobs = loadDataTypeSpecificJobs();
        getTransaction().getSchedule().addAll(datatypeJobs);
        for (final Job job : datatypeJobs) {
          getTransaction().getSchedule().runJob(job);
        }
      } else {

        // finally, no data type
        throw ServerMessages.NO_DATATYPE;
      }

    } catch (final Message m) {
      if (m == ServerMessages.ENTITY_DOES_NOT_EXIST) {
        getEntity().addError(ServerMessages.UNKNOWN_DATATYPE);
      } else {
        getEntity().addError(m);
      }
    } catch (final EntityDoesNotExistException exc) {
      getEntity().addError(ServerMessages.UNKNOWN_DATATYPE);
    } catch (final EntityWasNotUniqueException exc) {
      getEntity().addError(ServerMessages.DATA_TYPE_NAME_DUPLICATES);
    }
  }

  private void checkReference2(final ReferenceDatatype2 datatype) throws Message {

    if (datatype.getId() == null) {
      // try and get from container...
      final EntityInterface datatypeEntity = getEntityByName(datatype.getName());

      // if the container carried a corresponding entity
      if (datatypeEntity != null) {
        assertAllowedToUse(datatypeEntity);

        // ... we set it as the datatypevalue
        datatype.setEntity(datatypeEntity);
      } else {

        // else try and get from database.
        final EntityInterface validDatatypeEntity =
            retrieveValidSparseEntityByName(datatype.getName());
        assertAllowedToUse(validDatatypeEntity);
        datatype.setId(validDatatypeEntity.getId());
      }
    } else if (datatype.getId() < 0) {
      final EntityInterface datatypeEntity = getEntityById(datatype.getId());

      // if the container carried a corresponding entity
      if (datatypeEntity != null) {
        assertAllowedToUse(datatypeEntity);
        // ... we set it as the datatype
        datatype.setEntity(datatypeEntity);
      } else {

        throw ServerMessages.UNKNOWN_DATATYPE;
      }
    } else {

      final EntityInterface validDatatypeEntity = retrieveValidSparseEntityById(datatype.getId());
      assertAllowedToUse(validDatatypeEntity);
      datatype.setEntity(validDatatypeEntity);
    }
  }

  private void assertAllowedToUse(final EntityInterface datatype) throws Message {
    checkPermission(datatype, EntityPermission.USE_AS_DATA_TYPE);
  }

  private void checkIfOverride() throws Message {
    if (getEntity().hasId() && getEntity().getId() > 0) {
      // get data type from database
      final EntityInterface foreign = retrieveValidSparseEntityById(getEntity().getId());

      if (foreign.hasDatatype() && !foreign.getDatatype().equals(getEntity().getDatatype())) {
        // is override!
        getEntity().setDatatypeOverride(true);
      }
    } else {
      // get data type from container
      EntityInterface abstractProperty = null;
      if (getEntity().hasId()) {
        abstractProperty = getEntityById(getEntity().getId());
      } else if (getEntity().hasName()) {
        abstractProperty = getEntityByName(getEntity().getName());
      }
      if (abstractProperty != null && abstractProperty.hasDatatype()) {
        if (!getEntity().getDatatype().equals(abstractProperty.getDatatype())) {
          // is override!
          getEntity().setDatatypeOverride(true);
        }
      }
    }
  }

  private void inheritDatatypeFromAbstractEntity() throws Message {
    // if this is a record type property or a concrete property, assign
    // the data type of the corresponding abstract property.
    if (getEntity().hasId() && getEntity().getId() > 0) {
      // get from data base
      final EntityInterface foreign = retrieveValidSparseEntityById(getEntity().getId());
      inheritDatatypeFromForeignEntity(foreign);
    } else if (getEntity().hasId() && getEntity().getId() < 0) {
      // get from container
      final EntityInterface foreign = getEntityById(getEntity().getId());
      inheritDatatypeFromForeignEntity(foreign);
    }
  }

  private void inheritDatatypeFromForeignEntity(final EntityInterface foreign) {
    if (foreign != null && foreign.hasDatatype()) {
      getEntity().setDatatype(foreign.getDatatype());
    } else if (foreign != null && foreign != getEntity() && foreign.getRole() == Role.RecordType) {
      getEntity().setDatatype(ReferenceDatatype2.datatypeFactory(foreign.getId()));
    }
  }

  private void resolveId(final EntityInterface entity) {
    if (!entity.hasId() && entity.hasName()) {
      try {
        entity.setId(retrieveValidIDByName(entity.getName()));
        if (entity.getEntityStatus() != EntityStatus.UNQUALIFIED) {
          entity.setEntityStatus(EntityStatus.VALID);
        }
      } catch (final EntityDoesNotExistException exc) {
        entity.addError(ServerMessages.ENTITY_DOES_NOT_EXIST);
      } catch (final EntityWasNotUniqueException exc) {
        entity.addError(ServerMessages.CANNOT_IDENTIFY_ENTITY_UNIQUELY);
      }
    }
  }

  private void getDataTypeFromParent() throws Message {
    runJobFromSchedule(getEntity(), CheckParValid.class);

    AbstractDatatype datatype = null;
    for (final EntityInterface parent : getEntity().getParents()) {
      EntityInterface parentEntity = null;
      if (parent.getId() > 0) {
        parentEntity = retrieveValidSparseEntityById(parent.getId());
      } else {
        parentEntity = getEntityById(parent.getId());
        runJobFromSchedule(parentEntity, CheckDatatypePresent.class);
      }
      if (parentEntity.hasDatatype()) {
        if (datatype != null && !parentEntity.getDatatype().equals(datatype)) {
          getEntity().addError(ServerMessages.DATATYPE_INHERITANCE_AMBIGUOUS);
          return;
        } else {
          datatype = parentEntity.getDatatype();
        }
      }
    }
    if (datatype != null) {
      getEntity().setDatatype(datatype);
    }
  }
}
