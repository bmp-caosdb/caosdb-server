/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.core;

import caosdb.server.entity.Entity;
import caosdb.server.entity.EntityInterface;
import caosdb.server.jobs.ContainerJob;
import caosdb.server.jobs.JobAnnotation;
import caosdb.server.jobs.JobExecutionTime;
import caosdb.server.transaction.WriteTransaction;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.Observable;
import caosdb.server.utils.ServerMessages;

@JobAnnotation(
    time = JobExecutionTime.POST_CHECK,
    transaction = WriteTransaction.class,
    loadAlways = true)
public class Atomic extends ContainerJob {

  private boolean doCheck() {
    if (getContainer().getStatus() == EntityStatus.QUALIFIED) {
      for (final EntityInterface entity : getContainer()) {
        if (entity.getEntityStatus() == EntityStatus.UNQUALIFIED) {
          getContainer().setStatus(EntityStatus.UNQUALIFIED);
          getContainer().addMessage(ServerMessages.ATOMICITY_ERROR);
          // job done. remove observer.
          return false;
        }
      }
      // keep observer
      return true;
    }
    // remove observer if container is not in PARSED status anyway.
    return false;
  }

  @Override
  public boolean notifyObserver(final String e, final Observable o) {
    if (e == Entity.ENTITY_STATUS_CHANGED_EVENT) {
      return doCheck();
    }
    return true;
  }

  @Override
  protected void run() {
    if (getContainer().getStatus() == EntityStatus.QUALIFIED) {
      for (final EntityInterface entity : getContainer()) {
        entity.acceptObserver(this);
      }
    }
    doCheck();
  }
}
