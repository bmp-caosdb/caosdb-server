/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.core;

import caosdb.server.entity.EntityInterface;
import caosdb.server.jobs.FlagJob;
import caosdb.server.jobs.JobAnnotation;
import caosdb.server.jobs.JobExecutionTime;
import caosdb.server.transaction.Retrieve;
import caosdb.server.utils.EntityStatus;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@JobAnnotation(flag = "P", time = JobExecutionTime.PRE_TRANSACTION)
public class Paging extends FlagJob {

  public static final int DEFAULT_LENGTH = 100;
  public static final int DEFAULT_INDEX = 0;
  private static final Pattern pattern = Pattern.compile("(\\d+)?(?:L(\\d+))?");

  @Override
  protected void job(final String value) {
    if (getTransaction() instanceof Retrieve) {
      if (value != null) {
        int index1 = DEFAULT_INDEX;
        int index2 = index1 + DEFAULT_LENGTH;
        final Matcher m = pattern.matcher(value);
        if (m.matches()) {
          if (m.group(1) != null) {
            index1 = Integer.parseInt(m.group(1));
          }
          if (m.group(2) != null) {
            index2 = index1 + Integer.parseInt(m.group(2));
          }
        }
        int i = 0;
        for (final EntityInterface e : getContainer()) {
          if (i >= index2 || i < index1) {
            // do not retrieve this entity
            e.setEntityStatus(EntityStatus.IGNORE);
          }
          i++;
        }
      }
    }
  }
}
