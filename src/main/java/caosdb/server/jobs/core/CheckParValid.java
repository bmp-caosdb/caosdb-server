/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.core;

import caosdb.server.database.exceptions.EntityDoesNotExistException;
import caosdb.server.database.exceptions.EntityWasNotUniqueException;
import caosdb.server.entity.Affiliation;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.Message;
import caosdb.server.entity.Role;
import caosdb.server.entity.wrapper.Parent;
import caosdb.server.entity.wrapper.Property;
import caosdb.server.jobs.EntityJob;
import caosdb.server.permissions.EntityPermission;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;
import com.google.common.base.Objects;

/**
 * Check whether all parents of an entity are valid/qualified.
 *
 * @author tf
 */
public class CheckParValid extends EntityJob {
  @Override
  public final void run() {
    if (getEntity() instanceof Property || getEntity() instanceof Parent) {
      return;
    }

    // loop over all parents of the entity
    if (getEntity().hasParents()) {
      for (final Parent parent : getEntity().getParents()) {
        // test by id if an id is present or by name otherwise,
        try {
          if (!parent.hasId() && !parent.hasName()) {
            // The parent has neither an id nor a name.
            // Therefore it cannot be identified.

            throw ServerMessages.ENTITY_HAS_NO_NAME_AND_NO_ID;
          }

          if (parent.hasId()) {
            // check parent by id
            if (parent.getId() >= 0) {
              // id >= 0 (parent is yet in the database)
              // retrieve parent by id
              final EntityInterface foreign = retrieveValidSparseEntityById(parent.getId());
              // check permissions for this
              // parentforeign.acceptObserver(o)
              assertAllowedToUse(foreign);
              parent.setAffiliation(getAffiliation(getEntity().getRole(), foreign.getRole()));
              continue;
            } else {
              // id < 0 (parent is to be stored along with
              // this entity)

              // get entity with corresponding (negative) id
              // from container
              final EntityInterface foreign = getEntityById(parent.getId());

              // if the container carried a corresponding
              // entity
              if (foreign != null) {
                assertAllowedToUse(foreign);

                parent.setAffiliation(getAffiliation(getEntity().getRole(), foreign.getRole()));

                // ... we can set it as the parent
                parent.linkIdToEntity(foreign);
                continue;
              }
            }
          }

          // parent doesn't have an id.
          if (parent.hasName()) {
            if (getEntityByName(parent.getName()) != null) {
              // get the parent entity from the container by its
              // name
              final EntityInterface foreign = getEntityByName(parent.getName());

              assertAllowedToUse(foreign);
              parent.setAffiliation(getAffiliation(getEntity().getRole(), foreign.getRole()));
              parent.linkIdToEntity(foreign);
              continue;
            } else {
              // check parent by name (parent is expected to be
              // valid). This only works if the name is unique.
              final EntityInterface foreign = retrieveValidSparseEntityByName(parent.getName());
              assertAllowedToUse(foreign);
              parent.setAffiliation(getAffiliation(getEntity().getRole(), foreign.getRole()));
              parent.setId(foreign.getId());
              continue;
            }
          }

          addError(parent, ServerMessages.ENTITY_DOES_NOT_EXIST);
        } catch (final Message m) {
          addError(parent, m);
        } catch (final EntityDoesNotExistException exc) {
          addError(parent, ServerMessages.ENTITY_DOES_NOT_EXIST);
        } catch (final EntityWasNotUniqueException exc) {
          addError(parent, ServerMessages.NAME_DUPLICATES);
        }
      }
    }

    if (getEntity().getEntityStatus() != EntityStatus.UNQUALIFIED) {
      removeDuplicates();
    }
  }

  private void removeDuplicates() {
    for (final Parent par : getEntity().getParents()) {
      if (par.getEntityStatus() != EntityStatus.IGNORE) {
        for (final Parent par2 : getEntity().getParents()) {
          if (par != par2 && par2.getEntityStatus() != EntityStatus.IGNORE) {
            if ((par.hasId() && par2.hasId() && par.getId().equals(par2.getId()))
                || (par.hasName() && par2.hasName() && par.getName().equals(par2.getName()))) {
              if (!Objects.equal(par.getFlag("inheritance"), par2.getFlag("inheritance"))) {
                getEntity().addError(ServerMessages.PARENT_DUPLICATES_ERROR);
                getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
                return;
              } else {
                par.setEntityStatus(EntityStatus.IGNORE);
                getEntity().addWarning(ServerMessages.PARENT_DUPLICATES_WARNING);
              }
            }
          }
        }
      }
    }
  }

  private Affiliation getAffiliation(final Role childRole, final Role parentRole) throws Message {
    switch (childRole) {
      case File:
      case Record:
        switch (parentRole) {
          case Record:
            return Affiliation.PARTHOOD;
          case RecordType:
            return Affiliation.INSTANTIATION;
          default:
            break;
        }
        break;
      case RecordType:
        switch (parentRole) {
          case RecordType:
            return Affiliation.SUBTYPING;
          default:
            break;
        }
        break;
      case Property:
        switch (parentRole) {
          case Property:
            return Affiliation.SUBTYPING;
          default:
            break;
        }
        break;
      default:
        break;
    }
    throw ServerMessages.AFFILIATION_ERROR;
  }

  private void assertAllowedToUse(final EntityInterface entity) throws Message {
    checkPermission(entity, EntityPermission.USE_AS_PARENT);
  }

  private void addError(final EntityInterface parent, final Message m) {
    parent.addError(m);
    parent.setEntityStatus(EntityStatus.UNQUALIFIED);
  }
}
