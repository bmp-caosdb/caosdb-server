/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.core;

import caosdb.server.database.Database;
import caosdb.server.database.backend.transaction.RetrieveFullEntity;
import caosdb.server.entity.Entity;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.Message;
import caosdb.server.entity.Message.MessageType;
import caosdb.server.entity.wrapper.Property;
import caosdb.server.jobs.EntityJob;
import caosdb.server.transaction.Insert;
import caosdb.server.transaction.Update;
import caosdb.server.utils.EntityStatus;
import java.util.ArrayList;

public class Inheritance extends EntityJob {

  public enum INHERITANCE_MODE {
    NONE,
    ALL,
    OBLIGATORY,
    FIX,
    RECOMMENDED,
    DONE
  };

  public static final Message ILLEGAL_INHERITANCE_MODE =
      new Message(
          MessageType.Warning,
          0,
          "Unknown value for flag \"inheritance\". None of the parent's properties have been transfered to the child.");

  @Override
  protected void run() {
    if (getTransaction() instanceof Insert || getTransaction() instanceof Update) {
      if (getEntity().hasParents()) {
        final ArrayList<EntityInterface> transfer = new ArrayList<EntityInterface>();
        parentLoop:
        for (final EntityInterface parent : getEntity().getParents()) {
          try {
            if (parent.getFlags().get("inheritance") == null) {
              break parentLoop;
            }
            final INHERITANCE_MODE inheritance =
                INHERITANCE_MODE.valueOf(parent.getFlags().get("inheritance").toUpperCase());

            // mark inheritance flag as done
            parent.setFlag("inheritance", "done");
            if (inheritance == INHERITANCE_MODE.NONE || inheritance == INHERITANCE_MODE.DONE) {
              break parentLoop;
            }

            runJobFromSchedule(getEntity(), CheckParValid.class);

            Database.execute(new RetrieveFullEntity(parent), getTransaction().getAccess());

            if (parent.hasProperties()) {
              // loop over all properties of the parent and
              // collect
              // properties to be transfered
              for (final EntityInterface parProperty : parent.getProperties()) {
                switch (inheritance) {
                  case ALL:
                    transfer.add(parProperty);
                    break;
                  case RECOMMENDED:
                    if (parProperty
                        .getStatementStatus()
                        .toString()
                        .equalsIgnoreCase(INHERITANCE_MODE.RECOMMENDED.toString())) {
                      transfer.add(parProperty);
                    }
                  case OBLIGATORY:
                    if (parProperty
                        .getStatementStatus()
                        .toString()
                        .equalsIgnoreCase(INHERITANCE_MODE.OBLIGATORY.toString())) {
                      transfer.add(parProperty);
                    }
                  case FIX:
                    if (parProperty
                        .getStatementStatus()
                        .toString()
                        .equalsIgnoreCase(INHERITANCE_MODE.FIX.toString())) {
                      transfer.add(parProperty);
                    }
                    break;
                  default:
                    break;
                }
              }
            }
          } catch (final IllegalArgumentException e) {
            parent.addWarning(ILLEGAL_INHERITANCE_MODE);
            break parentLoop;
          }
        }

        // transfer properties if they are not implemented yet
        outerLoop:
        for (final EntityInterface prop : transfer) {
          for (final EntityInterface eprop : getEntity().getProperties()) {
            if (prop.hasId() && eprop.hasId() && prop.getId().equals(eprop.getId())) {
              continue outerLoop;
            }
          }
          // prop's Datatype might need to be resolved.
          this.appendJob(prop, CheckDatatypePresent.class);
          getEntity().addProperty(new Property(prop));
        }
      }

      // implement properties
      if (getEntity().hasProperties()) {
        propertyLoop:
        for (final EntityInterface property : getEntity().getProperties()) {
          final ArrayList<EntityInterface> transfer = new ArrayList<EntityInterface>();
          try {
            if (property.getFlags().get("inheritance") == null) {
              break propertyLoop;
            }
            final INHERITANCE_MODE inheritance =
                INHERITANCE_MODE.valueOf(property.getFlags().get("inheritance").toUpperCase());
            // mark inheritance flag as done
            property.setFlag("inheritance", "done");
            if (inheritance == INHERITANCE_MODE.NONE || inheritance == INHERITANCE_MODE.DONE) {
              break propertyLoop;
            }

            runJobFromSchedule(getEntity(), CheckPropValid.class);

            EntityInterface validProperty = new Entity(property.getId());
            if (getEntity().hasParents()) {
              outer:
              for (final EntityInterface par : getEntity().getParents()) {
                for (final EntityInterface prop : par.getProperties()) {
                  if (validProperty.hasId() && validProperty.getId().equals(prop.getId())) {
                    validProperty = prop;
                    break outer;
                  }
                }
              }
            } else {
              Database.execute(new RetrieveFullEntity(validProperty), getTransaction().getAccess());
            }

            if (validProperty.getEntityStatus() == EntityStatus.VALID
                && validProperty.hasProperties()) {
              // loop over all properties of the property and
              // collect
              // properties to be transfered
              for (final EntityInterface propProperty : validProperty.getProperties()) {
                switch (inheritance) {
                  case ALL:
                    transfer.add(propProperty);
                    break;
                  case RECOMMENDED:
                    if (propProperty
                        .getStatementStatus()
                        .toString()
                        .equalsIgnoreCase(INHERITANCE_MODE.RECOMMENDED.toString())) {
                      transfer.add(propProperty);
                    }
                  case OBLIGATORY:
                    if (propProperty
                        .getStatementStatus()
                        .toString()
                        .equalsIgnoreCase(INHERITANCE_MODE.OBLIGATORY.toString())) {
                      transfer.add(propProperty);
                    }
                  case FIX:
                    if (propProperty
                        .getStatementStatus()
                        .toString()
                        .equalsIgnoreCase(INHERITANCE_MODE.FIX.toString())) {
                      transfer.add(propProperty);
                    }
                    break;
                  default:
                    break;
                }
              }
            }
          } catch (final IllegalArgumentException e) {
            property.addWarning(ILLEGAL_INHERITANCE_MODE);
            break propertyLoop;
          }
          // transfer properties if they are not implemented yet
          outerLoop:
          for (final EntityInterface prop : transfer) {
            for (final EntityInterface eprop : property.getProperties()) {
              if (prop.hasId() && eprop.hasId() && prop.getId() == eprop.getId()) {
                continue outerLoop;
              }
            }
            // prop's Datatype might need to be resolved.
            this.appendJob(prop, CheckDatatypePresent.class);
            property.addProperty(new Property(prop));
          }
        }
      }
    }
  }
}
