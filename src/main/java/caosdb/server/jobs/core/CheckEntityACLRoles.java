/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.core;

import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import caosdb.server.accessControl.AuthenticationUtils;
import caosdb.server.entity.EntityInterface;
import caosdb.server.jobs.ContainerJob;
import caosdb.server.permissions.EntityACI;
import caosdb.server.transaction.Insert;
import caosdb.server.transaction.Update;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;

public class CheckEntityACLRoles extends ContainerJob {

  @Override
  protected void run() {
    if (!(getTransaction() instanceof Update || getTransaction() instanceof Insert)) {
      return;
    }

    for (final EntityInterface entity : getContainer()) {
      if (entity.getEntityACL() != null) {
        for (final EntityACI aci : entity.getEntityACL().getRules()) {
          if (!AuthenticationUtils.isResponsibleAgentExistent(aci.getResponsibleAgent())) {
            if (CaosDBServer.getServerProperty(ServerProperties.KEY_CHECK_ENTITY_ACL_ROLES_MODE)
                .equalsIgnoreCase("MUST")) {
              entity.addError(ServerMessages.ROLE_DOES_NOT_EXIST);
              entity.setEntityStatus(EntityStatus.UNQUALIFIED);
            } else {
              entity.addWarning(ServerMessages.ROLE_DOES_NOT_EXIST);
            }
            entity.addInfo(
                "User Role `" + aci.getResponsibleAgent().toString() + "` does not exist.");
          }
        }
      }
    }
  }
}
