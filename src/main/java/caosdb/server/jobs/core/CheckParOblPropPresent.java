/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.core;

import caosdb.server.database.Database;
import caosdb.server.database.backend.transaction.RetrieveFullEntity;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.StatementStatus;
import caosdb.server.jobs.EntityJob;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;

/**
 * Check whether an entity implements all obligatory properties of all parents.
 *
 * @author tf
 */
public class CheckParOblPropPresent extends EntityJob {
  @Override
  public final void run() {

    // This test needs the valid/qualified ids of all properties.
    // Thus, run a CheckPropValid first.
    runJobFromSchedule(getEntity(), CheckPropValid.class);

    // This test needs the valid/qualified parents. If a parent is only
    // referenced by name it has to be translated into a (hopefully) unique
    // id.
    runJobFromSchedule(getEntity(), CheckParValid.class);

    if (getEntity().hasParents()) {
      Database.execute(
          new RetrieveFullEntity(getEntity().getParents()), getTransaction().getAccess());
    }

    // inherit properties
    runJobFromSchedule(getEntity(), Inheritance.class);

    // loop over all parents
    if (getEntity().hasParents()) {
      for (final EntityInterface parent : getEntity().getParents()) {

        // if the parent has an id>0, it is to be a valid entity.
        if (parent.hasId() && parent.getId() > 0) {
          if (parent.getEntityStatus() == EntityStatus.NONEXISTENT) {
            continue;
          }

          // does this parent have properties?
          if (parent.hasProperties()) {

            // loop over all properties of this parent
            outerLoop:
            for (final EntityInterface parentProperty : parent.getProperties()) {

              // only obligatory properties are of interest.
              if (parentProperty.hasStatementStatus()
                  && parentProperty.getStatementStatus() == StatementStatus.OBLIGATORY) {

                // loop over all properties of the entity
                innerLoop:
                for (final EntityInterface entityProperty : getEntity().getProperties()) {

                  if (!entityProperty.hasId()) {
                    // continue inner loop means that this
                    // is not the entityProperty in
                    // question, continue search for an
                    // entityProperty.
                    continue innerLoop;
                  }

                  // TODO sub type instead of equals!!!
                  if (parentProperty.getId().equals(entityProperty.getId())) {
                    // continue outer loop means that we
                    // found an entityProperty that
                    // implements the obligatory
                    // parentProperty, continue with the
                    // other parentProperties.
                    continue outerLoop;
                  }
                }

                // No entityProperty has been found which
                // implements this parentProperty. Add the
                // respective messages.
                switch (getMode()) {
                  case MUST:
                    // TODO add information WHICH property is
                    // missing.
                    getEntity().addError(ServerMessages.OBLIGATORY_PROPERTY_MISSING);
                    break;
                  case SHOULD:
                    // TODO add information WHICH property is
                    // missing.
                    getEntity().addWarning(ServerMessages.OBLIGATORY_PROPERTY_MISSING);
                    break;
                  default:
                    break;
                }
              }
            }
          }
        } else {
          // TODO add processing of new parents, new properties
        }
      }
    }
  }
}
