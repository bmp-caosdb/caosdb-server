package caosdb.server.jobs.extension;

import caosdb.datetime.DateTimeInterface;
import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import caosdb.server.datatype.ReferenceValue;
import caosdb.server.datatype.SingleValue;
import caosdb.server.datatype.Value;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.wrapper.Property;
import caosdb.server.utils.mail.Mail;
import java.util.TimeZone;

public abstract class AWIBoxLoanCuratorEmail extends AWIBoxLoanModel {

  public static final String KEY_EXT_AWI_CURATOR_EMAIL = "EXT_AWI_BOX_CURATOR_EMAIL";
  protected static final String FROM_EMAIL =
      CaosDBServer.getServerProperty(ServerProperties.KEY_NO_REPLY_EMAIL);
  protected static final String FROM_NAME =
      CaosDBServer.getServerProperty(ServerProperties.KEY_NO_REPLY_NAME);
  protected static final String CURATOR_EMAIL =
      CaosDBServer.getServerProperty(KEY_EXT_AWI_CURATOR_EMAIL);

  protected String loanToString(EntityInterface e) {
    StringBuilder s = new StringBuilder();
    for (Property p : e.getProperties()) {
      s.append("\n");
      s.append(p.getName());
      s.append(": ");
      if (p.getValue() instanceof ReferenceValue) {
        Integer id = ((ReferenceValue) p.getValue()).getId();
        if (isBorrowerProperty(p)) {
          s.append(borrowerToString(id));
        } else if (isBoxProperty(p)) {
          s.append(boxToString(id));
        } else {
          s.append(valueToString(p.getValue()));
        }
      } else {
        s.append(valueToString(p.getValue()));
      }
    }
    return s.toString();
  }

  private String valueToString(Value value) {
    if (value == null) {
      return "";
    }
    if (value instanceof DateTimeInterface) {
      return ((DateTimeInterface) value).toDateTimeString(TimeZone.getDefault());
    }
    if (value instanceof SingleValue) {
      return ((SingleValue) value).toDatabaseString();
    } else {
      return value.toString();
    }
  }

  private String boxToString(Integer id) {
    StringBuilder s = new StringBuilder();
    EntityInterface box = retrieveValidEntity(id);
    for (Property sp : box.getProperties()) {
      if (isNumberProperty(sp) || isContentProperty(sp) || isLocationProperty(sp)) {
        s.append("\n  ");
        s.append(sp.getName());
        s.append(": ");
        s.append(valueToString(sp.getValue()));
      }
    }
    return s.toString();
  }

  private String borrowerToString(Integer id) {
    StringBuilder s = new StringBuilder();
    EntityInterface borrower = retrieveValidEntity(id);
    for (Property sp : borrower.getProperties()) {
      s.append("\n  ");
      s.append(sp.getName());
      s.append(": ");
      s.append(valueToString(sp.getValue()));
    }
    return s.toString();
  }

  protected void sendCuratorEmail(String body, String subject) {
    for (String addr : CURATOR_EMAIL.split(" ")) {
      Mail m = new Mail(FROM_NAME, FROM_EMAIL, null, addr, subject, body);
      m.send();
    }
  }
}
