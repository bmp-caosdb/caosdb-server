package caosdb.server.jobs.extension;

import caosdb.server.entity.EntityInterface;
import caosdb.server.jobs.JobAnnotation;
import caosdb.server.jobs.JobExecutionTime;

@JobAnnotation(time = JobExecutionTime.POST_TRANSACTION)
public class AWIBoxLoanRequestLoanCuratorEmail extends AWIBoxLoanCuratorEmail {

  private static final String SUBJECT = "Box Loan Requests";

  @Override
  protected void run() {
    StringBuilder body = new StringBuilder("LOAN REQUEST(S):");

    if (!getContainer().isEmpty()) {
      for (EntityInterface e : getContainer()) {
        body.append("\n");
        body.append(loanToString(e));
      }
      this.sendCuratorEmail(body.toString(), SUBJECT);
    }
  }
}
