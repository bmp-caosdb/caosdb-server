package caosdb.server.jobs.extension;

import caosdb.server.CaosDBServer;
import caosdb.server.database.exceptions.EntityDoesNotExistException;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.Role;
import caosdb.server.entity.wrapper.Property;
import caosdb.server.jobs.ContainerJob;
import caosdb.server.utils.Utils;
import java.util.Objects;

public abstract class AWIBoxLoanModel extends ContainerJob {

  /** Is Record and has single user parent. */
  boolean isPersonRecord(EntityInterface entity) {
    try {
      return entity.getParents().size() == 1
          && Objects.equals(
              retrieveValidIDByName(entity.getParents().get(0).getName()), getPersonID());
    } catch (EntityDoesNotExistException exc) {
      return false;
    }
  }

  /** Is Record an has single box parent. */
  boolean isBoxRecord(EntityInterface e) {
    return e.getRole() == Role.Record
        && e.getParents().size() == 1
        && Objects.equals(e.getParents().get(0).getId(), getBoxId());
  }

  /** Is Record and has single loan parent */
  boolean isLoanRecord(EntityInterface e) {
    try {
      return e.getRole() == Role.Record
          && e.getParents().size() == 1
          && Objects.equals(retrieveValidIDByName(e.getParents().get(0).getName()), getLoanId());
    } catch (EntityDoesNotExistException exc) {
      return false;
    }
  }

  boolean isBoxProperty(Property p) {
    return Objects.equals(p.getId(), getBoxId());
  }

  boolean isBorrowerProperty(Property p) {
    return Objects.equals(p.getId(), getBorrowerId());
  }

  boolean isLocationProperty(Property p) {
    return Objects.equals(p.getId(), getLocationId());
  }

  boolean isContentProperty(Property p) {
    return Objects.equals(p.getId(), getContentId());
  }

  boolean isNumberProperty(Property p) {
    return Objects.equals(p.getId(), getNumberId());
  }

  Integer getIdOf(String string) {
    String id = CaosDBServer.getServerProperty("EXT_AWI_" + string.toUpperCase() + "_ID");
    if (id != null && Utils.isNonNullInteger(id)) {
      return new Integer(id);
    }
    String name = CaosDBServer.getServerProperty("EXT_AWI_" + string.toUpperCase() + "_NAME");
    if (name == null || name.isEmpty()) {
      name = string;
    }
    return retrieveValidIDByName(name);
  }

  Integer getBorrowerId() {
    return getIdOf("Borrower");
  }

  Integer getCommentId() {
    return getIdOf("comment");
  }

  Integer getLocationId() {
    return getIdOf("Location");
  }

  Integer getDestinationId() {
    return getIdOf("destination");
  }

  Integer getContentId() {
    return getIdOf("Content");
  }

  Integer getBoxId() {
    return getIdOf("Box");
  }

  Integer getLoanId() {
    return getIdOf("Loan");
  }

  Integer getPersonID() {
    return getIdOf("Person");
  }

  Integer getEmailID() {
    return getIdOf("email");
  }

  Integer getLoanAcceptedId() {
    return getIdOf("loanAccepted");
  }

  Integer getReturnAcceptedId() {
    return getIdOf("returnAccepted");
  }

  Integer getLentId() {
    return getIdOf("lent");
  }

  Integer getLoanRequestedId() {
    return getIdOf("loanRequested");
  }

  Integer getExhaustContentsId() {
    return getIdOf("exhaustContents");
  }

  Integer getExpectedReturnId() {
    return getIdOf("expectedReturn");
  }

  Integer getReturnRequestedId() {
    return getIdOf("returnRequested");
  }

  Integer getLastNameId() {
    return getIdOf("lastName");
  }

  Integer getFirstNameId() {
    return getIdOf("firstName");
  }

  Integer getNumberId() {
    return getIdOf("Number");
  }

  Integer getReturnedId() {
    return getIdOf("returned");
  }
}
