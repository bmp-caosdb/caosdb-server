/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs.extension;

import caosdb.server.CaosDBException;
import caosdb.server.entity.FileProperties;
import caosdb.server.entity.Message;
import caosdb.server.jobs.EntityJob;
import caosdb.server.utils.FileUtils;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class SQLiteTransaction extends EntityJob {
  private static HashMap<String, ReentrantLock> lockedTables = new HashMap<String, ReentrantLock>();

  @Override
  public final void run() {
    Connection con = null;
    try {
      if (getEntity().hasFileProperties()) {
        if (getEntity().getFileProperties().retrieveFromFileSystem() != null) {

          if (getEntity().hasMessage("execute")) {
            try {
              if (!lockedTables.containsKey(
                  getEntity().getFileProperties().getFile().getCanonicalPath())) {
                synchronized (lockedTables) {
                  lockedTables.put(
                      getEntity().getFileProperties().getFile().getCanonicalPath(),
                      new ReentrantLock());
                }
              }

              lockedTables.get(getEntity().getFileProperties().getFile().getCanonicalPath()).lock();
              try {
                con =
                    getConnection(
                        getEntity()
                            .getFileProperties()
                            .retrieveFromFileSystem()
                            .getCanonicalPath());
                final Statement stmt = con.createStatement();

                //
                final List<Message> msgs = getEntity().getMessages("execute");
                Collections.sort(msgs);
                for (final Message m : msgs) {
                  getEntity().removeMessage(m);
                  stmt.execute(m.getBody());
                  final ResultSet rs = stmt.getResultSet();
                  final int updateCount = stmt.getUpdateCount();
                  if (rs != null) {
                    final ResultSetMetaData rsmd = rs.getMetaData();
                    final int columns = rsmd.getColumnCount();
                    final StringBuilder s = new StringBuilder();
                    if (columns > 0) {
                      s.append("|");
                      for (int i = 1; i <= columns; i++) {
                        s.append(rsmd.getColumnName(i));
                        s.append("|");
                      }
                      s.append("\n");
                      while (rs.next()) {
                        s.append("|");
                        for (int i = 1; i <= columns; i++) {
                          s.append(rs.getString(i));
                          s.append("|");
                        }
                        s.append("\n");
                      }
                    } else {
                      s.append("");
                    }
                    final Message mret = new Message("result", m.getCode(), null, s.toString());
                    getEntity().addMessage(mret);

                  } else if (updateCount != -1) {
                    final Message mret =
                        new Message(
                            "result",
                            m.getCode(),
                            null,
                            Integer.toString(updateCount) + " row(s) affected by the query.");
                    getEntity().addMessage(mret);
                  }
                }
              } finally {
                lockedTables
                    .get(getEntity().getFileProperties().getFile().getCanonicalPath())
                    .unlock();
              }
              synchronized (
                  lockedTables.get(getEntity().getFileProperties().getFile().getCanonicalPath())) {
                if (!lockedTables
                    .get(getEntity().getFileProperties().getFile().getCanonicalPath())
                    .hasQueuedThreads()) {
                  lockedTables.remove(
                      lockedTables.get(
                          getEntity().getFileProperties().getFile().getCanonicalPath()));
                }
              }
            } finally {
              if (con != null && !con.isClosed()) {
                con.close();
              }
              resetSizeAndChecksum(getEntity().getFileProperties());
            }
          }
        }
      }
    } catch (final NoSuchAlgorithmException e) {
      throw new JobException(this, e);
    } catch (final IOException e) {
      throw new JobException(this, e);
    } catch (final CaosDBException e) {
      throw new JobException(this, e);
    } catch (final ClassNotFoundException e) {
      throw new JobException(this, e);
    } catch (final SQLException e) {
      throw new JobException(this, e);
    }
  }

  private static void resetSizeAndChecksum(final FileProperties f)
      throws NoSuchAlgorithmException, IOException {
    f.setSize(f.getFile().length());
    f.setChecksum(FileUtils.getChecksum(f.getFile()));
  }

  private static Connection getConnection(final String database)
      throws ClassNotFoundException, SQLException {
    if (!driverLoaded) {
      Class.forName("org.sqlite.JDBC");
      driverLoaded = true;
    }
    final Connection connection = DriverManager.getConnection("jdbc:sqlite:" + database);
    return connection;
  }

  private static boolean driverLoaded = false;
}
