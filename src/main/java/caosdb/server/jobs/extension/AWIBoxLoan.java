package caosdb.server.jobs.extension;

import static caosdb.server.permissions.Role.ANONYMOUS_ROLE;

import caosdb.server.accessControl.UserSources;
import caosdb.server.database.exceptions.EntityDoesNotExistException;
import caosdb.server.datatype.SingleValue;
import caosdb.server.entity.Entity;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.Message;
import caosdb.server.entity.Message.MessageType;
import caosdb.server.entity.wrapper.Property;
import caosdb.server.jobs.JobAnnotation;
import caosdb.server.jobs.core.CheckNoAdditionalPropertiesPresent;
import caosdb.server.jobs.core.CheckNoOverridesPresent;
import caosdb.server.jobs.core.CheckPropValid;
import caosdb.server.permissions.EntityACL;
import caosdb.server.permissions.EntityACLFactory;
import caosdb.server.permissions.EntityPermission;
import caosdb.server.query.Query;
import caosdb.server.transaction.Delete;
import caosdb.server.transaction.Insert;
import caosdb.server.transaction.Update;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.ServerMessages;
import caosdb.server.utils.Utils;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@JobAnnotation(transaction = caosdb.server.transaction.WriteTransaction.class, loadAlways = true)
public class AWIBoxLoan extends AWIBoxLoanModel {

  public Logger logger = LoggerFactory.getLogger(getClass());
  private static final Message UNIQUE_USER =
      new Message("The user must have a unique combination of first name and last name!");
  private static final Message BOX_HAS_LOAN =
      new Message(
          "This box cannot be be requested right now because it appears to have a Loan property attached to it. This usually means, that the box is already requested or borrowed by someone.");

  @Override
  protected void run() {
    try {
      if (isAnonymous()
          && (getTransaction() instanceof Delete
              || isAcceptBorrowUpdateLoan()
              || isConfirmLoanUpdateLoan()
              || isRejectReturnUpdateLoan()
              || isAcceptReturnUpdateLoan()
              || isManualReturnUpdateBox()
              || isManualReturnUpdateLoan()
              || !(isRequestLoanSetUser()
                  || isRequestLoanInsertLoan()
                  || isRequestLoanUpdateBox()
                  || isRequestReturnSetUser()
                  || isRequestReturnUpdateLoan()))) {
        addError(ServerMessages.AUTHORIZATION_ERROR);
        getContainer()
            .addMessage(
                new Message(MessageType.Info, 0, "Anonymous users have restricted permissions."));
        return;
      }
    } catch (EntityDoesNotExistException exc) {
      addError(ServerMessages.AUTHORIZATION_ERROR);
      getContainer()
          .addMessage(
              new Message(MessageType.Info, 0, "Anonymous users have restricted permissions."));
      return;
    }

    try {
      if (!(getTransaction() instanceof Delete
          || isRequestLoanSetUser()
          || isRequestLoanInsertLoan()
          || isManualReturnUpdateLoan()
          || isManualReturnUpdateBox()
          || isRequestLoanUpdateBox()
          || isAcceptBorrowUpdateLoan()
          || isConfirmLoanUpdateLoan()
          || isRequestReturnSetUser()
          || isRequestReturnUpdateLoan()
          || isRejectReturnUpdateLoan())) {
        isAcceptReturnUpdateLoan();
      }
    } catch (EntityDoesNotExistException exc) {
      // ignore
    }

    // special ACL for boxes, loans and persons
    try {
      if (getTransaction() instanceof Insert) {
        for (EntityInterface e : getContainer()) {
          if (isBoxRecord(e)) {
            e.setEntityACL(EntityACL.combine(e.getEntityACL(), getBoxACL()));
          } else if (isLoanRecord(e)) {
            e.setEntityACL(EntityACL.combine(e.getEntityACL(), getLoanACL()));
          } else if (isPersonRecord(e)) {
            e.setEntityACL(EntityACL.combine(e.getEntityACL(), getPersonACL()));
          }
        }
      }
    } catch (EntityDoesNotExistException e) {
    }
  }

  boolean isManualReturnUpdateLoan() {
    for (EntityInterface e : getContainer()) {
      if (!isLoanRecord(e) || !hasManualReturnLoanProperties(e)) {
        logger.trace("isManualReturnUpdateLoan: false");
        return false;
      }
    }
    for (EntityInterface e : getContainer()) {
      removeAnonymousPermissions(e);
    }
    logger.trace("isManualReturnUpdateLoan: true");
    return true;
  }

  boolean hasManualReturnLoanProperties(EntityInterface e) {
    for (Property p : e.getProperties()) {
      if (isReturnedProperty(p)) {
        return true;
      }
    }
    return false;
  }

  boolean isReturnedProperty(Property p) {
    return Objects.equals(p.getId(), getReturnedId());
  }

  void removeAnonymousPermissions(EntityInterface e) {
    EntityACLFactory f = new EntityACLFactory();
    f.deny(ANONYMOUS_ROLE, false, EntityPermission.UPDATE_ADD_PROPERTY);
    f.deny(ANONYMOUS_ROLE, false, EntityPermission.UPDATE_REMOVE_PROPERTY);
    e.setEntityACL(EntityACL.combine(e.getEntityACL(), f.create()));
  }

  boolean isManualReturnUpdateBox() {
    for (EntityInterface e : getContainer()) {
      if (!isBoxRecord(e) || !hasManualReturnBoxProperties(e)) {
        logger.trace("isManualReturnUpdateBox: false");
        return false;
      }
    }
    logger.trace("isManualReturnUpdateBox: true");
    return true;
  }

  boolean hasManualReturnBoxProperties(EntityInterface e) {
    return validBoxHasLoanProperty(e) && boxLoanHasReturnedProperty(e);
  }

  boolean boxLoanHasReturnedProperty(EntityInterface e) {
    try {
      EntityInterface validBox = retrieveValidEntity(e.getId());

      for (Property p : validBox.getProperties()) {
        if (isLoanProperty(p)) {
          return validLoanHasReturnedProperty(p.getId());
        }
      }
    } catch (EntityDoesNotExistException exc) {
      return false;
    }
    return false;
  }

  boolean validLoanHasReturnedProperty(Integer id) {
    try {
      EntityInterface validLoan = retrieveValidEntity(id);
      for (Property p : validLoan.getProperties()) {
        if (isReturnedProperty(p)) {
          return true;
        }
      }
    } catch (EntityDoesNotExistException exc) {
      return false;
    }
    return false;
  }

  boolean isLoanProperty(Property p) {
    return Objects.equals(p.getId(), getLoanId());
  }

  private boolean isRejectReturnUpdateLoan() {
    for (EntityInterface e : getContainer()) {
      if (!isLoanRecord(e) || !hasRejectReturnProperties(e)) {
        logger.trace("isRejectReturnUpdateLoan: false");
        return false;
      }
    }
    for (EntityInterface e : getContainer()) {
      removeDestinationProperty(e);
    }
    logger.trace("isRejectReturnUpdateLoan: true");
    return true;
  }

  void removeDestinationProperty(EntityInterface e) {
    Iterator<Property> iterator = e.getProperties().iterator();
    while (iterator.hasNext()) {
      Property p = iterator.next();
      if (isDestinationProperty(p)) {
        iterator.remove();
      }
    }
  }

  boolean hasRejectReturnProperties(EntityInterface e) {
    boolean found = false;
    for (Property p : e.getProperties()) {
      if (isDestinationProperty(p)) {
        found = true;
      } else if (isReturnAcceptedProperty(p) || isReturnRequestProperty(p)) {
        logger.trace("hasRejectReturnProperties: false");
        return false;
      }
    }
    logger.trace("hasRejectReturnProperties found: {}", found);
    return found;
  }

  boolean isDestinationProperty(Property p) {
    return Objects.equals(p.getId(), getDestinationId());
  }

  boolean isAcceptReturnUpdateLoan() {
    for (EntityInterface e : getContainer()) {
      if (!isLoanRecord(e) || !hasAcceptReturnProperties(e)) {
        logger.trace("isAcceptReturnUpdateLoan: false");
        return false;
      }
    }
    logger.trace("isAcceptReturnUpdateLoan: true");
    return true;
  }

  boolean hasAcceptReturnProperties(EntityInterface e) {
    boolean found = false;
    for (Property p : e.getProperties()) {
      if (isReturnAcceptedProperty(p)) {
        found = true;
      }
    }
    return found;
  }

  boolean isConfirmLoanUpdateLoan() {
    for (EntityInterface e : getContainer()) {
      if (!isLoanRecord(e) || !hasConfirmLoanProperties(e)) {
        logger.trace("isConfirmLoanUpdateLoan: false");
        return false;
      }
    }
    // switch from destination to location
    for (EntityInterface e : getContainer()) {
      switchDestinationToLocation(e);
    }
    logger.trace("isConfirmLoanUpdateLoan: true");
    return true;
  }

  void switchDestinationToLocation(EntityInterface e) {
    Iterator<Property> iterator = e.getProperties().iterator();
    EntityInterface p = retrieveValidEntity(getLocationId());
    while (iterator.hasNext()) {
      Property next = iterator.next();
      if (isDestinationProperty(next)) {
        iterator.remove();
        p.setValue(next.getValue());
        e.addProperty(new Property(p));
        break;
      }
    }
  }

  boolean hasConfirmLoanProperties(EntityInterface e) {
    boolean found = false;
    for (Property p : e.getProperties()) {
      if (isLentProperty(p)) {
        found = true;
      } else if (isReturnAcceptedProperty(p) || isDestinationProperty(p)) {
        logger.trace("hasConfirmLoanProperties: false");
        return false;
      }
    }
    logger.trace("hasConfirmLoanProperties found: {}", found);
    return found;
  }

  boolean isAcceptBorrowUpdateLoan() {
    for (EntityInterface e : getContainer()) {
      if (!isLoanRecord(e) || !hasLoanAcceptProperties(e)) {
        logger.trace("isAcceptBorrowUpdateLoan: false");
        return false;
      }
    }
    logger.trace("isAcceptBorrowUpdateLoan: true");
    return true;
  }

  boolean hasLoanAcceptProperties(EntityInterface e) {
    boolean found = false;
    for (Property p : e.getProperties()) {
      if (isLoanAcceptProperty(p)) {
        found = true;
      } else if (isLentProperty(p) || isReturnAcceptedProperty(p)) {
        logger.trace("hasLoanAcceptProperties: false");
        return false;
      }
    }
    logger.trace("hasLoanAcceptProperties found: {}", found);
    return found;
  }

  boolean isReturnAcceptedProperty(Property p) {
    return Objects.equals(p.getId(), getReturnAcceptedId());
  }

  boolean isLentProperty(Property p) {
    return Objects.equals(p.getId(), getLentId());
  }

  boolean isLoanAcceptProperty(Property p) {
    return Objects.equals(p.getId(), getLoanAcceptedId());
  }

  EntityACL getPersonACL() {
    // same as loan acl - property updates are allowed for anonymous.
    return getLoanACL();
  }

  EntityACL getLoanACL() {
    EntityACLFactory f = new EntityACLFactory();
    f.grant(ANONYMOUS_ROLE, false, EntityPermission.UPDATE_ADD_PROPERTY);
    f.grant(ANONYMOUS_ROLE, false, EntityPermission.UPDATE_REMOVE_PROPERTY);
    return f.create();
  }

  EntityACL getBoxACL() {
    EntityACLFactory f = new EntityACLFactory();
    f.grant(ANONYMOUS_ROLE, false, EntityPermission.UPDATE_ADD_PROPERTY);
    f.grant(ANONYMOUS_ROLE, false, EntityPermission.UPDATE_REMOVE_PROPERTY);
    return f.create();
  }

  boolean isAnonymous() {
    boolean ret = getUser().hasRole(UserSources.ANONYMOUS_ROLE);
    logger.trace("is Anonymous: {}", ret);
    return ret;
  }

  boolean isRequestReturnUpdateLoan() {
    // is UPDATE transaction
    if (getTransaction() instanceof Update) {
      // Container has only loan elements with special properties
      for (EntityInterface e : getContainer()) {
        if (!isLoanRecord(e) || !hasOnlyAllowedLoanProperties4RequestReturn(e)) {
          logger.trace("isRequestReturnUpdateLoan: false");
          return false;
        }
        setReturnRequestedDate(e);
      }
      appendJob(AWIBoxLoanRequestReturnCuratorEmail.class);
      logger.trace("isRequestReturnUpdateLoan: true");
      return true;
    }
    logger.trace("isRequestReturnUpdateLoan: false");
    return false;
  }

  boolean isRequestReturnSetUser() {
    // same as request_loan.set_user
    logger.trace("isRequestReturnSetUser: ->");
    return isRequestLoanSetUser();
  }

  boolean isRequestLoanUpdateBox() {
    // is UPDATE transaction
    if (getTransaction() instanceof Update) {
      // Container has only box elements
      for (EntityInterface e : getContainer()) {
        if (validBoxHasLoanProperty(e)) {
          e.addError(BOX_HAS_LOAN);
          return false;
        }
        if (!isBoxRecord(e) || !hasOnlyAllowedBoxProperties4RequestLoan(e)) {
          return false;
        }
        appendJob(e, CheckNoAdditionalPropertiesPresent.class);
      }
      return true;
    }
    return false;
  }

  boolean validBoxHasLoanProperty(EntityInterface e) {
    try {
      EntityInterface validBox = retrieveValidEntity(e.getId());
      for (Property p : validBox.getProperties()) {
        if (isLoanProperty(p)) {
          return true;
        }
      }
    } catch (EntityDoesNotExistException exc) {
      return false;
    }
    return false;
  }

  /** Has only one new property -> Loan. */
  boolean hasOnlyAllowedBoxProperties4RequestLoan(EntityInterface e) {
    int count = 0;
    for (Property p : e.getProperties()) {
      if (p.getEntityStatus() == EntityStatus.QUALIFIED && Objects.equals(p.getId(), getLoanId())) {
        count++;
      }
    }

    // Box has only one update, a loan property
    return count == 1;
  }

  boolean isRequestLoanInsertLoan() {
    // is INSERT transaction
    if (getTransaction() instanceof Insert) {
      // Container has only loan elements
      for (EntityInterface e : getContainer()) {
        if (!isLoanRecord(e)) {
          return false;
        }
      }
      for (EntityInterface e : getContainer()) {
        if (isAnonymous()) {
          setCuratorAsOwner(e);
        }
        setLoanRequestDate(e);
        appendJob(e, CheckNoAdditionalPropertiesPresent.class);
        appendJob(e, CheckNoOverridesPresent.class);
      }
      appendJob(AWIBoxLoanRequestLoanCuratorEmail.class);
      return true;
    }
    return false;
  }

  void setCuratorAsOwner(EntityInterface e) {
    e.setEntityACL(EntityACL.getOwnerACLFor(caosdb.server.permissions.Role.create("curator")));
  }

  void setReturnRequestedDate(EntityInterface e) {
    // TODO setDateProperty(e, getReturnRequestedId());
  }

  private void setDateProperty(EntityInterface e, Integer propertyId) {
    // TODO
    EntityInterface p = retrieveValidEntity(propertyId);
    p.setValue(getTransaction().getTimestamp());
    e.addProperty(new Property(p));
  }

  void setLoanRequestDate(EntityInterface e) {
    // TODO setDateProperty(e, getLoanRequestedId());
  }

  boolean isRequestLoanSetUser() {
    // is INSERT/UPDATE transaction
    // Container has only one element, user
    if ((getTransaction() instanceof Update || getTransaction() instanceof Insert)
        && getContainer().size() == 1
        && isPersonRecord(getContainer().get(0))
        && checkUniqueName(getContainer().get(0))
        && checkEmail(getContainer().get(0))) {
      appendJob(getContainer().get(0), CheckNoAdditionalPropertiesPresent.class);
      appendJob(getContainer().get(0), CheckNoOverridesPresent.class);
      logger.trace("isRequestReturnSetUser: true");
      return true;
    }
    logger.trace("isRequestReturnSetUser: false");
    return false;
  }

  boolean checkEmail(Entity entity) {
    runJobFromSchedule(entity, CheckPropValid.class);
    for (Property p : entity.getProperties()) {
      if (Objects.equals(p.getId(), getEmailID()) && p.getValue() instanceof SingleValue) {
        if (!Utils.isRFC822Compliant(((SingleValue) p.getValue()).toDatabaseString())) {
          p.addError(ServerMessages.EMAIL_NOT_WELL_FORMED);
        } else {
          p.setName("email"); // TODO fix webinterface to use lower-case email
          p.setNameOverride(false);
        }
        return true;
      }
    }
    return false;
  }

  private boolean checkUniqueName(Entity entity) {
    String firstName = null;
    String lastName = null;
    Query q =
        new Query(
                "FIND "
                    + getPersonID().toString()
                    + " WITH "
                    + getFirstNameId().toString()
                    + "='"
                    + firstName
                    + "' AND "
                    + getLastNameId().toString()
                    + "='"
                    + lastName
                    + "'",
                getUser())
            .execute(getTransaction().getAccess());
    List<Integer> resultSet = q.getResultSet();
    if (resultSet.isEmpty()
        || (resultSet.size() == 1 && Objects.equals(resultSet.get(0), entity.getId()))) {
      return true;
    }
    entity.addError(UNIQUE_USER);
    return false;
  }

  /**
   * Has only 5/6 new/updated properties: content, returnRequested, destination, Borrower, comment
   * (optional), location
   *
   * @throws Message
   */
  boolean hasOnlyAllowedLoanProperties4RequestReturn(EntityInterface e) {
    runJobFromSchedule(e, CheckPropValid.class);
    appendJob(e, CheckNoOverridesPresent.class);

    boolean foundReturnRequested = false;
    for (Property p : e.getProperties()) {
      if (p.getEntityStatus() == EntityStatus.QUALIFIED) { // this means update
        if (isReturnRequestProperty(p)) {
          foundReturnRequested = true;
        } else if (isReturnAcceptedProperty(p) || isReturnedProperty(p)) {
          logger.trace("hasOnlyAllowedLoanProperties4RequestReturn: false");
          return false; // this is not a returnRequest, return has already been accepted
        }
      }
    }
    logger.trace("hasOnlyAllowedLoanProperties4RequestReturn found: {}", foundReturnRequested);
    return foundReturnRequested;
  }

  boolean isReturnRequestProperty(Property p) {
    return Objects.equals(p.getId(), getReturnRequestedId());
  }
}
