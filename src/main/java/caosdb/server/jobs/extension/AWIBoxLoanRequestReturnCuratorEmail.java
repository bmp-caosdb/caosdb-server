package caosdb.server.jobs.extension;

import caosdb.server.entity.EntityInterface;
import caosdb.server.jobs.JobAnnotation;
import caosdb.server.jobs.JobExecutionTime;

@JobAnnotation(time = JobExecutionTime.POST_TRANSACTION)
public class AWIBoxLoanRequestReturnCuratorEmail extends AWIBoxLoanCuratorEmail {
  private static final String SUBJECT = "Box Return Requests";

  @Override
  protected void run() {
    if (!getContainer().isEmpty()) {
      StringBuilder body = new StringBuilder("RETURN REQUEST(S):");
      for (EntityInterface e : getContainer()) {
        body.append("\n");
        body.append(loanToString(e));
      }
      this.sendCuratorEmail(body.toString(), SUBJECT);
    }
  }
}
