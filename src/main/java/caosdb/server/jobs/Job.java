/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs;

import caosdb.server.CaosDBException;
import caosdb.server.database.Database;
import caosdb.server.database.backend.transaction.GetIDByName;
import caosdb.server.database.backend.transaction.IsSubType;
import caosdb.server.database.backend.transaction.RetrieveFullEntity;
import caosdb.server.database.backend.transaction.RetrieveParents;
import caosdb.server.database.backend.transaction.RetrieveSparseEntity;
import caosdb.server.database.backend.transaction.RuleLoader;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.datatype.AbstractCollectionDatatype;
import caosdb.server.datatype.AbstractDatatype;
import caosdb.server.datatype.ReferenceDatatype2;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.Message;
import caosdb.server.entity.container.TransactionContainer;
import caosdb.server.jobs.core.Mode;
import caosdb.server.transaction.Transaction;
import caosdb.server.utils.AbstractObservable;
import caosdb.server.utils.EntityStatus;
import caosdb.server.utils.Observable;
import caosdb.server.utils.Observer;
import caosdb.server.utils.ServerMessages;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.subject.Subject;
import org.reflections.Reflections;

public abstract class Job extends AbstractObservable implements Observer {
  private Transaction<? extends TransactionContainer> transaction = null;
  private Mode mode = null;

  public abstract JobTarget getTarget();

  private EntityInterface entity = null;

  protected <S, T> HashMap<S, T> getCache(final String name) {
    return getTransaction().getCache(name);
  }

  protected ScheduledJob appendJob(final Class<? extends Job> clazz) {
    return appendJob(getEntity(), clazz);
  }

  protected String getRequestId() {
    return getContainer().getRequestId();
  }

  protected Subject getUser() {
    return getTransaction().getTransactor();
  }

  protected ScheduledJob appendJob(final EntityInterface entity, final Class<? extends Job> clazz) {
    try {
      final Job job = clazz.newInstance();
      job.init(getMode(), entity, getTransaction());
      return getTransaction().getSchedule().add(job);
    } catch (InstantiationException | IllegalAccessException e) {
      throw new CaosDBException(e);
    }
  }

  protected EntityInterface getEntityById(final int id) {
    return getContainer().getEntityById(id);
  }

  protected EntityInterface getEntityByName(final String name) {
    return getContainer().getEntityByName(name);
  }

  protected Job() {
    if (this.getClass().isAnnotationPresent(JobAnnotation.class)) {
      this.time = this.getClass().getAnnotation(JobAnnotation.class).time();
    } else {
      this.time = JobExecutionTime.CHECK;
    }
  }

  public final Job init(
      final Mode mode,
      final EntityInterface entity,
      final Transaction<? extends TransactionContainer> transaction) {
    this.mode = mode;
    this.entity = entity;
    this.transaction = transaction;
    return this;
  }

  /** to be overridden */
  protected abstract void run();

  protected void runJobFromSchedule(
      final EntityInterface entity, final Class<? extends Job> jobclass) {
    getTransaction().getSchedule().runJob(entity, jobclass);
  }

  protected void runJobFromSchedule(ScheduledJob job) {
    getTransaction().getSchedule().runJob(job);
  }

  public EntityInterface getEntity() {
    return this.entity;
  }

  protected final Mode getMode() {
    return this.mode;
  }

  TransactionContainer getContainer() {
    return getTransaction().getContainer();
  }

  protected final boolean isValidSubType(final int child, final int parent) {
    if (!"false".equals(getContainer().getFlags().get("cache"))) {
      final HashMap<String, Boolean> isSubTypeCache = getCache("isSubType");
      final String key = child + "->" + parent;
      final Boolean cached = isSubTypeCache.get(key);
      if (cached == null) {
        final Boolean toCache = isValidSubTypeNoCache(child, parent);
        isSubTypeCache.put(key, toCache);
        return toCache;
      } else {
        return cached;
      }
    } else {
      return isValidSubTypeNoCache(child, parent);
    }
  }

  protected final boolean isValidSubTypeNoCache(final int child, final int parent) {
    return child == parent
        || Database.execute(new IsSubType(child, parent), getTransaction().getAccess()).isSubType();
  }

  protected final EntityInterface retrieveValidSparseEntityByName(final String name)
      throws Message {
    return retrieveValidSparseEntityById(retrieveValidIDByName(name));
  }

  protected final EntityInterface retrieveValidSparseEntityById(final Integer id) throws Message {
    final EntityInterface ret =
        Database.execute(new RetrieveSparseEntity(id), getTransaction().getAccess()).getEntity();
    if (ret.getEntityStatus() == EntityStatus.NONEXISTENT) {
      throw ServerMessages.ENTITY_DOES_NOT_EXIST;
    }
    return ret;
  }

  protected final EntityInterface retrieveValidEntity(Integer id) {
    return Database.execute(new RetrieveFullEntity(id), getTransaction().getAccess())
        .getContainer()
        .get(0);
  }

  protected final Integer retrieveValidIDByName(final String name) {
    return Database.execute(new GetIDByName(name), getTransaction().getAccess()).getId();
  }

  protected EntityInterface retrieveParentsOfValidEntity(final EntityInterface entity) {
    Database.execute(new RetrieveParents(entity), getTransaction().getAccess());
    return entity;
  }

  protected final void checkPermission(final EntityInterface entity, final Permission permission)
      throws Message {
    if (!entity.getEntityACL().isPermitted(SecurityUtils.getSubject(), permission)) {
      throw ServerMessages.AUTHORIZATION_ERROR;
    }
  }

  @Override
  public boolean notifyObserver(final String e, final Observable o) {
    if (getEntity().getEntityStatus() != EntityStatus.UNQUALIFIED) {
      getTransaction().getSchedule().runJob(this);
    }
    return true;
  }

  static HashMap<String, Class<? extends Job>> allClasses = null;
  private static List<Class<? extends Job>> loadAlways;

  public static Job getJob(
      final String job,
      final Mode mode,
      final EntityInterface entity,
      final Transaction<? extends TransactionContainer> transaction) {
    scanJobClasspath();

    final Class<? extends Job> jobClass = allClasses.get(job.toLowerCase());
    return getJob(jobClass, mode, entity, transaction);
  }

  private static void scanJobClasspath() {

    if (allClasses == null || loadAlways == null) {
      allClasses = new HashMap<>();
      loadAlways = new ArrayList<>();
      Reflections jobPackage = new Reflections("caosdb.server.jobs.core");
      Set<Class<? extends Job>> allClassesSet = jobPackage.getSubTypesOf(Job.class);
      allClassesSet.addAll(jobPackage.getSubTypesOf(FlagJob.class));
      for (final Class<? extends Job> c : allClassesSet) {
        allClasses.put(c.getSimpleName().toLowerCase(), c);
        if (c.isAnnotationPresent(JobAnnotation.class)) {
          final String flagName = c.getAnnotation(JobAnnotation.class).flag();
          if (flagName.length() > 0) {
            allClasses.put(flagName.toLowerCase(), c);
          }
          if (c.getAnnotation(JobAnnotation.class).loadAlways()) {
            loadAlways.add(c);
          }
        }
      }
      // TODO merge these two parts of this function. Its the same!
      jobPackage = new Reflections("caosdb.server.jobs.extension");
      allClassesSet = jobPackage.getSubTypesOf(Job.class);
      for (final Class<? extends Job> c : allClassesSet) {
        allClasses.put(c.getSimpleName().toLowerCase(), c);
        if (c.isAnnotationPresent(JobAnnotation.class)) {
          final String flagName = c.getAnnotation(JobAnnotation.class).flag();
          if (flagName.length() > 0) {
            allClasses.put(flagName.toLowerCase(), c);
          }
          if (c.getAnnotation(JobAnnotation.class).loadAlways()) {
            loadAlways.add(c);
          }
        }
      }
    }
  }

  protected List<Job> loadDataTypeSpecificJobs() {
    return Job.loadDataTypeSpecificJobs(getEntity(), getTransaction());
  }

  public static List<Job> loadDataTypeSpecificJobs(
      final EntityInterface entity, final Transaction<? extends TransactionContainer> transaction) {

    return loadDataTypeSpecificJobs(entity.getDatatype(), entity, transaction);
  }

  private static List<Job> loadDataTypeSpecificJobs(
      final AbstractDatatype dt,
      final EntityInterface entity,
      final Transaction<? extends TransactionContainer> transaction) {
    if (dt == null) {
      return new ArrayList<Job>();
    }
    if (dt instanceof ReferenceDatatype2) {
      final RuleLoader t = new RuleLoader(0, 17, entity, transaction);
      return Database.execute(t, transaction.getAccess()).getJobs();
    } else if (dt instanceof AbstractCollectionDatatype) {
      final AbstractDatatype datatype = ((AbstractCollectionDatatype) dt).getDatatype();
      return loadDataTypeSpecificJobs(datatype, entity, transaction);
    } else if (dt.getId() != null) {
      final RuleLoader t = new RuleLoader(0, dt.getId(), entity, transaction);
      return Database.execute(t, transaction.getAccess()).getJobs();
    } else {
      return null;
    }
  }

  public static List<Job> loadStandardJobs(
      final EntityInterface entity, final Transaction<? extends TransactionContainer> transaction) {

    final ArrayList<Job> jobs = new ArrayList<>();
    // load permanent jobs
    for (Class<? extends Job> j : loadAlways) {
      if (EntityJob.class.isAssignableFrom(j)
          && j.getAnnotation(JobAnnotation.class).transaction().isInstance(transaction)) {
        jobs.add(getJob(j, Mode.MUST, entity, transaction));
      }
    }

    // load general rules
    {
      final RuleLoader t = new RuleLoader(0, 0, entity, transaction);
      jobs.addAll(Database.execute(t, transaction.getAccess()).getJobs());
    }

    // load Role specific rules
    if (entity.hasRole()) {
      final RuleLoader t = new RuleLoader(0, entity.getRole().getId(), entity, transaction);
      jobs.addAll(Database.execute(t, transaction.getAccess()).getJobs());
    }

    // load data type specific rules
    jobs.addAll(loadDataTypeSpecificJobs(entity, transaction));

    return jobs;
  }

  private static Job getJob(
      Class<? extends Job> jobClass,
      Mode mode,
      EntityInterface entity,
      Transaction<? extends TransactionContainer> transaction) {
    Job ret;
    try {

      if (jobClass != null) {
        ret = jobClass.newInstance();
        ret.init(mode, entity, transaction);
        return ret;
      }
      return null;
    } catch (final InstantiationException | IllegalAccessException e) {
      throw new TransactionException(e);
    }
  }

  public static List<Job> loadJobs(
      final EntityInterface entity, final Transaction<? extends TransactionContainer> transaction) {
    final LinkedList<Job> jobs = new LinkedList<>();

    // general rules, role rules, data type rules
    jobs.addAll(loadStandardJobs(entity, transaction));

    // load flag jobs
    if (!entity.getFlags().isEmpty()) {
      for (final String key : entity.getFlags().keySet()) {
        final Job j = getJob(key, Mode.MUST, entity, transaction);
        if (j != null) {
          if (j instanceof FlagJob) {
            ((FlagJob) j).setValue(entity.getFlag(key));
          }
          jobs.add(j);
        }
      }
    }

    // load parent flag jobs
    if (entity.hasParents()) {
      for (final EntityInterface p : entity.getParents()) {
        if (!p.getFlags().isEmpty()) {
          for (final String key : p.getFlags().keySet()) {
            final Job j = getJob(key, Mode.MUST, entity, transaction);
            if (j != null) {
              if (j instanceof FlagJob) {
                ((FlagJob) j).setValue(p.getFlag(key));
              }
              jobs.add(j);
            }
          }
        }
      }
    }

    // load property flag jobs
    // FIXME unnecessary since properties do run the flag stuff again (see
    // next for loop)?
    if (entity.hasProperties()) {
      for (final EntityInterface p : entity.getProperties()) {
        if (!p.getFlags().isEmpty()) {
          for (final String key : p.getFlags().keySet()) {
            final Job j = getJob(key, Mode.MUST, entity, transaction);
            if (j != null) {
              if (j instanceof FlagJob) {
                ((FlagJob) j).setValue(p.getFlag(key));
              }
              jobs.add(j);
            }
          }
        }
      }
    }

    // load jobs for the properties
    if (entity.hasProperties()) {
      for (final EntityInterface p : entity.getProperties()) {
        jobs.addAll(loadJobs(p, transaction));
      }
    }

    return jobs;
  }

  public Transaction<? extends TransactionContainer> getTransaction() {
    return this.transaction;
  }

  @Override
  public String toString() {
    return "JOB["
        + this.getClass().getSimpleName()
        + "-"
        + (getEntity() != null ? getEntity().toString() : "NOENTITY")
        + " #"
        + getMode().toString()
        + "]";
  }

  public void finish() {
    super.removeAllObservers();
  }

  public void print() {
    System.out.println(toString());
  }

  private final JobExecutionTime time;

  public JobExecutionTime getExecutionTime() {
    return this.time;
  }

  public static List<Job> loadPermanentContainerJobs(Transaction<?> transaction) {
    final ArrayList<Job> jobs = new ArrayList<>();
    // load permanent jobs
    for (Class<? extends Job> j : loadAlways) {
      if (ContainerJob.class.isAssignableFrom(j)
          && j.getAnnotation(JobAnnotation.class).transaction().isInstance(transaction)) {
        jobs.add(getJob(j, Mode.MUST, null, transaction));
      }
    }
    return jobs;
  }
}
