/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs;

import caosdb.server.transaction.TransactionInterface;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface JobAnnotation {
  JobExecutionTime time() default JobExecutionTime.CHECK;

  String flag() default "";

  Class<? extends TransactionInterface> transaction() default TransactionInterface.class;

  String description() default "N/A";

  String[] values() default {};

  String defaultValue() default "";

  /**
   * Whether this Job is to be executed if the value equals the default value. This will only be
   * effective if the default value is defined.
   *
   * @return true per default.
   */
  boolean loadOnDefault() default true;

  /**
   * Load for every transaction that is a subclass of 'transaction'.
   *
   * @return false per default.
   */
  boolean loadAlways() default false;
}
