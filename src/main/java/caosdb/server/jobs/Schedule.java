/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.jobs;

import caosdb.server.entity.EntityInterface;
import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

class ScheduledJob {

  long runtime = 0;
  final Job job;
  private long startTime = -1;

  public ScheduledJob(final Job j) {
    this.job = j;
  }

  public void run() {
    if (!hasStarted()) {
      start();
      this.job.run();
      finish();

      this.job.notifyObservers(null);
    }
  }

  private void start() {
    this.startTime = System.currentTimeMillis();
  }

  private void finish() {
    this.runtime += System.currentTimeMillis() - this.startTime;
    this.job
        .getContainer()
        .getTransactionBenchmark()
        .addBenchmark(this.job.getClass().getSimpleName(), this.runtime);
  }

  void pause() {
    this.runtime += System.currentTimeMillis() - this.startTime;
  }

  void unpause() {
    start();
  }

  private boolean hasStarted() {
    return this.startTime != -1;
  }

  public JobExecutionTime getExecutionTime() {
    return this.job.getExecutionTime();
  }

  public boolean skip() {
    return this.job.getTarget().skipJob();
  }
}

public class Schedule {

  private final CopyOnWriteArrayList<ScheduledJob> jobs = new CopyOnWriteArrayList<ScheduledJob>();
  private ScheduledJob running = null;

  public void addAll(final Collection<Job> jobs) {
    for (final Job j : jobs) {
      add(j);
    }
  }

  public ScheduledJob add(final Job j) {
    ScheduledJob ret = new ScheduledJob(j);
    this.jobs.add(ret);
    return ret;
  }

  public void runJobs(final JobExecutionTime time) {
    for (final ScheduledJob scheduledJob : this.jobs) {
      if (scheduledJob.getExecutionTime().ordinal() == time.ordinal()
          || (time.ordinal() <= JobExecutionTime.POST_CHECK.ordinal()
              && scheduledJob.getExecutionTime().ordinal() < time.ordinal())) {
        runJob(scheduledJob);
      }
    }
  }

  protected void runJob(final ScheduledJob scheduledJob) {
    if (!this.jobs.contains(scheduledJob)) {
      throw new RuntimeException("Job was not in schedule.");
    }
    if (scheduledJob.skip()) {
      return;
    }
    final ScheduledJob parent = this.running;
    if (parent != null) {
      parent.pause();
    }

    this.running = scheduledJob;
    scheduledJob.run();

    if (parent != null) {
      this.running = parent;
      parent.unpause();
    }
  }

  public void runJob(final Job j) {
    for (final ScheduledJob scheduledJob : this.jobs) {
      if (scheduledJob.job == j) {
        scheduledJob.run();
        return;
      }
    }
    throw new RuntimeException("Job was not in schedule.");
  }

  public void runJob(final EntityInterface entity, final Class<? extends Job> jobclass) {
    for (final ScheduledJob scheduledJob : this.jobs) {
      if (jobclass.isInstance(scheduledJob.job)) {
        if (scheduledJob.job.getEntity() == entity) {
          runJob(scheduledJob);
        }
      }
    }
  }
}
