/*
 * ** header v3.0 This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics, Max-Planck-Institute for Dynamics and
 * Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server;

import caosdb.server.accessControl.AnonymousRealm;
import caosdb.server.accessControl.AuthenticationUtils;
import caosdb.server.accessControl.CaosDBAuthorizingRealm;
import caosdb.server.accessControl.CaosDBDefaultRealm;
import caosdb.server.accessControl.OneTimeTokenRealm;
import caosdb.server.accessControl.Principal;
import caosdb.server.accessControl.SessionToken;
import caosdb.server.accessControl.SessionTokenRealm;
import caosdb.server.database.Database;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.transaction.RetrieveDatatypes;
import caosdb.server.database.misc.TransactionBenchmark;
import caosdb.server.datatype.AbstractDatatype;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.Role;
import caosdb.server.entity.container.Container;
import caosdb.server.logging.RequestErrorLogMessage;
import caosdb.server.resource.AuthenticationResource;
import caosdb.server.resource.DefaultResource;
import caosdb.server.resource.EntityOwnerResource;
import caosdb.server.resource.EntityPermissionsResource;
import caosdb.server.resource.FileSystemResource;
import caosdb.server.resource.InfoResource;
import caosdb.server.resource.LogoutResource;
import caosdb.server.resource.PermissionRulesResource;
import caosdb.server.resource.RolesResource;
import caosdb.server.resource.ScriptingResource;
import caosdb.server.resource.ServerLogsResource;
import caosdb.server.resource.ServerPropertiesResource;
import caosdb.server.resource.TestCaseFileSystemResource;
import caosdb.server.resource.TestCaseResource;
import caosdb.server.resource.ThumbnailsResource;
import caosdb.server.resource.UserResource;
import caosdb.server.resource.UserRolesResource;
import caosdb.server.resource.Webinterface;
import caosdb.server.resource.transaction.EntityResource;
import caosdb.server.terminal.CaosDBTerminal;
import caosdb.server.terminal.StatsPanel;
import caosdb.server.terminal.SystemErrPanel;
import caosdb.server.utils.ChecksumUpdater;
import caosdb.server.utils.FileUtils;
import caosdb.server.utils.Initialization;
import caosdb.server.utils.NullPrintStream;
import caosdb.server.utils.Utils;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;
import java.util.TimeZone;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.config.Ini;
import org.apache.shiro.config.Ini.Section;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.apache.shiro.util.ThreadContext;
import org.restlet.Application;
import org.restlet.Component;
import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Restlet;
import org.restlet.Server;
import org.restlet.data.CookieSetting;
import org.restlet.data.Parameter;
import org.restlet.data.Protocol;
import org.restlet.data.Reference;
import org.restlet.data.ServerInfo;
import org.restlet.data.Status;
import org.restlet.engine.Engine;
import org.restlet.routing.Route;
import org.restlet.routing.Router;
import org.restlet.routing.Template;
import org.restlet.routing.TemplateRoute;
import org.restlet.routing.Variable;
import org.restlet.util.Series;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CaosDBServer extends Application {

  private static Logger logger = LoggerFactory.getLogger(CaosDBServer.class);
  private static boolean DEBUG_MODE = false;
  private static boolean START_GUI = true;
  private static Properties SERVER_PROPERTIES = null;
  private static Component component = null;
  private static ArrayList<Runnable> postShutdownHooks = new ArrayList<Runnable>();
  private static ArrayList<Runnable> preShutdownHooks = new ArrayList<Runnable>();
  private static boolean START_BACKEND = true;
  private static boolean INSECURE = false;

  public static String getServerProperty(final String key) {
    return getServerProperties().getProperty(key);
  }

  public static void initServerProperties() throws IOException {
    SERVER_PROPERTIES = ServerProperties.initServerProperties();
  }

  /**
   * Precedence order:
   *
   * <ol>
   *   <li>ServerProperty "TIMEZONE"
   *   <li>JVM property "user.timezone"
   *   <li>Environment variable "TZ"
   *   <li>Output of posix' "date +%Z"
   * </ol>
   *
   * @throws InterruptedException
   * @throws IOException
   */
  public static void initTimeZone() throws InterruptedException, IOException {
    String serverProperty = getServerProperty(ServerProperties.KEY_TIMEZONE);
    if (serverProperty != null && !serverProperty.isEmpty()) {
      logger.info(
          "SET TIMEZONE = "
              + serverProperty
              + " from ServerProperty `"
              + ServerProperties.KEY_TIMEZONE
              + "`.");
      TimeZone.setDefault(TimeZone.getTimeZone(ZoneId.of(serverProperty)));
      logger.info("TIMEZONE = " + TimeZone.getDefault());
      return;
    }

    String prop = System.getProperty("user.timezone");
    if (prop != null && !prop.isEmpty()) {
      logger.info("SET TIMEZONE = " + prop + " from JVM property `user.timezone`.");
      TimeZone.setDefault(TimeZone.getTimeZone(ZoneId.of(prop)));
      logger.info("TIMEZONE = " + TimeZone.getDefault());
      return;
    }

    String envVar = System.getenv("TZ");
    if (envVar != null && !envVar.isEmpty()) {
      logger.info("SET TIMEZONE = " + envVar + " from evironment variable `TZ`.");
      TimeZone.setDefault(TimeZone.getTimeZone(ZoneId.of(envVar)));
      logger.info("TIMEZONE = " + TimeZone.getDefault());
      return;
    }

    String fromDate = getTimeZoneFromDate();
    if (fromDate != null && fromDate.isEmpty()) {
      logger.info("SET TIMEZONE = " + fromDate + " from `date +%Z`.");
      TimeZone.setDefault(TimeZone.getTimeZone(ZoneId.of(fromDate)));
      logger.info("TIMEZONE = " + TimeZone.getDefault());
      return;
    }

    logger.warn("COULD NOT SET TIMEZONE. DEFAULTS TO " + TimeZone.getDefault());
  }

  public static String getTimeZoneFromDate() throws InterruptedException, IOException {
    final StringBuffer outputStringBuffer = new StringBuffer();
    final Process cmd = Runtime.getRuntime().exec(new String[] {"date", "+%z"});
    final int status = cmd.waitFor();

    if (status != 0) {
      logger.warn(
          "Could not determine time zone from `date +%z`. The command returned with exit code "
              + cmd.exitValue());
      return null;
    }
    final Reader r = new InputStreamReader(cmd.getInputStream());
    final BufferedReader buf = new BufferedReader(r);
    String line;
    while ((line = buf.readLine()) != null) {
      outputStringBuffer.append(line);
    }

    if (outputStringBuffer.length() > 0) {
      return outputStringBuffer.toString().trim();
    } else {
      throw new RuntimeException("Output of `date +%z` command was empty.");
    }
  }

  private static void init(final String[] args) {
    // Important change:
    // Make silent the default option
    START_GUI = false;
    for (final String s : args) {
      if (s.equals("silent")) {
        START_GUI = false;
      } else if (s.equals("gui")) {
        START_GUI = true;
      } else if (s.equals("nobackend")) {
        START_BACKEND = false;
      } else if (s.equals("insecure")) {
        INSECURE = true;
      }
    }
    DEBUG_MODE = Boolean.getBoolean("caosdb.debug");
  }

  /**
   * This main method starts up a web application that will listen on a port defined in the config
   * file.
   *
   * @param args One option temporarily (for testing) available: silent: If present: disable
   *     System.out-stream (stream to a NullPrintStream). This makes the response of the database
   *     amazingly faster.
   * @throws IOException
   * @throws FileNotFoundException
   * @throws SecurityException
   * @throws Exception If problems occur.
   */
  public static void main(final String[] args)
      throws SecurityException, FileNotFoundException, IOException {
    try {
      init(args);
      initServerProperties();
      initTimeZone();
    } catch (IOException | InterruptedException e1) {
      logger.error("Could not configure the server.", e1);
      System.exit(1);
    }

    INSECURE = INSECURE && DEBUG_MODE; // only allow insecure in debug mode
    START_BACKEND = START_BACKEND || !DEBUG_MODE; // always start backend if
    // not in debug mode

    // init Shiro (user authentication/authorization and session management)
    final Ini config = new Ini();
    final Section mainSec = config.addSection("main");
    mainSec.put("CaosDB", CaosDBDefaultRealm.class.getCanonicalName());
    mainSec.put("SessionTokenValidator", SessionTokenRealm.class.getCanonicalName());
    mainSec.put("OneTimeTokenValidator", OneTimeTokenRealm.class.getCanonicalName());
    mainSec.put("CaosDBAuthorizingRealm", CaosDBAuthorizingRealm.class.getCanonicalName());
    mainSec.put("AnonymousRealm", AnonymousRealm.class.getCanonicalName());
    mainSec.put(
        "securityManager.realms",
        "$CaosDB, $SessionTokenValidator, $OneTimeTokenValidator, $CaosDBAuthorizingRealm, $AnonymousRealm");

    // disable shiro's default session management. We have quasi-stateless
    // sessions
    // using our SessionToken class.
    mainSec.put(
        "securityManager.subjectDAO.sessionStorageEvaluator.sessionStorageEnabled", "false");

    final Factory<SecurityManager> factory = new IniSecurityManagerFactory(config);
    final SecurityManager securityManager = factory.getInstance();
    SecurityUtils.setSecurityManager(securityManager);

    final Initialization init = Initialization.setUp();
    try {
      // init backend
      if (START_BACKEND) {
        // init benchmark
        TransactionBenchmark.getInstance();

        // Role
        Role.init(init.getAccess());

        // Data types
        initDatatypes(init.getAccess());

        // check for chown script
        FileUtils.testChownScript();

        // ChecksumUpdater
        ChecksumUpdater.start();
      } else {
        logger.info("NO BACKEND");
      }

      // GUI
      if (START_GUI) {
        final CaosDBTerminal caosDBTerminal = new CaosDBTerminal();
        caosDBTerminal.setName("CaosDBTerminal");
        caosDBTerminal.start();

        addPreShutdownHook(
            new Runnable() {

              @Override
              public void run() {
                caosDBTerminal.shutDown();
                SystemErrPanel.close();
              }
            });
        // wait until the terminal is initialized.
        Thread.sleep(1000);

        // add Benchmark
        StatsPanel.addStat("TransactionBenchmark", TransactionBenchmark.getInstance());
      } else {
        logger.info("NO GUI");
        System.setOut(new NullPrintStream());
      }

      // Web server properties
      final int port_https =
          Integer.parseInt(getServerProperty(ServerProperties.KEY_SERVER_PORT_HTTPS));
      final int port_http =
          Integer.parseInt(getServerProperty(ServerProperties.KEY_SERVER_PORT_HTTP));
      int port_redirect_https;
      try {
        port_redirect_https =
            Integer.parseInt(getServerProperty(ServerProperties.KEY_REDIRECT_HTTP_TO_HTTPS_PORT));
      } catch (NumberFormatException e) {
        port_redirect_https = port_https;
      }
      final int initialConnections =
          Integer.parseInt(getServerProperty(ServerProperties.KEY_INITIAL_CONNECTIONS));
      final int maxTotalConnections =
          Integer.parseInt(getServerProperty(ServerProperties.KEY_MAX_CONNECTIONS));

      init.release();

      if (INSECURE) {
        runHTTPServer(port_http, initialConnections, maxTotalConnections);
      } else {
        runHTTPSServer(
            port_https, port_http, port_redirect_https, initialConnections, maxTotalConnections);
      }
      initShutDownHook();
    } catch (final Exception e) {
      logger.error("Server start failed.", e);
      init.release();
      System.exit(1);
    }
  }

  private static void initDatatypes(final Access access) throws Exception {
    final RetrieveDatatypes t = new RetrieveDatatypes();
    final Container<? extends EntityInterface> dts = Database.execute(t, access).getDatatypes();
    AbstractDatatype.initializeDatatypes(dts);
  }

  /**
   * Start an http server. This is insecure!
   *
   * @throws Exception
   */
  private static void runHTTPServer(
      final int port_http, final int initialConnections, final int maxTotalConnections)
      throws Exception {
    Engine.getInstance()
        .getRegisteredServers()
        .add(new org.restlet.ext.jetty.HttpServerHelper(null));

    // Create a component.
    component = new CaosDBComponent();

    final Server httpServer =
        new Server(
            (Context) null,
            Arrays.asList(Protocol.HTTP),
            null,
            port_http,
            (Restlet) null,
            "org.restlet.ext.jetty.HttpServerHelper");
    component.getServers().add(httpServer);

    // set initial and maximal connections
    final Series<Parameter> parameters = httpServer.getContext().getParameters();
    parameters.add("initialConnections", Integer.toString(initialConnections));
    parameters.add("maxTotalConnections", Integer.toString(maxTotalConnections));

    // Create an application (this class).
    final Application application = new CaosDBServer();
    application
        .getStatusService()
        .setContactEmail(getServerProperty(ServerProperties.KEY_ADMIN_EMAIL));
    application
        .getStatusService()
        .setHomeRef(new Reference(getServerProperty(ServerProperties.KEY_CONTEXT_ROOT) + "/"));

    // Attach the application to the component with a defined contextRoot.
    component
        .getDefaultHost()
        .attach(getServerProperty(ServerProperties.KEY_CONTEXT_ROOT), application);

    component.start();

    if (port_http == 0) {
      System.err.println("ACTUAL HTTP PORT:" + httpServer.getActualPort());
    }
  }

  /**
   * Starts a https server running on the specified `port_https`, listening also for http
   * connections on `port_http` and redirect any http connections to `port_redirect_https`.
   *
   * @author Timm Fitschen
   * @param port_https Listen on this port for https connections.
   * @param port_http Listen on this port for http connections and send http-to-https redirect with
   *     different port.
   * @parem port_redirect_https Redirect any http connections to this port.
   * @throws Exception if problems occur starting up this server.
   */
  private static void runHTTPSServer(
      final int port_https,
      final int port_http,
      final int port_redirect_https,
      final int initialConnections,
      final int maxTotalConnections)
      throws Exception {

    Engine.getInstance().getRegisteredServers().add(new CaosDBServerConnectorHelper(null));

    // Create a component.
    component = new CaosDBComponent();

    final Server httpsServer =
        new Server(
            (Context) null,
            Arrays.asList(Protocol.HTTPS),
            null,
            port_https,
            (Restlet) null,
            "caosdb.server.CaosDBServerConnectorHelper");
    component.getServers().add(httpsServer);

    // redirector http to https
    if (port_http != 0) {
      logger.info("Redirecting to " + port_redirect_https);
      component
          .getServers()
          .add(Protocol.HTTP, port_http)
          .setNext(new HttpToHttpsRedirector(port_redirect_https));
    }

    // set initial and maximal connections
    final Series<Parameter> parameters = httpsServer.getContext().getParameters();
    parameters.add("initialConnections", Integer.toString(initialConnections));
    parameters.add("maxTotalConnections", Integer.toString(maxTotalConnections));
    parameters.add("sslContextFactory", "org.restlet.engine.ssl.DefaultSslContextFactory");
    parameters.add(
        "disabledProtocols", getServerProperty(ServerProperties.KEY_HTTPS_DISABLED_PROTOCOLS));
    parameters.add(
        "enabledProtocols", getServerProperty(ServerProperties.KEY_HTTPS_ENABLED_PROTOCOLS));
    parameters.add(
        "enabledCipherSuites", getServerProperty(ServerProperties.KEY_HTTPS_ENABLED_CIPHER_SUITES));
    parameters.add(
        "disabledCipherSuites",
        getServerProperty(ServerProperties.KEY_HTTPS_DISABLED_CIPHER_SUITES));
    parameters.add(
        "keyStorePath", getServerProperty(ServerProperties.KEY_CERTIFICATES_KEY_STORE_PATH));
    parameters.add(
        "keyStorePassword",
        getServerProperty(ServerProperties.KEY_CERTIFICATES_KEY_STORE_PASSWORD));
    parameters.add(
        "keyPassword", getServerProperty(ServerProperties.KEY_CERTIFICATES_KEY_PASSWORD));
    parameters.add("keyStoreType", "JKS");
    parameters.add("certAlias", "1");

    // Create an application (this class).
    final Application application = new CaosDBServer();
    application.getStatusService().setContactEmail("timm.fitschen@ds.mpg.de");
    application
        .getStatusService()
        .setHomeRef(new Reference(getServerProperty(ServerProperties.KEY_CONTEXT_ROOT) + "/"));

    // Attach the application to the component with a defined contextRoot.
    application.getStatusService().setContactEmail("timm.fitschen@ds.mpg.de");
    application
        .getStatusService()
        .setHomeRef(new Reference(getServerProperty(ServerProperties.KEY_CONTEXT_ROOT) + "/"));

    component
        .getDefaultHost()
        .attach(getServerProperty(ServerProperties.KEY_CONTEXT_ROOT), application);

    component.start();

    if (port_https == 0) {
      System.err.println("ACTUAL HTTPS PORT:" + httpsServer.getActualPort());
    }
  }

  public static final String REQUEST_TIME_LOGGER = "REQUEST_TIME_LOGGER";
  public static final String REQUEST_ERRORS_LOGGER = "REQUEST_ERRORS_LOGGER";

  /**
   * Specify the dispatching restlet that maps URIs to their associated resources for processing.
   *
   * @return A Router restlet that implements dispatching.
   */
  @Override
  public Restlet createInboundRoot() {
    /** Authenticator. Protects the protectedRouter. */
    final CaosAuthenticator authenticator = new CaosAuthenticator(getContext());

    /** ProtectedRouter. Is protected by the authenticator. */
    final Router protectedRouter =
        new Router(getContext()) {
          @Override
          public void handle(final Request request, final Response response) {
            try {
              super.handle(request, response);
            } catch (final NullPointerException e) {
              response.setStatus(Status.CLIENT_ERROR_REQUEST_URI_TOO_LONG);
              response.setEntity(null);
            }
          }
        };

    /**
     * BaseRouter. Routes the call to some special Resources, which are available without any
     * authentication (e.g. login and start pages, scripts for the web interface). Everything else
     * is forwarded to the Authenticator.
     */
    final Router baseRouter =
        new Router(getContext()) {
          @Override
          public void handle(final Request request, final Response response) {
            try {
              super.handle(request, response);

              // after everything, set session cookies
              setSessionCookies(response);

            } finally {
              // remove subject from this thread so that we can reuse the
              // thread.
              ThreadContext.unbindSubject();
            }
          }

          private void setSessionCookies(final Response response) {

            final Subject subject = SecurityUtils.getSubject();
            if (subject.isAuthenticated()
                && subject.getPrincipal() != AuthenticationUtils.ANONYMOUS_USER.getPrincipal()) {
              final SessionToken sessionToken =
                  SessionToken.generate((Principal) subject.getPrincipal(), null);

              // set session token cookie (httpOnly, secure cookie which
              // is used to recognize a user session)
              final CookieSetting sessionTokenCookie =
                  AuthenticationUtils.createSessionTokenCookie(sessionToken);
              if (sessionTokenCookie != null) {
                response.getCookieSettings().add(sessionTokenCookie);
              }

              // set session timeout cookie (secure cookie which may be
              // used
              // by the user interfaces for anything)
              final CookieSetting sessionTimeoutCookie =
                  AuthenticationUtils.createSessionTimeoutCookie(sessionToken);
              if (sessionTimeoutCookie != null) {
                response.getCookieSettings().add(sessionTimeoutCookie);
              }
            }
          }
        };

    // -- Section only for debug mode --
    if (isDebugMode()) {
      baseRouter.attach("/TestCase/", DefaultResource.class);

      final Variable pathVariable =
          baseRouter
              .attach(
                  "/TestCase/FileSystem/{path}",
                  baseRouter.createFinder(TestCaseFileSystemResource.class))
              .getTemplate()
              .getDefaultVariable();
      pathVariable.setRequired(false);
      pathVariable.setType(Variable.TYPE_URI_PATH);
      pathVariable.setDefaultValue("");

      baseRouter
          .attach("/TestCase/Thumbnails/{path}", ThumbnailsResource.class)
          .getTemplate()
          .getDefaultVariable()
          .setType(Variable.TYPE_URI_PATH);

      baseRouter
          .attach("/TestCase/webinterface/{path}", Webinterface.class)
          .getTemplate()
          .getDefaultVariable()
          .setType(Variable.TYPE_URI_PATH);
      baseRouter.attach("/TestCase/Entity", TestCaseResource.class);
      baseRouter.attach("/TestCase/Entity/", TestCaseResource.class);
      baseRouter.attach("/TestCase/Entity/{specifier}", TestCaseResource.class);
    }
    // -- End of debug section --

    // These routes can be used without logging in:
    baseRouter
        .attach("/webinterface/{path}", Webinterface.class)
        .getTemplate()
        .getDefaultVariable()
        .setType(Variable.TYPE_URI_PATH);
    baseRouter
        .attach("/login?username={username}", AuthenticationResource.class)
        .setMatchingQuery(true);
    baseRouter.attach("/login", AuthenticationResource.class);
    baseRouter.attach("", authenticator).setMatchingQuery(false);

    // root every other request to the authenticator
    final TemplateRoute tr = baseRouter.attach(authenticator);
    tr.setTemplate(
        new Template(null) {
          @Override
          public int parse(
              final String formattedString,
              final java.util.Map<String, Object> variables,
              final boolean loggable) {
            if (formattedString.startsWith("/")) {
              return 0;
            }
            return -1;
          };

          @Override
          public int match(final String formattedString) {
            if (formattedString.startsWith("/")) {
              return 0;
            }
            return -1;
          }
        });
    // baseRouter.attachDefault(authenticator);

    final CaosAuthorizer authorizer = new CaosAuthorizer();

    // The protectedRouter handles the request if the authorization
    // succeeds.
    authorizer.setNext(protectedRouter);

    // After authentication comes authorization...
    authenticator.setNext(authorizer);

    protectedRouter.attach("/scripting", ScriptingResource.class);
    protectedRouter.attach("/Entities", EntityResource.class);
    protectedRouter.attach("/Entities/", EntityResource.class);
    protectedRouter.attach("/Entities/{specifier}", EntityResource.class);
    protectedRouter.attach("/Entity", EntityResource.class);
    protectedRouter.attach("/Entity/", EntityResource.class);
    protectedRouter.attach("/Entity/{specifier}", EntityResource.class);
    protectedRouter.attach("/Users", UserResource.class);
    protectedRouter.attach("/Users/", UserResource.class);
    protectedRouter.attach("/Users/{specifier}", UserResource.class);
    protectedRouter.attach("/User", UserResource.class);
    protectedRouter.attach("/User/", UserResource.class);
    protectedRouter.attach("/User/{specifier}", UserResource.class);
    protectedRouter.attach("/User/{realm}/", UserResource.class);
    protectedRouter.attach("/User/{realm}/{specifier}", UserResource.class);
    protectedRouter.attach("/Users/{realm}/", UserResource.class);
    protectedRouter.attach("/Users/{realm}/{specifier}", UserResource.class);
    protectedRouter.attach("/UserRoles/{specifier}", UserRolesResource.class);
    protectedRouter.attach("/EntityPermissions", EntityPermissionsResource.class);
    protectedRouter.attach("/EntityPermissions/", EntityPermissionsResource.class);
    protectedRouter.attach("/EntityPermissions/{specifier}", EntityPermissionsResource.class);
    protectedRouter.attach("/Owner/{specifier}", EntityOwnerResource.class);
    protectedRouter.attach("/FileSystem/", FileSystemResource.class);
    protectedRouter
        .attach("/FileSystem/{path}", FileSystemResource.class)
        .getTemplate()
        .getDefaultVariable()
        .setType(Variable.TYPE_URI_PATH);
    protectedRouter
        .attach("/Thumbnails/{path}", ThumbnailsResource.class)
        .getTemplate()
        .getDefaultVariable()
        .setType(Variable.TYPE_URI_PATH);
    protectedRouter.attach("/Info", InfoResource.class);
    protectedRouter.attach("/Info/", InfoResource.class);
    protectedRouter.attach("/Role", RolesResource.class);
    protectedRouter.attach("/Role/{specifier}", RolesResource.class);
    protectedRouter.attach("/PermissionRules/{specifier}", PermissionRulesResource.class);
    protectedRouter.attach("/PermissionRules/{realm}/{specifier}", PermissionRulesResource.class);
    protectedRouter
        .attach("/ServerLogs/", ServerLogsResource.class)
        .setMatchingMode(Template.MODE_STARTS_WITH);
    protectedRouter
        .attach("/ServerLogs", ServerLogsResource.class)
        .setMatchingMode(Template.MODE_STARTS_WITH);
    protectedRouter.attach("/login?username={username}", AuthenticationResource.class);
    protectedRouter.attach("/logout", LogoutResource.class);
    protectedRouter.attach("/_server_properties", ServerPropertiesResource.class);
    protectedRouter.attachDefault(DefaultResource.class);

    /*
     * Dirty Hack - no clean solution found yet (except for patching the restlet framework). The
     * logging handler causes a NullPointerException when the Template.match method logs a warning.
     * This warning (seemingly) always means that the RequestUri cannot be matched because its to
     * long and causes a StackOverflow. Therefore we want to generate an HTTP 414 error.
     */

    final Handler handler =
        new Handler() {

          @Override
          public void publish(final LogRecord record) {
            if (record.getLevel() == Level.WARNING
                && record
                    .getMessage()
                    .startsWith(
                        "StackOverflowError exception encountered while matching this string")) {
              // cause a NullPointerException
              throw new NullPointerException();
            }
          }

          @Override
          public void flush() {}

          @Override
          public void close() throws SecurityException {}
        };

    routes:
    for (final Route r : protectedRouter.getRoutes()) {
      if (r instanceof TemplateRoute) {
        final TemplateRoute t = (TemplateRoute) r;
        for (final Handler h : t.getTemplate().getLogger().getHandlers()) {
          if (h == handler) {
            continue routes;
          }
        }
        t.getTemplate().getLogger().addHandler(handler);
      }
    }

    return baseRouter;
  }

  private static ServerInfo serverInfo = null;

  public static ServerInfo getServerInfo() {
    if (serverInfo == null) {
      serverInfo = new ServerInfo();
      serverInfo.setAgent("CaosDB Server");
    }
    return serverInfo;
  }

  private static void callPostShutdownHooks() {
    for (final Runnable r : postShutdownHooks) {
      try {
        final Thread t = new Thread(r);
        t.start();
        t.join();
      } catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }

  private static void callPreShutdownHooks() {
    for (final Runnable r : preShutdownHooks) {
      try {
        final Thread t = new Thread(r);
        t.start();
        t.join();
      } catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }

  public static void addPostShutdownHook(final Thread t) {
    postShutdownHooks.add(t);
  }

  public static void addPreShutdownHook(final Runnable runnable) {
    preShutdownHooks.add(runnable);
  }

  private static void initShutDownHook() {
    Runtime.getRuntime()
        .addShutdownHook(
            new Thread("SHUTDOWN_HTTP_SERVER") {
              @Override
              public void run() {
                callPreShutdownHooks();
                try {
                  component.stop();
                  System.err.print("Stopping HTTP server [OK]\n");
                } catch (final Exception e) {
                  System.err.print("Stopping HTTP server [failed]\n");
                  e.printStackTrace();
                } finally {
                  callPostShutdownHooks();
                }
              }
            });
  }

  public static Component getComponent() {
    return component;
  }

  public static boolean isDebugMode() {
    return DEBUG_MODE;
  }

  /**
   * Set a server property to a new value. This might not have an immediate effect if classes did
   * already read an older configuration and stick to that.
   *
   * @param key, the server property.
   * @param value, the new value.
   */
  public static void setProperty(String key, String value) {
    SERVER_PROPERTIES.setProperty(key, value);
  }

  public static Properties getServerProperties() {
    return SERVER_PROPERTIES;
  }
}

class CaosDBComponent extends Component {

  private static Logger request_error_logger =
      LoggerFactory.getLogger(CaosDBServer.REQUEST_ERRORS_LOGGER);
  private static Logger request_time_logger =
      LoggerFactory.getLogger(CaosDBServer.REQUEST_TIME_LOGGER);

  public CaosDBComponent() {
    super();
    setName(CaosDBServer.getServerProperty(ServerProperties.KEY_SERVER_NAME));
    setOwner(CaosDBServer.getServerProperty(ServerProperties.KEY_SERVER_OWNER));
  }

  /**
   * This function is doing the actual work as soon as a request arrives. In this case this consists
   * in: - Logging the request, the response and the current time - The response gets updated with
   * server info - The request gets updated with an SRID (server request ID)
   *
   * <p>Apart from that super.handle will be called.
   *
   * <p>The main purpose of the SRID is to allow efficient debugging by checking the error log for
   * the request causing the error.
   */
  @Override
  public void handle(final Request request, final Response response) {
    long t1 = System.currentTimeMillis();
    // The server request ID is just a long random number
    request.getAttributes().put("SRID", Utils.getUID());
    response.setServerInfo(CaosDBServer.getServerInfo());
    super.handle(request, response);
    log(request, response, t1);
  }

  private void log(final Request request, final Response response, long t1) {
    if (response.getStatus() == Status.SERVER_ERROR_INTERNAL) {
      final Object object = request.getAttributes().get("THROWN");
      Throwable t = null;
      if (object instanceof Throwable) {
        t = (Throwable) object;
      }

      request_error_logger.error(
          "SRID: {}\n{}",
          request.getAttributes().get("SRID").toString(),
          new RequestErrorLogMessage(request, response),
          t);
    }
    request_time_logger.trace(
        "SRID: {} - Dur: {}",
        request.getAttributes().get("SRID").toString(),
        Long.toString(System.currentTimeMillis() - t1));
  }
}
