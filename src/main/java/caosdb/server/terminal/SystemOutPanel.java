/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.terminal;

import com.googlecode.lanterna.gui.component.Label;
import java.io.IOException;
import java.io.PrintStream;

public final class SystemOutPanel extends OutputStreamPanel {

  private static final SystemOutPanel instance = new SystemOutPanel();

  public static final SystemOutPanel getInstance() {
    return instance;
  }

  private SystemOutPanel() {
    super();
    try {
      init();
      System.setOut(new PrintStream(getPanelOutputStream()));
    } catch (final IOException e) {
      addComponent(new Label("Sorry, could not initialize this SystemOutPanel"));
    }
  }
}
