/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.terminal;

import caosdb.server.utils.Observer;
import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.Border;
import com.googlecode.lanterna.gui.Window;
import com.googlecode.lanterna.gui.component.Button;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.dialog.DialogButtons;
import com.googlecode.lanterna.gui.dialog.DialogResult;
import com.googlecode.lanterna.gui.dialog.MessageBox;
import com.googlecode.lanterna.gui.layout.LinearLayout;
import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.input.Key.Kind;

public class MainWindow extends Window {

  // A panel at the top of the window which contains buttons to switch
  // between several data panels which are to be shown in the dataPanel.
  final Panel dataSelectorPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORISONTAL);

  // A panel right below the dataSelectorPanel. Here the interesting
  // data is to be show. The contained panels can be made visible or
  // invisible via the buttons in the dataSelectorPanel.
  final Panel dataPanel = new Panel(new Border.Bevel(true), Panel.Orientation.HORISONTAL);

  public final synchronized void setVisibleDataPanel(final Panel visibleDataPanel) {
    this.dataPanel.removeAllComponents();
    this.dataPanel.addComponent(visibleDataPanel);
  }

  public MainWindow() {
    super("Caos DB");
    addComponent(this.dataSelectorPanel, LinearLayout.MAXIMIZES_HORIZONTALLY);
    addComponent(
        this.dataPanel, LinearLayout.MAXIMIZES_HORIZONTALLY, LinearLayout.MAXIMIZES_VERTICALLY);

    // initializing stream output panels. These pipe the output of
    // System.out and System.err to a TextArea.
    final SystemOutPanel systemOutPanel = SystemOutPanel.getInstance();
    final Thread systemOutThread = new Thread(systemOutPanel);
    systemOutThread.setName("systemOutPanel");

    final SystemErrPanel systemErrPanel = SystemErrPanel.getInstance();
    final Thread systemErrThread = new Thread(systemErrPanel);
    systemErrThread.setName("systemErrPanel");

    systemOutThread.start();
    systemErrThread.start();

    // initializing DatabaseAccessPanel that shows which thread is accessing
    // the database and how many threads are waiting for access.
    final DatabaseAccessPanel databaseAccessPanel = DatabaseAccessPanel.getInstance();

    // init StatsPanel
    StatsPanel.getPanel();

    // init entities stats
    EntitiesPanel.getInstance();

    // add all initialized panels to the main window
    addDataPanel("System.out", systemOutPanel);
    addDataPanel("System.err", systemErrPanel);
    addDataPanel("DB Access", databaseAccessPanel);
    addDataPanel("Misc", StatsPanel.getPanel());

    // add a welcome panel
    this.dataPanel.addComponent(
        new Label("Welcome. This is CaosDB - An Open Scientific DataBase."));

    // add shutdown button
    this.dataSelectorPanel.addComponent(new ShutdownButton());

    // set focus on the dataSelectorPanel
    setFocus(this.dataSelectorPanel.nextFocus(null));
  }

  /**
   * Add a panel to the dataPanel. A button is created within the dataSelectorPanel which shows the
   * name of the panel
   *
   * @param name
   * @param dataPanel
   */
  private void addDataPanel(final String name, final Panel dataPanel) {
    this.dataSelectorPanel.addComponent(new DataPanelSelectorButton(name, dataPanel));
  }

  /**
   * A Button that causes the dataPanel to show a certain panel.
   *
   * @author tf
   */
  class DataPanelSelectorButton extends Button {
    public DataPanelSelectorButton(final String name, final Panel panel) {
      super(
          name,
          new Action() {

            @Override
            public void doAction() {
              // show panel in dataPanel
              setVisibleDataPanel(panel);

              // update panel if necessary
              if (panel instanceof Observer) {
                ((Observer) panel).notifyObserver(null, null);
              }

              // move focus to panel
              setFocus(panel.nextFocus(null));
            }
          });
    }
  }

  @Override
  public void onKeyPressed(final Key key) {
    if (key.getKind() == Kind.Escape) {
      setFocus(this.dataSelectorPanel.nextFocus(null));
    } else {
      super.onKeyPressed(key);
    }
  }

  /**
   * A button that causes the server to shut down when pressed.
   *
   * @author tf
   */
  class ShutdownButton extends Button {
    public ShutdownButton() {
      super(
          "Shutdown Server",
          new Action() {
            @Override
            public void doAction() {
              final DialogResult result =
                  MessageBox.showMessageBox(
                      getOwner(),
                      "Server shutdown",
                      "Select [OK] to shut down the server now.",
                      DialogButtons.OK_CANCEL);
              if (result == DialogResult.OK) {
                System.exit(0);
              }
            }
          });
    }
  }
}
