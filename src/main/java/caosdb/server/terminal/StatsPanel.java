/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.terminal;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.Border;
import com.googlecode.lanterna.gui.component.Button;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.layout.LinearLayout;
import java.util.HashMap;

public class StatsPanel extends Panel {

  private static StatsPanel instance = null;

  HashMap<String, StatTable> stats = new HashMap<String, StatTable>();
  Panel dataPanel = new Panel("Data", new Border.Bevel(true), Panel.Orientation.VERTICAL);
  Panel selectorPanel = new Panel("Selector", new Border.Bevel(true), Panel.Orientation.VERTICAL);
  StatComponent current = null;

  private StatsPanel() {
    super(Panel.Orientation.HORISONTAL);
    addComponent(this.selectorPanel, LinearLayout.MAXIMIZES_VERTICALLY);
    addComponent(
        this.dataPanel, LinearLayout.MAXIMIZES_VERTICALLY, LinearLayout.MAXIMIZES_HORIZONTALLY);
  }

  public static void addStat(final String name, final Object obj) {
    if (instance != null) {
      instance.addStatComponent(name, new StatLabel(obj));
    }
  }

  public static void addStat(final String name, final StatComponent statComponent) {
    if (instance != null) {
      instance.addStatComponent(name, statComponent);
    }
  }

  private void addStatComponent(final String name, final StatComponent statComponent) {
    final StatTable c = this.stats.get(name);
    if (c != null) {
      c.addComponent(statComponent);
    } else {
      final StatTable table = new StatTable();
      table.addComponent(statComponent);
      this.selectorPanel.addComponent(new SelectorButton(name, table));
      this.stats.put(name, table);
    }
  }

  private void setVisibleDataPanel(final StatComponent panel) {
    this.dataPanel.removeAllComponents();

    if (this.current != null) {
      this.current.stop();
    }
    this.current = panel;
    this.current.start();
    this.current.update();

    this.dataPanel.addComponent(this.current);
  }

  class SelectorButton extends Button {
    public SelectorButton(final String name, final StatComponent panel) {
      super(
          name,
          new Action() {

            @Override
            public void doAction() {
              // show panel in dataPanel
              setVisibleDataPanel(panel);
            }
          });
    }
  }

  public static Panel getPanel() {
    if (instance == null) {
      instance = new StatsPanel();
    }
    return instance;
  }
}
