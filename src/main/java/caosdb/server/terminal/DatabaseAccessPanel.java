/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.terminal;

import caosdb.server.database.DatabaseMonitor;
import caosdb.server.utils.Observable;
import caosdb.server.utils.Observer;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.component.Table;

public final class DatabaseAccessPanel extends Panel implements Observer {

  private static final DatabaseAccessPanel instance = new DatabaseAccessPanel();
  private final Table table = new Table(2);
  private final Label label1 = new Label();
  private final Label label2 = new Label();
  private final Label label3 = new Label();
  private final Label label4 = new Label();
  private final Label label5 = new Label();

  public static final DatabaseAccessPanel getInstance() {
    return instance;
  }

  private DatabaseAccessPanel() {
    super();
    DatabaseMonitor.acceptWeakAccessObserver(this);
    DatabaseMonitor.acceptStrongAccessObserver(this);

    this.table.addRow(new Label("Weak access, acquired:"), this.label1);
    this.table.addRow(new Label("Weak access, waiting:"), this.label2);
    this.table.addRow(new Label("Strong access, allocated:"), this.label3);
    this.table.addRow(new Label("Strong access, acquired:"), this.label4);
    this.table.addRow(new Label("Strong access, waiting:"), this.label5);
    addComponent(this.table);

    notifyObserver(null, null);
  }

  @Override
  public boolean notifyObserver(final String e, final Observable o) {
    try {
      synchronized (this) {
        if (getParent() != null) {
          this.label1.setText(Integer.toString(DatabaseMonitor.acquiredWeakAccess()));
          this.label2.setText(Integer.toString(DatabaseMonitor.waitingAcquireWeakAccess()));
          this.label3.setText(
              DatabaseMonitor.whoHasAllocatedStrongAccess() != null
                  ? DatabaseMonitor.whoHasAllocatedStrongAccess().getName()
                  : "None");
          this.label4.setText(
              DatabaseMonitor.whoHasAcquiredStrongAccess() != null
                  ? DatabaseMonitor.whoHasAcquiredStrongAccess().getName()
                  : "None");
          this.label5.setText(Integer.toString(DatabaseMonitor.waitingAllocateStrongAccess()));
        }
      }
    } catch (final Exception exc) {
      exc.printStackTrace();
    }
    return true;
  }
}
