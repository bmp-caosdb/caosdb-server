/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.terminal;

import caosdb.server.utils.Info;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.component.Table;

public final class EntitiesPanel extends Panel implements StatComponent {

  private static final EntitiesPanel instance = new EntitiesPanel();
  private boolean active = false;
  private final Table table = new Table(2);
  private final Label label1 = new Label();
  private final Label label2 = new Label();
  private final Label label3 = new Label();
  private final Label label4 = new Label();

  public static final EntitiesPanel getInstance() {
    return instance;
  }

  private EntitiesPanel() {
    super();

    this.table.addRow(new Label("RecordTypes:"), this.label2);
    this.table.addRow(new Label("Properties:"), this.label3);
    this.table.addRow(new Label("Records:"), this.label4);
    this.table.addRow(new Label("Files:"), this.label1);
    addComponent(this.table);

    // init INFO
    Info.getInstance().notifyObserver(null, null);

    // adds itself to StatsPanel
    StatsPanel.addStat("Entities", this);
    start();
  }

  @Override
  public void start() {
    this.active = true;
    update();
  }

  @Override
  public void stop() {
    this.active = false;
  }

  @Override
  public void update() {
    if (this.active) {
      try {
        this.label1.setText(Info.getFilesCount().toString());
        this.label2.setText(Info.getRecordTypesCount().toString());
        this.label3.setText(Info.getPropertiesCount().toString());
        this.label4.setText(Info.getRecordsCount().toString());
      } catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }
}
