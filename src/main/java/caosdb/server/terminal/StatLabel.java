/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.terminal;

import caosdb.server.utils.Observable;
import caosdb.server.utils.Observer;
import com.googlecode.lanterna.gui.component.Label;

public class StatLabel extends Label implements Observer, StatComponent {

  boolean active = false;
  Object obj;
  String name;

  @Override
  public boolean notifyObserver(final String e, final Observable o) {
    update();
    return true;
  }

  public StatLabel(final Object obj) {
    this(null, obj);
  }

  public StatLabel(final String name, final Object obj) {
    this.name = name;
    this.obj = obj;
    if (obj instanceof Observable) {
      ((Observable) obj).acceptObserver(this);
    }
    start();
    update();
  }

  @Override
  public void stop() {
    this.active = false;
  }

  @Override
  public void start() {
    this.active = true;
  }

  @Override
  public void update() {
    if (this.active) {
      setText((this.name == null ? "" : this.name + ": ") + this.obj.toString());
    }
  }
}
