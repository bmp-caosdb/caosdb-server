/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.terminal;

import com.googlecode.lanterna.gui.Component;
import com.googlecode.lanterna.gui.component.Panel;

public class StatTable extends Panel implements StatComponent {

  @Override
  public void start() {
    for (final Component c : components()) {
      if (c instanceof StatComponent) {
        ((StatComponent) c).start();
      }
    }
  }

  @Override
  public void stop() {
    for (final Component c : components()) {
      if (c instanceof StatComponent) {
        ((StatComponent) c).stop();
      }
    }
  }

  @Override
  public void update() {
    for (final Component c : components()) {
      if (c instanceof StatComponent) {
        ((StatComponent) c).update();
      }
    }
  }
}
