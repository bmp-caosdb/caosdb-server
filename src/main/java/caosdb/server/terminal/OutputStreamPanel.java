/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.terminal;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.component.Button;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.component.TextArea;
import com.googlecode.lanterna.gui.layout.LinearLayout;
import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.input.Key.Kind;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;

class PipedPrintStream extends PrintStream {
  private final TextArea out;
  int lines = 0;

  public PipedPrintStream(final TextArea out) {
    super(new NullOutputStream());
    this.out = out;
  }

  @Override
  public void println(final String x) {
    if (open) {
      synchronized (this) {
        out.appendLine(x);
        if (lines >= 4000) {
          out.removeLine(0);
        } else {
          lines++;
        }
      }
    }
  }

  @Override
  public void print(final String x) {}

  private final boolean open = false;
}

class NullOutputStream extends OutputStream {

  @Override
  public void write(final int b) throws IOException {}
}

public abstract class OutputStreamPanel extends Panel implements Runnable {

  private final TextArea logArea = new TextArea();
  private BufferedReader reader = null;
  protected final PipedOutputStream panelOutputStream = new PipedOutputStream();

  protected final PipedOutputStream getPanelOutputStream() {
    return panelOutputStream;
  }

  Button toogle =
      new Button(
          "on/off",
          new Action() {

            @Override
            public void doAction() {
              toogle();
            }
          });

  private boolean on = true;

  void toogle() {
    on = !on;
  }

  public OutputStreamPanel() {
    super();
    // addComponent(toogle);
    addComponent(logArea, LinearLayout.MAXIMIZES_HORIZONTALLY, LinearLayout.MAXIMIZES_VERTICALLY);
  }

  protected void init() throws IOException {
    final PipedInputStream pIn = new PipedInputStream(panelOutputStream);
    reader = new BufferedReader(new InputStreamReader(pIn));
  }

  @Override
  public final void run() {
    int lines = 0;
    while (true) {
      while (on) {
        try {
          if (reader.ready()) {
            logArea.appendLine(reader.readLine());
            if (lines >= 4000) {
              logArea.removeLine(0);
            } else {
              lines++;
            }
            try {
              logArea.keyboardInteraction(new Key(Kind.End));
            } catch (final NullPointerException e) {
              // this usually happens when the logArea is updated
              // but
              // not painted (while not being an active panel)
            }
          } else {
            Thread.sleep(1000);
          }

        } catch (final Exception e2) {
          e2.printStackTrace();
        }
      }
    }
  }

  @Override
  protected void finalize() throws Throwable {
    if (reader != null) {
      reader.close();
    }
    super.finalize();
  }
}
