/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.terminal;

import com.googlecode.lanterna.TerminalFacade;
import com.googlecode.lanterna.gui.GUIScreen;
import com.googlecode.lanterna.gui.GUIScreen.Position;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.text.UnixTerminal;
import java.nio.charset.Charset;

public class CaosDBTerminal extends Thread {

  public CaosDBTerminal() {
    if ((this.terminal =
            TerminalFacade.createTerminal(System.in, System.out, Charset.forName("UTF8")))
        instanceof UnixTerminal) {
      this.terminal =
          new UnixTerminal(
              System.in,
              System.out,
              Charset.forName("UTF8"),
              null,
              UnixTerminal.Behaviour.CTRL_C_KILLS_APPLICATION);
    }
    this.screen = new Screen(this.terminal);
    this.guiScreen = new GUIScreen(this.screen);
  }

  private Terminal terminal = null;
  private Screen screen = null;
  private GUIScreen guiScreen = null;

  @Override
  public void run() {
    this.guiScreen.getScreen().startScreen();
    final MainWindow m = new MainWindow();

    this.guiScreen.showWindow(m, Position.FULL_SCREEN);
    this.guiScreen.getScreen().stopScreen();
  }

  public void shutDown() {
    this.guiScreen.getActiveWindow().close();
  }
}
