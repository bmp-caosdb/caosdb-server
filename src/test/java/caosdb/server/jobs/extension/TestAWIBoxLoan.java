package caosdb.server.jobs.extension;

import static org.junit.Assert.assertEquals;

import caosdb.server.entity.container.TransactionContainer;
import caosdb.server.jobs.core.Mode;
import caosdb.server.transaction.Transaction;
import caosdb.server.utils.EntityStatus;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.ExecutionException;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

public class TestAWIBoxLoan {

  @Test
  public void testNonAnonymousUser() {
    TransactionContainer container = new TransactionContainer();
    AWIBoxLoan j =
        new AWIBoxLoan() {

          @Override
          protected Subject getUser() {
            return new Subject() {

              @Override
              public void runAs(PrincipalCollection principals)
                  throws NullPointerException, IllegalStateException {}

              @Override
              public PrincipalCollection releaseRunAs() {
                return null;
              }

              @Override
              public void logout() {}

              @Override
              public void login(AuthenticationToken token) throws AuthenticationException {}

              @Override
              public boolean isRunAs() {
                return false;
              }

              @Override
              public boolean isRemembered() {
                return false;
              }

              @Override
              public boolean isPermittedAll(Collection<Permission> permissions) {
                return false;
              }

              @Override
              public boolean isPermittedAll(String... permissions) {
                return false;
              }

              @Override
              public boolean[] isPermitted(List<Permission> permissions) {
                return null;
              }

              @Override
              public boolean[] isPermitted(String... permissions) {
                return null;
              }

              @Override
              public boolean isPermitted(Permission permission) {

                return false;
              }

              @Override
              public boolean isPermitted(String permission) {

                return false;
              }

              @Override
              public boolean isAuthenticated() {

                return false;
              }

              @Override
              public boolean[] hasRoles(List<String> roleIdentifiers) {

                return null;
              }

              @Override
              public boolean hasRole(String roleIdentifier) {

                return false;
              }

              @Override
              public boolean hasAllRoles(Collection<String> roleIdentifiers) {

                return false;
              }

              @Override
              public Session getSession(boolean create) {
                return null;
              }

              @Override
              public Session getSession() {
                return null;
              }

              @Override
              public PrincipalCollection getPrincipals() {
                return null;
              }

              @Override
              public Object getPrincipal() {
                return null;
              }

              @Override
              public PrincipalCollection getPreviousPrincipals() {
                return null;
              }

              @Override
              public void execute(Runnable runnable) {}

              @Override
              public <V> V execute(Callable<V> callable) throws ExecutionException {
                return null;
              }

              @Override
              public void checkRoles(String... roleIdentifiers) throws AuthorizationException {}

              @Override
              public void checkRoles(Collection<String> roleIdentifiers)
                  throws AuthorizationException {}

              @Override
              public void checkRole(String roleIdentifier) throws AuthorizationException {}

              @Override
              public void checkPermissions(Collection<Permission> permissions)
                  throws AuthorizationException {}

              @Override
              public void checkPermissions(String... permissions) throws AuthorizationException {}

              @Override
              public void checkPermission(Permission permission) throws AuthorizationException {}

              @Override
              public void checkPermission(String permission) throws AuthorizationException {}

              @Override
              public Runnable associateWith(Runnable runnable) {
                return null;
              }

              @Override
              public <V> Callable<V> associateWith(Callable<V> callable) {
                return null;
              }
            };
          }
        };
    Transaction<TransactionContainer> t =
        new Transaction<TransactionContainer>(container, null) {

          @Override
          protected void transaction() throws Exception {}

          @Override
          protected void preTransaction() throws InterruptedException {}

          @Override
          protected void preCheck() throws InterruptedException, Exception {}

          @Override
          protected void postTransaction() throws Exception {}

          @Override
          protected void postCheck() {}

          @Override
          public boolean logHistory() {
            return false;
          }

          @Override
          protected void init() throws Exception {}

          @Override
          protected void cleanUp() {}
        };

    j.init(Mode.MUST, null, t);
    assertEquals(0, j.getContainer().getMessages().size());
    assertEquals(EntityStatus.QUALIFIED, j.getContainer().getStatus());

    // non-anonymous user
    j.run();
    assertEquals(0, j.getContainer().getMessages().size());
    assertEquals(EntityStatus.QUALIFIED, j.getContainer().getStatus());
  }

  @Test
  public void testAnonymousUserUnqualified() {
    TransactionContainer container = new TransactionContainer();
    AWIBoxLoan j =
        new AWIBoxLoan() {

          @Override
          protected Subject getUser() {
            return null;
          }

          @Override
          boolean isAnonymous() {
            return true;
          }
        };
    Transaction<TransactionContainer> t =
        new Transaction<TransactionContainer>(container, null) {

          @Override
          protected void transaction() throws Exception {}

          @Override
          protected void preTransaction() throws InterruptedException {}

          @Override
          protected void preCheck() throws InterruptedException, Exception {}

          @Override
          protected void postTransaction() throws Exception {}

          @Override
          protected void postCheck() {}

          @Override
          public boolean logHistory() {
            return false;
          }

          @Override
          protected void init() throws Exception {}

          @Override
          protected void cleanUp() {}
        };

    j.init(Mode.MUST, null, t);
    assertEquals(0, j.getContainer().getMessages().size());
    assertEquals(EntityStatus.QUALIFIED, j.getContainer().getStatus());

    j.run();
    assertEquals(2, j.getContainer().getMessages().size());
    assertEquals(EntityStatus.UNQUALIFIED, j.getContainer().getStatus());
  }
}
