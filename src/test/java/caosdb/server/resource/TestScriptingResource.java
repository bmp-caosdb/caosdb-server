/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.resource;

import static org.junit.Assert.assertEquals;

import caosdb.server.entity.Message;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import org.junit.Test;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;

public class TestScriptingResource {

  ScriptingResource resource =
      new ScriptingResource() {
        @Override
        public int callScript(
            java.util.List<String> invokation,
            Integer timeout_ms,
            java.util.List<caosdb.server.entity.FileProperties> files,
            Object authToken)
            throws Message {
          return -1;
        };

        @Override
        public Object generateAuthToken() {
          return "";
        }
      };

  @Test
  public void testUnsupportedMediaType() {
    Representation entity = new StringRepresentation("asdf");
    entity.setMediaType(MediaType.TEXT_ALL);
    Request request = new Request(Method.POST, "../test", entity);
    request.getAttributes().put("SRID", "asdf1234");
    request.setDate(new Date());
    resource.init(null, request, new Response(null));
    resource.handle();
    assertEquals(Status.CLIENT_ERROR_UNSUPPORTED_MEDIA_TYPE, resource.getResponse().getStatus());
  }

  @Test
  public void testForm2invocation() throws Message {
    Form form =
        new Form(
            "-Ooption=OPTION&call=CA%20LL&-Ooption2=OPTION2&-p=POS1&-p4=POS3&-p2=POS2&IGNORED");
    List<String> list = resource.form2CommandLine(form);
    assertEquals("CA LL", list.get(0));
    assertEquals("--option=OPTION", list.get(1));
    assertEquals("--option2=OPTION2", list.get(2));
    assertEquals("POS1", list.get(3));
    assertEquals("POS2", list.get(4));
    assertEquals("POS3", list.get(5));
    assertEquals(6, list.size());
  }

  @Test
  public void testHandleForm() throws Message, IOException {
    Form form =
        new Form("-Ooption=OPTION&call=CALL&-Ooption2=OPTION2&-p=POS1&-p4=POS3&-p2=POS2&IGNORED");
    assertEquals(-1, resource.handleForm(form));
  }
}
