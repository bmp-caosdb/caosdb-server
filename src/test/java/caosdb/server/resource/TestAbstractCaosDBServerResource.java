package caosdb.server.resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import caosdb.server.CaosDBException;
import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import caosdb.server.accessControl.AnonymousAuthenticationToken;
import caosdb.server.accessControl.AnonymousRealm;
import caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.DelegatingSubject;
import org.jdom2.Element;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.restlet.data.Reference;
import org.restlet.representation.Representation;

public class TestAbstractCaosDBServerResource {

  @Rule public TemporaryFolder tempFolder = new TemporaryFolder();

  @BeforeClass
  public static void initServerProperties() throws IOException {
    CaosDBServer.initServerProperties();
  }

  @Test
  public void testReponseRootElement() throws IOException {
    final Subject user = new DelegatingSubject(new DefaultSecurityManager(new AnonymousRealm()));
    user.login(AnonymousAuthenticationToken.getInstance());
    AbstractCaosDBServerResource s =
        new AbstractCaosDBServerResource() {

          @Override
          protected Representation httpGetInChildClass()
              throws ConnectionException, IOException, SQLException, CaosDBException,
                  NoSuchAlgorithmException, Exception {
            // TODO Auto-generated method stub
            return null;
          }

          @Override
          public String getSRID() {
            return "TEST-SRID";
          }

          @Override
          public String getCRID() {
            return "TEST-CRID";
          }

          @Override
          public Long getTimestamp() {
            return 0L;
          }

          @Override
          public Reference getRootRef() {
            return new Reference("https://example.com/root/");
          }

          @Override
          public Subject getUser() {
            // TODO Auto-generated method stub
            return user;
          }
        };
    provideUserSourcesFile();
    Element response = s.generateRootElement();
    assertNotNull(response);
    assertEquals("TEST-SRID", response.getAttribute("srid").getValue());
    assertEquals("TEST-CRID", response.getAttribute("crid").getValue());
    assertEquals("0", response.getAttribute("timestamp").getValue());
    assertEquals("https://example.com/root/", response.getAttributeValue("baseuri"));
    Element userInfo = response.getChild("UserInfo");
    assertNotNull(userInfo);
  }

  /** Creates a dummy usersources.ini and injects it into the server properties. */
  private void provideUserSourcesFile() throws IOException {
    String usersourcesFileName = tempFolder.newFile("usersources.ini").getAbsolutePath();
    String usersourcesContent =
        "realms = PAM\n"
            + "defaultRealm = PAM\n"
            + "\n"
            + "[PAM]\n"
            + "class = caosdb.server.accessControl.Pam\n"
            + "default_status = ACTIVE\n"
            + "include.user = admin\n"
            + ";include.group = [uncomment and put your groups here]\n"
            + ";exclude.user = [uncomment and put excluded users here]\n"
            + ";exclude.group = [uncomment and put excluded groups here]\n"
            + "\n"
            + ";it is necessary to add at least one admin\n"
            + "user.admin.roles = administration";

    Files.write(Paths.get(usersourcesFileName), usersourcesContent.getBytes());
    CaosDBServer.setProperty(ServerProperties.KEY_USER_SOURCES_INI_FILE, usersourcesFileName);
  }
}
