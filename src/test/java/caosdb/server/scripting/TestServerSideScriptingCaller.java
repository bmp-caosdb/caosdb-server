/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.scripting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import caosdb.CaosDBTestClass;
import caosdb.server.CaosDBException;
import caosdb.server.CaosDBServer;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.entity.FileProperties;
import caosdb.server.entity.Message;
import caosdb.server.utils.ServerMessages;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import org.apache.commons.io.FileUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TestServerSideScriptingCaller extends CaosDBTestClass {

  public static File testFolder =
      TEST_DIR.toPath().resolve("serverSideScriptingCallerTestFolder").toFile();
  public static File testFile = testFolder.toPath().resolve("TestFile").toFile();
  public static File testExecutable = testFolder.toPath().resolve("TestScript").toFile();

  @BeforeClass
  public static void initServerProperties() throws IOException {
    CaosDBServer.initServerProperties();
  }

  @BeforeClass
  public static void setupTestFolder() throws IOException {
    FileUtils.forceDeleteOnExit(testFolder);
    FileUtils.forceDeleteOnExit(testFile);
    FileUtils.forceDeleteOnExit(testExecutable);

    assertFalse(testFolder.exists());
    assertFalse(testFile.exists());
    assertFalse(testExecutable.exists());
    testFolder.mkdirs();
    testFile.createNewFile();
    FileUtils.write(testFile, "asdfhaskdjfhaskjdf", "UTF-8");

    testExecutable.createNewFile();
    testExecutable.setExecutable(true);
  }

  @AfterClass
  public static void deleteTestFolder() throws IOException {
    FileUtils.forceDelete(testFile);
    FileUtils.forceDelete(testFolder);
    assertFalse(testFolder.exists());
    assertFalse(testFile.exists());
  }

  public void testExeExit(final int code) throws IOException {
    FileUtils.write(testExecutable, "exit " + code, "UTF-8");
  }

  public void testSleep(final int seconds) throws IOException {
    FileUtils.write(testExecutable, "sleep " + seconds, "UTF-8");
  }

  public void testPrintArgsToStdErr() throws IOException {
    FileUtils.write(testExecutable, "(>&2 echo \"$@\")", "UTF-8");
  }

  public void testPrintArgsToStdOut() throws IOException {
    FileUtils.write(testExecutable, "echo \"$@\"", "UTF-8");
  }

  private File pwd;

  @Before
  public void setupPWD() throws IOException {
    this.pwd = testFolder.toPath().resolve("testPWD").toFile();
    FileUtils.forceDeleteOnExit(this.pwd);
    assertFalse(this.pwd.exists());
  }

  @After
  public void deletePWD() throws IOException {
    if (this.pwd.exists()) {
      FileUtils.forceDelete(this.pwd);
    }
    assertFalse(this.pwd.exists());
  }

  @Rule public ExpectedException exception = ExpectedException.none();

  @Test
  public void testCreateWorkingDirFailure() throws Exception {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(null, -1, null, null, new ScriptingUtils(), this.pwd);

    this.exception.expect(Exception.class);
    this.exception.expectMessage(
        "The working directory must be non-existing when the caller is invoked.");

    this.pwd.mkdirs();
    caller.createWorkingDir();
  }

  @Test
  public void testCreateWorkingDirOk() throws Exception {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(null, -1, null, null, new ScriptingUtils(), this.pwd);

    caller.createWorkingDir();

    // exists
    assertTrue(this.pwd.exists());
    assertTrue(this.pwd.isDirectory());
  }

  /**
   * Throw {@link FileNotFoundException} because PWD does not exist.
   *
   * @throws IOException
   * @throws CaosDBException
   */
  @Test()
  public void testPutFilesInWorkingDirFailure1() throws IOException, CaosDBException {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(null, -1, null, null, new ScriptingUtils(), this.pwd);

    this.exception.expect(FileNotFoundException.class);
    this.exception.expectMessage("FILE_UPLOAD_DIR");

    caller.putFilesInWorkingDir(new ArrayList<>());
  }

  /**
   * Throw {@link CaosDBException} because tmpIdentifyer is null or empty.
   *
   * @throws FileNotFoundException
   * @throws CaosDBException
   */
  @Test
  public void testPutFilesInWorkingDirFailure2() throws FileNotFoundException, CaosDBException {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(null, -1, null, null, new ScriptingUtils(), this.pwd);
    this.pwd.mkdirs();

    final ArrayList<FileProperties> files = new ArrayList<>();
    files.add(new FileProperties(null, null, null));

    this.exception.expect(CaosDBException.class);
    this.exception.expectMessage("The path must not be null or empty!");

    caller.putFilesInWorkingDir(files);
  }

  /**
   * Throw {@link NullPointerException} because file is null.
   *
   * @throws FileNotFoundException
   * @throws CaosDBException
   */
  @Test
  public void testPutFilesInWorkingDirFailure3() throws FileNotFoundException, CaosDBException {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(null, -1, null, null, new ScriptingUtils(), this.pwd);
    this.pwd.mkdirs();

    final ArrayList<FileProperties> files = new ArrayList<>();
    final FileProperties f = new FileProperties(null, null, null);
    f.setTmpIdentifyer("a2s3d4f5");
    f.setPath("testfile");
    files.add(f);

    this.exception.expect(NullPointerException.class);
    this.exception.expectMessage("target was null.");

    caller.putFilesInWorkingDir(files);
  }

  /**
   * Throw {@link TransactionException} because file does not exist.
   *
   * @throws FileNotFoundException
   * @throws CaosDBException
   */
  @Test
  public void testPutFilesInWorkingDirFailure4() throws FileNotFoundException, CaosDBException {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(null, -1, null, null, new ScriptingUtils(), this.pwd);
    this.pwd.mkdirs();

    final ArrayList<FileProperties> files = new ArrayList<>();
    final FileProperties f = new FileProperties(null, null, null);
    f.setTmpIdentifyer("a2s3d4f5");
    f.setFile(new File("blablabla_non_existing"));
    f.setPath("bla");
    files.add(f);

    this.exception.expect(TransactionException.class);
    this.exception.expectMessage("target does not exist");

    caller.putFilesInWorkingDir(files);
  }

  /**
   * putFilesInWorkingDir returns silently because files is null.
   *
   * @throws IOException
   * @throws CaosDBException
   */
  @Test()
  public void testPutFilesInWorkingDirReturnSilently() throws IOException, CaosDBException {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(null, -1, null, null, new ScriptingUtils(), this.pwd);

    caller.putFilesInWorkingDir(null);
  }

  @Test
  public void testPutFilesInWorkingDirOk() throws CaosDBException, IOException {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(null, -1, null, null, new ScriptingUtils(), this.pwd);
    this.pwd.mkdirs();

    final ArrayList<FileProperties> files = new ArrayList<>();
    final FileProperties f = new FileProperties(null, null, null);
    f.setTmpIdentifyer("a2s3d4f5");
    f.setFile(testFile);
    f.setPath("testfile");
    files.add(f);

    caller.putFilesInWorkingDir(files);

    assertEquals(1, caller.getUploadFilesDir().listFiles().length);
    final File file = caller.getUploadFilesDir().listFiles()[0];
    FileUtils.contentEquals(file, testFile);
  }

  /**
   * Throw Exception because Script returns with error code 1.
   *
   * @throws Exception
   */
  @Test
  public void testCallScriptFailure1() throws Exception {
    final String[] cmd = {testExecutable.getAbsolutePath()};
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(cmd, -1, null, "", new ScriptingUtils(), this.pwd);

    caller.createWorkingDir();
    assertTrue(caller.getTmpWorkingDir().exists());

    testExeExit(1);

    assertEquals(1, caller.callScript());
  }

  @Test
  public void testCallScriptOkSimple() throws Exception {
    final String[] cmd = {testExecutable.getAbsolutePath()};
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(cmd, -1, null, "", new ScriptingUtils(), this.pwd);

    caller.createWorkingDir();
    assertTrue(caller.getTmpWorkingDir().exists());

    testExeExit(0);

    assertEquals(0, caller.callScript());
  }

  @Test
  public void testWatchTimeout() throws InterruptedException {
    final TimeoutProcess p =
        new TimeoutProcess(
            new Process() {

              int exit = 0;

              @Override
              public int waitFor() throws InterruptedException {
                return this.exit;
              }

              @Override
              public OutputStream getOutputStream() {
                return null;
              }

              @Override
              public InputStream getInputStream() {
                return null;
              }

              @Override
              public InputStream getErrorStream() {
                return null;
              }

              @Override
              public int exitValue() {
                return this.exit;
              }

              @Override
              public void destroy() {
                this.exit = 1;
              }

              @Override
              public boolean isAlive() {
                return this.exit == 0;
              }
            },
            -1);

    assertFalse(p.wasTimeouted());
    final Thread watchTimeout = TimeoutProcess.watchTimeout(p, 100);
    watchTimeout.start();
    watchTimeout.join();
    assertEquals(1, p.exitValue());
    assertTrue(p.wasTimeouted());
  }

  @Test(timeout = 1000)
  public void testTimeout() throws Exception {
    final String[] cmd = {testExecutable.getAbsolutePath()};
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(cmd, 500, null, "", new ScriptingUtils(), this.pwd);

    caller.createWorkingDir();
    assertTrue(caller.getTmpWorkingDir().exists());

    testSleep(10);

    this.exception.expect(TimeoutException.class);
    caller.callScript();
  }

  @Test
  public void testCleanup() throws Exception {
    final String[] cmd = {testExecutable.getAbsolutePath()};
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(cmd, 500, null, null, new ScriptingUtils(), this.pwd);

    caller.createWorkingDir();
    assertTrue(caller.getTmpWorkingDir().exists());
    caller.cleanup();
    assertFalse(caller.getTmpWorkingDir().exists());
  }

  @Test
  public void testInvokeWithErrorInCreateWorkingDir() throws Message {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            new String[] {""}, -1, null, null, new ScriptingUtils(), this.pwd) {
          @Override
          public void createWorkingDir() throws Exception {
            throw new Exception();
          }

          @Override
          public void putFilesInWorkingDir(final Collection<FileProperties> files)
              throws FileNotFoundException, CaosDBException {}

          @Override
          public int callScript() {
            return 0;
          }

          @Override
          public void cleanup() {}
        };
    this.exception.expect(
        new BaseMatcher<Exception>() {

          @Override
          public boolean matches(final Object item) {
            return item == ServerMessages.SERVER_SIDE_SCRIPT_SETUP_ERROR;
          }

          @Override
          public void describeTo(final Description description) {
            description.appendValue(ServerMessages.SERVER_SIDE_SCRIPT_SETUP_ERROR);
          }
        });
    caller.invoke();
  }

  @Test
  public void testInvokeWithErrorInPutFilesInWorkingDir() throws Message {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            new String[] {""}, -1, null, "", new ScriptingUtils(), this.pwd) {
          @Override
          public void createWorkingDir() throws Exception {}

          @Override
          public void putFilesInWorkingDir(final Collection<FileProperties> files)
              throws FileNotFoundException, CaosDBException {
            throw new CaosDBException("");
          }

          @Override
          public int callScript() {
            return 0;
          }

          @Override
          public void cleanup() {}
        };
    this.exception.expect(
        new BaseMatcher<Exception>() {

          @Override
          public boolean matches(final Object item) {
            return item == ServerMessages.SERVER_SIDE_SCRIPT_SETUP_ERROR;
          }

          @Override
          public void describeTo(final Description description) {
            description.appendValue(ServerMessages.SERVER_SIDE_SCRIPT_SETUP_ERROR);
          }
        });
    caller.invoke();
  }

  @Test
  public void testInvokeWithErrorInCallScript() throws Message {
    final String[] cmd = {testExecutable.getAbsolutePath()};
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(cmd, -1, null, null, new ScriptingUtils(), this.pwd) {
          @Override
          public void createWorkingDir() throws Exception {}

          @Override
          public void putFilesInWorkingDir(final Collection<FileProperties> files)
              throws FileNotFoundException, CaosDBException {}

          @Override
          public int callScript() throws IOException {
            throw new IOException();
          }

          @Override
          public void cleanup() {}
        };
    this.exception.expect(
        new BaseMatcher<Exception>() {

          @Override
          public boolean matches(final Object item) {
            return item == ServerMessages.SERVER_SIDE_SCRIPT_ERROR;
          }

          @Override
          public void describeTo(final Description description) {
            description.appendValue(ServerMessages.SERVER_SIDE_SCRIPT_ERROR);
          }
        });
    caller.invoke();
  }

  @Test
  public void testInvokeWithErrorInCleanup() throws Message {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(null, -1, null, null, new ScriptingUtils(), this.pwd) {
          @Override
          public void createWorkingDir() throws Exception {}

          @Override
          public void putFilesInWorkingDir(final Collection<FileProperties> files)
              throws FileNotFoundException, CaosDBException {}

          @Override
          public int callScript() {
            return 0;
          }

          @Override
          public void cleanup() {
            super.cleanup();
          }

          @Override
          public void deleteWorkingDir(File pwd) throws IOException {
            throw new IOException();
          }
        };
    this.exception.expect(RuntimeException.class);
    this.exception.expectMessage("Cleanup failed.");
    caller.invoke();
  }

  @Test
  public void testPrintStdErr() throws Exception {
    final String[] cmd = {testExecutable.getAbsolutePath(), "opt1", "opt2"};
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(cmd, -1, null, "authToken", new ScriptingUtils(), this.pwd);

    caller.createWorkingDir();

    testPrintArgsToStdErr();

    caller.callScript();

    assertEquals(
        "--auth-token=authToken opt1 opt2\n", FileUtils.readFileToString(caller.getStdErrFile()));
  }

  @Test
  public void testPrintStdOut() throws Exception {
    final String[] cmd = {testExecutable.getAbsolutePath(), "opt1", "opt2"};
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(cmd, -1, null, "authToken", new ScriptingUtils(), this.pwd);

    caller.createWorkingDir();

    testPrintArgsToStdOut();

    caller.callScript();

    assertEquals(
        "--auth-token=authToken opt1 opt2\n", FileUtils.readFileToString(caller.getStdOutFile()));
  }

  @Test
  public void testCleanupNonExistingWorkingDir() {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(null, -1, null, "authToken", new ScriptingUtils(), this.pwd);
    caller.cleanup();
  }
}
