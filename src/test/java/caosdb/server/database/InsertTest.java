/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database;

import static caosdb.server.database.DatabaseUtils.deriveStage1Inserts;
import static caosdb.server.database.DatabaseUtils.deriveStage2Inserts;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import caosdb.server.datatype.CollectionValue;
import caosdb.server.datatype.GenericValue;
import caosdb.server.datatype.SingleValue;
import caosdb.server.entity.Entity;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.Role;
import caosdb.server.entity.StatementStatus;
import caosdb.server.entity.wrapper.Property;
import java.util.LinkedList;
import org.junit.Test;

public class InsertTest {

  /**
   * Record with very deep property tree.
   *
   * @throws Exception
   */
  @Test
  public void testTransformation1() throws Exception {
    final Entity r = new Entity(-1, Role.Record);
    final Property p1 = new Property(1);
    p1.setRole("Property");
    p1.setValue(new GenericValue("V1"));
    p1.setDatatype("TEXT");
    p1.setStatementStatus(StatementStatus.FIX);
    r.addProperty(p1);

    final Property p2 = new Property(2);
    p2.setRole("Property");
    p2.setValue(new GenericValue("V2"));
    p2.setDatatype("TEXT");
    p2.setStatementStatus(StatementStatus.RECOMMENDED);
    r.addProperty(p2);

    final Property p3 = new Property(3);
    p3.setRole("Property");
    p3.setValue(new GenericValue("V3"));
    p3.setDatatype("TEXT");
    p3.setStatementStatus(StatementStatus.OBLIGATORY);
    p2.addProperty(p3);

    final Property p4 = new Property(4);
    p4.setRole("Property");
    p4.setValue(new GenericValue("V4"));
    p4.setDatatype("TEXT");
    p4.setStatementStatus(StatementStatus.OBLIGATORY);
    r.addProperty(p4);

    final Property p5 = new Property(5);
    p5.setRole("Property");
    p5.setValue(new GenericValue("V5"));
    p5.setDatatype("TEXT");
    p5.setStatementStatus(StatementStatus.OBLIGATORY);
    p4.addProperty(p5);

    final Property p6 = new Property(6);
    p6.setRole("Property");
    p6.setValue(new GenericValue("V6"));
    p6.setDatatype("TEXT");
    p6.setStatementStatus(StatementStatus.OBLIGATORY);
    p5.addProperty(p6);

    final Property p7 = new Property(7);
    p7.setRole("Property");
    p7.setValue(new GenericValue("V7"));
    p7.setDatatype("TEXT");
    p7.setStatementStatus(StatementStatus.OBLIGATORY);
    r.addProperty(p7);

    final Property p8 = new Property(8);
    p8.setRole("Property");
    p8.setValue(new GenericValue("V8"));
    p8.setDatatype("TEXT");
    p8.setStatementStatus(StatementStatus.OBLIGATORY);
    p7.addProperty(p8);

    final Property p9 = new Property(9);
    p9.setRole("Property");
    p9.setValue(new GenericValue("V9"));
    p9.setDatatype("TEXT");
    p9.setStatementStatus(StatementStatus.OBLIGATORY);
    p8.addProperty(p9);

    final Property p10 = new Property(10);
    p10.setRole("Property");
    p10.setValue(new GenericValue("V10"));
    p10.setDatatype("TEXT");
    p10.setStatementStatus(StatementStatus.OBLIGATORY);
    p9.addProperty(p10);

    final LinkedList<EntityInterface> stage1Inserts = new LinkedList<EntityInterface>();
    final LinkedList<EntityInterface> stage2Inserts = new LinkedList<EntityInterface>();
    new LinkedList<EntityInterface>();
    deriveStage1Inserts(stage1Inserts, r);

    assertEquals(7, stage1Inserts.size());
    assertEquals(Role.Property, stage1Inserts.get(0).getRole());
    assertEquals((Integer) 1, stage1Inserts.get(0).getId());
    assertEquals("V1", ((SingleValue) stage1Inserts.get(0).getValue()).toDatabaseString());
    assertFalse(stage1Inserts.get(0).hasReplacement());

    assertEquals(Role.Property, stage1Inserts.get(1).getRole());
    assertEquals((Integer) 2, stage1Inserts.get(1).getId());
    assertEquals("V2", ((SingleValue) stage1Inserts.get(1).getValue()).toDatabaseString());
    assertFalse(stage1Inserts.get(1).hasReplacement());

    assertEquals(Role.Property, stage1Inserts.get(2).getRole());
    assertEquals((Integer) 4, stage1Inserts.get(2).getId());
    assertEquals("V4", ((SingleValue) stage1Inserts.get(2).getValue()).toDatabaseString());
    assertFalse(stage1Inserts.get(2).hasReplacement());

    assertEquals(Role.Domain, stage1Inserts.get(3).getRole());
    assertEquals("V5", ((SingleValue) stage1Inserts.get(3).getValue()).toDatabaseString());
    assertFalse(stage1Inserts.get(3).hasReplacement());

    assertEquals(Role.Property, stage1Inserts.get(4).getRole());
    assertEquals((Integer) 7, stage1Inserts.get(4).getId());
    assertEquals("V7", ((SingleValue) stage1Inserts.get(4).getValue()).toDatabaseString());
    assertFalse(stage1Inserts.get(4).hasReplacement());

    assertEquals(Role.Domain, stage1Inserts.get(5).getRole());
    assertEquals("V8", ((SingleValue) stage1Inserts.get(5).getValue()).toDatabaseString());
    assertFalse(stage1Inserts.get(5).hasReplacement());

    assertEquals(Role.Domain, stage1Inserts.get(6).getRole());
    assertEquals("V9", ((SingleValue) stage1Inserts.get(6).getValue()).toDatabaseString());
    assertFalse(stage1Inserts.get(6).hasReplacement());

    deriveStage2Inserts(stage2Inserts, stage1Inserts);

    assertEquals(6, stage2Inserts.size());
    assertEquals(Role.Property, stage2Inserts.get(0).getRole());
    assertEquals((Integer) 3, stage2Inserts.get(0).getId());
    assertEquals("V3", ((SingleValue) stage2Inserts.get(0).getValue()).toDatabaseString());
    assertFalse(stage2Inserts.get(0).hasReplacement());

    assertEquals(Role.Property, stage2Inserts.get(1).getRole());
    assertEquals((Integer) 5, stage2Inserts.get(1).getId());
    assertEquals("V5", ((SingleValue) stage2Inserts.get(1).getValue()).toDatabaseString());
    assertTrue(stage2Inserts.get(1).hasReplacement());
    assertEquals(stage1Inserts.get(3), stage2Inserts.get(1).getReplacement());

    assertEquals(Role.Property, stage2Inserts.get(2).getRole());
    assertEquals((Integer) 6, stage2Inserts.get(2).getId());
    assertEquals("V6", ((SingleValue) stage2Inserts.get(2).getValue()).toDatabaseString());
    assertFalse(stage2Inserts.get(2).hasReplacement());

    assertEquals(Role.Property, stage2Inserts.get(3).getRole());
    assertEquals((Integer) 8, stage2Inserts.get(3).getId());
    assertEquals("V8", ((SingleValue) stage2Inserts.get(3).getValue()).toDatabaseString());
    assertTrue(stage2Inserts.get(3).hasReplacement());
    assertEquals(stage1Inserts.get(5), stage2Inserts.get(3).getReplacement());

    assertEquals(Role.Property, stage2Inserts.get(4).getRole());
    assertEquals((Integer) 9, stage2Inserts.get(4).getId());
    assertEquals("V9", ((SingleValue) stage2Inserts.get(4).getValue()).toDatabaseString());
    assertTrue(stage2Inserts.get(4).hasReplacement());
    assertEquals(stage1Inserts.get(6), stage2Inserts.get(4).getReplacement());

    assertEquals(Role.Property, stage2Inserts.get(5).getRole());
    assertEquals((Integer) 10, stage2Inserts.get(5).getId());
    assertEquals("V10", ((SingleValue) stage2Inserts.get(5).getValue()).toDatabaseString());
    assertFalse(stage2Inserts.get(5).hasReplacement());
  }

  /**
   * Record with two properties of the same id and different values. One has a sub property, the
   * other does not.
   *
   * @throws Exception
   */
  @Test
  public void testTransformation2() throws Exception {
    final Entity r = new Entity(-1, Role.Record);
    final Property p1 = new Property(1);
    p1.setRole("Property");
    p1.setValue(new GenericValue("V1-1"));
    p1.setDatatype("TEXT");
    p1.setStatementStatus(StatementStatus.FIX);
    r.addProperty(p1);

    final Property p2 = new Property(1);
    p2.setRole("Property");
    p2.setValue(new GenericValue("V1-2"));
    p2.setDatatype("TEXT");
    p2.setStatementStatus(StatementStatus.FIX);
    r.addProperty(p2);

    final Property subp = new Property(2);
    subp.setRole("Property");
    subp.setValue(new GenericValue("V2"));
    subp.setDatatype("TEXT");
    subp.setStatementStatus(StatementStatus.FIX);
    p2.addProperty(subp);

    r.print();

    final LinkedList<EntityInterface> stage1Inserts = new LinkedList<EntityInterface>();
    final LinkedList<EntityInterface> stage2Inserts = new LinkedList<EntityInterface>();

    deriveStage1Inserts(stage1Inserts, r);

    assertEquals(3, stage1Inserts.size());
    assertEquals(Role.Property, stage1Inserts.get(0).getRole());
    assertEquals((Integer) 1, stage1Inserts.get(0).getId());
    assertEquals("V1-1", ((SingleValue) stage1Inserts.get(0).getValue()).toDatabaseString());
    assertFalse(stage1Inserts.get(0).hasReplacement());

    assertEquals(Role.Property, stage1Inserts.get(1).getRole());
    assertEquals("V1-2", ((SingleValue) stage1Inserts.get(1).getValue()).toDatabaseString());
    assertEquals((Integer) 1, stage1Inserts.get(1).getId());
    assertTrue(stage1Inserts.get(1).hasReplacement());
    assertEquals(stage1Inserts.get(2), stage1Inserts.get(1).getReplacement());

    assertEquals(Role.Domain, stage1Inserts.get(2).getRole());
    assertEquals("V1-2", ((SingleValue) stage1Inserts.get(2).getValue()).toDatabaseString());
    assertFalse(stage1Inserts.get(2).hasReplacement());

    deriveStage2Inserts(stage2Inserts, stage1Inserts);

    assertEquals(1, stage2Inserts.size());
    assertEquals(Role.Property, stage2Inserts.get(0).getRole());
    assertEquals((Integer) 2, stage2Inserts.get(0).getId());
    assertEquals("V2", ((SingleValue) stage2Inserts.get(0).getValue()).toDatabaseString());
    assertFalse(stage2Inserts.get(0).hasReplacement());

    System.out.println("######### stage 1 #########");
    for (EntityInterface e : stage1Inserts) {
      e.print();
    }
    System.out.println("######### stage 2 #########");
    for (EntityInterface e : stage2Inserts) {
      e.print();
    }
  }

  /**
   * Record with three properties of the same id without any value. The first has a sub property,
   * the others do not.
   *
   * @throws Exception
   */
  @Test
  public void testTransformation3() throws Exception {
    final Entity r = new Entity(-1, Role.Record);
    final Property p1 = new Property(1);
    p1.setRole("Property");
    p1.setDatatype("TEXT");
    p1.setStatementStatus(StatementStatus.FIX);
    r.addProperty(p1);

    final Property p2 = new Property(1);
    p2.setRole("Property");
    p2.setDatatype("TEXT");
    p2.setStatementStatus(StatementStatus.FIX);
    r.addProperty(p2);

    final Property p3 = new Property(1);
    p3.setRole("Property");
    p3.setDatatype("TEXT");
    p3.setStatementStatus(StatementStatus.FIX);
    r.addProperty(p3);

    final Property sub1 = new Property(2);
    sub1.setRole("Property");
    sub1.setDatatype("TEXT");
    sub1.setValue(new GenericValue("V1"));
    sub1.setStatementStatus(StatementStatus.FIX);
    p1.addProperty(sub1);

    final LinkedList<EntityInterface> stage1Inserts = new LinkedList<EntityInterface>();
    final LinkedList<EntityInterface> stage2Inserts = new LinkedList<EntityInterface>();

    deriveStage1Inserts(stage1Inserts, r);

    assertEquals(4, stage1Inserts.size());
    assertEquals(Role.Property, stage1Inserts.get(0).getRole());
    assertEquals((Integer) 1, stage1Inserts.get(0).getId());
    assertTrue(stage1Inserts.get(0).hasReplacement());
    assertEquals(stage1Inserts.get(1), stage1Inserts.get(0).getReplacement());
    assertEquals(null, stage1Inserts.get(0).getValue());

    assertEquals(Role.Domain, stage1Inserts.get(1).getRole());
    assertFalse(stage1Inserts.get(1).hasReplacement());
    assertEquals(null, stage1Inserts.get(1).getValue());

    assertEquals(Role.Property, stage1Inserts.get(2).getRole());
    assertEquals((Integer) 1, stage1Inserts.get(2).getId());
    assertFalse(stage1Inserts.get(2).hasReplacement());
    assertEquals(null, stage1Inserts.get(2).getValue());

    assertEquals(Role.Property, stage1Inserts.get(3).getRole());
    assertEquals((Integer) 1, stage1Inserts.get(3).getId());
    assertFalse(stage1Inserts.get(3).hasReplacement());
    assertEquals(null, stage1Inserts.get(3).getValue());

    deriveStage2Inserts(stage2Inserts, stage1Inserts);

    assertEquals(1, stage2Inserts.size());
    assertEquals(Role.Property, stage2Inserts.get(0).getRole());
    assertEquals((Integer) 2, stage2Inserts.get(0).getId());
    assertEquals("V1", ((SingleValue) stage2Inserts.get(0).getValue()).toDatabaseString());
    assertFalse(stage2Inserts.get(0).hasReplacement());
  }

  /**
   * Record with a list property.
   *
   * @throws Exception
   */
  @Test
  public void testTransformation4() throws Exception {

    final Entity r = new Entity(-1, Role.Record);
    final Property p1 = new Property(1);
    p1.setRole("Property");
    p1.setDatatype("TEXT");
    p1.setValue(new GenericValue("V1"));
    p1.setStatementStatus(StatementStatus.FIX);
    r.addProperty(p1);

    final Property p2 = new Property(2);
    p2.setRole("Property");
    p2.setDatatype("LIST<TEXT>");
    p2.setStatementStatus(StatementStatus.FIX);
    final CollectionValue vals = new CollectionValue();
    vals.add(new GenericValue("L1"));
    vals.add(new GenericValue("L2"));
    vals.add(new GenericValue("L3"));
    p2.setValue(vals);
    r.addProperty(p2);

    final Property p3 = new Property(3);
    p3.setRole("Property");
    p3.setDatatype("TEXT");
    p3.setValue(new GenericValue("V3"));
    p3.setStatementStatus(StatementStatus.FIX);
    r.addProperty(p3);

    final LinkedList<EntityInterface> stage1Inserts = new LinkedList<EntityInterface>();
    final LinkedList<EntityInterface> stage2Inserts = new LinkedList<EntityInterface>();

    deriveStage1Inserts(stage1Inserts, r);

    assertEquals(4, stage1Inserts.size());
    assertEquals(Role.Property, stage1Inserts.get(0).getRole());
    assertEquals((Integer) 1, stage1Inserts.get(0).getId());
    assertFalse(stage1Inserts.get(0).hasReplacement());
    assertEquals("V1", ((SingleValue) stage1Inserts.get(0).getValue()).toDatabaseString());

    assertEquals(Role.Property, stage1Inserts.get(1).getRole());
    assertEquals((Integer) 2, stage1Inserts.get(1).getId());
    assertTrue(stage1Inserts.get(1).hasReplacement());
    assertEquals(stage1Inserts.get(2), stage1Inserts.get(1).getReplacement());
    assertTrue(stage1Inserts.get(1).getValue() instanceof CollectionValue);

    assertEquals(Role.Domain, stage1Inserts.get(2).getRole());
    assertFalse(stage1Inserts.get(2).hasReplacement());
    assertTrue(stage1Inserts.get(2).getValue() instanceof CollectionValue);

    assertEquals(Role.Property, stage1Inserts.get(3).getRole());
    assertEquals((Integer) 3, stage1Inserts.get(3).getId());
    assertFalse(stage1Inserts.get(3).hasReplacement());
    assertEquals("V3", ((SingleValue) stage1Inserts.get(3).getValue()).toDatabaseString());

    deriveStage2Inserts(stage2Inserts, stage1Inserts);
  }
}
