/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.database;

import org.junit.Assert;
import org.junit.Test;

public class DatabaseMonitorTest {
  public static final WeakAccessSemaphore wa = new WeakAccessSemaphore();
  public static final StrongAccessLock sa = new StrongAccessLock(wa);

  /**
   * Two read-, two write-threads. The read-threads request weak access, the write-threads request
   * strong access.<br>
   * The read-thread rt1 is started and gets weak access. Then the write-thread wt1 starts and
   * allocates strong access.<br>
   * A second read-thread rt2 starts and also gets weak access. A second write thread wt2 starts and
   * requests allocation of strong access. It has to wait until wt2 releases it. <br>
   * wt1 acquires strong access as soon as both read-threads released their weak-accesss.<br>
   *
   * @throws InterruptedException
   */
  @Test
  public void test1() throws InterruptedException {
    // first invoke a read thread
    final ReadThread rt1 = new ReadThread();
    rt1.start(); // paused, has acquired weak access now

    // invoke a write thread
    final WriteThread wt1 = new WriteThread();
    wt1.start(); // paused, has allocated strong access now
    synchronized (this) {
      this.wait(1000);
    }
    // waiting means any processing
    Assert.assertEquals(wt1.getState(), Thread.State.WAITING);

    final ReadThread rt2 = new ReadThread();
    rt2.start(); // paused, has acquired a second weak access now
    synchronized (this) {
      this.wait(1000);
    }
    synchronized (rt2) {
      rt2.notify();
    }
    synchronized (this) {
      this.wait(1000);
    }

    // rt2 was processed while wt1 has allocated but not yet acquired strong
    // access. rt2 terminated after releasing its weak access.
    Assert.assertEquals(rt2.getState(), Thread.State.TERMINATED);

    final WriteThread wt2 = new WriteThread();
    wt2.start(); // wt2 immediatelly allocates strong access and is block
    // since wt1 already yields it.
    synchronized (this) {
      this.wait(1000);
    }
    // Assert.assertEquals(wt2.getState(), Thread.State.BLOCKED);

    // wt1 request strong access.
    synchronized (wt1) {
      wt1.notify();
    }
    // it is blocked as rt1 yet yields it.
    Assert.assertEquals(wt1.getState(), Thread.State.BLOCKED);

    // rt1 is waiting (due to pause()).
    Assert.assertEquals(rt1.getState(), Thread.State.WAITING);
    synchronized (rt1) {
      rt1.notify();
    }
    synchronized (this) {
      this.wait(1000);
    }
    // rt1 was notified an terminated, releasing the weak acccess.
    // so wt1 acquires strong access and pauses the second time.
    Assert.assertEquals(rt1.getState(), Thread.State.TERMINATED);

    synchronized (wt1) {
      wt1.notify();
    }
    synchronized (this) {
      this.wait(1000);
    }
    // wt2 allocates strong access as wt1 released it now.
    Assert.assertEquals(wt1.getState(), Thread.State.TERMINATED);
    Assert.assertEquals(wt2.getState(), Thread.State.WAITING);

    // while wt2 has not yet acquired strong access, rt3 acquires weak
    // access
    final ReadThread rt3 = new ReadThread();
    rt3.start();
    synchronized (this) {
      this.wait(1000);
    }
    Assert.assertEquals(rt3.getState(), Thread.State.WAITING);

    synchronized (wt2) {
      wt2.notify();
    }
    synchronized (this) {
      this.wait(1000);
    }
    // Assert.assertEquals(wt2.getState(), Thread.State.BLOCKED);

    synchronized (rt3) {
      rt3.notify();
    }
    synchronized (this) {
      this.wait(1000);
    }
    Assert.assertEquals(rt3.getState(), Thread.State.TERMINATED);
    Assert.assertEquals(wt2.getState(), Thread.State.WAITING);

    synchronized (wt2) {
      wt2.notify();
    }
    synchronized (this) {
      this.wait(1000);
    }
    Assert.assertEquals(wt2.getState(), Thread.State.TERMINATED);
  }

  @Test
  public void test2() throws InterruptedException {

    // start a write-thread
    final WriteThread wt1 = new WriteThread();
    wt1.start();
    // start another write-thread. It is blocked until wt1 releases the
    // strong access.
    final WriteThread wt2 = new WriteThread();
    wt2.start();
    synchronized (this) {
      this.wait(1000);
    }

    // and interrupt wt1 after allocating, but before acquiring the strong
    // access.
    wt1.interrupt();

    synchronized (this) {
      this.wait(1000);
    }
    synchronized (wt2) {
      wt2.notify();
    }

    // read access should still be blocked.
    final ReadThread rt1 = new ReadThread();
    rt1.start();

    synchronized (this) {
      this.wait(1000);
    }
    synchronized (wt2) {
      wt2.notify();
    }
  }

  class WriteThread extends Thread {
    @Override
    public void run() {
      try {
        System.out.println("T" + currentThread().getId() + " request allocation sa");
        sa.allocate();
        System.out.println("T" + currentThread().getId() + " allocates sa");
        pause();
        sa.lockInterruptibly();
        System.out.println("T" + currentThread().getId() + " acquires sa");
        pause();
        sa.unlock();
        System.out.println("T" + currentThread().getId() + " releases sa");
      } catch (final InterruptedException e) {
        System.out.println("T" + currentThread().getId() + " was interrupted");
        sa.unlock();
      }
    }

    private synchronized void pause() throws InterruptedException {
      this.wait();
    }
  }

  class ReadThread extends Thread {
    @Override
    public void run() {
      try {
        System.out.println("T" + currentThread().getId() + " requests wa");
        wa.acquire();
        System.out.println("T" + currentThread().getId() + " acquires wa");
        pause();
        wa.release();
        System.out.println("T" + currentThread().getId() + " releases wa");
      } catch (final InterruptedException e) {
        e.printStackTrace();
      }
    }

    private synchronized void pause() throws InterruptedException {
      this.wait();
    }
  }
}
