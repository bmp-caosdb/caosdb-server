/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import caosdb.server.database.misc.TransactionBenchmark;
import caosdb.server.jobs.core.CheckFileStorageConsistency;
import caosdb.server.utils.CronJob;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.jcs.JCS;
import org.apache.commons.jcs.access.CacheAccess;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.jvnet.libpam.PAMException;
import org.jvnet.libpam.UnixUser;

public class Misc {
  @BeforeClass
  public static void initServerProperties() throws IOException {
    CaosDBServer.initServerProperties();
  }

  @Test
  public void testUnixUser() throws PAMException {
    final UnixUser u = new UnixUser("root");
    Assert.assertEquals("/root", u.getDir());
    Assert.assertNotNull(u.getUserName());
    Assert.assertNotNull(u.getGroups());
    Assert.assertEquals(0, u.getUID());

    Assert.assertFalse(UnixUser.exists("non-existing-user"));
  }

  @Test
  public void test1() {
    final String s1 = "100:200";
    final String s2 = "100";
    final Pattern p = Pattern.compile("(\\d+)(?::(\\d+))?");
    final Matcher m1 = p.matcher(s1);
    final Matcher m2 = p.matcher(s2);
    Assert.assertTrue(m1.matches());
    assertTrue(m2.matches());
    Assert.assertEquals(2, m1.groupCount());
    Assert.assertEquals("100", m1.group(1));
    Assert.assertEquals("200", m1.group(2));
    Assert.assertEquals(2, m2.groupCount());
    assertEquals("100", m2.group(1));
    assertNull(m2.group(2));

    final String s3 = ":200";
    final Matcher m3 = p.matcher(s3);
    assertFalse(m3.matches());
  }

  @Test
  public void testParseArgs() {
    final Pattern parseargs = CheckFileStorageConsistency.parseArgs;

    Matcher matcher = parseargs.matcher("-t 1000");
    assertTrue(matcher.find());
    assertEquals("1000", matcher.group(1));
    assertFalse(matcher.find());

    matcher = parseargs.matcher("-t 1000 ");
    assertTrue(matcher.find());
    assertEquals("1000", matcher.group(1));
    assertFalse(matcher.find());

    matcher = parseargs.matcher(" -t 1000 ");
    assertTrue(matcher.find());
    assertEquals("1000", matcher.group(1));
    assertFalse(matcher.find());

    matcher = parseargs.matcher("-t 1000 -c SOMETEST");
    assertTrue(matcher.find());
    assertEquals("1000", matcher.group(1));
    assertNull(matcher.group(2));
    assertTrue(matcher.find());
    assertEquals("SOMETEST", matcher.group(2));
    assertNull(matcher.group(1));
    assertFalse(matcher.find());

    matcher = parseargs.matcher("-cSOMETEST");
    assertTrue(matcher.find());
    assertEquals("SOMETEST", matcher.group(2));
    assertNull(matcher.group(1));
    assertFalse(matcher.find());

    matcher = parseargs.matcher("-t 12000/ExperimentalData");
    assertTrue(matcher.find());
    assertEquals("-t 12000", matcher.group(0));
    assertEquals("12000", matcher.group(1));
    assertNull(matcher.group(2));
    assertNull(matcher.group(3));
    assertTrue(matcher.find());
    assertEquals("/ExperimentalData", matcher.group(0));
    assertNull(matcher.group(1));
    assertNull(matcher.group(2));
    assertFalse(matcher.find());
  }

  @Test
  public void asdf() {

    String v = "3.0m";
    final Pattern dp = Pattern.compile("([0-9]+(?:\\.[0-9]+)?)(.*)");
    Matcher m = dp.matcher(v);
    m.matches();
    String vDoubleStr = m.group(1);
    String unitStr = m.group(2);

    assertEquals("3.0", vDoubleStr);
    assertEquals("m", unitStr);

    v = "3.0";
    m = dp.matcher(v);
    m.matches();
    vDoubleStr = m.group(1);
    unitStr = m.group(2);

    assertEquals("3.0", vDoubleStr);
    assertEquals("", unitStr);

    v = "3";
    m = dp.matcher(v);
    m.matches();
    vDoubleStr = m.group(1);
    unitStr = m.group(2);

    assertEquals("3", vDoubleStr);
    assertEquals("", unitStr);

    v = "3m";
    m = dp.matcher(v);
    m.matches();
    vDoubleStr = m.group(1);
    unitStr = m.group(2);

    assertEquals("3", vDoubleStr);
    assertEquals("m", unitStr);
  }

  @Test
  public void testBla() {
    String v = "3140m";
    final Pattern dp = Pattern.compile("^(-?[0-9]++)([^(\\.[0-9])-][^-]*)?$");
    Matcher m = dp.matcher(v);
    assertTrue(m.matches());

    assertEquals("3140", m.group(1));
    assertEquals("m", m.group(2));

    v = "3140.0m";
    m = dp.matcher(v);
    assertFalse(m.matches());

    v = "2015-02";
    m = dp.matcher(v);
    assertFalse(m.matches());

    v = "2015";
    m = dp.matcher(v);
    assertTrue(m.matches());
    assertEquals("2015", m.group(1));
    assertEquals(null, m.group(2));
  }

  @Test
  public void testCache() throws IOException {
    final Properties p = new Properties();
    p.load(
        new FileInputStream(
            System.getProperty("user.dir") + "/src/test/java/caosdb/server/cache.ccf"));
    JCS.setConfigProperties(p);

    final CacheAccess<Object, Object> cache = JCS.getInstance("default");
    JCS.getInstance("default");

    final String key = "KEY";
    final String value = "VALUE";

    cache.put(key, value);

    assertEquals(value, cache.get(key));
    assertSame(value, cache.get(key));
  }

  @Test
  public void testCrobJob() throws InterruptedException {
    new CronJob(
        "2seconds",
        new Runnable() {

          private long last = 0;

          @Override
          public void run() {
            final long now = System.currentTimeMillis();
            System.out.println(
                "2seconds "
                    + Long.toString(this.last)
                    + " "
                    + Long.toString(now)
                    + " "
                    + Long.toString(now - this.last));
            this.last = now;
          }
        },
        2);

    new CronJob(
        "4seconds",
        new Runnable() {

          private long last = 0;

          @Override
          public void run() {
            final long now = System.currentTimeMillis();
            System.out.println(
                "4seconds "
                    + Long.toString(this.last)
                    + " "
                    + Long.toString(now)
                    + " "
                    + Long.toString(now - this.last));
            this.last = now;
          }
        },
        4);

    new CronJob(
        "10seconds",
        new Runnable() {

          private long last = 0;

          @Override
          public void run() {
            final long now = System.currentTimeMillis();
            System.out.println(
                "10seconds "
                    + Long.toString(this.last)
                    + " "
                    + Long.toString(now)
                    + " "
                    + Long.toString(now - this.last));
            this.last = now;
          }
        },
        10);

    Thread.sleep(10010);
  }

  @Test
  public void benchmarkSerialization() throws IOException, ClassNotFoundException {
    TransactionBenchmark.getInstance().addBenchmark("bla", 1000);

    final FileOutputStream fileOut = new FileOutputStream("/tmp/benchmark.java.ser");
    final ObjectOutputStream out = new ObjectOutputStream(fileOut);
    out.writeObject(TransactionBenchmark.getInstance());
    out.close();
    fileOut.close();
    System.out.printf("Serialized data is saved in /tmp/benchmark.java.ser");

    final FileInputStream fileIn = new FileInputStream("/tmp/benchmark.java.ser");
    final ObjectInputStream in = new ObjectInputStream(fileIn);
    final TransactionBenchmark e = (TransactionBenchmark) in.readObject();
    in.close();
    fileIn.close();

    new File("/tmp/benchmark.java.ser").deleteOnExit();

    System.out.println(e.toString());
  }

  @Test
  public void testShiro() {
    final Factory<SecurityManager> factory = new IniSecurityManagerFactory();

    final SecurityManager securityManager = factory.getInstance();

    SecurityUtils.setSecurityManager(securityManager);

    final Subject subject = SecurityUtils.getSubject();

    final Subject subject2 = SecurityUtils.getSubject();

    assertEquals(subject, subject2);
    assertTrue(subject == subject2);
  }
}
