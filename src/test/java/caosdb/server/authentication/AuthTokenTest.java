/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.authentication;

import caosdb.server.CaosDBServer;
import caosdb.server.accessControl.AuthenticationUtils;
import caosdb.server.accessControl.OneTimeAuthenticationToken;
import caosdb.server.accessControl.Principal;
import caosdb.server.accessControl.SessionToken;
import java.io.IOException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class AuthTokenTest {

  @BeforeClass
  public static void initServerProperties() throws IOException {
    CaosDBServer.initServerProperties();
  }

  @Test
  public void testSessionToken() throws InterruptedException {
    final String curry = "somecurry";
    final SessionToken t1 =
        new SessionToken(
            new Principal("somerealm", "someuser1"),
            System.currentTimeMillis(),
            60000,
            "345sdf56sdf",
            curry,
            "wrong checksum");
    Assert.assertFalse(t1.isExpired());
    Assert.assertFalse(t1.isHashValid());
    Assert.assertFalse(t1.isValid());

    final SessionToken t2 =
        new SessionToken(
            new Principal("somerealm", "someuser1"),
            System.currentTimeMillis(),
            60000,
            "345sdf56sdf",
            null,
            "wrong checksum");
    Assert.assertFalse(t2.isExpired());
    Assert.assertFalse(t2.isHashValid());
    Assert.assertFalse(t2.isValid());

    final SessionToken t3 =
        new SessionToken(
            new Principal("somerealm", "someuser2"),
            System.currentTimeMillis(),
            60000,
            "72723gsdg",
            curry);
    Assert.assertFalse(t3.isExpired());
    Assert.assertTrue(t3.isHashValid());
    Assert.assertTrue(t3.isValid());

    final SessionToken t4 =
        new SessionToken(
            new Principal("somerealm", "someuser2"),
            System.currentTimeMillis(),
            60000,
            "72723gsdg",
            null);
    Assert.assertFalse(t4.isExpired());
    Assert.assertTrue(t4.isHashValid());
    Assert.assertTrue(t4.isValid());

    final SessionToken t5 =
        new SessionToken(
            new Principal("somerealm", "someuser3"),
            System.currentTimeMillis(),
            2000,
            "23sdfsg34",
            curry);
    Assert.assertFalse(t5.isExpired());
    Assert.assertTrue(t5.isHashValid());
    Assert.assertTrue(t5.isValid());
    // wait until expired
    Thread.sleep(2001);
    Assert.assertTrue(t5.isExpired());
    Assert.assertTrue(t5.isHashValid());
    Assert.assertFalse(t5.isValid());

    final SessionToken t6 =
        new SessionToken(
            new Principal("somerealm", "someuser3"),
            System.currentTimeMillis(),
            0,
            "23sdfsg34",
            null);
    Assert.assertTrue(t6.isExpired());
    Assert.assertTrue(t6.isHashValid());
    Assert.assertFalse(t6.isValid());

    Assert.assertEquals(t1.toString(), SessionToken.parse(t1.toString(), curry).toString());
    Assert.assertEquals(t2.toString(), SessionToken.parse(t2.toString(), curry).toString());
    Assert.assertEquals(t3.toString(), SessionToken.parse(t3.toString(), curry).toString());
    Assert.assertEquals(t4.toString(), SessionToken.parse(t4.toString(), curry).toString());
    Assert.assertEquals(t5.toString(), SessionToken.parse(t5.toString(), curry).toString());
    Assert.assertEquals(t6.toString(), SessionToken.parse(t6.toString(), curry).toString());
    Assert.assertEquals(t1.toString(), SessionToken.parse(t1.toString(), null).toString());
    Assert.assertEquals(t2.toString(), SessionToken.parse(t2.toString(), null).toString());
    Assert.assertEquals(t3.toString(), SessionToken.parse(t3.toString(), null).toString());
    Assert.assertEquals(t4.toString(), SessionToken.parse(t4.toString(), null).toString());
    Assert.assertEquals(t5.toString(), SessionToken.parse(t5.toString(), null).toString());
    Assert.assertEquals(t6.toString(), SessionToken.parse(t6.toString(), null).toString());

    Assert.assertFalse(SessionToken.parse(t1.toString(), curry).isHashValid());
    Assert.assertFalse(SessionToken.parse(t2.toString(), null).isHashValid());
    Assert.assertTrue(SessionToken.parse(t3.toString(), curry).isHashValid());
    Assert.assertTrue(SessionToken.parse(t4.toString(), null).isHashValid());
    Assert.assertTrue(SessionToken.parse(t5.toString(), curry).isHashValid());
    Assert.assertTrue(SessionToken.parse(t6.toString(), null).isHashValid());
    Assert.assertFalse(SessionToken.parse(t1.toString(), null).isHashValid());
    Assert.assertFalse(SessionToken.parse(t2.toString(), curry).isHashValid());
    Assert.assertFalse(SessionToken.parse(t3.toString(), null).isHashValid());
    Assert.assertFalse(SessionToken.parse(t4.toString(), curry).isHashValid());
    Assert.assertFalse(SessionToken.parse(t5.toString(), null).isHashValid());
    Assert.assertFalse(SessionToken.parse(t6.toString(), curry).isHashValid());

    Assert.assertFalse(
        AuthenticationUtils.parseSessionTokenCookie(
                AuthenticationUtils.createSessionTokenCookie(t4), null)
            .isExpired());
    Assert.assertTrue(
        AuthenticationUtils.parseSessionTokenCookie(
                AuthenticationUtils.createSessionTokenCookie(t4), null)
            .isHashValid());
    Assert.assertTrue(
        AuthenticationUtils.parseSessionTokenCookie(
                AuthenticationUtils.createSessionTokenCookie(t4), null)
            .isValid());
    Assert.assertFalse(
        AuthenticationUtils.parseSessionTokenCookie(
                AuthenticationUtils.createSessionTokenCookie(t4), curry)
            .isExpired());
    Assert.assertFalse(
        AuthenticationUtils.parseSessionTokenCookie(
                AuthenticationUtils.createSessionTokenCookie(t4), curry)
            .isHashValid());
    Assert.assertFalse(
        AuthenticationUtils.parseSessionTokenCookie(
                AuthenticationUtils.createSessionTokenCookie(t4), curry)
            .isValid());
    Assert.assertFalse(
        AuthenticationUtils.parseSessionTokenCookie(
                AuthenticationUtils.createSessionTokenCookie(t3), null)
            .isExpired());
    Assert.assertFalse(
        AuthenticationUtils.parseSessionTokenCookie(
                AuthenticationUtils.createSessionTokenCookie(t3), null)
            .isHashValid());
    Assert.assertFalse(
        AuthenticationUtils.parseSessionTokenCookie(
                AuthenticationUtils.createSessionTokenCookie(t3), null)
            .isValid());
    Assert.assertFalse(
        AuthenticationUtils.parseSessionTokenCookie(
                AuthenticationUtils.createSessionTokenCookie(t3), curry)
            .isExpired());
    Assert.assertTrue(
        AuthenticationUtils.parseSessionTokenCookie(
                AuthenticationUtils.createSessionTokenCookie(t3), curry)
            .isHashValid());
    Assert.assertTrue(
        AuthenticationUtils.parseSessionTokenCookie(
                AuthenticationUtils.createSessionTokenCookie(t3), curry)
            .isValid());
  }

  @Test
  public void testOneTimeToken() {
    final String curry = null;
    final OneTimeAuthenticationToken t1 =
        new OneTimeAuthenticationToken(
            new Principal("somerealm", "someuser"),
            System.currentTimeMillis(),
            60000L,
            "sdfh37456sd",
            curry,
            new String[] {""});
    System.err.println(t1.toString());
    Assert.assertFalse(t1.isExpired());
    Assert.assertTrue(t1.isHashValid());
    Assert.assertTrue(t1.isValid());

    Assert.assertEquals(
        t1.toString(), OneTimeAuthenticationToken.parse(t1.toString(), curry).toString());
    Assert.assertFalse(OneTimeAuthenticationToken.parse(t1.toString(), curry).isExpired());
    Assert.assertTrue(OneTimeAuthenticationToken.parse(t1.toString(), curry).isHashValid());
    Assert.assertTrue(OneTimeAuthenticationToken.parse(t1.toString(), curry).isValid());
  }
}
