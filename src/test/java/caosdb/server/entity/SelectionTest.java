/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.entity;

import caosdb.server.entity.xml.SetFieldStrategy;
import caosdb.server.query.Query.Selection;
import org.junit.Assert;
import org.junit.Test;

public class SelectionTest {

  @Test
  public void testEmpty1() {
    final SetFieldStrategy setFieldStrategy = new SetFieldStrategy();

    Assert.assertTrue(setFieldStrategy.isToBeSet("id"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("name"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("description"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("blabla"));

    setFieldStrategy.addSelection(null);

    Assert.assertTrue(setFieldStrategy.isToBeSet("id"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("name"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("description"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("blabla"));
  }

  @Test
  public void testName1() {
    final Selection selection = new Selection("name");
    final SetFieldStrategy setFieldStrategy = (new SetFieldStrategy()).addSelection(selection);

    Assert.assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testName2() {
    final Selection selection = new Selection("id");
    final SetFieldStrategy setFieldStrategy = (new SetFieldStrategy()).addSelection(selection);

    Assert.assertFalse(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testName3() {
    final Selection selection = new Selection("value");
    final SetFieldStrategy setFieldStrategy = (new SetFieldStrategy()).addSelection(selection);

    Assert.assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testName4() {
    final Selection selection = new Selection("description");
    final SetFieldStrategy setFieldStrategy = (new SetFieldStrategy()).addSelection(selection);

    Assert.assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testName5() {
    final Selection selection = new Selection("datatype");
    final SetFieldStrategy setFieldStrategy = (new SetFieldStrategy()).addSelection(selection);

    Assert.assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testName6() {
    final Selection selection = new Selection("datatype");
    final SetFieldStrategy setFieldStrategy = (new SetFieldStrategy()).addSelection(selection);

    Assert.assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testName7() {
    final Selection selection = new Selection("blabla");
    final SetFieldStrategy setFieldStrategy = (new SetFieldStrategy()).addSelection(selection);

    Assert.assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testId1() {
    final Selection selection = new Selection("id");
    final SetFieldStrategy setFieldStrategy = (new SetFieldStrategy()).addSelection(selection);

    Assert.assertTrue(setFieldStrategy.isToBeSet("id"));
  }

  @Test
  public void testId2() {
    final Selection selection = new Selection("name");
    final SetFieldStrategy setFieldStrategy = (new SetFieldStrategy()).addSelection(selection);

    Assert.assertTrue(setFieldStrategy.isToBeSet("id"));
  }

  @Test
  public void testId3() {
    final Selection selection = new Selection("description");
    final SetFieldStrategy setFieldStrategy = (new SetFieldStrategy()).addSelection(selection);

    Assert.assertTrue(setFieldStrategy.isToBeSet("id"));
  }

  @Test
  public void testId4() {
    final Selection selection = new Selection("blablabla");
    final SetFieldStrategy setFieldStrategy = (new SetFieldStrategy()).addSelection(selection);

    Assert.assertTrue(setFieldStrategy.isToBeSet("id"));
  }

  @Test
  public void testDesc1() {
    final Selection selection = new Selection("description");
    final SetFieldStrategy setFieldStrategy = (new SetFieldStrategy()).addSelection(selection);

    Assert.assertTrue(setFieldStrategy.isToBeSet("description"));
  }

  @Test
  public void testDesc2() {
    final Selection selection = new Selection("name");
    final SetFieldStrategy setFieldStrategy = (new SetFieldStrategy()).addSelection(selection);

    Assert.assertFalse(setFieldStrategy.isToBeSet("description"));
  }

  @Test
  public void testDesc3() {
    final Selection selection = new Selection("id");
    final SetFieldStrategy setFieldStrategy = (new SetFieldStrategy()).addSelection(selection);

    Assert.assertFalse(setFieldStrategy.isToBeSet("description"));
  }

  @Test
  public void testDesc4() {
    final Selection selection = new Selection("blablaba");
    final SetFieldStrategy setFieldStrategy = (new SetFieldStrategy()).addSelection(selection);

    Assert.assertFalse(setFieldStrategy.isToBeSet("description"));
  }

  @Test
  public void testMulti1() {
    final SetFieldStrategy setFieldStrategy =
        (new SetFieldStrategy())
            .addSelection(new Selection("id"))
            .addSelection(new Selection("name"));

    Assert.assertFalse(setFieldStrategy.isToBeSet("blablabla"));
    Assert.assertFalse(setFieldStrategy.isToBeSet("description"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("id"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testMulti2() {
    final SetFieldStrategy setFieldStrategy =
        (new SetFieldStrategy())
            .addSelection(new Selection("id"))
            .addSelection(new Selection("description"));

    Assert.assertFalse(setFieldStrategy.isToBeSet("blablabla"));
    Assert.assertFalse(setFieldStrategy.isToBeSet("datatype"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("description"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("id"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testMulti3() {
    final SetFieldStrategy setFieldStrategy =
        (new SetFieldStrategy())
            .addSelection(new Selection("datatype"))
            .addSelection(new Selection("description"));

    Assert.assertFalse(setFieldStrategy.isToBeSet("blablabla"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("datatype"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("description"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("id"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testMulti4() {
    final SetFieldStrategy setFieldStrategy =
        (new SetFieldStrategy())
            .addSelection(new Selection("datatype"))
            .addSelection(new Selection("value"));

    Assert.assertFalse(setFieldStrategy.isToBeSet("blablabla"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("datatype"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("value"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("id"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testComposition1() {
    final SetFieldStrategy setFieldStrategy =
        (new SetFieldStrategy()).addSelection(new Selection("blabla"));

    Assert.assertTrue(setFieldStrategy.isToBeSet("blabla"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("id"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("name"));
    Assert.assertFalse(setFieldStrategy.isToBeSet("bleb"));

    final SetFieldStrategy forProperty = setFieldStrategy.forProperty("blabla");
    Assert.assertTrue(forProperty.isToBeSet("id"));
    Assert.assertTrue(forProperty.isToBeSet("name"));
    Assert.assertTrue(forProperty.isToBeSet("blub"));
  }

  @Test
  public void testComposition2() {
    final SetFieldStrategy setFieldStrategy =
        new SetFieldStrategy()
            .addSelection(new Selection("blabla"))
            .addSelection(new Selection("blabla.name"));

    Assert.assertTrue(setFieldStrategy.isToBeSet("blabla"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("id"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("name"));
    Assert.assertFalse(setFieldStrategy.isToBeSet("bleb"));

    final SetFieldStrategy forProperty = setFieldStrategy.forProperty("blabla");
    Assert.assertTrue(forProperty.isToBeSet("id"));
    Assert.assertTrue(forProperty.isToBeSet("name"));
    Assert.assertTrue(forProperty.isToBeSet("blub"));
  }

  @Test
  public void testComposition3() {
    final SetFieldStrategy setFieldStrategy =
        new SetFieldStrategy()
            .addSelection(new Selection("blabla").setSubSelection(new Selection("value")))
            .addSelection(new Selection("blabla").setSubSelection(new Selection("description")));

    Assert.assertTrue(setFieldStrategy.isToBeSet("blabla"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("id"));
    Assert.assertTrue(setFieldStrategy.isToBeSet("name"));
    Assert.assertFalse(setFieldStrategy.isToBeSet("bleb"));

    final SetFieldStrategy forProperty = setFieldStrategy.forProperty("blabla");
    Assert.assertTrue(forProperty.isToBeSet("id"));
    Assert.assertTrue(forProperty.isToBeSet("name"));
    Assert.assertTrue(forProperty.isToBeSet("description"));
    Assert.assertTrue(forProperty.isToBeSet("value"));
    Assert.assertFalse(forProperty.isToBeSet("blub"));
  }
}
