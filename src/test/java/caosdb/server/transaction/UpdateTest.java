/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.transaction;

import static caosdb.server.utils.EntityStatus.QUALIFIED;
import static caosdb.server.utils.EntityStatus.VALID;
import static org.junit.Assert.assertEquals;

import caosdb.server.CaosDBException;
import caosdb.server.datatype.GenericValue;
import caosdb.server.entity.Entity;
import caosdb.server.entity.EntityInterface;
import caosdb.server.entity.StatementStatus;
import caosdb.server.entity.wrapper.Property;
import caosdb.server.utils.EntityStatus;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import org.junit.Test;

public class UpdateTest {

  @Test
  public void testDeriveUpdate_SameName()
      throws NoSuchAlgorithmException, IOException, CaosDBException {
    final Entity newEntity = new Entity("Name");
    final Entity oldEntity = new Entity("Name");
    Update.deriveUpdate(newEntity, oldEntity);
    assertEquals(newEntity.getEntityStatus(), EntityStatus.VALID);
  }

  @Test
  public void testDeriveUpdate_DifferentName()
      throws NoSuchAlgorithmException, IOException, CaosDBException {
    final Entity newEntity = new Entity("NewName");
    final Entity oldEntity = new Entity("OldName");
    Update.deriveUpdate(newEntity, oldEntity);
    assertEquals(newEntity.getEntityStatus(), EntityStatus.QUALIFIED);
  }

  @Test
  public void testDeriveUpdate_SameProperty()
      throws NoSuchAlgorithmException, IOException, CaosDBException {
    final Entity newEntity = new Entity();
    final Property newProperty = new Property(1);
    newEntity.addProperty(newProperty);
    final Property oldProperty = new Property(1);
    final Entity oldEntity = new Entity();
    oldEntity.addProperty(oldProperty);

    Update.deriveUpdate(newEntity, oldEntity);
    assertEquals(newEntity.getEntityStatus(), VALID);
  }

  @Test
  public void testDeriveUpdate_AnotherProperty()
      throws NoSuchAlgorithmException, IOException, CaosDBException {
    final Entity newEntity = new Entity();
    final Property newProperty = new Property(1);
    final Property newProperty2 = new Property(2);
    newEntity.addProperty(newProperty);
    newEntity.addProperty(newProperty2);
    final Property oldProperty = new Property(1);
    final Entity oldEntity = new Entity();
    oldEntity.addProperty(oldProperty);

    Update.deriveUpdate(newEntity, oldEntity);
    assertEquals(newEntity.getEntityStatus(), QUALIFIED);
    assertEquals(newProperty.getEntityStatus(), VALID);
    assertEquals(newProperty2.getEntityStatus(), QUALIFIED);
  }

  @Test
  public void testDeriveUpdate_SameUnit()
      throws NoSuchAlgorithmException, IOException, CaosDBException {
    final EntityInterface magicUnit = new Entity("Unit");
    magicUnit.setId(24);
    magicUnit.setDatatype("TEXT");

    final Entity newEntity = new Entity();
    final Property newProperty = new Property(1);

    final Property newUnit = new Property();
    newUnit.setName(magicUnit.getName());
    newUnit.setId(magicUnit.getId());
    newUnit.setDatatype(magicUnit.getDatatype());
    newUnit.setStatementStatus(StatementStatus.FIX);
    newUnit.setValue(new GenericValue("m"));
    newUnit.setEntityStatus(EntityStatus.QUALIFIED);
    newProperty.addProperty(newUnit);

    newEntity.addProperty(newProperty);

    final Entity oldEntity = new Entity();
    final Property oldProperty = new Property(1);

    final Property oldUnit = new Property();
    oldUnit.setName(magicUnit.getName());
    oldUnit.setId(magicUnit.getId());
    oldUnit.setDatatype(magicUnit.getDatatype());
    oldUnit.setStatementStatus(StatementStatus.FIX);
    oldUnit.setValue(new GenericValue("m"));
    oldUnit.setEntityStatus(EntityStatus.QUALIFIED);
    oldProperty.addProperty(oldUnit);

    oldEntity.addProperty(oldProperty);

    Update.deriveUpdate(newEntity, oldEntity);
    assertEquals(newUnit.getEntityStatus(), VALID);
    assertEquals(newProperty.getEntityStatus(), VALID);
    assertEquals(newEntity.getEntityStatus(), VALID);
  }

  @Test
  public void testDeriveUpdate_DifferentUnit()
      throws NoSuchAlgorithmException, IOException, CaosDBException {
    final EntityInterface magicUnit = new Entity("Unit");
    magicUnit.setId(24);
    magicUnit.setDatatype("TEXT");

    final Entity newEntity = new Entity();
    final Property newProperty = new Property(1);

    final Property newUnit = new Property();
    newUnit.setName(magicUnit.getName());
    newUnit.setId(magicUnit.getId());
    newUnit.setDatatype(magicUnit.getDatatype());
    newUnit.setStatementStatus(StatementStatus.FIX);
    newUnit.setValue(new GenericValue("m"));
    newUnit.setEntityStatus(EntityStatus.QUALIFIED);
    newProperty.addProperty(newUnit);

    newEntity.addProperty(newProperty);

    final Entity oldEntity = new Entity();
    final Property oldProperty = new Property(1);

    final Property oldUnit = new Property();
    oldUnit.setName(magicUnit.getName());
    oldUnit.setId(magicUnit.getId());
    oldUnit.setDatatype(magicUnit.getDatatype());
    oldUnit.setStatementStatus(StatementStatus.FIX);
    oldUnit.setValue(new GenericValue("s"));
    oldUnit.setEntityStatus(EntityStatus.QUALIFIED);
    oldProperty.addProperty(oldUnit);

    oldEntity.addProperty(oldProperty);

    Update.deriveUpdate(newEntity, oldEntity);
    assertEquals(newEntity.getEntityStatus(), QUALIFIED);
    assertEquals(newProperty.getEntityStatus(), QUALIFIED);
  }
}
