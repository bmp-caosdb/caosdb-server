/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.permissions;

import static org.junit.Assert.assertNotNull;

import caosdb.server.CaosDBServer;
import caosdb.server.resource.AbstractCaosDBServerResource;
import caosdb.server.resource.AbstractCaosDBServerResource.XMLParser;
import caosdb.server.utils.Utils;
import java.io.IOException;
import java.util.BitSet;
import java.util.LinkedList;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class EntityACLTest {

  public static long convert(final BitSet bits) {
    long value = 0L;
    for (int i = 0; i < bits.length(); ++i) {
      value += bits.get(i) ? (1L << i) : 0L;
    }
    return value;
  }

  @BeforeClass
  public static void init() throws IOException {
    CaosDBServer.initServerProperties();
    assertNotNull(EntityACL.GLOBAL_PERMISSIONS);
  }

  @Test
  public void testConvert() {
    long l = Integer.MAX_VALUE;
    System.out.println(EntityACL.convert(l));
    l = Integer.MIN_VALUE;
    System.out.println(EntityACL.convert(l));
    l = Long.MAX_VALUE;
    System.out.println(EntityACL.convert(l));
    l = Long.MIN_VALUE;
    System.out.println(EntityACL.convert(l));
    final BitSet bitSet = new BitSet(64);
    Assert.assertEquals(0, convert(bitSet));

    bitSet.set(0);
    Assert.assertEquals(1, convert(bitSet));

    bitSet.clear();
    bitSet.set(63);
    Assert.assertEquals(Long.MIN_VALUE, convert(bitSet));

    bitSet.clear();
    bitSet.set(0, 63, true);
    Assert.assertEquals(Long.MAX_VALUE, convert(bitSet));

    bitSet.clear();
    bitSet.set(62);
    Assert.assertEquals(EntityACL.MIN_PRIORITY_BITSET, convert(bitSet));

    bitSet.set(63);
    Assert.assertEquals(-EntityACL.MIN_PRIORITY_BITSET, convert(bitSet));

    bitSet.clear();
    bitSet.set(0, 64, true);
    Assert.assertEquals(-1L, convert(bitSet));

    Assert.assertTrue(EntityACL.convert(-12523).get(63));
    Assert.assertFalse(EntityACL.convert(12523).get(63));

    bitSet.clear();
    bitSet.set(0, 63, true);
    Assert.assertEquals(Long.MAX_VALUE + 1L, Long.MIN_VALUE);
    Assert.assertEquals(Long.MAX_VALUE, convert(bitSet));
  }

  @Test
  public void testIsDenial() {
    final BitSet bitSet = new BitSet(64);
    bitSet.set(63);

    Assert.assertTrue(EntityACL.isDenial(convert(bitSet)));
    Assert.assertTrue(EntityACL.isDenial(-12523));
    Assert.assertFalse(EntityACL.isDenial(0));
    Assert.assertFalse(EntityACL.isDenial(246345));
    Assert.assertTrue(EntityACL.isDenial(-EntityACL.MIN_PRIORITY_BITSET));
  }

  @Test
  public void testIsAllowance() {
    final BitSet bitSet = new BitSet(32);
    bitSet.set(27);

    Assert.assertTrue(EntityACL.isAllowance(convert(bitSet)));
    Assert.assertTrue(EntityACL.isAllowance(12523));
    Assert.assertTrue(EntityACL.isAllowance(0));
    Assert.assertFalse(EntityACL.isAllowance(-246345));
    Assert.assertTrue(EntityACL.isAllowance(EntityACL.MIN_PRIORITY_BITSET));
  }

  @Test
  public void testGetResultingACL() {
    final LinkedList<Long> acl = new LinkedList<Long>();
    acl.add(1L); // +P1
    Assert.assertEquals(1L, EntityACL.getResultingACL(acl)); // P1

    acl.add(2L); // +P2
    Assert.assertEquals(3L, EntityACL.getResultingACL(acl)); // P1,P2

    acl.add(4L); // +P3
    Assert.assertEquals(7L, EntityACL.getResultingACL(acl)); // P1,P2,P3

    acl.add(Long.MIN_VALUE + 1); // -P1
    Assert.assertEquals(6L, EntityACL.getResultingACL(acl)); // P2,P3

    acl.add(Long.MIN_VALUE + 1); // -P1Allow
    Assert.assertEquals(6L, EntityACL.getResultingACL(acl)); // P2,P3

    acl.add(Long.MIN_VALUE + 8); // -P4
    Assert.assertEquals(6L, EntityACL.getResultingACL(acl)); // P2,P3

    acl.add(8L); // +P4
    Assert.assertEquals(6L, EntityACL.getResultingACL(acl)); // P2,P3

    acl.add(EntityACL.MIN_PRIORITY_BITSET + 1); // ++P1
    Assert.assertEquals(7L, EntityACL.getResultingACL(acl)); // P1,P2,P3

    acl.add(EntityACL.MIN_PRIORITY_BITSET + 2); // ++P2
    Assert.assertEquals(7L, EntityACL.getResultingACL(acl)); // P1,P2,P3

    acl.add(Long.MIN_VALUE + 1); // -P1
    Assert.assertEquals(7l, EntityACL.getResultingACL(acl)); // P1,P2,P3

    acl.add(Long.MIN_VALUE + 4); // -P1
    Assert.assertEquals(3l, EntityACL.getResultingACL(acl)); // P1,P2

    acl.add(EntityACL.MIN_PRIORITY_BITSET + 8); // ++P4
    Assert.assertEquals(11L, EntityACL.getResultingACL(acl)); // P1,P2,P4

    acl.add(Long.MIN_VALUE + EntityACL.MIN_PRIORITY_BITSET + 2); // --P2
    Assert.assertEquals(9L, EntityACL.getResultingACL(acl)); // P1,P4

    acl.add(Long.MIN_VALUE + EntityACL.MIN_PRIORITY_BITSET + 1); // --P1Allow
    Assert.assertEquals(8L, EntityACL.getResultingACL(acl)); // P4
  }

  @Test
  public void testOwnerBitSet() {
    Assert.assertEquals(convert(EntityACL.convert(EntityACL.OWNER_BITSET).get(1, 32)), 0);
  }

  // @Test
  // public void testDeserialize() {
  // Assert.assertTrue(EntityACL.deserialize("{}") instanceof EntityACL);
  // Assert.assertTrue(EntityACL.deserialize("{tf:134}") instanceof
  // EntityACL);
  // Assert.assertTrue(EntityACL.deserialize("{tf:6343;bla:884}") instanceof
  // EntityACL);
  // Assert.assertTrue(EntityACL.deserialize("{tf:-2835;bla:884}") instanceof
  // EntityACL);
  // Assert.assertTrue(EntityACL.deserialize("{?OWNER?:526;tahsdh   : -235;}")
  // instanceof EntityACL);
  // Assert.assertTrue(EntityACL.deserialize("{asdf:2345;}") instanceof
  // EntityACL);
  // Assert.assertTrue(raisesIllegalArguementException("{"));
  // Assert.assertTrue(raisesIllegalArguementException("}"));
  // Assert.assertTrue(raisesIllegalArguementException("{tf:}"));
  // Assert.assertTrue(raisesIllegalArguementException("{tf:;}"));
  // Assert.assertTrue(raisesIllegalArguementException("{:234}"));
  // Assert.assertTrue(raisesIllegalArguementException("{:234;}"));
  // Assert.assertTrue(raisesIllegalArguementException("{tf:tf;}"));
  // Assert.assertTrue(raisesIllegalArguementException("{tf: +5259;}"));
  // Assert.assertTrue(raisesIllegalArguementException("{tf;}"));
  // Assert.assertTrue(raisesIllegalArguementException("{tf:123223727356235782735235;}"));
  // }

  public boolean raisesIllegalArguementException(final String input) {
    try {
      EntityACL.deserialize(input);
    } catch (final IllegalArgumentException e) {
      return true;
    }
    return false;
  }

  public static AbstractCaosDBServerResource.XMLParser parser = new XMLParser();

  public Element stringToJdom(final String s) throws JDOMException, IOException {
    return parser.parse(Utils.String2InputStream(s)).getRootElement();
  }

  // @Test
  // public void testParseFromElement() throws JDOMException, IOException {
  // Assert.assertEquals("{}",
  // EntityACL.serialize(EntityACL.parseFromElement(stringToJdom("<ACL></ACL>"))));
  // Assert.assertEquals("{}", EntityACL.serialize(EntityACL
  // .parseFromElement(stringToJdom("<ACL><Grant></Grant></ACL>"))));
  // Assert.assertEquals("{}", EntityACL.serialize(EntityACL
  // .parseFromElement(stringToJdom("<ACL><Deny></Deny></ACL>"))));
  // Assert.assertEquals("{}", EntityACL.serialize(EntityACL
  // .parseFromElement(stringToJdom("<ACL><Grant role='bla'></Grant></ACL>"))));
  // Assert.assertEquals("{}", EntityACL.serialize(EntityACL
  // .parseFromElement(stringToJdom("<ACL><Deny role='bla'></Deny></ACL>"))));
  // Assert.assertEquals(
  // "{bla:2;}",
  // EntityACL.serialize(EntityACL
  // .parseFromElement(stringToJdom("<ACL><Grant role='bla'><Permission name='DELETE'
  // /></Grant></ACL>"))));
  // Assert.assertEquals(
  // "{bla:" + (Long.MIN_VALUE + 2) + ";}",
  // EntityACL.serialize(EntityACL
  // .parseFromElement(stringToJdom("<ACL><Deny role='bla'><Permission name='DELETE'
  // /></Deny></ACL>"))));
  // Assert.assertEquals(
  // "{bla:32;}",
  // EntityACL.serialize(EntityACL
  // .parseFromElement(stringToJdom("<ACL><Grant role='bla'><Permission name='RETRIEVE:ACL'
  // /></Grant></ACL>"))));
  // }

  // @Test
  // public void testFactory() {
  // final EntityACLFactory f = new EntityACLFactory();
  // f.grant("user1", "UPDATE:NAME");
  // Assert.assertTrue((f.create().isPermitted("user1",
  // EntityPermission.UPDATE_NAME)));
  // Assert.assertFalse((f.create().isPermitted("user2",
  // EntityPermission.UPDATE_NAME)));
  // f.grant("user2", "DELETE");
  // Assert.assertFalse((f.create().isPermitted("user1",
  // EntityPermission.DELETE)));
  // Assert.assertTrue((f.create().isPermitted("user2",
  // EntityPermission.DELETE)));
  // f.deny("user2", 1);
  // f.deny("user1", 1);
  // Assert.assertFalse((f.create().isPermitted("user1",
  // EntityPermission.DELETE)));
  // Assert.assertFalse((f.create().isPermitted("user2",
  // EntityPermission.DELETE)));
  // f.grant("user1", true, 1);
  // Assert.assertTrue((f.create().isPermitted("user1",
  // EntityPermission.DELETE)));
  // Assert.assertFalse((f.create().isPermitted("user2",
  // EntityPermission.DELETE)));
  // f.deny("user2", true, 1);
  // Assert.assertTrue((f.create().isPermitted("user1",
  // EntityPermission.DELETE)));
  // Assert.assertFalse((f.create().isPermitted("user2",
  // EntityPermission.DELETE)));
  // f.grant("user2", true, 1);
  // Assert.assertTrue((f.create().isPermitted("user1",
  // EntityPermission.DELETE)));
  // Assert.assertFalse((f.create().isPermitted("user2",
  // EntityPermission.DELETE)));
  // f.deny("user1", true, 1);
  // Assert.assertFalse((f.create().isPermitted("user1",
  // EntityPermission.DELETE)));
  // Assert.assertFalse((f.create().isPermitted("user2",
  // EntityPermission.DELETE)));
  // Assert.assertTrue((f.create().isPermitted("user1",
  // EntityPermission.UPDATE_NAME)));
  // Assert.assertFalse((f.create().isPermitted("user2",
  // EntityPermission.UPDATE_NAME)));
  // }

  // @Test
  // public void niceFactoryStuff() {
  // final EntityACLFactory f = new EntityACLFactory();
  // f.grant("user1", "*");
  // final EntityACL acl1 = f.create();
  // Assert.assertTrue(acl1.isPermitted("user1", EntityPermission.EDIT_ACL));
  // Assert.assertTrue(acl1.isPermitted("user1", EntityPermission.DELETE));
  // Assert.assertTrue(acl1.isPermitted("user1",
  // EntityPermission.RETRIEVE_ENTITY));
  // Assert.assertTrue(acl1.isPermitted("user1",
  // EntityPermission.UPDATE_DATA_TYPE));
  // Assert.assertTrue(acl1.isPermitted("user1",
  // EntityPermission.USE_AS_PROPERTY));
  //
  // f.grant("?OWNER?", "DELETE", "EDIT:ACL", "RETRIEVE:*", "UPDATE:*",
  // "USE:*");
  // f.grant("user2", "EDIT:ACL");
  // final EntityACL acl2 = f.create();
  // Assert.assertTrue(acl2.isPermitted("user2", EntityPermission.EDIT_ACL));
  // Assert.assertTrue(acl2.isPermitted("user2", EntityPermission.DELETE));
  // Assert.assertTrue(acl2.isPermitted("user2",
  // EntityPermission.RETRIEVE_ENTITY));
  // Assert.assertTrue(acl2.isPermitted("user2",
  // EntityPermission.UPDATE_DATA_TYPE));
  // Assert.assertTrue(acl2.isPermitted("user2",
  // EntityPermission.USE_AS_PROPERTY));
  //
  // }

  // @Test
  // public void testDeny() {
  // EntityACLFactory f = new EntityACLFactory();
  // f.deny("test", "DELETE");
  // Assert.assertFalse(f.create().isPermitted("test",
  // EntityPermission.DELETE));
  //
  // System.out.println(Utils.element2String(f.create().toElement()));
  //
  // System.out.println(Utils.element2String(EntityACL.GLOBAL_PERMISSIONS.toElement()));
  //
  // f.grant("test", "USE:*");
  // Assert.assertFalse(f.create().isPermitted("test",
  // EntityPermission.DELETE));
  //
  // System.out.println(Utils.element2String(f.create().toElement()));
  //
  // f = new EntityACLFactory();
  // f.grant(EntityACL.OTHER_ROLE, "RETRIEVE:*");
  // f.deny(EntityACL.OTHER_ROLE, "DELETE");
  // final EntityACL a = f.create();
  //
  // System.out.println(Utils.element2String(a.toElement()));
  //
  // System.out.println(Utils.element2String(EntityACL.deserialize(a.serialize()).toElement()));
  // }

}
