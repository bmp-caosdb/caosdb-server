/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.utils.fsm;

import static org.junit.Assert.assertEquals;

import com.google.common.collect.Lists;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

class SimpleFiniteStateMachine extends FiniteStateMachine<State, Transition> {

  public SimpleFiniteStateMachine(
      final State initial, final Map<State, Map<Transition, State>> transitions)
      throws StateNotReachableException {
    super(initial, transitions);
  }
}

enum TestState implements State {
  State1,
  State2,
  State3;

  @Override
  public List<State> getAllStates() {
    return Lists.newArrayList(values());
  }
}

enum TestTransition implements Transition {
  toState2,
  toState3
}

public class TestFiniteStateMachine {

  @Rule public ExpectedException exc = ExpectedException.none();

  @Test
  public void testTransitionNotAllowedException()
      throws StateNotReachableException, TransitionNotAllowedException {
    final Map<State, Map<Transition, State>> map = new HashMap<>();
    final HashMap<Transition, State> from1 = new HashMap<>();
    from1.put(TestTransition.toState2, TestState.State2);
    from1.put(TestTransition.toState3, TestState.State3);
    map.put(TestState.State1, from1);

    final SimpleFiniteStateMachine fsm = new SimpleFiniteStateMachine(TestState.State1, map);
    assertEquals(TestState.State1, fsm.getCurrentState());
    fsm.trigger(TestTransition.toState2);
    assertEquals(TestState.State2, fsm.getCurrentState());

    // only 1->2 and from 1->3 is allowed. not 2->3
    this.exc.expect(TransitionNotAllowedException.class);
    fsm.trigger(TestTransition.toState3);
  }

  @Test
  public void testStateNotReachable() throws StateNotReachableException {
    final Map<State, Map<Transition, State>> empty = new HashMap<>();

    this.exc.expect(StateNotReachableException.class);
    new SimpleFiniteStateMachine(TestState.State1, empty);
  }
}
