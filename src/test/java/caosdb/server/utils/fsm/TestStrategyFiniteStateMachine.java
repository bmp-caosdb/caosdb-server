/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.utils.fsm;

import java.util.HashMap;
import java.util.Map;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TestStrategyFiniteStateMachine {

  @Rule public ExpectedException exc = ExpectedException.none();

  @Test
  public void testStateHasNoImplementation()
      throws MissingImplementationException, StateNotReachableException {
    final Map<State, Map<Transition, State>> map = new HashMap<>();
    final HashMap<Transition, State> from1 = new HashMap<>();
    from1.put(TestTransition.toState2, TestState.State2);
    from1.put(TestTransition.toState3, TestState.State3);
    map.put(TestState.State1, from1);

    final Map<State, Object> stateImplementations = new HashMap<>();

    this.exc.expect(MissingImplementationException.class);
    new StrategyFiniteStateMachine<State, Transition, Object>(
        TestState.State1, stateImplementations, map);
  }
}
