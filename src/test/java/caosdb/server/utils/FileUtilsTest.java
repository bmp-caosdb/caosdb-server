/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import caosdb.server.CaosDBException;
import caosdb.server.CaosDBServer;
import caosdb.server.FileSystem;
import caosdb.server.database.Database;
import caosdb.server.database.access.Access;
import caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemGetFileIterator.FileNameIterator;
import caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemHelper;
import caosdb.server.database.backend.transaction.FileConsistencyCheck;
import caosdb.server.database.backend.transaction.GetFileIterator;
import caosdb.server.database.exceptions.TransactionException;
import caosdb.server.entity.Message;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import org.eclipse.jetty.io.RuntimeIOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FileUtilsTest {
  /** @throws IOException */
  @BeforeClass
  public static void initServerProperties() throws IOException {
    CaosDBServer.initServerProperties();
  }

  public static final File testRoot =
      new File("").getAbsoluteFile().toPath().resolve("fileutils_testfolder").toFile();
  public static final File someDir = testRoot.toPath().resolve("some_dir").toFile();
  public static final File linkToSomeDir = testRoot.toPath().resolve("link_to_some_dir").toFile();
  public static final File someFile = testRoot.toPath().resolve("some_file").toFile();
  public static final File someDir_someFile = someDir.toPath().resolve("someFile").toFile();
  public static final File linkToSomeFile = testRoot.toPath().resolve("link_to_some_file").toFile();

  @BeforeClass
  public static void setup() throws Message, IOException {
    Assert.assertTrue(new File(FileSystem.getBasepath()).canWrite());
    Assert.assertTrue(new File(FileSystem.getBasepath()).canRead());
    Assert.assertTrue(new File(FileSystem.getBasepath()).canExecute());
    Assert.assertTrue(new File(FileSystem.getTmp()).canWrite());
    Assert.assertTrue(new File(FileSystem.getTmp()).canRead());
    Assert.assertTrue(new File(FileSystem.getTmp()).canExecute());
    Assert.assertTrue(new File(FileSystem.getDropOffBox()).canWrite());
    Assert.assertTrue(new File(FileSystem.getDropOffBox()).canRead());
    Assert.assertTrue(new File(FileSystem.getDropOffBox()).canExecute());

    deleteTmp();
    FileUtils.createFolders(testRoot);
    FileUtils.createFolders(someDir);
    someFile.createNewFile();
    someDir_someFile.createNewFile();
    FileUtils.createSymlink(linkToSomeDir, someDir);
    FileUtils.createSymlink(linkToSomeFile, someDir_someFile);
  }

  @AfterClass
  public static void teardown() throws IOException {
    System.err.println("teardown");
    FileUtils.delete(testRoot, true);
    deleteTmp();
    assertFalse(testRoot.exists());
    assertEquals(0, new File(FileSystem.getTmp()).listFiles().length);
  }

  @Before
  public void testTmpEmpty() throws IOException {
    if (new File(FileSystem.getTmp()).exists()) {
      if (0 != new File(FileSystem.getTmp()).list().length) {
        System.err.println("TMP not empty.");
        teardown();
        System.exit(1);
      }
      assertEquals(0, new File(FileSystem.getTmp()).list().length);
    }
  }

  public static void deleteTmp() throws IOException {
    if (new File(FileSystem.getTmp()).exists())
      for (File f : new File(FileSystem.getTmp()).listFiles()) {
        System.err.println(f.getAbsolutePath());
        FileUtils.delete(f, true).cleanUp();
      }
  }

  @After
  public void callDeleteTmp() throws IOException {
    deleteTmp();
  }

  @Test(expected = NullPointerException.class)
  public void testGetMimeTypeNull() {
    FileUtils.getMimeType(null);
  }

  @Test(expected = RuntimeIOException.class)
  public void testGetMimeTypeNonExisting() {
    final File f = new File("nonexisting");
    assertFalse(f.exists());
    FileUtils.getMimeType(f);
  }

  @Test
  public void testGetMimeTypeTextFile() {
    final File f = someFile;
    assertTrue(f.exists());
    assertEquals("inode/x-empty", FileUtils.getMimeType(f));
  }

  @Test
  public void testGetMimeTypeSymlink() throws IOException, InterruptedException, CaosDBException {
    final File symlink = testRoot.toPath().resolve("simlink").toFile();

    try {
      FileUtils.createSymlink(symlink, someFile);
      assertEquals("inode/x-empty", FileUtils.getMimeType(symlink));
    } catch (final Exception e) {
      throw e;
    } finally {
      FileUtils.unlink(symlink);
    }
  }

  @Test(expected = NullPointerException.class)
  public void testIsSubDirNullChild() {
    FileUtils.isSubDirSymlinkSave(null, someDir);
  }

  @Test(expected = NullPointerException.class)
  public void testIsSubDirNullPar() {
    FileUtils.isSubDirSymlinkSave(someFile, null);
  }

  @Test(expected = NullPointerException.class)
  public void testIsSubDirNull() {
    FileUtils.isSubDirSymlinkSave(null, null);
  }

  @Test
  public void directChild1() {
    final File child = someFile;
    final File parent = testRoot;
    assertTrue(FileUtils.isSubDirSymlinkSave(child, parent));
  }

  @Test
  public void deepChild() {
    final File child = someDir_someFile;
    final File parent = testRoot;
    assertTrue(FileUtils.isSubDirSymlinkSave(child, parent));
  }

  @Test
  public void testSymlinkRoot() throws IOException, InterruptedException, CaosDBException {
    final File symlink_root = linkToSomeDir;
    assertTrue(symlink_root.exists());
    assertTrue(symlink_root.isDirectory());
    assertTrue(FileUtils.isSymlink(symlink_root));
    try {
      FileUtils.isSubDirSymlinkSave(someFile, symlink_root);
    } catch (final RuntimeException e) {
      assertEquals("Illegal symlink found!", e.getMessage());
    }
  }

  @Test
  public void testSymlinkChildFile() {
    final File child = linkToSomeFile;
    assertTrue(child.exists());
    assertTrue(child.isFile());
    assertTrue(FileUtils.isSymlink(child));
    assertTrue(FileUtils.isSubDirSymlinkSave(child, testRoot));
  }

  @Test
  public void testSymlinkChildDir() {
    final File root = testRoot;
    final File symlink_child = linkToSomeDir;
    assertTrue(symlink_child.exists());
    assertTrue(symlink_child.isDirectory());
    assertTrue(FileUtils.isSymlink(symlink_child));
    try {
      FileUtils.isSubDirSymlinkSave(symlink_child, root);
    } catch (final RuntimeException e) {
      assertEquals("Illegal symlink found!", e.getMessage());
    }
  }

  @Test
  public void testIllegalDoubleDot() {
    final File child = new File(linkToSomeDir.getAbsolutePath() + "../");
    assertFalse(child.exists());
  }

  @Test
  public void TestChownScript() throws IOException, InterruptedException, CaosDBException {
    FileUtils.testChownScript();

    final File f = new File(FileSystem.getDropOffBox() + "chown_test_file");
    f.createNewFile();
    try {
      FileUtils.runChownScript(f);
    } finally {
      if (f.exists()) {
        f.delete();
      }
    }
  }

  @Test
  public void testCreateFolders() throws Message {
    final File f = new File(testRoot.getAbsoluteFile() + "/testfolder/testsubfolder/testfile");
    Assert.assertFalse(f.getAbsoluteFile().getParentFile().exists());
    final Undoable u = FileUtils.createFolders(f.getAbsoluteFile().getParentFile());
    Assert.assertTrue(f.getAbsoluteFile().getParentFile().exists());
    u.undo();
    Assert.assertFalse(f.getAbsoluteFile().getParentFile().exists());
  }

  @Test
  public void testRenameUndoable1() throws IOException, Message {
    final String newFileTmpName = FileSystem.getTmp() + "renameTestFile" + Utils.getUID();
    final File newFile = new File(newFileTmpName);
    Assert.assertTrue(newFile.createNewFile());
    newFile.deleteOnExit();
    final String targetName = FileSystem.getPath("renameTestFile.dat");
    final File target = new File(targetName);
    Assert.assertFalse(target.exists());
    target.deleteOnExit();

    final Undoable u = FileUtils.rename(newFile, target);

    // file is moved to file system
    Assert.assertFalse(new File(newFileTmpName).exists());
    Assert.assertTrue(new File(targetName).exists());
    Assert.assertEquals(target.getAbsolutePath(), targetName);
    Assert.assertFalse(newFile.exists());

    u.undo();

    // file is moved back to tmp dir
    Assert.assertTrue(new File(newFileTmpName).exists());
    Assert.assertFalse(new File(targetName).exists());

    newFile.delete();
    target.delete();
  }

  @Test
  public void testRenameUndoable2() throws IOException, Message, NoSuchAlgorithmException {
    final String newFileTmpName = FileSystem.getTmp() + "renameTestFile" + Utils.getUID();
    final File newFile = new File(newFileTmpName);
    Assert.assertFalse(newFile.exists());

    final PrintWriter outNew = new PrintWriter(newFileTmpName);
    outNew.println("newFile");
    outNew.close();

    Assert.assertTrue(newFile.exists());
    newFile.deleteOnExit();

    final String newHash = FileUtils.getChecksum(newFile);

    final String targetName = FileSystem.getPath("renameTestFile.dat");
    final File target = new File(targetName);
    Assert.assertFalse(target.exists());
    final PrintWriter outOld = new PrintWriter(targetName);
    outOld.println("oldFile");
    outOld.close();

    Assert.assertTrue(target.exists());
    target.deleteOnExit();

    final String oldHash = FileUtils.getChecksum(target);

    final Undoable u = FileUtils.rename(newFile, target);

    // file is moved to file system, old file is moved to tmp
    Assert.assertFalse(new File(newFileTmpName).exists());
    Assert.assertTrue(new File(targetName).exists());
    Assert.assertEquals(target.getAbsolutePath(), targetName);
    Assert.assertFalse(newFile.exists());

    u.undo();

    // file is moved back to tmp dir, old file is moved back to filesystem.
    Assert.assertTrue(new File(newFileTmpName).exists());
    Assert.assertTrue(new File(targetName).exists());

    Assert.assertEquals(newHash, FileUtils.getChecksum(new File(newFileTmpName)));
    Assert.assertEquals(oldHash, FileUtils.getChecksum(new File(targetName)));

    target.delete();
    newFile.delete();
  }

  @Test
  public void testRenameUndoable3()
      throws IOException, Message, NoSuchAlgorithmException, InterruptedException {
    assertEquals(0, new File(FileSystem.getTmp()).listFiles().length);

    final String newFileTmpName = FileSystem.getTmp() + "renameTestFile" + Utils.getUID();
    final File newFile = new File(newFileTmpName);
    Assert.assertFalse(newFile.exists());

    final PrintWriter outNew = new PrintWriter(newFileTmpName);
    outNew.println("newFile");
    outNew.close();

    Assert.assertTrue(newFile.exists());
    newFile.deleteOnExit();

    final String newHash = FileUtils.getChecksum(newFile);

    final String targetName = FileSystem.getPath("renameTestFile.dat");
    final File target = new File(targetName);
    Assert.assertFalse(target.exists());
    final PrintWriter outOld = new PrintWriter(targetName);
    outOld.println("oldFile");
    outOld.close();

    Assert.assertTrue(target.exists());
    target.deleteOnExit();

    final Undoable u = FileUtils.rename(newFile, target);

    // file is moved to file system, old file is moved to tmp
    Assert.assertFalse(new File(newFileTmpName).exists());
    Assert.assertTrue(new File(targetName).exists());
    Assert.assertEquals(target.getAbsolutePath(), targetName);
    Assert.assertFalse(newFile.exists());

    u.cleanUp();

    // tmp file has been deleted and new file exists in the file system.
    Assert.assertFalse(new File(newFileTmpName).exists());
    Assert.assertTrue(new File(targetName).exists());

    // tmp folder is empty again.
    Assert.assertEquals(0, new File(FileSystem.getTmp()).listFiles().length);

    Assert.assertEquals(newHash, FileUtils.getChecksum(new File(targetName)));

    newFile.delete();
    target.delete();
  }

  @Test
  public void testRenameUndoable4() throws IOException, Message, NoSuchAlgorithmException {

    final String targetName = FileSystem.getPath("renameTestFile.dat");
    final File target = new File(targetName);
    Assert.assertFalse(target.exists());
    final PrintWriter outOld = new PrintWriter(targetName);
    outOld.println("file");
    outOld.close();

    Assert.assertTrue(target.exists());
    target.deleteOnExit();

    final String hash = FileUtils.getChecksum(target);

    final File newFile = new File(targetName);
    Assert.assertEquals(newFile.getAbsolutePath(), target.getAbsolutePath());

    // rename file to itself
    final Undoable u = FileUtils.rename(newFile, target);

    u.cleanUp();

    assertTrue(new File(targetName).exists());
    assertEquals(hash, FileUtils.getChecksum(new File(targetName)));

    target.delete();
  }

  @Test
  public void testSymlinkBehavior() throws IOException, InterruptedException, CaosDBException {

    // root dir for this test
    final String rootPath = "./testSymlinkBehavior/";
    final File root = new File(rootPath);
    root.deleteOnExit();
    root.mkdir();

    assertTrue(root.isDirectory());
    assertFalse(FileUtils.isSymlink(root));

    // one file, ...
    final File dir1 = new File(root + "/dir1/");
    dir1.deleteOnExit();

    // ... one symlink
    final File dir1Link = new File(root + "/dir1Link/");
    dir1Link.deleteOnExit();

    dir1.mkdir();
    FileUtils.createSymlink(dir1Link, dir1);

    assertTrue(dir1.isDirectory());
    assertFalse(FileUtils.isSymlink(dir1));
    assertTrue(dir1Link.isDirectory());
    assertTrue(FileUtils.isSymlink(dir1Link));

    // delete symlink (do not delete dir1!)
    dir1Link.delete();
    assertFalse(dir1Link.exists());
    assertTrue(dir1.exists());

    // recreate symlink
    FileUtils.createSymlink(dir1Link, dir1);
    assertTrue(dir1.isDirectory());
    assertFalse(FileUtils.isSymlink(dir1));
    assertTrue(dir1Link.isDirectory());
    assertTrue(FileUtils.isSymlink(dir1Link));

    // delete dir1
    dir1.delete();
    // ... dir1 does not exist
    assertFalse(dir1.exists());
    // ... and dir1Link does also not exists!!!
    assertFalse(dir1Link.exists());

    // recreate dir1
    dir1.mkdir();
    assertTrue(dir1.exists());
    assertTrue(dir1.isDirectory());
    assertFalse(FileUtils.isSymlink(dir1));
    assertTrue(dir1Link.exists());
    assertTrue(dir1Link.isDirectory());
    assertTrue(FileUtils.isSymlink(dir1Link));
  }

  @Test
  public void testFileTimestamp() throws IOException, InterruptedException {

    // root dir for this test
    final String rootPath = "./testFileTimestamp/";
    final File root = new File(rootPath);
    root.deleteOnExit();

    root.mkdir();
    assertTrue(root.isDirectory());
    assertTrue(root.exists());

    // root timestamp
    final long t1 = root.lastModified();
    assertFalse(t1 == 0);

    // timestamp has 1 second accuracy
    Thread.sleep(1000);

    // createFile
    final File file = new File(rootPath + "file");
    file.deleteOnExit();
    file.createNewFile();
    assertTrue(file.exists());
    assertTrue(file.isFile());

    // file timestampm
    final long t2 = file.lastModified();
    assertFalse(t2 == 0);

    // file was created -> root dir changed
    assertFalse(t1 == t2);

    // timestamp has 1 second accuracy
    Thread.sleep(1000);

    // write something to file
    final PrintStream out = new PrintStream(file);
    out.println("blablabla");
    out.flush();
    out.close();

    final long t3 = file.lastModified();
    assertFalse(t3 == 0);

    assertFalse(t2 == t3);
  }

  static Access access = Initialization.setUp().getAccess();

  @Test
  public void testFileSystemConsistencyCheck()
      throws NoSuchAlgorithmException, IOException, TransactionException, InterruptedException,
          Message {
    final String rootPath = "./testFileSystemConsistencyCheck/";
    final File root = new File(rootPath);
    root.deleteOnExit();
    root.mkdir();

    final UnixFileSystemHelper h = new UnixFileSystemHelper(rootPath);
    access.setHelper("UnixFileSystemHelper", h);

    FileConsistencyCheck check =
        Database.execute(new FileConsistencyCheck("test1.dat", 0, null, 0L, null), access);
    assertEquals(FileConsistencyCheck.FILE_DOES_NOT_EXIST, check.getResult());

    final File f = new File(rootPath + "test1.dat");
    f.deleteOnExit();
    f.createNewFile();

    PrintStream out = new PrintStream(f);
    out.println("blablabla");
    out.flush();
    out.close();
    final SHA512 hasher = new SHA512();
    final String hash = hasher.hash(f);
    final long size = f.length();
    final long timestamp = f.lastModified();

    check =
        Database.execute(
            new FileConsistencyCheck("test1.dat", size, hash, timestamp, hasher), access);
    assertEquals(FileConsistencyCheck.OK, check.getResult());

    // timestamp has 1 second accuracy
    Thread.sleep(1000);

    out = new PrintStream(f);
    out.println("blablabla");
    out.flush();
    out.close();
    final String hash2 = hasher.hash(f);
    final long timestamp2 = f.lastModified();
    final long size2 = f.length();
    assertEquals(hash, hash2);
    assertTrue(timestamp2 > timestamp);

    assertTrue(f.exists());
    check =
        Database.execute(
            new FileConsistencyCheck("test1.dat", size2, hash, timestamp, hasher), access);
    assertEquals(FileConsistencyCheck.OK, check.getResult());

    out = new PrintStream(f);
    out.println("asdfasdfasdf");
    out.flush();
    out.close();

    check =
        Database.execute(
            new FileConsistencyCheck("test1.dat", size, hash, timestamp, hasher), access);
    assertEquals(FileConsistencyCheck.FILE_MODIFIED, check.getResult());
  }

  @Test
  public void testFileIterator() throws IOException {
    final String rootPath = "./testFileIterator/";
    final File root = new File(rootPath);
    root.deleteOnExit();
    root.mkdir();

    final File dat1 = new File(rootPath + "dat1");
    dat1.deleteOnExit();
    dat1.createNewFile();

    final File dat2 = new File(rootPath + "dat2");
    dat2.deleteOnExit();
    dat2.createNewFile();

    final File subdir = new File(rootPath + "subdir/");
    subdir.deleteOnExit();
    subdir.mkdir();

    final File empty = new File(rootPath + "empty/");
    empty.deleteOnExit();
    empty.mkdir();

    final File dat3 = new File(rootPath + "subdir/dat3");
    dat3.deleteOnExit();
    dat3.createNewFile();

    final FileNameIterator iterator = new FileNameIterator(root, rootPath);

    assertEquals(rootPath + "dat1", iterator.next());
    assertEquals(rootPath + "dat2", iterator.next());
    assertEquals(rootPath + "empty/", iterator.next());
    assertEquals(rootPath + "subdir/dat3", iterator.next());
  }

  @Test
  public void testGetFileIterator() throws TransactionException, IOException, Message {
    final String rootPath = "/testGetFileIterator/";

    final File root = new File(FileSystem.getPath(rootPath));
    root.deleteOnExit();
    root.mkdir();

    final UnixFileSystemHelper h = new UnixFileSystemHelper(rootPath);
    access.setHelper("UnixFileSystemHelper", h);

    final File dat1 = new File(FileSystem.getPath(rootPath + "dat1"));
    dat1.deleteOnExit();
    dat1.createNewFile();

    final File dat2 = new File(FileSystem.getPath(rootPath + "dat2"));
    dat2.deleteOnExit();
    dat2.createNewFile();

    final File subdir = new File(FileSystem.getPath(rootPath + "subdir/"));
    subdir.deleteOnExit();
    subdir.mkdir();

    final File empty = new File(FileSystem.getPath(rootPath + "empty/"));
    empty.deleteOnExit();
    empty.mkdir();

    final File dat3 = new File(FileSystem.getPath(rootPath + "subdir/dat3"));
    dat3.deleteOnExit();
    dat3.createNewFile();

    final Iterator<String> iterator =
        Database.execute(new GetFileIterator(rootPath), access).getIterator();

    assertEquals(rootPath + "dat1", iterator.next());
    assertEquals(rootPath + "dat2", iterator.next());
    assertEquals(rootPath + "empty/", iterator.next());
    assertEquals(rootPath + "subdir/dat3", iterator.next());
  }
}
