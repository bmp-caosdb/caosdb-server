/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server.utils.mail;

import caosdb.CaosDBTestClass;
import caosdb.server.CaosDBServer;
import caosdb.server.ServerProperties;
import java.io.IOException;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestMail extends CaosDBTestClass {
  @BeforeClass
  public static void initServerProperties() throws IOException {
    CaosDBServer.initServerProperties();
  }

  @BeforeClass
  public static void setupUp() {
    // output mails to the test dir
    CaosDBServer.setProperty(
        ServerProperties.KEY_MAIL_TO_FILE_HANDLER_LOC, TEST_DIR.getAbsolutePath());
  }

  @Test
  public void testMail() {
    final Mail mail =
        new Mail(
            "The Admin",
            "test@example.com",
            "The User",
            "test2@example.com",
            "Test",
            "This is a Test");
    mail.send();
  }
}
