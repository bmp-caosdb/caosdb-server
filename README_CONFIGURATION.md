# Server Configuration

The server configuration is a list of key-value pairs. A configuration file may contain empty lines, comment lines, and key-value lines.
Comment lines begin with a hash (`#`). Key-value lines must have the format `KEY_NAME=VALUE` or `KEY_NAME = VALUE`.

The server default configuration is located at `./conf/core/server.conf`.

The default configuration can be overriden by

1. the file ./conf/ext/server.conf
2. any file in ./conf/ext/server.conf.d/ in (approximately?) alphabetical order
3. environment variables with the prefix `CAOSDB_CONFIG_`

in this order.

